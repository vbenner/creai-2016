@extends('layouts.app')
@section('title', $titre)
@section('subtitle', $subtitre)

@section('script')

    <script src="{{ URL::asset('/scripts/params/user.js') }}"></script>

@endsection
@section('content')

    <div class="row">

        <div class="col-md-12 col-sm-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box red">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>Gestion des {!! $subtitre !!} </div>
                    <div class="actions">
                        <a href="javascript:;" class="btn btn-default btn-sm" id="btnAdd">
                            <i class="fa fa-plus"></i> Ajout </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <table data-toggle="table" class="table table-striped table-bordered table-hover order-column"
                           id="table">
                        <thead>
                        <tr>
                            <th data-halign="center" data-align="left"> Nom</th>
                            <th data-halign="center" data-align="left"> Email</th>
                            <th data-halign="center" data-align="left"> Actif</th>
                            <th data-halign="center" data-align="left"> Rôle</th>
                            <th data-halign="center" data-align="right"> Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <!-- START Dialogue d'exécution -->
    <div class="modal fade" id="dialogExec" tabindex="-1" aria-hidden="true" data-backdrop="static"
         data-keyboard="false">
    </div>
    <!-- END Dialogue d'exécution -->

    <!-- START Liste de Modals -->
    <!-- START Modal Pays -->
    <div id="form" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header caption">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form role="form">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                        <input id="ztNom" class="form-control required" type="text"/>
                                        <label for="ztNom">Saisie du Nom + Prénom<span class="font-red">*</span></label>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                        <input id="ztMail" class="form-control required" type="text"/>
                                        <label for="ztMail">Saisie du Mail<span class="font-red">*</span></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                        <select id="zlRole" class="form-control"></select>
                                        <label for="zlRole">Rôle <span class="font-red">*</span></label>
                                    </div>
                                </div>
                            </div>

                            <hr>
                            <div class="note note-info" style="margin-bottom: 0px;">
                                <i class="fa fa-warning font-red"></i> Un email contenant les identifiants sera envoyé à
                                l'adresse mail indiquée. Merci de bien vérifier votre saisie
                                avant de valider.
                            </div>

                        </div>
                        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fermer</button>
                    <button type="button" class="btn" id="btnAction"></button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal Pays -->

    <!-- START Modal GRANT -->
    <div id="formGrant" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header caption">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-6">
                            <button class="btn blue-madison" type="button">Appliquer Modèle</button>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box red">
                                <div class="portlet-title">
                                    <div class="caption">BLOCS</div>
                                </div>
                                <div class="portlet-body">
                                    <div id="table_wrapper" class="dataTables_wrapper no-footer">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table data-toggle="table"
                                                       class="table table-striped table-bordered table-hover order-column"
                                                       id="tableBlocs">
                                                    <thead>
                                                    <tr>
                                                        <th data-halign="center" data-align="left"> Nom</th>
                                                        <th data-halign="center" data-align="left" width="100px"> Droits
                                                        </th>
                                                        <th data-halign="center" data-align="left" width="100px">
                                                            Ajout
                                                        </th>
                                                        <th data-halign="center" data-align="left" width="100px"> Edit
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box blue-steel">
                                <div class="portlet-title">
                                    <div class="caption">MODULES</div>
                                </div>
                                <div class="portlet-body">
                                    <div id="table_wrapper" class="dataTables_wrapper no-footer">
                                        <div class="row">
                                            <div class="col-md-12">


                                                <table data-toggle="table"
                                                       class="table table-striped table-bordered table-hover order-column"
                                                       id="tableModules">
                                                    <thead>
                                                    <tr>
                                                        <th data-halign="center" data-align="left"> Nom</th>
                                                        <th data-halign="center" data-align="left" width="100px"> Droits</th>
                                                        <th data-halign="center" data-align="left" width="100px"> Ajout</th>
                                                        <th data-halign="center" data-align="left" width="100px"> Edit</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box yellow-casablanca">
                                <div class="portlet-title">
                                    <div class="caption">SOUS-MODULES</div>
                                </div>
                                <div class="portlet-body">
                                    <div id="table_wrapper" class="dataTables_wrapper no-footer">
                                        <div class="row">
                                            <div class="col-md-12">


                                                <table data-toggle="table"
                                                       class="table table-striped table-bordered table-hover order-column"
                                                       id="tableSubModules">
                                                    <thead>
                                                    <tr>
                                                        <th data-halign="center" data-align="left"> Nom</th>
                                                        <th data-halign="center" data-align="left" width="100px"> Droits</th>
                                                        <th data-halign="center" data-align="left" width="100px"> Ajout</th>
                                                        <th data-halign="center" data-align="left" width="100px"> Edit</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal GRANT -->

    <!-- END Liste de Modals -->


@endsection
