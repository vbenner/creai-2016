@extends('layouts.app')
@section('title', $titre)
@section('subtitle', $subtitre)

@section('script')

    <script src="{{ URL::asset('/scripts/params/typeetablissement.js') }}"></script>

@endsection
@section('content')

    <div class="row">

        <div class="col-md-12 col-sm-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box red">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>Gestion des {!! $subtitre !!} </div>
                    <div class="actions">
                        <a href="javascript:;" class="btn btn-default btn-sm" id="btnAdd">
                            <i class="fa fa-plus"></i> Ajout </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <table data-toggle="table" class="table table-striped table-bordered table-hover order-column"
                           id="table">
                        <thead>
                        <tr>
                            <th data-halign="center" data-align="left"> Type Etablissement</th>
                            <th data-halign="center" data-align="left"> Type Secteur</th>
                            <th data-halign="center" data-align="left"> Type Secteur MS</th>
                            <th data-halign="center" data-align="left"> Type Age</th>
                            <th data-halign="center" data-align="left"> Actif</th>
                            <th data-halign="center" data-align="right"> Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <!-- START Dialogue d'exécution -->
    <div class="modal fade" id="dialogExec" tabindex="-1" aria-hidden="true" data-backdrop="static"
         data-keyboard="false">
    </div>
    <!-- END Dialogue d'exécution -->

    <!-- START Liste de Modals -->
    <!-- START Modal Type Téléphone -->
    <div id="form" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header caption">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form role="form">
                        <div class="form-body">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                        <input id="ztLibelle" class="form-control required text-uppercase" type="text"/>
                                        <label for="ztLibelle">Saisie du libellé <span class="font-red">*</span></label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                        <input id="ztSigle" class="form-control text-uppercase" type="text"/>
                                        <label for="ztSigle">Saisie du sigle</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                        <select id="zlTypeSecteur" class="form-control"></select>
                                        <label for="zlTypeSecteur">Type secteur <span class="font-red">*</span></label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                        <select id="zlTypeSecteurMS" class="form-control"></select>
                                        <label for="zlTypeSecteurMS">Type secteur MS</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                        <select id="zlTypeAge" class="form-control"></select>
                                        <label for="zlTypeAge">Type âge <span class="font-red">*</span></label>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fermer</button>
                    <button type="button" class="btn" id="btnAction"></button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal Type Téléphone -->
    <!-- END Liste de Modals -->




@endsection
