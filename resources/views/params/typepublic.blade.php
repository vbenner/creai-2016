@extends('layouts.app')
@section('title', $titre)
@section('subtitle', $subtitre)

@section('script')

    <script src="{{ URL::asset('/scripts/params/typepublic.js') }}"></script>

@endsection
@section('content')

    <div class="row">

        <div class="col-md-12 col-sm-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box red">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>Gestion des {!! $subtitre !!} </div>
                    <div class="actions">
                        <a href="javascript:;" class="btn btn-default btn-sm" id="btnAdd">
                            <i class="fa fa-plus"></i> Ajout </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <table data-toggle="table" class="table table-striped table-bordered table-hover order-column" id="tableType">
                        <thead>
                        <tr>
                            <th data-halign="center" data-align="left"> Type Public </th>
                            <th data-halign="center" data-align="left"> Note </th>
                            <th data-halign="center" data-align="left"> Actif </th>
                            <th data-halign="center" data-align="right"> Actions </th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <!-- START Dialogue d'exécution -->
    <div class="modal fade" id="dialogExec" tabindex="-1" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    </div>
    <div class="modal fade" id="dialogExecLvl1" tabindex="-1" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    </div>
    <!-- END Dialogue d'exécution -->

    <!-- START Liste de Modals -->
    <!-- START Modal Type Public -->
    <div id="formType" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header caption">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form role="form">
                        <div class="form-body">
                            <div class="form-group form-md-line-input form-md-floating-label has-success">
                                <input id="ztLibelle" class="form-control required text-uppercase" type="text"/>
                                <label for="ztLibelle">Saisie du libellé<span class="font-red">*</span></label>
                            </div>

                            <div class="form-group form-md-line-input form-md-floating-label has-success">
                                <textarea id="ztNote" class="form-control" placeholder="" rows="5" style="resize: none !important"></textarea>
                                <label for="ztNote">Saisie de notes complémentaires</label>
                            </div>

                        </div>
                        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fermer</button>
                    <button type="button" class="btn" id="btnAction"></button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal Type Public -->

    <!-- START Modal Type Détail -->
    <div id="formDetail" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header caption">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form role="form">
                        <div class="form-body">
                            <div class="form-group form-md-line-input form-md-floating-label has-success">
                                <input id="ztLibelle" class="form-control required text-uppercase" type="text"/>
                                <label for="ztLibelle">Saisie du libellé<span class="font-red">*</span></label>
                            </div>

                            <div class="form-group form-md-line-input form-md-floating-label has-success">
                                <textarea id="ztNote" class="form-control" placeholder="" rows="5" style="resize: none !important"></textarea>
                                <label for="ztNote">Saisie de notes complémentaires</label>
                            </div>

                        </div>
                        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fermer</button>
                    <button type="button" class="btn" id="btnAction"></button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal Type Détail -->
    <!-- END Liste de Modals -->

    <!-- START Modal Liste DETAIL -->
    <div id="formListDetail" style="display: none;">
        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
        <div class="modal-dialog modal-full">
            <div class="modal-content">

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box blue-steel">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cogs"></i>Gestion des Détails de Types Publics
                                    </div>
                                    <div class="actions">
                                        <a href="javascript:;" class="btn btn-default btn-sm" id="btnAddDetail">
                                            <i class="fa fa-plus"></i> Ajout </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table data-toggle="table"
                                           class="table table-striped table-bordered table-hover order-column"
                                           id="tableDetail">
                                        <thead>
                                        <tr>
                                            <th data-halign="center" data-align="left"> Libelle</th>
                                            <th data-halign="center" data-align="left"> Actif</th>
                                            <th data-halign="center" data-align="left"> Note</th>
                                            <th data-halign="center" data-align="right"> Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button id="btnCloseDetail" type="button" class="btn dark btn-outline" data-dismiss="modal">Fermer
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal  DETAIL -->




@endsection
