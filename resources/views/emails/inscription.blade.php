<body>
Bonjour {{$nom}},<br/>
<br/>
Votre compte d'accès au nouvel annuaire CREAI Bourgogne/Franche-Comté<br/>
vient d'être créé.<br/>
<br/>
Vous pouvez vous connecter avec les identifiants suivants :<br/>
<br/>
login : <strong>{{$login}}</strong><br/>
mot de passe : <strong>{{$password}}</strong><br/>
<br/>
<br/>
Nouvel annuaire : <a href="http://139.59.171.6/">cliquer-ici</a><br/>
<br/>
<br/>
Ceci est un email automatique, merci de ne pas y répondre.<br/>
</body>