@extends('layouts.login')

<!-- Main Content -->
@section('content')




            <form class="forget-formd" method="post" action="{{ url('/password/email') }}" style="display: block;">
                {{ csrf_field() }}
                <h3 class="font-green">Forgot Password ?</h3>
                <p> Enter your e-mail address below to reset your password. </p>
                <div class="form-group">
                    <input class="form-control placeholder-no-fix form-group" type="text" name="email" placeholder="Email" autocomplete="off">
                </div>
                <div class="form-actions">
                    <button id="back-btn" class="btn green btn-outline" type="button">Back</button>
                    <button class="btn btn-success uppercase pull-right" type="submit">Submit</button>
                </div>
            </form>


@endsection
