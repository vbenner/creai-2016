@extends('layouts.login')

@section('content')

    <form class="login-form" method="POST" action="{{ url('/login') }}">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-xs-6">
                <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Identifiant" id="email" name="email"/>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="col-xs-6">
                <input class="form-control form-control-solid placeholder-no-fix form-group" type="password" autocomplete="off" placeholder="Mot de passe" id="password" name="password"/>
                @if ($errors->has('password'))
                    <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif

            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="rem-password">
                    <label class="rememberme mt-checkbox mt-checkbox-outline">
                        <input type="checkbox" name="remember" value="1" /> Se souvenir
                        <span></span>
                    </label>
                </div>
            </div>
            <div class="col-sm-8 text-right">
                <div class="forgot-password">
                    <a id="forget-password" class="btn btn-link forget-password" href="javascript:;">Mot de passe oublié ?</a>
                </div>
                <button class="btn green" type="submit">Connexion</button>
            </div>
        </div>
    </form>

    <form class="forget-form" method="post" action="{{ url('/password/email') }}" style="display: block;">
        {{ csrf_field() }}
        <h3 class="font-green">Mot de passe oublié ?</h3>
        <p> Saisissez votre adresse pour recevoir un nouveau mot de passe. </p>
        <div class="form-group">
            <input class="form-control placeholder-no-fix form-group" type="text" name="email" placeholder="Email" autocomplete="off">
        </div>
        <div class="form-actions">
            <button id="back-btn" class="btn green btn-outline" type="button">Retour</button>
            <button class="btn btn-danger uppercase pull-right" type="submit">Réinitialiser</button>
        </div>
    </form>
    <!--
    <form class="form-horizontal login-container" role="form" method="POST" action="">

        <div class="row">
            <div class="col-xs-6">
                <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Identifiant" id="email" name="email" value="{{ old('email') }}"/>


            </div>
            <div class="col-xs-6">
                <input class="form-control form-control-solid placeholder-no-fix form-group" type="password" autocomplete="off" placeholder="Mot de passe" id="password" name="password" />

            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="rem-password">
                    <label class="rememberme mt-checkbox mt-checkbox-outline">
                        <input type="checkbox" name="remember" value="1" /> Se souvenir
                        <span></span>
                    </label>
                </div>
            </div>
            <div class="col-sm-8 text-right">
                <div class="forgot-password">
                    <a href="" id="forget-password" class="forget-password">Mot de passe oublié ?</a>
                </div>
                <button class="btn green" type="submit">Connexion</button>
            </div>
        </div>
    </form>
    -->

@endsection
