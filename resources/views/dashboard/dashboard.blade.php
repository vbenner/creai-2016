@extends('layouts.app')
@section('title', $titre)
@section('subtitle', $subtitre)

@section('script')

    <script src="{{ URL::asset('/scripts/dashboard/dashboard.js') }}"></script>

@endsection
@section('content')

    <div class="row">


        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat2 bordered">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="345">345</span>
                        </h3>
                        <small>Abonnés</small>
                    </div>
                    <div class="icon">
                        <i class="icon-pie-chart"></i>
                    </div>
                </div>
                <div class="progress-info">
                    <div class="progress">
                                        <span style="width: 7%;" class="progress-bar progress-bar-success green-sharp">
                                            <span class="sr-only">+7%</span>
                                        </span>
                    </div>
                    <div class="status">
                        <div class="status-title"> progression </div>
                        <div class="status-number"> 7% </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat2 bordered">
                <div class="display">
                    <div class="number">
                        <h3 class="font-red-haze">
                            <span data-counter="counterup" data-value="1765">1765</span>
                            <small class="font-red-haze">€</small>
                        </h3>
                        <small>Cotisations en attente</small>
                    </div>
                    <div class="icon">
                        <i class="icon-like"></i>
                    </div>
                </div>
                <div class="progress-info">
                    <div class="progress">
                                        <span style="width: 85%;" class="progress-bar progress-bar-success red-haze">
                                            <span class="sr-only">1765 € change</span>
                                        </span>
                    </div>
                    <div class="status">
                        <div class="status-title"> Attente </div>
                        <div class="status-number"> 1765 €  </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- START DASHBOARD TILE - PROJECT -->
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">Projets</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-green fa fa-building"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-subtitle">En attente d'Ouverture</span>
                        <span class="widget-thumb-body-stat">{{$nbProjets}}</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- END DASHBOARD TILE - PROJECT -->

    </div>

    <div class="row">
        <!-- START DASHBOARD TILE - COUNTER ORGANISMES -->
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">Organismes</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-blue-steel fa fa-folder-open"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-subtitle">&nbsp;</span>
                        <span class="widget-thumb-body-stat">{{$nbOrganismes}}</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- END DASHBOARD TILE - COUNTER ORGANISMES -->

        <!-- START DASHBOARD TILE - COUNTER POLES -->
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">Pôles</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-red-intense fa fa-building"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-subtitle">&nbsp;</span>
                        <span class="widget-thumb-body-stat">{{$nbPoles}}</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- END DASHBOARD TILE - COUNTER POLES -->

        <!-- START DASHBOARD TILE - COUNTER ETABLISSEMENTS -->
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">Etablissements</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-blue-soft fa fa-bed"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-subtitle">&nbsp;</span>
                        <span class="widget-thumb-body-stat">{{$nbEtablissements}}</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- END DASHBOARD TILE - COUNTER ETABLISSEMENTS -->

        <!-- START DASHBOARD TILE - COUNTER ANTENNES -->
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">Antennes</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-purple-plum fa fa-table"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-subtitle">&nbsp;</span>
                        <span class="widget-thumb-body-stat">{{$nbAntennes}}</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- END DASHBOARD TILE - COUNTER ANTENNES -->

    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger">
                <p><i class="fa fa-warning"></i> Il vous reste 34 demandes de contributions à envoyer ce mois.</p>
            </div>
        </div>
    </div>
@endsection
