@extends('layouts.app')
@section('title', $titre)
@section('subtitle', $subtitre)

@section('script')

    <script src="{{ URL::asset('/scripts/annuaire/pp.js') }}"></script>

@endsection
@section('content')

    <!-- START Filtre Multicritères -->
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-filter"></i>Recherche Multicritères
                    </div>


                    <div class="tools">
                        <a class="expand" href="javascript:;"> </a>
                    </div>
                </div>

                <div class="portlet-body portlet-collapsed">
                    <div class="row">
                        <strong>Default Collapsed Portlet</strong>
                        <p> Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec
                            elit. Cras mattis consectetur purus sit amet fermentum. est non commodo luctus, nisi erat
                            porttitor ligula, eget lacinia odio
                            sem nec elit. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Filtre Multicritères -->

    <!-- START Datatables -->
    <div class="row">

        <div class="col-md-12 col-sm-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box red">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>Gestion des {!! $subtitre !!} </div>
                    <div class="actions">
                        <a href="javascript:;" class="btn btn-default btn-sm" id="btnAdd">
                            <i class="fa fa-plus"></i> Ajout </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <table data-toggle="table"
                           class="table table-striped table-bordered table-hover order-column"
                           id="table">
                        <thead>
                        <tr>
                            <th data-halign="center" data-align="left"> Nom</th>
                            <th data-halign="center" data-align="left"> Prénom</th>
                            <th data-halign="center" data-align="left"> Ville</th>
                            <th data-halign="center" data-align="right"> Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <!-- END Datatables -->

    <!-- START Dialogue d'exécution -->
    <div class="modal fade dialog-exec" id="dialogExecLev0" aria-hidden="true" data-backdrop="static"
         data-keyboard="false">
    </div>
    <!-- END Dialogue d'exécution -->

    <!-- START Dialogue d'exécution Level 9 -->
    <div class="modal fade dialog-exec" id="dialogExecLev9" aria-hidden="true" data-backdrop="static"
         data-keyboard="false">
    </div>
    <!-- END Dialogue d'exécution Level 9 -->

    <!-- START Liste de Modals -->

    <!-- START Modal TEL -->
    <div id="formTel" style="display: none;">
        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
        <div class="modal-dialog modal-medium-lg">
            <div class="modal-content">
                <div class="modal-header caption">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title bold uppercase"></h4>
                </div>
                <div class="modal-body">
                    <div class="row" id="portletTel" style="padding-top: 50px;">
                        <div class="col-md-2">
                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                <input class="form-control text-uppercase" id="ztSaisie"
                                       placeholder="Numéro" type="text">
                                <label for="ztSaisie">Numéro <span class="font-red">*</span></label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                <select id="zlTypeSaisie" class="form-control"></select>
                                <label for="zlTypeSaisie">Type <span class="font-red">*</span></label>
                            </div>
                        </div>
                        <div class="col-md-1" style="padding-top: 20px">
                            <button id="btnAddTel" class="btn btn-success" type="button">
                                <i class="fa fa-plus-circle"></i>
                            </button>
                            <label for="btnAddTel"></label>
                        </div>
                    </div>
                    <div class="row" style="padding-left: 15px;padding-right: 15px;">
                        <table data-toggle="table"
                               class="table table-striped table-bordered table-hover order-column"
                               id="tableTel">
                            <thead>
                            <tr>
                                <th class="text-center"> Téléphone</th>
                                <th class="text-center"> Type</th>
                                <th class="text-center"> Notes</th>
                                <th class="text-center"> Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal TEL -->

    <!-- START Modal MAIL -->
    <div id="formMail" style="display: none;">
        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
        <div class="modal-dialog modal-medium-lg">
            <div class="modal-content">
                <div class="modal-header caption">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title bold uppercase"></h4>
                </div>
                <div class="modal-body">
                    <div class="row" id="portletMail" style="padding-top: 50px;">
                        <div class="col-md-4">
                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                <input class="form-control" id="ztSaisie"
                                       placeholder="Adresse" type="text">
                                <label for="ztSaisie">Adresse <span class="font-red">*</span></label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                <select id="zlTypeSaisie" class="form-control"></select>
                                <label for="zlTypeSaisie">Type <span class="font-red">*</span></label>
                            </div>
                        </div>
                        <div class="col-md-1" style="padding-top: 20px">
                            <button id="btnAddMail" class="btn btn-success" type="button">
                                <i class="fa fa-plus-circle"></i>
                            </button>
                            <label for="btnAddMail"></label>
                        </div>
                    </div>

                    <div class="row" style="padding-left: 15px;padding-right: 15px;">
                        <table data-toggle="table"
                               class="table table-striped table-bordered table-hover order-column"
                               id="tableMail">
                            <thead>
                            <tr>
                                <th class="text-center"> Adresse</th>
                                <th class="text-center"> Type</th>
                                <th class="text-center"> Notes</th>
                                <th class="text-center"> Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal MAIL -->

    <!-- START Modal ACTIVITES -->
    <div id="formActivite" style="display: none;">
        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header caption">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title bold uppercase"></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <!--
                        <div class="col-md-2">
                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                <input class="form-control text-uppercase" id="ztNumTel"
                                       placeholder="Numéro" type="text">
                                <label for="ztNumTel">Numéro <span class="font-red">*</span></label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                <select id="zlTypeTel" class="form-control"></select>
                                <label for="zlTypeTel">Type <span class="font-red">*</span></label>
                            </div>
                        </div>
                        <div class="col-md-1" style="padding-top: 20px">
                            <button id="btnAddTel" class="btn btn-success" type="button">
                                <i class="fa fa-plus-circle"></i>
                            </button>
                            <label for="btnAddTel"></label>
                        </div>
                        -->
                    </div>

                    <div class="row" style="padding-left: 15px;padding-right: 15px;">
                        <table data-toggle="table"
                               class="table table-striped table-bordered table-hover order-column"
                               id="tableTel">
                            <thead>
                            <tr>
                                <th class="text-center"> Date</th>
                                <th class="text-center"> Type</th>
                                <th class="text-center"> Notes</th>
                                <th class="text-center"> Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal ACTIVITES -->

    <!-- START Modal PERSONNES PHYSIQUES-->
    <div id="form" style="display: none;">
        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
        <div class="modal-dialog modal-full">
            <div class="modal-content">
                <div class="modal-header caption">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title bold uppercase"></h4>
                </div>
                <div class="modal-body">

                    <div class="row">

                        <div class="col-md-8">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box green-meadow">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-edit"></i>Données de base
                                    </div>
                                </div>
                                <div class="portlet-body">

                                    <form role="form">
                                        <div class="form-body">

                                            <!-- START LIGNE 1 -->
                                            <div class="row">

                                                <div class="col-md-2">
                                                    <div class="form-group form-md-line-input">
                                                        <select id="zlCivilite" class="form-control">

                                                        </select>
                                                        <label for="zlCivilite">Civilité</label>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control required text-uppercase" id="ztNom"
                                                               type="text">
                                                        <label for="ztNom">Nom <span class="font-red">*</span></label>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control required text-uppercase"
                                                               id="ztPrenom" type="text">
                                                        <label for="ztPrenom">Prénom <span
                                                                    class="font-red">*</span></label>
                                                    </div>
                                                </div>

                                                <div class="col-md-2">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control text-uppercase" id="ztCodeInterne"
                                                               type="text">
                                                        <label for="ztCodeInterne">Code interne</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END LIGNE 1 -->

                                            <div class="row">
                                                <div class="col-md-2">

                                                    <label class="font-red-intense">Usager</label>
                                                    <div class="checkbox checkbox-danger">
                                                        <input id="ccUSAGER" class="styled" type="checkbox">
                                                        <label for="ccUSAGER"></label>
                                                    </div>

                                                </div>
                                                <div class="col-md-10">
                                                    <div class="form-group form-md-line-input">
                                                        <select id="zlEtablissement" class="form-control"
                                                                disabled="disabled">

                                                        </select>
                                                        <label for="zlEtablissement">Établissement</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group form-md-line-input">
                                                        <select id="zlTypeUsager" class="form-control"
                                                                disabled="disabled">

                                                        </select>
                                                        <label for="zlTypeUsager">Type d'Usager</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- START LIGNE 2 -->
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control text-uppercase" id="ztAdr1"
                                                               type="text">
                                                        <label for="ztAdr1">Adresse 1</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control text-uppercase" id="ztAdr2"
                                                               type="text">
                                                        <label for="ztAdr2">Adresse 2</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control text-uppercase" id="ztAdr3"
                                                               type="text">
                                                        <label for="ztAdr3">Adresse 3</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END LIGNE 2 -->


                                            <!-- START LIGNE 3 -->
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control text-uppercase required" id="ztCP"
                                                               type="text">
                                                        <label for="ztCP">CP<span
                                                                    class="font-red">*</span></label>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control text-uppercase required" id="ztVille"
                                                               type="text">
                                                        <label for="ztVille">Ville<span
                                                                    class="font-red">*</span></label>
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group form-md-line-input">
                                                        <select id="zlPays" class="form-control">

                                                        </select>
                                                        <label for="zlPays">Pays</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- START LIGNE 3 -->

                                        </div>

                                    </form>

                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->
                        </div>

                        <div class="col-md-4 ">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box red" id="portletTel">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-phone"></i>Téléphones / Fax
                                    </div>
                                    <div class="tools">
                                        <a title="" data-original-title="Blublu" href="javascript:;"
                                           class="expand"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body portlet-collapsed">

                                    <div class="row">


                                        <div class="col-md-4">
                                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                                <input class="form-control text-uppercase" id="ztSaisie"
                                                       placeholder="Numéro" type="text">
                                                <label for="ztSaisie">Numéro <span class="font-red">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                                <select id="zlTypeSaisie" class="form-control"></select>
                                                <label for="zlTypeSaisie">Type <span class="font-red">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding-top: 20px">
                                            <button id="btnAddTel" class="btn btn-success" type="button">
                                                <i class="fa fa-plus-circle"></i>
                                            </button>
                                            <label for="btnAddTel"></label>
                                        </div>


                                    </div>

                                    <div class="row" style="padding-left: 5px;padding-right: 5px;">
                                        <table data-toggle="table"
                                               class="table table-striped table-bordered table-hover order-column"
                                               id="tableTel">
                                            <thead>
                                            <tr>
                                                <th class="text-center"> Téléphone</th>
                                                <th class="text-center"> Type</th>
                                                <th class="text-center"> Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box red" id="portletMail">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-send"></i>Mails
                                    </div>
                                    <div class="tools">
                                        <a title="" data-original-title="" href="javascript:;" class="expand"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body portlet-collapsed">

                                    <div class="row">


                                        <div class="col-md-6">
                                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                                <input class="form-control" id="ztSaisie" placeholder="Mail"
                                                       type="text">
                                                <label for="ztSaisie">Adresse mail <span
                                                            class="font-red">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                                <select id="zlTypeSaisie" class="form-control"></select>
                                                <label for="zlTypeSaisie">Type <span class="font-red">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding-top: 20px">
                                            <button id="btnAddMail" class="btn btn-success" type="button">
                                                <i class="fa fa-plus-circle"></i>
                                            </button>
                                            <label for="btnAddMail"></label>
                                        </div>


                                    </div>

                                    <div class="row" style="padding-left: 5px;padding-right: 5px;">

                                        <table data-toggle="table"
                                               class="table table-striped table-bordered table-hover order-column"
                                               id="tableMail">
                                            <thead>
                                            <tr>
                                                <th data-halign="center" data-align="left"> Mail</th>
                                                <th data-halign="center" data-align="left"> Type</th>
                                                <th data-halign="center" data-align="right"> Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->

                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box red" id="portleAccesAnnuaire">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gear"></i>Accès Annuaire
                                    </div>
                                    <div class="tools">
                                        <a title="" data-original-title="" href="javascript:;" class="expand"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body portlet-collapsed">

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Actif </label>
                                                <div class="checkbox checkbox-success">
                                                    <input id="ccACTIF" class="styled" type="checkbox">
                                                    <label for="ccACTIF"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Visible </label>
                                                <div class="checkbox checkbox-success">
                                                    <input id="ccVISIBLE" class="styled" type="checkbox">
                                                    <label for="ccVISIBLE"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Accès Annuaire </label>
                                                <div class="md-checkbox-inline">
                                                    <div class="checkbox checkbox-success">
                                                        <input id="ccACCES" class="styled" type="checkbox">
                                                        <label for="ccACCES"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->

<<<<<<< HEAD
                            <div class="alert alert-info complement-footer note" style="left: 10px;">
=======
                            <div class="alert alert-info historique note" style="left: 10px;">
>>>>>>> debug8
                            </div>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-8 ">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-database"></i>Données Complémentaires
                                    </div>
                                    <div class="tools">
                                    </div>
                                </div>
                                <div class="portlet-body">

                                    <div class="row">

                                        <div class="col-md-2">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Cotisant</label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccCOTIS" class="styled" type="checkbox">
                                                    <label for="ccCOTIS"></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Newsletter</label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccNL" class="styled" type="checkbox">
                                                    <label for="ccNL"></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group form-md-checkboxes">
                                                <label>BI Payant</label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccBI_PAYANT" class="styled" type="checkbox">
                                                    <label for="ccBI_PAYANT"></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Inscription BI</label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccBI" class="styled" type="checkbox">
                                                    <label for="ccBI"></label>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-4">
                                            <div class="form-group form-md-line-input" style="margin-top: 5px;">
                                                <select id="zlReceptionBI" class="form-control">

                                                </select>
                                                <label for="zlReceptionBI">Réception BI</label>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Cat. Formation</label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccCF" class="styled" type="checkbox">
                                                    <label for="ccCF"></label>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-4">
                                            <div class="form-group form-md-line-input" style="margin-top: 5px;">
                                                <select id="zlReceptionCF" class="form-control">

                                                </select>
                                                <label for="zlReceptionCF">Réception CF</label>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Participation AG</label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccAG" class="styled" type="checkbox">
                                                    <label for="ccAG"></label>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-4">
                                            <div class="form-group form-md-line-input" style="margin-top: 5px;">
                                                <select id="zlRoleAG" class="form-control">

                                                </select>
                                                <label for="zlRoleAG">Rôle AG</label>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-md-2">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Bureau</label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccBURO" class="styled" type="checkbox">
                                                    <label for="ccBURO"></label>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-4">
                                            <div class="form-group form-md-line-input" style="margin-top: 5px;">
                                                <select id="zlRoleBURO" class="form-control">

                                                </select>
                                                <label for="zlRoleBURO">Rôle Bureau</label>
                                            </div>
                                        </div>


                                        <div class="col-md-2">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Participation CA</label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccCA" class="styled" type="checkbox">
                                                    <label for="ccCA"></label>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-4">
                                            <div class="form-group form-md-line-input" style="margin-top: 5px;">
                                                <select id="zlRoleCA" class="form-control">

                                                </select>
                                                <label for="zlRoleCA">Rôle CA</label>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="row">

                                        <div class="col-md-2">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Voeux</label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccVOEUX" class="styled" type="checkbox">
                                                    <label for="ccVOEUX"></label>
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->
                        </div>
                        <div class="col-md-4 ">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box green-sharp">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-sticky-note-o"></i>Notes Complémentaires
                                    </div>
                                    <div class="tools">
                                    </div>
                                </div>
                                <div class="portlet-body">

                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="form-group form-md-line-input">
                                                <textarea id="ztNotes" style="resize: none !important"
                                                          class="form-control" placeholder="Saisir vos notes ici"
                                                          rows="6"></textarea>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->
                        </div>

                    </div>

                    <!-- START Portlet Activité -->
                    <div class="row">

                        <div class="col-md-12 ">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box blue-dark ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-group"></i>Participation aux Activités
                                    </div>
                                    <div class="tools">
                                    </div>
                                </div>
                                <div class="portlet-body">

                                    <div class="row">
                                    </div>

                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->
                        </div>

                    </div>
                    <!-- END Portlet Activité -->

                    <!-- START Portlet Cotisations -->
                    <div class="row">

                        <div class="col-md-12 ">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box blue-soft ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-money"></i>Suivi des Cotisations
                                    </div>
                                    <div class="tools">
                                    </div>
                                </div>
                                <div class="portlet-body">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <table data-toggle="table"
                                                   class="table table-striped table-bordered table-hover order-column"
                                                   id="tableCotisation">
                                                <thead>
                                                <tr>
                                                    <th class="text-center"> Année</th>
                                                    <th class="text-center"> Type Cotisation</th>
                                                    <th class="text-center"> Montant</th>
                                                    <th class="text-center"> Type Règlement</th>
                                                    <th class="text-center"> Date Règlement</th>
                                                    <th class="text-center"> Référence Règlement</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->
                        </div>

                    </div>
                    <!-- END Portlet Cotisations -->

                </div>

                <div class="modal-footer">
                    <button type="button" id="btnCopieData" class="btn red-mint btn-outline display-hide">Recopier
                        Etablissement
                    </button>
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fermer</button>
                    <button type="button" class="btn" id="btnAction"></button>
                </div>
            </div>

        </div>
        <!-- END Modal  PERSONNES PHYSIQUES-->

        <!-- END Liste de Modals -->




@endsection
