@extends('layouts.app')
@section('title', $titre)
@section('subtitle', $subtitre)

@section('script')

    <script src="{{ URL::asset('/scripts/annuaire/oo.js') }}"></script>

@endsection
@section('content')

    <!-- START Filtre -->
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-filter"></i>Recherche Multicritères
                    </div>
                    <div class="tools">
                        <a class="expand" href="javascript:;"> </a>
                    </div>
                </div>
                <div class="portlet-body portlet-collapsed">
                    <div class="row">
                        <div class="col-md-1">
                            <div class="form-group form-md-line-input" style="margin-bottom: 0px;padding-top: 0px;">
                                <label for="ccFiltreOO">
                                    <small>Organismes</small>
                                </label>
                            </div>
                            <div class="form-group form-md-line-input" style="margin-bottom: 5px;padding-top: 0px;">
                                <input id="ccFiltreOO" type="checkbox" class="make-switch" data-on-color="primary"
                                       data-on-text="Oui" data-off-color="default" data-off-text="Non" data-size="mini"
                                       checked>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group form-md-line-input" style="margin-bottom: 0px;padding-top: 0px;">
                                <label for="ccFiltrePO">
                                    <small>Pôles</small>
                                </label>
                            </div>
                            <div class="form-group form-md-line-input" style="margin-bottom: 5px;padding-top: 0px;">
                                <input id="ccFiltrePO" type="checkbox" class="make-switch" data-on-color="danger"
                                       data-on-text="Oui" data-off-color="default" data-off-text="Non" data-size="mini"
                                       checked>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group form-md-line-input" style="margin-bottom: 0px;padding-top: 0px;">
                                <label for="ccFiltreET">
                                    <small>Etablissements</small>
                                </label>
                            </div>
                            <div class="form-group form-md-line-input" style="margin-bottom: 5px;padding-top: 0px;">
                                <input id="ccFiltreET" type="checkbox" class="make-switch"
                                       data-on-color="green-seagreen" data-on-text="Oui" data-off-color="default"
                                       data-off-text="Non" data-size="mini" checked>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group form-md-line-input" style="margin-bottom: 0px;padding-top: 0px;">
                                <label for="ccFiltreAN">
                                    <small>Antennes</small>
                                </label>
                            </div>
                            <div class="form-group form-md-line-input" style="margin-bottom: 5px;padding-top: 0px;">
                                <input id="ccFiltreAN" type="checkbox" class="make-switch"
                                       data-on-color="yellow-casablanca" data-on-text="Oui" data-off-color="default"
                                       data-off-text="Non" data-size="mini" checked>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group form-md-line-input" style="margin-bottom: 0px;padding-top: 0px;">
                                <label for="ccFiltreAA">
                                    <small>Adresses</small>
                                </label>
                            </div>
                            <div class="form-group form-md-line-input" style="margin-bottom: 5px;padding-top: 0px;">
                                <input id="ccFiltreAA" type="checkbox" class="make-switch" data-on-color="purple-plum"
                                       data-on-text="Oui" data-off-color="default" data-off-text="Non" data-size="mini"
                                       checked>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group form-md-line-input" style="margin-bottom: 0px;padding-top: 0px;">
                                <label for="ccFiltrePE">
                                    <small>Personnel</small>
                                </label>
                            </div>
                            <div class="form-group form-md-line-input" style="margin-bottom: 5px;padding-top: 0px;">
                                <input id="ccFiltrePE" type="checkbox" class="make-switch" data-on-color="yellow"
                                       data-on-text="Oui" data-off-color="default" data-off-text="Non" data-size="mini"
                                       checked>
                            </div>
                        </div>
                    </div>

<<<<<<< HEAD

                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group form-md-line-input" style="margin-bottom: 0px;padding-top: 0px;">
                                <label for="ccFiltreNL">
                                    <small>Abonné NL</small>
                                </label>
                            </div>
                            <div class="form-group form-md-line-input" style="margin-bottom: 5px;padding-top: 0px;">

                                <!--
                                <input id="ccFiltreNL" type="checkbox" class="make-switch" data-on-color="primary"
                                       data-on-text="Oui" data-off-color="default" data-off-text="Non" data-size="mini"
                                       checked>
                                -->

                                <div id="btnFiltreNL" class="btn-group" role="group" aria-label="..." data-nl="-1">
                                      
                                    <button id="btnFiltreNL_oui" type="button" class="btn btn-xs">Oui</button>
                                      
                                    <button id="btnFiltreNL_na" type="button" class="btn btn-xs btn-default">&nbsp;&nbsp;-&nbsp;&nbsp;</button>
                                      
                                    <button id="btnFiltreNL_non" type="button" class="btn btn-xs">Non</button>
                                </div>

                            </div>
                        </div>
                    </div>

=======
>>>>>>> debug8
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group form-md-line-input form-md-floating-label has-success"
                                 style="margin-bottom: 15px;">
                                <input id="ztFiltreNom" class="form-control text-uppercase"
                                       type="text">
                                <label for="ztFiltreNom">Nom</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group form-md-line-input form-md-floating-label has-success"
                                 style="margin-bottom: 15px;">
                                <input id="ztFiltreAdresse" class="form-control text-uppercase"
                                       type="text">
                                <label for="ztFiltreAdresse">Adresse</label>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group form-md-line-input form-md-floating-label has-success"
                                 style="margin-bottom: 15px;">
                                <input id="ztFiltreCP" class="form-control text-uppercase"
                                       type="text">
                                <label for="ztFiltreCP">CP</label>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group form-md-line-input form-md-floating-label has-success"
                                 style="margin-bottom: 15px;">
                                <input id="ztFiltreVille" class="form-control text-uppercase"
                                       type="text">
                                <label for="ztFiltreVille">Ville</label>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group form-md-line-input form-md-floating-label has-success"
                                 style="margin-bottom: 15px;">
                                <input id="ztFiltreFiness" class="form-control text-uppercase"
                                       type="text">
                                <label for="ztFiltreFiness">Finess</label>
<<<<<<< HEAD
=======
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group form-md-line-input form-md-floating-label has-success"
                                 style="margin-bottom: 15px;">
                                <input id="ztFiltreCode" class="form-control text-uppercase"
                                       type="text">
                                <label for="ztFiltreCode">Code / Code interne</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group form-md-line-input form-md-floating-label has-success"
                                 style="margin-bottom: 15px;">
                                <input id="ztFiltreService" class="form-control text-uppercase"
                                       type="text">
                                <label for="ztFiltreService">Service</label>
>>>>>>> debug8
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group form-md-line-input form-md-floating-label has-success"
                                 style="margin-bottom: 15px;">
                                <input id="ztFiltreCode" class="form-control text-uppercase"
                                       type="text">
                                <label for="ztFiltreCode">Code / Code interne</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group form-md-line-input form-md-floating-label has-success"
                                 style="margin-bottom: 15px;">
                                <input id="ztFiltreService" class="form-control text-uppercase"
                                       type="text">
                                <label for="ztFiltreService">Service</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group form-md-line-input form-md-floating-label has-success"
                                 style="margin-bottom: 15px;">
                                <input id="ztFiltreSigle" class="form-control text-uppercase"
                                       type="text">
                                <label for="ztFiltreSigle">Sigle</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group form-md-line-input form-md-floating-label has-success"
                                 style="margin-bottom: 15px;">
                                <input id="ztFiltreNbPlaces" class="form-control text-uppercase"
                                       type="text">
                                <label for="ztFiltreNbPlaces">Nb Places (>=) </label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                <select id="zlFiltreTypeET" class="form-control" multiple></select>
                                <label for="zlFiltreTypeET">Type Etablissement</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                <select id="zlFiltreTypeSecteur" class="form-control" multiple></select>
                                <label for="zlFiltreTypeSecteur">Type Secteur</label>
<<<<<<< HEAD
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                <select id="zlFiltreTypeSecteurSMS" class="form-control" multiple></select>
                                <label for="zlFiltreTypeSecteurSMS">Type Secteur SMS</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                <select id="zlFiltreTrancheAge" class="form-control" multiple></select>
                                <label for="zlFiltreTrancheAge">Tranche Age</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                <select id="zlFiltreTypePublic" class="form-control" multiple></select>
                                <label for="zlFiltreTypePublic">Type Public</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                <select id="zlFiltreDetailTypePublic" class="form-control" multiple></select>
                                <label for="zlFiltreDetailTypePublic">Détail Type Public</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                <select id="zlFiltreTypeModalite" class="form-control" multiple></select>
                                <label for="zlFiltreTypeModalite">Type Modalité Accueil</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                <select id="zlFiltreTypeOrganisme" class="form-control" multiple></select>
                                <label for="zlFiltreTypeOrganisme">Type Organisme</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                <select id="zlFiltreFonction" class="form-control" multiple></select>
                                <label for="zlFiltreFonction">Fonction</label>
                            </div>
                        </div>
                    </div>
=======
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                <select id="zlFiltreTypeSecteurSMS" class="form-control" multiple></select>
                                <label for="zlFiltreTypeSecteurSMS">Type Secteur SMS</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                <select id="zlFiltreTrancheAge" class="form-control" multiple></select>
                                <label for="zlFiltreTrancheAge">Tranche Age</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                <select id="zlFiltreTypePublic" class="form-control" multiple></select>
                                <label for="zlFiltreTypePublic">Type Public</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                <select id="zlFiltreDetailTypePublic" class="form-control" multiple></select>
                                <label for="zlFiltreDetailTypePublic">Détail Type Public</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                <select id="zlFiltreTypeModalite" class="form-control" multiple></select>
                                <label for="zlFiltreTypeModalite">Type Modalité Accueil</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                <select id="zlFiltreTypeOrganisme" class="form-control" multiple></select>
                                <label for="zlFiltreTypeOrganisme">Type Organisme</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                <select id="zlFiltreFonction" class="form-control" multiple></select>
                                <label for="zlFiltreFonction">Fonction</label>
                            </div>
                        </div>
                    </div>
>>>>>>> debug8

                    <div class="row" style="padding-bottom: 30px;">
                        <div class="col-md-12">
                            <input type="hidden" id="filtreToken" name="_token" value="{{ csrf_token() }}">
                            <button class="btn btn-info" type="button" id="btnFilterExcel">
                                <i class="fa fa-file-excel-o" style="font-size:16px;"></i> Excel
                            </button>
                            <button class="btn btn-primary" type="button" id="btnFilterOn">
                                <i class="fa fa-filter" style="font-size:16px;"></i> Filtrer
                            </button>
                            <button class="btn btn-danger" type="button" id="btnFilterOff">
                                <i class="fa fa-trash" style="font-size:16px;"></i> Effacer
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table data-toggle="table"
                                   class="table table-striped table-bordered table-hover order-column display-hide"
                                   id="tableFiltre">
                                <thead>
                                <tr>
                                    <th data-halign="center" data-align="left"> Catégorie</th>
                                    <th data-halign="center" data-align="left"> Code</th>
                                    <th data-halign="center" data-align="left"> Nom</th>
                                    <th data-halign="center" data-align="left"> Type / Secteur</th>
                                    <th data-halign="center" data-align="left"> Ville</th>
                                    <!--<th data-halign="center" data-align="middle"> Statut</th>-->
                                    <th data-halign="center" data-align="middle"> Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Filtre -->

    <!-- START Datatable -->
    <div class="row">

        <div class="col-md-12 col-sm-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box red">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>Gestion des {!! $subtitre !!}
                    </div>
                    <div class="actions">
                        <a href="javascript:;" class="btn btn-default btn-sm" id="btnAdd">
                            <i class="fa fa-plus"></i> Ajout </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table data-toggle="table"
                                   class="table table-striped table-bordered table-hover order-column"
                                   id="tableOrga">
                                <thead>
                                <tr>
                                    <th data-halign="center" data-align="left"> Code</th>
                                    <th data-halign="center" data-align="left"> Nom</th>
                                    <th data-halign="center" data-align="left"> Type</th>
                                    <th data-halign="center" data-align="left"> Ville</th>
                                    <!--<th data-halign="center" data-align="middle"> Statut</th>-->
                                    <th data-halign="center" data-align="middle"> Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>
    <!-- END Datatable -->


<<<<<<< HEAD
=======
    <!-- START Execution Modal -->
    <!-- START Dialogue d'exécution Level 0 -->
    <div class="modal fade" id="dialogExec" data-level="" aria-hidden="true" data-backdrop="static"
         data-keyboard="false">
    </div>
    <!-- END Dialogue d'exécution Level 0 -->

    <!-- START Dialogue d'exécution Level 0 -->
    <div class="modal fade dialog-exec" id="dialogExecLev0" aria-hidden="true" data-backdrop="static"
         data-keyboard="false">
    </div>
    <!-- END Dialogue d'exécution Level 0 -->

    <!-- START Dialogue d'exécution Level 1 -->
    <div class="modal fade dialog-exec" id="dialogExecLev1" aria-hidden="true" data-backdrop="static"
         data-keyboard="false">
    </div>
    <!-- END Dialogue d'exécution Level 1 -->

    <!-- START Dialogue d'exécution Level 2 -->
    <div class="modal fade dialog-exec" id="dialogExecLev2" aria-hidden="true" data-backdrop="static"
         data-keyboard="false">
    </div>
    <!-- END Dialogue d'exécution Level 2 -->

    <!-- START Dialogue d'exécution Level 3 -->
    <div class="modal fade dialog-exec" id="dialogExecLev3" aria-hidden="true" data-backdrop="static"
         data-keyboard="false">
    </div>
    <!-- END Dialogue d'exécution Level 3 -->

    <!-- START Dialogue d'exécution Level 4 -->
    <div class="modal fade dialog-exec" id="dialogExecLev4" aria-hidden="true" data-backdrop="static"
         data-keyboard="false">
    </div>
    <!-- END Dialogue d'exécution Level 4 -->

    <!-- START Dialogue d'exécution Level 5 -->
    <div class="modal fade dialog-exec" id="dialogExecLev5" aria-hidden="true" data-backdrop="static"
         data-keyboard="false">
    </div>
    <!-- END Dialogue d'exécution Level 5 -->

    <!-- START Dialogue d'exécution Level 9 -->
    <div class="modal fade dialog-exec" id="dialogExecLev9" aria-hidden="true" data-backdrop="static"
         data-keyboard="false">
    </div>
    <!-- END Dialogue d'exécution Level 9 -->
    <!-- END Execution Modal -->

>>>>>>> debug8

    <!-- START Liste de Modals -->
    <!-- START Modal LOGO -->
    <div id="formShowLogo" style="display: none;">
        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
        <div class="modal-dialog modal-small-lg">
            <div class="modal-content">
                <div class="modal-header caption">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title bold uppercase"></h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="padding-top: 50px;">
                        <div class="col-md-6">
                            <div class="display-none" id="blocLogo">
                                <div style="border: 1px solid #c2cad8;padding:5px;">
                                    <div class="close fileinput-remove">×</div>
                                    <img id="picLogo" style="max-width:400px;">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="kv-main col-md-12">
                                    <form id="formLogo" action="" method="POST">
                                        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
                                        <label for="ztLogo">Choisir le Logo</label>
                                        <input id="ztLogo" class="file" type="file" multiple data-min-file-count="1">
                                    </form>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal LOGO -->

    <!-- START Modal TEL -->
    <div id="formTel" style="display: none;">
        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
        <div class="modal-dialog modal-medium-lg">
            <div class="modal-content">
                <div class="modal-header caption">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title bold uppercase"></h4>
                </div>
                <div class="modal-body">
                    <div class="row" id="portletTel" style="padding-top: 50px;">
                        <div class="col-md-2">
                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                <input class="form-control text-uppercase" id="ztSaisie"
                                       placeholder="Numéro" type="text">
                                <label for="ztSaisie">Numéro <span class="font-red">*</span></label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                <select id="zlTypeSaisie" class="form-control"></select>
                                <label for="zlTypeSaisie">Type <span class="font-red">*</span></label>
                            </div>
                        </div>
                        <div class="col-md-1" style="padding-top: 20px">
                            <button id="btnAddTel" class="btn btn-success" type="button">
                                <i class="fa fa-plus-circle"></i>
                            </button>
                            <label for="btnAddTel"></label>
                        </div>
                    </div>
                    <div class="row" style="padding-left: 15px;padding-right: 15px;">
                        <table data-toggle="table"
                               class="table table-striped table-bordered table-hover order-column"
                               id="tableTel">
                            <thead>
                            <tr>
                                <th class="text-center"> Téléphone</th>
                                <th class="text-center"> Type</th>
                                <th class="text-center"> Notes</th>
                                <th class="text-center"> Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal TEL -->

    <!-- START Modal MAIL -->
    <div id="formMail" style="display: none;">
        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
        <div class="modal-dialog modal-medium-lg">
            <div class="modal-content">
                <div class="modal-header caption">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title bold uppercase"></h4>
                </div>
                <div class="modal-body">
                    <div class="row" id="portletMail" style="padding-top: 50px;">
                        <div class="col-md-4">
                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                <input class="form-control" id="ztSaisie"
                                       placeholder="Adresse" type="text">
                                <label for="ztSaisie">Adresse <span class="font-red">*</span></label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                <select id="zlTypeSaisie" class="form-control"></select>
                                <label for="zlTypeSaisie">Type <span class="font-red">*</span></label>
                            </div>
                        </div>
                        <div class="col-md-1" style="padding-top: 20px">
                            <button id="btnAddMail" class="btn red" type="button">
                                <i class="fa fa-plus-circle"></i>
                            </button>
                            <label for="btnAddMail"></label>
                        </div>
                    </div>

                    <div class="row" style="padding-left: 15px;padding-right: 15px;">
                        <table data-toggle="table"
                               class="table table-striped table-bordered table-hover order-column"
                               id="tableMail">
                            <thead>
                            <tr>
                                <th class="text-center"> Adresse</th>
                                <th class="text-center"> Type</th>
                                <th class="text-center"> Notes</th>
                                <th class="text-center"> Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal MAIL -->

    <!-- START Modal OFFRES (D'EMPLOI) -->
    <div id="formOffre" style="display: none;">
        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header caption">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title bold uppercase"></h4>
                </div>
                <div class="modal-body">
                    <div class="row" id="portletOffre" style="margin-bottom: -20px">

                        <div class="col-md-2" style="margin-bottom: -20px">
                            <div class="form-group form-md-line-input form-md-floating-label has-success">
                                <input id="ztAnnee" class="form-control required text-uppercase"
                                       type="text">
                                <label for="ztAnnee">Année<span class="font-red">*</span></label>
                            </div>
                        </div>

                        <div class="col-md-2" style="margin-bottom: -20px">
                            <div class="form-group form-md-line-input form-md-floating-label has-success">
                                <input id="ztNb" class="form-control required text-uppercase"
                                       type="text">
                                <label for="ztNb">Nombre<span class="font-red">*</span></label>
                            </div>
                        </div>

                        <div class="col-md-2" style="margin-bottom: -20px">
                            <div class="form-group form-md-line-input form-md-floating-label has-success">
                                <input id="ztDate" class="form-control required text-uppercase date-picker"
                                       type="text" readonly>
                                <label for="ztDate">Date Saisie<span class="font-red">*</span></label>
                            </div>
                        </div>

                        <div class="col-md-1" style="padding-top: 20px;margin-bottom: -20px;">
                            <button id="btnAddOffre" class="btn red" type="button">
                                <i class="fa fa-plus-circle"></i>
                            </button>
                            <label for="btnAddOffre"></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group form-md-line-input form-md-floating-label has-success">
                                <input id="ztNotes" class="form-control text-uppercase"
                                       type="text">
                                <label for="ztNotes">Notes</label>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="padding-left: 15px;padding-right: 15px;">
                        <table data-toggle="table"
                               class="table table-striped table-bordered table-hover order-column"
                               id="tableOffre">
                            <thead>
                            <tr>
                                <th class="text-center"> Année</th>
                                <th class="text-center"> Nb</th>
                                <th class="text-center"> Date</th>
                                <th class="text-center"> Notes</th>
                                <th class="text-center"> Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal OFFRES (D'EMPLOI) -->

    <!-- START Modal ORGANISMES -->
    <div id="formOrganisme" style="display: none;">
        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
        <div class="modal-dialog modal-full">
            <div class="modal-content">
                <div class="modal-header caption">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title bold uppercase"></h4>
                </div>
                <div class="modal-body">
                    <div class="row">

                        <div class="col-md-8">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box green-meadow">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-edit"></i>Données de base
                                    </div>
                                </div>
                                <div class="portlet-body">

                                    <form role="form">
                                        <div class="form-body">

                                            <!-- START LIGNE 1 -->
                                            <div class="row">

                                                <div class="col-md-10">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input id="ztNom"
                                                               class="form-control required text-uppercase"
                                                               type="text">
                                                        <label for="ztNom">Nom Organisme<span
                                                                    class="font-red">*</span></label>
                                                    </div>
                                                </div>

                                                <div class="col-md-2">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control text-uppercase" id="ztSigle"
                                                               type="text">
                                                        <label for="ztSigle">Sigle</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="portlet box green-haze">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-edit"></i>Caractéristiques Organisme
                                                    </div>
                                                    <div class="tools">
                                                        <a title="" data-original-title="" href="javascript:;"
                                                           class="collapse"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body">
                                                    <div class="row">

                                                        <div class="col-md-4">
                                                            <div class="form-group form-md-line-input">
                                                                <select id="zlType" class="form-control">

                                                                </select>
                                                                <label for="zlType">Type Organisme<span
                                                                            class="font-red">*</span></label>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6 display-none">
                                                            <div class="form-group form-md-line-input">
                                                                <select id="zlDetailType" class="form-control">

                                                                </select>
                                                                <label for="zlDetailType">Détail Type Organisme<span
                                                                            class="font-red">*</span></label>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">

                                                            <div class="form-group no-padding-top">
                                                                <label>Gestionnaire</label>
<<<<<<< HEAD
                                                                <div class="checkbox">
                                                                    <input id="ccGestionnaire" type="checkbox">
                                                                    <label for="ccGestionnaire"> </label>
                                                                </div>
                                                                <!--
                                                                <div class="md-checkbox-inline">
                                                                    <div class="md-checkbox">
                                                                        <input id="ccGestionnaire" class="md-check"
                                                                               type="checkbox">
                                                                        <label for="ccGestionnaire">
                                                                            <span></span>
                                                                            <span class="check"></span>
                                                                            <span class="box"></span></label>
                                                                    </div>
=======
                                                                <div class="checkbox checkbox-primary">
                                                                    <input id="ccGestionnaire" class="styled"
                                                                           type="checkbox">
                                                                    <label for="ccGestionnaire"></label>
>>>>>>> debug8
                                                                </div>
                                                                -->
                                                            </div>

                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group form-md-line-input">
                                                                <select id="zlStatut" class="form-control">

                                                                </select>
                                                                <label for="zlStatut">Statut <span
                                                                            class="font-red">*</span></label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group form-md-line-input form-md-floating-label has-success"
                                                                 style="display: none;">
                                                                <input id="ztNomParrain"
                                                                       class="form-control text-uppercase"
                                                                       type="text">
                                                                <label for="ztNomParrain">Nom Parrain</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- START LIGNE 2 -->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control text-uppercase" id="ztAdr1"
                                                               type="text">
                                                        <label for="ztAdr1">Adresse 1</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END LIGNE 2 -->

                                            <!-- START LIGNE 3 -->
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control text-uppercase" id="ztAdr2"
                                                               type="text">
                                                        <label for="ztAdr2">Adresse 2</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END LIGNE 3 -->

                                            <!-- START LIGNE 4 -->
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control text-uppercase" id="ztAdr3"
                                                               type="text">
                                                        <label for="ztAdr3">Adresse 3</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END LIGNE 4 -->

                                            <!-- START LIGNE 5 -->
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control text-uppercase required"
                                                               id="ztCP"
                                                               type="text" data-inputmask="'mask': '9{5}'">
                                                        <label for="ztCP">Code Postal <span
                                                                    class="font-red">*</span></label>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control text-uppercase required"
                                                               id="ztVille"
                                                               type="text">
                                                        <label for="ztVille">Ville <span
                                                                    class="font-red">*</span></label>
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group form-md-line-input">
                                                        <select id="zlPays" class="form-control">

                                                        </select>
                                                        <label for="zlPays">Pays</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- START LIGNE 5 -->

                                            <!-- START LIGNE 6 -->
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control text-uppercase" id="ztFINESS"
                                                               type="text" data-inputmask="'mask': '9{9}'">
                                                        <label for="ztFINESS">FINESS</label>
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control text-uppercase" id="ztSIRET"
                                                               type="text">
                                                        <label for="ztSIRET">SIRET</label>
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control text-uppercase" id="ztIBAN"
                                                               type="text">
                                                        <label for="ztIBAN">IBAN</label>
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control text-uppercase" id="ztBIC"
                                                               type="text">
                                                        <label for="ztBIC">BIC</label>
                                                    </div>
                                                </div>

                                            </div>
                                            <!-- START LIGNE 6 -->

                                        </div>

                                    </form>

                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->
                        </div>

                        <div class="col-md-4 ">

                            <div class="form-group form-md-line-input form-md-floating-label has-success">
                                <input class="form-control" id="ztWEB" type="text">
                                <label for="ztWEB">Site Internet</label>
                            </div>

                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box red" id="portletTel">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-phone"></i>Téléphones / Fax
                                    </div>
                                    <div class="tools">
                                        <a title="" href="javascript:;"
                                           class="expand"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body portlet-collapsed">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group form-md-line-input form-md-floating-label has-success"
                                                 style="margin-bottom: 15px;">
                                                <input class="form-control text-uppercase" id="ztSaisie"
                                                       type="text">
                                                <label for="ztSaisie">Téléphone <span
                                                            class="font-red">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                                <select id="zlTypeSaisie" class="form-control"></select>
                                                <label for="zlTypeSaisie">Type <span
                                                            class="font-red">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding-top: 20px">
                                            <button id="btnAddTel" class="btn btn-success" type="button">
                                                <i class="fa fa-plus-circle"></i>
                                            </button>
                                            <label for="btnAddTel"></label>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-left: 5px;padding-right: 5px;">
                                        <table data-toggle="table"
                                               class="table table-striped table-bordered table-hover order-column"
                                               id="tableTel">
                                            <thead>
                                            <tr>
                                                <th class="text-center"> Téléphone</th>
                                                <th class="text-center"> Type</th>
                                                <th class="text-center"> Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->

                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box red" id="portletMail">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-send"></i>Mails
                                    </div>
                                    <div class="tools">
                                        <a title="" data-original-title="" href="javascript:;" class="expand"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body portlet-collapsed">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group form-md-line-input form-md-floating-label has-success"
                                                 style="margin-bottom: 15px;">
                                                <input class="form-control" id="ztSaisie" type="text">
                                                <label for="ztSaisie">Adresse mail <span
                                                            class="font-red">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                                <select id="zlTypeSaisie" class="form-control"></select>
                                                <label for="zlTypeSaisie">Type <span
                                                            class="font-red">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding-top: 20px">
                                            <button id="btnAddMail" class="btn red" type="button">
                                                <i class="fa fa-plus-circle"></i>
                                            </button>
                                            <label for="btnAddMail"></label>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-left: 5px;padding-right: 5px;">
                                        <table data-toggle="table"
                                               class="table table-striped table-bordered table-hover order-column"
                                               id="tableMail">
                                            <thead>
                                            <tr>
                                                <th data-halign="center" data-align="left"> Mail</th>
                                                <th data-halign="center" data-align="left"> Type</th>
                                                <th data-halign="center" data-align="right"> Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->

                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box red" id="portleAccesAnnuaire">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gear"></i>Accès Annuaire
                                    </div>
                                    <div class="tools">
                                        <a title="" data-original-title="" href="javascript:;" class="expand"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body portlet-collapsed">

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Actif </label>
<<<<<<< HEAD
                                                <div class="md-checkbox-inline">
                                                    <div class="md-checkbox">
                                                        <input id="ccACTIF" class="md-check" type="checkbox"
                                                               checked>
                                                        <label for="ccACTIF">
                                                            <span></span>
                                                            <span class="check"></span>
                                                            <span class="box"></span></label>
                                                    </div>
=======
                                                <div class="checkbox checkbox-success">
                                                    <input id="ccACTIF" class="styled" type="checkbox">
                                                    <label for="ccACTIF"></label>
>>>>>>> debug8
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Visible </label>
<<<<<<< HEAD
                                                <div class="md-checkbox-inline">
                                                    <div class="md-checkbox">
                                                        <input id="ccVISIBLE" class="md-check" type="checkbox"
                                                               checked>
                                                        <label for="ccVISIBLE">
                                                            <span></span>
                                                            <span class="check"></span>
                                                            <span class="box"></span></label>
                                                    </div>
=======
                                                <div class="checkbox checkbox-success">
                                                    <input id="ccVISIBLE" class="styled" type="checkbox">
                                                    <label for="ccVISIBLE"></label>
>>>>>>> debug8
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Accès Annuaire </label>
<<<<<<< HEAD
                                                <div class="md-checkbox-inline">
                                                    <div class="md-checkbox">
                                                        <input id="ccACCES" class="md-check" type="checkbox"
                                                               checked>
                                                        <label for="ccACCES">
                                                            <span></span>
                                                            <span class="check"></span>
                                                            <span class="box"></span></label>
                                                    </div>
=======
                                                <div class="checkbox checkbox-success">
                                                    <input id="ccACCES" class="styled" type="checkbox">
                                                    <label for="ccACCES"></label>
>>>>>>> debug8
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->

<<<<<<< HEAD
                            <div class="alert alert-info complement-footer note" style="left: 10px;">
=======
                            <div class="alert alert-info note historique" style="left: 10px;">
>>>>>>> debug8
                            </div>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-8 ">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-database"></i>Données Complémentaires
                                    </div>
                                    <div class="tools">
                                    </div>
                                </div>
                                <div class="portlet-body">

                                    <!-- START Ligne 1 -->
                                    <div class="row">

                                        <div class="col-md-2">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Inscription BI</label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccBI" class="styled" type="checkbox">
                                                    <label for="ccBI"></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group form-md-line-input" style="margin-top: 5px;">
                                                <select id="zlReceptionBI" class="form-control">

                                                </select>
                                                <label for="zlReceptionBI">Réception BI</label>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Newsletter </label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccNL" class="styled" type="checkbox">
                                                    <label for="ccNL"></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Cat. Formation</label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccCF" class="styled" type="checkbox">
                                                    <label for="ccCF"></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group form-md-line-input" style="margin-top: 5px;">
                                                <select id="zlReceptionCF" class="form-control">

                                                </select>
                                                <label for="zlReceptionCF">Réception CF</label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Ligne 1 -->

                                    <div class="row">

                                        <div class="col-md-2">
                                            <div class="form-group form-md-checkboxes">
                                                <label>BI Payant</label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccBI_PAYANT" class="styled" type="checkbox">
                                                    <label for="ccBI_PAYANT"></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Cotisant</label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccCOTIS" class="styled" type="checkbox">
                                                    <label for="ccCOTIS"></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group form-md-line-input">
                                                <input class="form-control text-uppercase" id="ztCodeInterne"
                                                       placeholder="Code" type="text">
                                                <label for="ztCodeInterne">Code interne</label>
                                            </div>
                                        </div>


                                    </div>

                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->
                        </div>
                        <div class="col-md-4 ">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box green-sharp">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-sticky-note-o"></i>Notes Complémentaires
                                    </div>
                                    <div class="tools">
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group form-md-line-input">
                                                <textarea id="ztNotes" style="resize: none !important"
                                                          class="form-control" placeholder="Saisir vos notes ici"
                                                          rows="5"></textarea>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box red-flamingo">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-database"></i>Offres d'Emploi
                                    </div>
                                    <div class="tools">
                                    </div>
                                </div>
                                <div class="portlet-body">

                                </div>
                            </div>
                        </div>
                    </div>

<<<<<<< HEAD

                    <div class="row">
                        &nbsp;
=======
                    <div class="row">
>>>>>>> debug8
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fermer</button>
                    <button type="button" class="btn" id="btnAction"></button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal  ORGANISMES -->

    <!-- START Modal POLES -->
    <div id="formPole" style="display: none;">
        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
        <div class="modal-dialog modal-full">
            <div class="modal-content">
                <div class="modal-header caption">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title bold uppercase"></h4>
                </div>
<<<<<<< HEAD
                <div class="bg-grey ariane">
=======
                <div class="bg-grey ariane-title">
>>>>>>> debug8
                    <div id="prec" class="col-md-12">
                    </div>
                </div>
                <div class="modal-body ariane-body">
                    <div class="row">
                        <div class="col-md-8">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box green-meadow">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-edit"></i>Données de base
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <form role="form">
                                        <div class="form-body">

                                            <!-- START LIGNE 1 -->
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control required text-uppercase" id="ztNom"
                                                               type="text">
                                                        <label for="ztNom">Saisir le nom <span class="font-red">*</span></label>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control text-uppercase"
                                                               id="ztSigle" type="text">
                                                        <label for="ztSigle">Sigle</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group form-md-line-input">
                                                        <select id="zlTypePole" class="form-control">

                                                        </select>
                                                        <label for="zlTypePole">Type Pôle<span
                                                                    class="font-red">*</span></label>
                                                    </div>
                                                </div>
                                            </div>

                                            <!--
                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-group form-md-line-input">
                                                        <select id="zlTypePublic" class="form-control">

                                                        </select>
                                                        <label for="zlTypePublic">Type Public <span
                                                                    class="font-red">*</span></label>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 display-none">
                                                    <div class="form-group form-md-line-input">
                                                        <select id="zlDetailTypePublic" class="form-control">

                                                        </select>
                                                        <label for="zlDetailTypePublic">Détail Type Public </label>
                                                    </div>
                                                </div>

                                            </div>
                                            -->

                                            <div class="portlet box grey-mint display-none" id="portletET">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-edit"></i>Caractéristiques Cumulées
                                                        Établissement / Service
                                                    </div>
                                                    <div class="tools">
                                                        <a title="" data-original-title="" href="javascript:;"
                                                           class="collapse"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body">

                                                    <!-- START LIGNE 4 -->
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group form-md-line-input">
                                                                <select id="zlTypePublic" class="form-control" multiple>
                                                                </select>
                                                                <label for="zlTypePublic">Types de Publics</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-10">
                                                            <div class="form-group form-md-line-input">
                                                                <select id="zlTrancheAge" class="form-control" multiple>
                                                                </select>
                                                                <label for="zlTrancheAge">Tranche d'Ages</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group form-md-line-input  form-md-floating-label has-success">
                                                                <input class="form-control text-uppercase"
                                                                       id="znNbPlacesET" type="text">
                                                                <label for="znNbPlacesET">Nb Places</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- END LIGNE 3 -->

                                                </div>
                                            </div>

                                            <!--
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control"
                                                               id="ztNbPlaces" type="number">
                                                        <label for="ztNbPlaces">Nb Places</label>
                                                    </div>
                                                </div>

                                                <div class="col-md-2">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control" id="ztAge" type="text">
                                                        <label for="ztAge">Age</label>
                                                    </div>
                                                </div>
                                            </div>
                                            -->
                                            <!-- END LIGNE 1 -->

                                            <!-- START LIGNE 2 -->
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control text-uppercase" id="ztAdr1"
                                                               type="text">
                                                        <label for="ztAdr1">Adresse 1</label>
                                                    </div>
                                                </div>

                                            </div>
                                            <!-- END LIGNE 2 -->

                                            <!-- START LIGNE 3 -->
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control text-uppercase" id="ztAdr2"
                                                               type="text">
                                                        <label for="ztAdr2">Adresse 2</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END LIGNE 3 -->

                                            <!-- START LIGNE 4 -->
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control text-uppercase" id="ztAdr3"
                                                               type="text">
                                                        <label for="ztAdr3">Adresse 3</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END LIGNE 4 -->

                                            <!-- START LIGNE 5 -->
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control text-uppercase required" id="ztCP"
                                                               type="text" data-inputmask="'mask': '9{5}'">
                                                        <label for="ztCP"> CP <span
                                                                    class="font-red">*</span></label>

                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control text-uppercase required" id="ztVille"
                                                               type="text">
                                                        <label for="ztVille">Ville<span
                                                                    class="font-red">*</span></label>
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group form-md-line-input">
                                                        <select id="zlPays" class="form-control">

                                                        </select>
                                                        <label for="zlPays">Pays</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- START LIGNE 5 -->

                                        </div>
                                    </form>

                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->
                        </div>

                        <div class="col-md-4 ">
                            <div class="form-group form-md-line-input form-md-floating-label has-success">
                                <input class="form-control" id="ztWEB" type="text">
                                <label for="ztWEB">Site Internet</label>
                            </div>

                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box red" id="portletTel">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-phone"></i>Téléphones / Fax
                                    </div>
                                    <div class="tools">
                                        <a title="" data-original-title="Blublu" href="javascript:;"
                                           class="expand"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body portlet-collapsed">

                                    <div class="row">


                                        <div class="col-md-4">
                                            <div class="form-group form-md-line-input form-md-floating-label has-success"
                                                 style="margin-bottom: 15px;">
                                                <input class="form-control text-uppercase" id="ztSaisie"
                                                       type="text">
                                                <label for="ztSaisie">Numéro <span class="font-red">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                                <select id="zlTypeSaisie" class="form-control"></select>
                                                <label for="zlTypeSaisie">Type <span class="font-red">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding-top: 20px">
                                            <button id="btnAddTel" class="btn btn-success" type="button">
                                                <i class="fa fa-plus-circle"></i>
                                            </button>
                                            <label for="btnAddTel"></label>
                                        </div>


                                    </div>

                                    <div class="row" style="padding-left: 5px;padding-right: 5px;">
                                        <table data-toggle="table"
                                               class="table table-striped table-bordered table-hover order-column"
                                               id="tableTel">
                                            <thead>
                                            <tr>
                                                <th class="text-center"> Téléphone</th>
                                                <th class="text-center"> Type</th>
                                                <th class="text-center"> Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->

                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box red" id="portletMail">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-send"></i>Mails
                                    </div>
                                    <div class="tools">
                                        <a title="" data-original-title="" href="javascript:;" class="expand"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body portlet-collapsed">

                                    <div class="row">


                                        <div class="col-md-6">
                                            <div class="form-group form-md-line-input form-md-floating-label has-success"
                                                 style="margin-bottom: 15px;">
                                                <input class="form-control" id="ztSaisie"
                                                       type="text">
                                                <label for="ztSaisie">Adresse mail <span
                                                            class="font-red">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                                <select id="zlTypeSaisie" class="form-control"></select>
                                                <label for="zlTypeSaisie">Type <span class="font-red">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding-top: 20px">
                                            <button id="btnAddMail" class="btn red" type="button">
                                                <i class="fa fa-plus-circle"></i>
                                            </button>
                                            <label for="btnAddMail"></label>
                                        </div>


                                    </div>

                                    <div class="row" style="padding-left: 5px;padding-right: 5px;">

                                        <table data-toggle="table"
                                               class="table table-striped table-bordered table-hover order-column"
                                               id="tableMail">
                                            <thead>
                                            <tr>
                                                <th data-halign="center" data-align="left"> Mail</th>
                                                <th data-halign="center" data-align="left"> Type</th>
                                                <th data-halign="center" data-align="right"> Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->

                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box red" id="portleAccesAnnuaire">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gear"></i>Accès Annuaire
                                    </div>
                                    <div class="tools">
                                        <a title="" data-original-title="" href="javascript:;" class="expand"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body portlet-collapsed">

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Actif </label>
                                                <div class="checkbox checkbox-success">
                                                    <input id="ccACTIF" class="styled" type="checkbox">
                                                    <label for="ccACTIF"></label>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Visible </label>
                                                <div class="checkbox checkbox-success">
                                                    <input id="ccVISIBLE" class="styled" type="checkbox">
                                                    <label for="ccVISIBLE"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Accès Annuaire </label>
                                                <div class="md-checkbox-inline">
                                                    <div class="checkbox checkbox-success">
                                                        <input id="ccACCES" class="styled" type="checkbox">
                                                        <label for="ccACCES"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->
                            <div class="alert alert-info historique note" style="left: 10px;">
                            </div>

                            <div class="alert alert-info complement-footer note" style="left: 10px;">
                            </div>
                        </div>

                    </div>


                    <div class="row">

                        <div class="col-md-8 ">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-database"></i>Données Complémentaires
                                    </div>
                                    <div class="tools">
                                    </div>
                                </div>
                                <div class="portlet-body">

                                    <!-- START Ligne 1 -->
                                    <div class="row">

                                        <div class="col-md-2">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Inscription BI</label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccBI" class="styled" type="checkbox">
                                                    <label for="ccBI"></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group form-md-line-input" style="margin-top: 5px;">
                                                <select id="zlReceptionBI" class="form-control">

                                                </select>
                                                <label for="zlReceptionBI">Réception BI</label>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Newsletter</label>
                                                <div class="md-checkbox-inline">
                                                    <div class="checkbox checkbox-primary">
                                                        <input id="ccNL" class="styled" type="checkbox">
                                                        <label for="ccNL"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Cat. Formation</label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccCF" class="styled" type="checkbox">
                                                    <label for="ccCF"></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group form-md-line-input" style="margin-top: 5px;">
                                                <select id="zlReceptionCF" class="form-control">

                                                </select>
                                                <label for="zlReceptionCF">Réception CF</label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Ligne 1 -->

                                    <div class="row">

                                        <div class="col-md-2">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Contribuant</label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccCONT" class="styled" type="checkbox">
                                                    <label for="ccCONT"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->
                        </div>
                        <div class="col-md-4 ">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box green-sharp">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-sticky-note-o"></i>Notes Complémentaires
                                    </div>
                                    <div class="tools">
                                    </div>
                                </div>
                                <div class="portlet-body">

                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="form-group form-md-line-input">
                                                <textarea id="ztNotes" style="resize: none !important"
                                                          class="form-control" placeholder="Saisir vos notes ici"
                                                          rows="5"></textarea>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->

                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-12 ">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box red-flamingo">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-database"></i>Offres d'Emploi
                                    </div>
                                    <div class="tools">
                                    </div>
                                </div>
                                <div class="portlet-body">

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" id="btnCopieDataOO" class="btn red-mint btn-outline">Recopier Organisme
                    </button>
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fermer</button>
                    <button type="button" class="btn" id="btnAction"></button>
                </div>
            </div>
        </div>

    </div>
    <!-- END Modal  POLES -->

    <!-- START Modal ETABLISSEMENT -->
    <div id="formEtablissement" style="display: none;">
        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
        <div class="modal-dialog modal-full">
            <div class="modal-content">
                <div class="modal-header caption">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title bold uppercase"></h4>
                </div>
<<<<<<< HEAD
                <div class="bg-grey ariane">
=======
                <div class="bg-grey ariane-title">
>>>>>>> debug8
                    <div id="prec" class="col-md-12">
                    </div>
                </div>
                <div class="modal-body ariane-body">
                    <!-- START DONNEES DE BASE + TEL + MAILS + ACCESS + ORGANIGRAMME -->
                    <div class="row">
                        <div class="col-md-8">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box green-meadow">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-edit"></i>Données de base
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="form-body">


                                        <!-- START LIGNE 1 -->
                                        <div class="row">

                                            <div class="col-md-10">
                                                <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                    <input class="form-control required text-uppercase" id="ztNom"
                                                           type="text">
                                                    <label for="ztNom">Nom <span
                                                                class="font-red">*</span></label>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                    <input class="form-control text-uppercase"
                                                           id="ztSigle" type="text">
                                                    <label for="ztSigle">Sigle</label>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END LIGNE 1 -->

                                        <div class="portlet box green-haze">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-edit"></i>Caractéristiques Établissement / Service
                                                </div>
                                                <div class="tools">
                                                    <a title="" data-original-title="" href="javascript:;"
                                                       class="collapse"> </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">

                                                <!-- START LIGNE 4 -->
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <div class="form-group form-md-checkboxes"
                                                             style="padding-top: 0px !important;">
                                                            <label>Dispositif</label>
                                                            <div class="checkbox checkbox-primary">
                                                                <input id="ccDISP" class="styled" type="checkbox">
                                                                <label for="ccDISP"></label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group form-md-line-input">
                                                            <select id="zlSecteur" class="form-control">

                                                            </select>
                                                            <label for="zlSecteur">Secteur
                                                                <span class="font-red">*</span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group form-md-line-input">
                                                            <select id="zlSecteurSMS" class="form-control">

                                                            </select>
                                                            <label for="zlSecteurSMS">Secteur SMS
                                                                <span class="font-red">*</span></label>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-2 display-none section-handicap">
                                                        <div class="form-group form-md-checkboxes"
                                                             style="padding-top: 0px !important;">
                                                            <label>Section Handicap</label>
                                                            <div class="checkbox checkbox-primary">
                                                                <input id="ccHANDICAP" class="styled" type="checkbox">
                                                                <label for="ccHANDICAP"></label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- END LIGNE 3 -->

                                                <!-- START LIGNE 3 -->
                                                <div class="row">

                                                    <div class="col-md-4">
                                                        <div class="form-group form-md-line-input">
                                                            <select id="zlTypeAge" class="form-control">

                                                            </select>
                                                            <label for="zlTypeAge">Tranche d'Age
                                                                <span class="font-red">*</span></label>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group form-md-line-input">
                                                            <select id="zlTypeEtabl" class="form-control">

                                                            </select>
                                                            <label for="zlTypeEtabl">Type Établissement / Service
                                                                <span class="font-red">*</span></label>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <br/>
                                                        <button id="btnAddModalite" class="btn btn-success"
                                                                type="button">+ Modalités
                                                        </button>
                                                    </div>

                                                </div>

                                                <div class="row" id="divDetail" style="margin-top: 15px;">
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                            <input class="form-control text-uppercase"
                                                                   id="ztArrete" type="text">
                                                            <label for="ztArrete">Arrêté Préf.</label>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                            <input class="form-control date-picker"
                                                                   id="ztDateArrete" readonly type="text">
                                                            <label for="ztDateArrete">Date Arrêté</label>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                            <input class="form-control text-uppercase" id="ztAge"
                                                                   type="text">
                                                            <label for="ztAge">Age</label>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                            <input class="form-control text-uppercase" id="ztFINESS"
                                                                   type="text">
                                                            <label for="ztFINESS">FINESS</label>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <div class="form-group form-md-checkboxes">
                                                            <label>Projet </label>
                                                            <div class="checkbox checkbox-primary">
                                                                <input id="ccPROJ" class="styled" type="checkbox">
                                                                <label for="ccPROJ"></label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                            <input class="form-control date-picker"
                                                                   id="ztDateOpen" readonly
                                                                   type="text">
                                                            <label for="ztDateOpen">Date Ouverture</label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                            <input class="form-control text-uppercase"
                                                                   id="ztNoteAccueilPublic"
                                                                   type="text">
                                                            <label for="ztNoteTypePublic">Notes Complémentaires sur
                                                                l'Accueil Public</label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                            <input class="form-control text-uppercase"
                                                                   id="ztNbPlacesET"
                                                                   type="text">
                                                            <label for="ztNbPlacesET">Nb Total Places</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <div class="alert alert-danger">
                                                            <strong>Attention !</strong> Le nombre total de places
                                                            correspond à la somme des places par modalités d’accueil. Il
                                                            peut aussi être saisi en cas de connaissance du nombre total
                                                            de places dans un ESMS sans détail des modalités.
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <!-- START LIGNE 2 -->
                                        <div class="row">
                                            <div class="col-md-10">
                                                <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                    <input class="form-control text-uppercase" id="ztAdr1"
                                                           type="text">
                                                    <label for="ztAdr1">Adresse 1</label>
                                                </div>
                                            </div>

                                        </div>
                                        <!-- END LIGNE 2 -->

                                        <!-- START LIGNE 3 -->
                                        <div class="row">
                                            <div class="col-md-10">
                                                <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                    <input class="form-control text-uppercase" id="ztAdr2"
                                                           type="text">
                                                    <label for="ztAdr2">Adresse 2</label>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END LIGNE 3 -->

                                        <!-- START LIGNE 4 -->
                                        <div class="row">
                                            <div class="col-md-10">
                                                <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                    <input class="form-control text-uppercase" id="ztAdr3"
                                                           type="text">
                                                    <label for="ztAdr3">Adresse 3</label>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END LIGNE 4 -->

                                        <!-- START LIGNE 5 -->
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                    <input class="form-control text-uppercase required" id="ztCP"
                                                           type="text" data-inputmask="'mask': '9{5}'">
                                                    <label for="ztCP">CP <span
                                                                class="font-red">*</span></label>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                    <input class="form-control text-uppercase required" id="ztVille"
                                                           type="text">
                                                    <label for="ztVille">Ville <span
                                                                class="font-red">*</span></label>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group form-md-line-input">
                                                    <select id="zlPays" class="form-control">

                                                    </select>
                                                    <label for="zlPays">Pays</label>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->
                        </div>
                        <div class="col-md-4 ">

                            <div class="form-group form-md-line-input">
                                <select id="zlPole" class="form-control">

                                </select>
                                <label for="zlPole">Rattacher à un Pôle</label>
                            </div>

                            <div class="form-group form-md-line-input form-md-floating-label has-success">
                                <input class="form-control" id="ztWEB" type="text">
                                <label for="ztWEB">Site Internet</label>
                            </div>

                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box red" id="portletTel">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-phone"></i>Téléphones / Fax
                                    </div>
                                    <div class="tools">
                                        <a title="" data-original-title="Blublu" href="javascript:;"
                                           class="expand"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body portlet-collapsed">

                                    <div class="row">

                                        <div class="col-md-4">
                                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                                <input class="form-control text-uppercase" id="ztSaisie"
                                                       placeholder="Numéro" type="text">
                                                <label for="ztSaisie">Numéro <span class="font-red">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                                <select id="zlTypeSaisie" class="form-control"></select>
                                                <label for="zlTypeSaisie">Type <span class="font-red">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding-top: 20px">
                                            <button id="btnAddTel" class="btn btn-success" type="button">
                                                <i class="fa fa-plus-circle"></i>
                                            </button>
                                            <label for="btnAddTel"></label>
                                        </div>


                                    </div>

                                    <div class="row" style="padding-left: 5px;padding-right: 5px;">
                                        <table data-toggle="table"
                                               class="table table-striped table-bordered table-hover order-column"
                                               id="tableTel">
                                            <thead>
                                            <tr>
                                                <th class="text-center"> Téléphone</th>
                                                <th class="text-center"> Type</th>
                                                <th class="text-center"> Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->

                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box red" id="portletMail">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-send"></i>Mails
                                    </div>
                                    <div class="tools">
                                        <a title="" data-original-title="" href="javascript:;" class="expand"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body portlet-collapsed">

                                    <div class="row">


                                        <div class="col-md-6">
                                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                                <input class="form-control" id="ztSaisie" placeholder="Mail"
                                                       type="text">
                                                <label for="ztSaisie">Adresse mail <span
                                                            class="font-red">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                                <select id="zlTypeSaisie" class="form-control"></select>
                                                <label for="zlTypeSaisie">Type <span class="font-red">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding-top: 20px">
                                            <button id="btnAddMail" class="btn red" type="button">
                                                <i class="fa fa-plus-circle"></i>
                                            </button>
                                            <label for="btnAddMail"></label>
                                        </div>


                                    </div>

                                    <div class="row" style="padding-left: 5px;padding-right: 5px;">

                                        <table data-toggle="table"
                                               class="table table-striped table-bordered table-hover order-column"
                                               id="tableMail">
                                            <thead>
                                            <tr>
                                                <th data-halign="center" data-align="left"> Mail</th>
                                                <th data-halign="center" data-align="left"> Type</th>
                                                <th data-halign="center" data-align="right"> Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->

                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box red" id="portleAccesAnnuaire">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gear"></i>Accès Annuaire
                                    </div>
                                    <div class="tools">
                                        <a title="" data-original-title="" href="javascript:;" class="expand"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body portlet-collapsed">

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Actif </label>
                                                <div class="checkbox checkbox-success">
                                                    <input id="ccACTIF" class="styled" type="checkbox">
                                                    <label for="ccACTIF"></label>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Visible </label>
                                                <div class="checkbox checkbox-success">
                                                    <input id="ccVISIBLE" class="styled" type="checkbox">
                                                    <label for="ccVISIBLE"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Accès Annuaire </label>
                                                <div class="md-checkbox-inline">
                                                    <div class="checkbox checkbox-success">
                                                        <input id="ccACCES" class="styled" type="checkbox">
                                                        <label for="ccACCES"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->

                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box blue-dark" id="portleOrganisation">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-caret-square-o-down"></i>Organigramme Gestionnaire
                                    </div>
                                    <div class="tools">
                                        <a title="" data-original-title="" href="javascript:;" class="expand"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body portlet-collapsed">


                                    <div id="tree_1" class="tree-demo jstree jstree-1 jstree-default" role="tree"
                                         aria-multiselectable="true" aria-activedescendant="j1_1"
                                         aria-busy="false">
                                    </div>


                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->

<<<<<<< HEAD
                            <div class="alert alert-info complement-footer note" style="left: 10px;">
=======
                            <div class="alert alert-info historique note" style="left: 10px;">
>>>>>>> debug8
                            </div>

                        </div>
                    </div>
                    <!-- END DONNEES DE BASE + TEL + MAILS + ACCESS + ORGANIGRAMME -->

                    <!-- START DONNEES COMPLEMENTAIRES + NOTES COMPLEMENTAIRES -->
                    <div class="row">

                        <div class="col-md-8 ">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-database"></i>Données Complémentaires
                                    </div>
                                    <div class="tools">
                                    </div>
                                </div>
                                <div class="portlet-body">

                                    <!-- START Ligne 1 -->
                                    <div class="row">

                                        <div class="col-md-2">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Inscription BI</label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccBI" class="styled" type="checkbox">
                                                    <label for="ccBI"></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group form-md-line-input" style="margin-top: 5px;">
                                                <select id="zlReceptionBI" class="form-control">

                                                </select>
                                                <label for="zlReceptionBI">Réception BI</label>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group form-md-checkboxes">
                                                <label>BI Payant</label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccBI_PAYANT" class="styled" type="checkbox">
                                                    <label for="ccBI_PAYANT"></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Cat. Formation</label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccCF" class="styled" type="checkbox">
                                                    <label for="ccCF"></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group form-md-line-input" style="margin-top: 5px;">
                                                <select id="zlReceptionCF" class="form-control">

                                                </select>
                                                <label for="zlReceptionCF">Réception CF</label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Ligne 1 -->

                                    <div class="row">

                                        <div class="col-md-2">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Newsletter </label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccNL" class="styled" type="checkbox">
                                                    <label for="ccNL"></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Contribuant</label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccCONT" class="styled" type="checkbox">
                                                    <label for="ccCONT"></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group form-md-line-input">
                                                <input class="form-control text-uppercase"
                                                       id="ztCodeInterne"
                                                       placeholder="Code Interne" type="text">
                                                <label for="ztCodeInterne">Code Interne </label>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->
                        </div>
                        <div class="col-md-4 ">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box green-sharp">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-sticky-note-o"></i>Notes Complémentaires
                                    </div>
                                    <div class="tools">
                                    </div>
                                </div>
                                <div class="portlet-body">

                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="form-group form-md-line-input">
                                            <textarea id="ztNotes" style="resize: none !important"
                                                      class="form-control" placeholder="Saisir vos notes ici"
                                                      rows="5"></textarea>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->
                        </div>

                    </div>
                    <!-- END DONNEES COMPLEMENTAIRES + NOTES COMPLEMENTAIRES -->

                    <!-- START OFFRES EMPLOI -->
                    <div class="row">
                        <div class="col-md-12 ">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box red-flamingo">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-database"></i>Offres d'Emploi
                                    </div>
                                    <div class="tools">
                                    </div>
                                </div>
                                <div class="portlet-body">

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END OFFRES EMPLOI -->
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnCopieData" class="btn red-mint btn-outline">Recopier Organisme</button>
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fermer</button>
                    <button type="button" class="btn" id="btnAction"></button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal  ETABLISSEMENT -->

    <!-- START Modal ANTENNE -->
    <div id="formAntenne" style="display: none;">
        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
        <div class="modal-dialog modal-full">
            <div class="modal-content">
                <div class="modal-header caption">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title bold uppercase"></h4>
                </div>
<<<<<<< HEAD
                <div class="bg-grey ariane">
=======
                <div class="bg-grey ariane-title">
>>>>>>> debug8
                    <div id="prec" class="col-md-12">
                    </div>
                </div>
                <div class="modal-body ariane-body">
                    <!-- START DONNEES DE BASE + TEL + MAILS + ACCESS + ORGANIGRAMME -->
                    <div class="row">
                        <div class="col-md-8">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box green-meadow">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-edit"></i>Données de base
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="form-body">


                                        <!-- START LIGNE 1 -->
                                        <div class="row">

                                            <div class="col-md-10">
                                                <div class="form-group form-md-line-input">
                                                    <input class="form-control required text-uppercase" id="ztNom"
                                                           placeholder="Nom" type="text">
                                                    <label for="ztNom">Saisir le nom <span
                                                                class="font-red">*</span></label>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END LIGNE 1 -->

                                        <div class="portlet box green-haze">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-edit"></i>Caractéristiques Antenne
                                                </div>
                                                <div class="tools">
                                                    <a title="" data-original-title="" href="javascript:;"
                                                       class="collapse"> </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group form-md-line-input">
                                                            <select id="zlModaliteAccueil" class="form-control"
                                                                    multiple>
                                                            </select>
                                                            <label for="zlModaliteAccueil">Modalités d'Accueil
                                                                <span class="font-red">*</span></label>
                                                        </div>
                                                    </div>

                                                </div>
                                                <!-- END LIGNE 3 -->
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <div class="form-group form-md-line-input">
                                                            <input class="form-control text-uppercase"
                                                                   id="ztArrete"
                                                                   placeholder="Arrêté" type="text">
                                                            <label for="ztArrete">Arrêté Préf.</label>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <div class="form-group form-md-line-input">
                                                            <input class="form-control date-picker"
                                                                   id="ztDateArrete" readonly
                                                                   placeholder="Date" type="text">
                                                            <label for="ztDateArrete">Date Arrêté</label>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group form-md-line-input">
                                                            <input class="form-control text-uppercase" id="ztFINESS"
                                                                   placeholder="FINESS" type="text">
                                                            <label for="ztFINESS">Saisir le FINESS</label>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <div class="form-group form-md-line-input">
                                                            <input class="form-control date-picker"
                                                                   id="ztDateOpen" readonly
                                                                   placeholder="Date" type="text">
                                                            <label for="ztDateOpen">Date Ouverture</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- START LIGNE 2 -->
                                        <div class="row">
                                            <div class="col-md-10">
                                                <div class="form-group form-md-line-input ">
                                                    <input class="form-control text-uppercase" id="ztAdr1"
                                                           placeholder="Adresse 1" type="text">
                                                    <label for="ztAdr1">Saisir l'adresse 1</label>
                                                </div>
                                            </div>

                                        </div>
                                        <!-- END LIGNE 2 -->

                                        <!-- START LIGNE 3 -->
                                        <div class="row">
                                            <div class="col-md-10">
                                                <div class="form-group form-md-line-input">
                                                    <input class="form-control text-uppercase" id="ztAdr2"
                                                           placeholder="Adresse 2" type="text">
                                                    <label for="ztAdr2">Saisir l'adresse 2</label>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END LIGNE 3 -->

                                        <!-- START LIGNE 4 -->
                                        <div class="row">
                                            <div class="col-md-10">
                                                <div class="form-group form-md-line-input">
                                                    <input class="form-control text-uppercase" id="ztAdr3"
                                                           placeholder="Adresse 3" type="text">
                                                    <label for="ztAdr3">Saisir l'adresse 3</label>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END LIGNE 4 -->

                                        <!-- START LIGNE 5 -->
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group form-md-line-input">
                                                    <input class="form-control text-uppercase required" id="ztCP"
                                                           type="text" data-inputmask="'mask': '9{5}'">
                                                    <label for="ztCP">Saisir le CP <span
                                                                class="font-red">*</span></label>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group form-md-line-input">
                                                    <input class="form-control text-uppercase required" id="ztVille"
                                                           placeholder="Ville" type="text">
                                                    <label for="ztVille">Saisir la Ville <span
                                                                class="font-red">*</span></label>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group form-md-line-input">
                                                    <select id="zlPays" class="form-control">

                                                    </select>
                                                    <label for="zlPays">Pays</label>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->
                        </div>
                        <div class="col-md-4 ">

                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box red" id="portletTel">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-phone"></i>Téléphones / Fax
                                    </div>
                                    <div class="tools">
                                        <a title="" data-original-title="Blublu" href="javascript:;"
                                           class="expand"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body portlet-collapsed">

                                    <div class="row">

                                        <div class="col-md-4">
                                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                                <input class="form-control text-uppercase" id="ztSaisie"
                                                       placeholder="Numéro" type="text">
                                                <label for="ztSaisie">Numéro <span class="font-red">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                                <select id="zlTypeSaisie" class="form-control"></select>
                                                <label for="zlTypeSaisie">Type <span class="font-red">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding-top: 20px">
                                            <button id="btnAddTel" class="btn btn-success" type="button">
                                                <i class="fa fa-plus-circle"></i>
                                            </button>
                                            <label for="btnAddTel"></label>
                                        </div>


                                    </div>

                                    <div class="row" style="padding-left: 5px;padding-right: 5px;">
                                        <table data-toggle="table"
                                               class="table table-striped table-bordered table-hover order-column"
                                               id="tableTel">
                                            <thead>
                                            <tr>
                                                <th class="text-center"> Téléphone</th>
                                                <th class="text-center"> Type</th>
                                                <th class="text-center"> Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->

                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box red" id="portletMail">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-send"></i>Mails
                                    </div>
                                    <div class="tools">
                                        <a title="" data-original-title="" href="javascript:;" class="expand"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body portlet-collapsed">

                                    <div class="row">


                                        <div class="col-md-6">
                                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                                <input class="form-control" id="ztSaisie" placeholder="Mail"
                                                       type="text">
                                                <label for="ztSaisie">Adresse mail <span
                                                            class="font-red">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                                <select id="zlTypeSaisie" class="form-control"></select>
                                                <label for="zlTypeSaisie">Type <span class="font-red">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding-top: 20px">
                                            <button id="btnAddMail" class="btn red" type="button">
                                                <i class="fa fa-plus-circle"></i>
                                            </button>
                                            <label for="btnAddMail"></label>
                                        </div>


                                    </div>

                                    <div class="row" style="padding-left: 5px;padding-right: 5px;">

                                        <table data-toggle="table"
                                               class="table table-striped table-bordered table-hover order-column"
                                               id="tableMail">
                                            <thead>
                                            <tr>
                                                <th data-halign="center" data-align="left"> Mail</th>
                                                <th data-halign="center" data-align="left"> Type</th>
                                                <th data-halign="center" data-align="right"> Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->

                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box red" id="portleAccesAnnuaire">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gear"></i>Accès Annuaire
                                    </div>
                                    <div class="tools">
                                        <a title="" data-original-title="" href="javascript:;" class="expand"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body portlet-collapsed">

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Actif </label>
                                                <div class="checkbox checkbox-success">
                                                    <input id="ccACTIF" class="styled" type="checkbox">
                                                    <label for="ccACTIF"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Visible </label>
                                                <div class="checkbox checkbox-success">
                                                    <input id="ccVISIBLE" class="styled" type="checkbox">
                                                    <label for="ccVISIBLE"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Accès Annuaire </label>
                                                <div class="md-checkbox-inline">
                                                    <div class="checkbox checkbox-success">
                                                        <input id="ccACCES" class="styled" type="checkbox">
                                                        <label for="ccACCES"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->

<<<<<<< HEAD
                            <div class="alert alert-info complement-footer note" style="left: 10px;">
=======
                            <div class="alert alert-info historique note" style="left: 10px;">
>>>>>>> debug8
                            </div>

                        </div>
                    </div>
                    <!-- END DONNEES DE BASE + TEL + MAILS + ACCESS + ORGANIGRAMME -->

                    <!-- START DONNEES COMPLEMENTAIRES + NOTES COMPLEMENTAIRES -->
                    <div class="row">

                        <div class="col-md-8 ">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-database"></i>Données Complémentaires
                                    </div>
                                    <div class="tools">
                                    </div>
                                </div>
                                <div class="portlet-body">

                                    <div class="row">

                                        <div class="col-md-2">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Contribuant</label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccCONT" class="styled" type="checkbox">
                                                    <label for="ccCONT"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->
                        </div>
                        <div class="col-md-4 ">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box green-sharp">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-sticky-note-o"></i>Notes Complémentaires
                                    </div>
                                    <div class="tools">
                                    </div>
                                </div>
                                <div class="portlet-body">

                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="form-group form-md-line-input">
                                            <textarea id="ztNotes" style="resize: none !important"
                                                      class="form-control" placeholder="Saisir vos notes ici"
                                                      rows="5"></textarea>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->
                        </div>

                    </div>
                    <!-- END DONNEES COMPLEMENTAIRES + NOTES COMPLEMENTAIRES -->

                    <!-- START OFFRES EMPLOI -->
                    <div class="row">
                        <div class="col-md-12 ">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box red-flamingo">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-database"></i>Offres d'Emploi
                                    </div>
                                    <div class="tools">
                                    </div>
                                </div>
                                <div class="portlet-body">

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END OFFRES EMPLOI -->
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnCopieData" class="btn red-mint btn-outline">Recopier Organisme</button>
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fermer</button>
                    <button type="button" class="btn" id="btnAction"></button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal  ANTENNE -->

    <!-- START Modal PERSONNEL -->
    <div id="formPersonnel" style="display: none;">
        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
        <div class="modal-dialog modal-full">
            <div class="modal-content">
                <div class="modal-header caption">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title bold uppercase"></h4>
                </div>
<<<<<<< HEAD
                <div class="bg-grey ariane">
=======
                <div class="bg-grey ariane-title">
>>>>>>> debug8
                    <div id="prec" class="col-md-12">
                    </div>
                </div>
                <div class="modal-body ariane-body">
<<<<<<< HEAD

=======
>>>>>>> debug8
                    <!-- START DONNEES DE BASE + TEL + MAILS + ACCESS + ORGANIGRAMME -->
                    <div class="row">
                        <div class="col-md-8">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box green-meadow">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-edit"></i>Données de base
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="form-body">


                                        <!-- START LIGNE 1 -->
                                        <div class="row">

                                            <div class="col-md-2">
                                                <div class="form-group form-md-line-input">
                                                    <select id="zlCivilite" class="form-control">
                                                    </select>
                                                    <label for="zlCivilite">Civilité</label>
                                                </div>
                                            </div>

                                            <div class="col-md-5">
                                                <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                    <input class="form-control required text-uppercase" id="ztNom"
                                                           type="text">
                                                    <label for="ztNom">Nom<span class="font-red">*</span></label>
                                                </div>
                                            </div>

                                            <div class="col-md-5">
                                                <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                    <input class="form-control required text-uppercase" id="ztPrenom"
                                                           type="text">
                                                    <label for="ztPrenom">Prénom<span class="font-red">*</span></label>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END LIGNE 1 -->

                                        <div class="portlet box green-haze">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-edit"></i>Caractéristiques Personnel
                                                </div>
                                                <div class="tools">
                                                    <a title="" data-original-title="" href="javascript:;"
                                                       class="collapse"> </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group form-md-line-input">
                                                            <select id="zlFonction" class="form-control">
                                                            </select>
                                                            <label for="zlFonction">Fonction</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                            <input class="form-control text-uppercase"
                                                                   id="ztComplementFonction" type="text">
                                                            <label for="ztArrete">Complément Fonction.</label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                            <input class="form-control text-uppercase "
                                                                   id="ztService" type="text">
                                                            <label for="ztService">Service</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- START LIGNE 2 -->
                                        <div class="row">
                                            <div class="col-md-10">
                                                <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                    <input class="form-control text-uppercase" id="ztAdr1" type="text">
                                                    <label for="ztAdr1">Adresse 1</label>
                                                </div>
                                            </div>

                                        </div>
                                        <!-- END LIGNE 2 -->

                                        <!-- START LIGNE 3 -->
                                        <div class="row">
                                            <div class="col-md-10">
                                                <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                    <input class="form-control text-uppercase" id="ztAdr2" type="text">
                                                    <label for="ztAdr2">Adresse 2</label>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END LIGNE 3 -->

                                        <!-- START LIGNE 4 -->
                                        <div class="row">
                                            <div class="col-md-10">
                                                <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                    <input class="form-control text-uppercase" id="ztAdr3" type="text">
                                                    <label for="ztAdr3">Adresse 3</label>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END LIGNE 4 -->

                                        <!-- START LIGNE 5 -->
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                    <input class="form-control text-uppercase required" id="ztCP"
                                                           type="text" data-inputmask="'mask': '9{5}'">
                                                    <label for="ztCP">CP <span class="font-red">*</span></label>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                    <input class="form-control text-uppercase required" id="ztVille"
                                                           type="text">
                                                    <label for="ztVille">Ville <span class="font-red">*</span></label>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group form-md-line-input">
                                                    <select id="zlPays" class="form-control">

                                                    </select>
                                                    <label for="zlPays">Pays</label>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->
                        </div>
                        <div class="col-md-4 ">

                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box red" id="portletTel">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-phone"></i>Téléphones / Fax
                                    </div>
                                    <div class="tools">
                                        <a title="" data-original-title="Blublu" href="javascript:;"
                                           class="expand"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body portlet-collapsed">

                                    <div class="row">

                                        <div class="col-md-4">
                                            <div class="form-group form-md-line-input form-md-floating-label has-success"
                                                 style="margin-bottom: 15px;">
                                                <input class="form-control text-uppercase" id="ztSaisie" type="text">
                                                <label for="ztSaisie">Numéro <span class="font-red">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                                <select id="zlTypeSaisie" class="form-control"></select>
                                                <label for="zlTypeSaisie">Type <span class="font-red">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding-top: 20px">
                                            <button id="btnAddTel" class="btn btn-success" type="button">
                                                <i class="fa fa-plus-circle"></i>
                                            </button>
                                            <label for="btnAddTel"></label>
                                        </div>


                                    </div>

                                    <div class="row" style="padding-left: 5px;padding-right: 5px;">
                                        <table data-toggle="table"
                                               class="table table-striped table-bordered table-hover order-column"
                                               id="tableTel">
                                            <thead>
                                            <tr>
                                                <th class="text-center"> Téléphone</th>
                                                <th class="text-center"> Type</th>
                                                <th class="text-center"> Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->

                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box red" id="portletMail">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-send"></i>Mails
                                    </div>
                                    <div class="tools">
                                        <a title="" data-original-title="" href="javascript:;" class="expand"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body portlet-collapsed">

                                    <div class="row">


                                        <div class="col-md-6">
                                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                                <input class="form-control" id="ztSaisie" placeholder="Mail"
                                                       type="text">
                                                <label for="ztSaisie">Adresse mail <span
                                                            class="font-red">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                                <select id="zlTypeSaisie" class="form-control"></select>
                                                <label for="zlTypeSaisie">Type <span class="font-red">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding-top: 20px">
                                            <button id="btnAddMail" class="btn red" type="button">
                                                <i class="fa fa-plus-circle"></i>
                                            </button>
                                            <label for="btnAddMail"></label>
                                        </div>


                                    </div>

                                    <div class="row" style="padding-left: 5px;padding-right: 5px;">

                                        <table data-toggle="table"
                                               class="table table-striped table-bordered table-hover order-column"
                                               id="tableMail">
                                            <thead>
                                            <tr>
                                                <th data-halign="center" data-align="left"> Mail</th>
                                                <th data-halign="center" data-align="left"> Type</th>
                                                <th data-halign="center" data-align="right"> Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->

                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box red" id="portleAccesAnnuaire">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gear"></i>Accès Annuaire
                                    </div>
                                    <div class="tools">
                                        <a title="" data-original-title="" href="javascript:;" class="expand"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body portlet-collapsed">

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Actif </label>
                                                <div class="checkbox checkbox-success">
                                                    <input id="ccACTIF" class="styled" type="checkbox">
                                                    <label for="ccACTIF"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Visible </label>
                                                <div class="checkbox checkbox-success">
                                                    <input id="ccVISIBLE" class="styled" type="checkbox">
                                                    <label for="ccVISIBLE"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Accès Annuaire </label>
                                                <div class="md-checkbox-inline">
                                                    <div class="checkbox checkbox-success">
                                                        <input id="ccACCES" class="styled" type="checkbox">
                                                        <label for="ccACCES"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->

<<<<<<< HEAD
                            <div class="alert alert-info complement-footer note" style="left: 10px;">
=======
                            <div class="alert alert-info historique note" style="left: 10px;">
>>>>>>> debug8
                            </div>

                        </div>
                    </div>
                    <!-- END DONNEES DE BASE + TEL + MAILS + ACCESS + ORGANIGRAMME -->

                    <!-- START DONNEES COMPLEMENTAIRES + NOTES COMPLEMENTAIRES -->
                    <div class="row">

                        <div class="col-md-8 ">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-database"></i>Données Complémentaires
                                    </div>
                                    <div class="tools">
                                    </div>
                                </div>
                                <div class="portlet-body">

                                    <div class="row">

                                        <div class="col-md-2">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Voeux</label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccVOEUX" class="styled" type="checkbox">
                                                    <label for="ccVOEUX"></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Cotisant</label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccCOTIS" class="styled" type="checkbox">
                                                    <label for="ccCOTIS"></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Inscription BI</label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccBI" class="styled" type="checkbox">
                                                    <label for="ccBI"></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group form-md-line-input" style="margin-top: 5px;">
                                                <select id="zlReceptionBI" class="form-control">

                                                </select>
                                                <label for="zlReceptionBI">Réception BI</label>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Newsletter</label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccNL" class="styled" type="checkbox">
                                                    <label for="ccNL"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Cat. Formation</label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccCF" class="styled" type="checkbox">
                                                    <label for="ccCF"></label>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-4">
                                            <div class="form-group form-md-line-input" style="margin-top: 5px;">
                                                <select id="zlReceptionCF" class="form-control">

                                                </select>
                                                <label for="zlReceptionCF">Réception CF</label>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Participation AG</label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccAG" class="styled" type="checkbox">
                                                    <label for="ccAG"></label>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-4">
                                            <div class="form-group form-md-line-input" style="margin-top: 5px;">
                                                <select id="zlRoleAG" class="form-control">

                                                </select>
                                                <label for="zlRoleAG">Rôle AG</label>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-md-2">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Bureau</label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccBURO" class="styled" type="checkbox">
                                                    <label for="ccBURO"></label>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-4">
                                            <div class="form-group form-md-line-input" style="margin-top: 5px;">
                                                <select id="zlRoleBURO" class="form-control">

                                                </select>
                                                <label for="zlRoleBURO">Rôle Bureau</label>
                                            </div>
                                        </div>


                                        <div class="col-md-2">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Participation CA</label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccCA" class="styled" type="checkbox">
                                                    <label for="ccCA"></label>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-4">
                                            <div class="form-group form-md-line-input" style="margin-top: 5px;">
                                                <select id="zlRoleCA" class="form-control">

                                                </select>
                                                <label for="zlRoleCA">Rôle CA</label>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="row">

                                        <div class="col-md-4">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Comité Téchnique Consultatif</label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccCTC" class="styled" type="checkbox">
                                                    <label for="ccCTC"></label>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-4">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Correspondant Commission</label>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="ccCC" class="styled" type="checkbox">
                                                    <label for="ccCC"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->
                        </div>
                        <div class="col-md-4 ">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box green-sharp">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-sticky-note-o"></i>Notes Complémentaires
                                    </div>
                                    <div class="tools">
                                    </div>
                                </div>
                                <div class="portlet-body">

                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="form-group form-md-line-input">
                                            <textarea id="ztNotes" style="resize: none !important"
                                                      class="form-control" placeholder="Saisir vos notes ici"
                                                      rows="5"></textarea>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->
                        </div>

                    </div>
                    <!-- END DONNEES COMPLEMENTAIRES + NOTES COMPLEMENTAIRES -->

                    <!-- START OFFRES EMPLOI -->
                    <div class="row">
                        <div class="col-md-12 ">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box blue-dark ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-group"></i>Participation aux Activités
                                    </div>
                                    <div class="tools">
                                    </div>
                                </div>
                                <div class="portlet-body">

                                    <div class="row">
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- END OFFRES EMPLOI -->
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnCopieData" class="btn red-mint btn-outline">Recopier Organisme</button>
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fermer</button>
                    <button type="button" class="btn" id="btnAction"></button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal  PERSONNEL -->


    <!-- START Modal ADRESSES -->
    <div id="formAdr" style="display: none;">
        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
        <div class="modal-dialog modal-full">
            <div class="modal-content">
                <div class="modal-header caption">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title bold uppercase"></h4>
                </div>
<<<<<<< HEAD
                <div class="bg-grey ariane">
=======
                <div class="bg-grey ariane-title">
>>>>>>> debug8
                    <div id="prec" class="col-md-12">
                    </div>
                </div>
                <div class="modal-body ariane-body">
<<<<<<< HEAD

=======
>>>>>>> debug8
                    <div class="row">
                        <div class="col-md-8">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box green-meadow">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-edit"></i>Données de base
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <form role="form">
                                        <div class="form-body">

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control text-uppercase required" id="ztNom"
                                                               type="text">
                                                        <label for="ztNom">Nom<span
                                                                    class="font-red">*</span></label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-group form-md-line-input">
                                                        <select id="zlTypePublic" class="form-control">

                                                        </select>
                                                        <label for="zlTypePublic">Type Public <span
                                                                    class="font-red">*</span></label>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 display-none">
                                                    <div class="form-group form-md-line-input">
                                                        <select id="zlDetailTypePublic" class="form-control">

                                                        </select>
                                                        <label for="zlDetailTypePublic">Détail Type Public </label>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">

                                                <div class="col-md-2">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control" id="ztNbPlaces"
                                                               type="number">
                                                        <label for="ztNbPlaces">Nb Places</label>
                                                    </div>
                                                </div>

                                                <div class="col-md-2">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control" id="ztAge"
                                                               type="text">
                                                        <label for="ztAge">Age</label>
                                                    </div>
                                                </div>

                                            </div>
                                            <!-- END LIGNE 1 -->

                                            <!-- START LIGNE 2 -->
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control text-uppercase"
                                                               id="ztAdr1" type="text">
                                                        <label for="ztAdr1">Adresse 1</label>
                                                    </div>
                                                </div>

                                            </div>
                                            <!-- END LIGNE 2 -->

                                            <!-- START LIGNE 3 -->
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control text-uppercase" id="ztAdr2"
                                                               type="text">
                                                        <label for="ztAdr2">Adresse 2</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END LIGNE 3 -->

                                            <!-- START LIGNE 4 -->
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control text-uppercase" id="ztAdr3"
                                                               type="text">
                                                        <label for="ztAdr3">Adresse 3</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END LIGNE 4 -->

                                            <!-- START LIGNE 5 -->
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control text-uppercase required" id="ztCP"
                                                               type="text" data-inputmask="'mask': '9{5}'">
                                                        <label for="ztCP">CP <span
                                                                    class="font-red">*</span></label>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group form-md-line-input form-md-floating-label has-success">
                                                        <input class="form-control text-uppercase required" id="ztVille"
                                                               type="text">
                                                        <label for="ztVille">Saisir la Ville <span
                                                                    class="font-red">*</span></label>
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group form-md-line-input">
                                                        <select id="zlPays" class="form-control">

                                                        </select>
                                                        <label for="zlPays">Pays</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- START LIGNE 5 -->

                                        </div>

                                    </form>

                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->
                        </div>

                        <div class="col-md-4 ">

                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box red" id="portletTel">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-phone"></i>Téléphones / Fax
                                    </div>
                                    <div class="tools">
                                        <a title="" data-original-title="Blublu" href="javascript:;"
                                           class="expand"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body portlet-collapsed">

                                    <div class="row">


                                        <div class="col-md-4">
                                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                                <input class="form-control text-uppercase" id="ztSaisie"
                                                       placeholder="Numéro" type="text">
                                                <label for="ztSaisie">Numéro <span class="font-red">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                                <select id="zlTypeSaisie" class="form-control"></select>
                                                <label for="zlTypeSaisie">Type <span class="font-red">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding-top: 20px">
                                            <button id="btnAddTel" class="btn btn-success" type="button">
                                                <i class="fa fa-plus-circle"></i>
                                            </button>
                                            <label for="btnAddTel"></label>
                                        </div>


                                    </div>

                                    <div class="row" style="padding-left: 5px;padding-right: 5px;">
                                        <table data-toggle="table"
                                               class="table table-striped table-bordered table-hover order-column"
                                               id="tableTel">
                                            <thead>
                                            <tr>
                                                <th class="text-center"> Téléphone</th>
                                                <th class="text-center"> Type</th>
                                                <th class="text-center"> Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->

                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box red" id="portletMail">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-send"></i>Mails
                                    </div>
                                    <div class="tools">
                                        <a title="" data-original-title="" href="javascript:;" class="expand"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body portlet-collapsed">

                                    <div class="row">


                                        <div class="col-md-6">
                                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                                <input class="form-control" id="ztSaisie" placeholder="Mail"
                                                       type="text">
                                                <label for="ztSaisie">Adresse mail <span
                                                            class="font-red">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group form-md-line-input" style="margin-bottom: 15px;">
                                                <select id="zlTypeSaisie" class="form-control"></select>
                                                <label for="zlTypeSaisie">Type <span class="font-red">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding-top: 20px">
                                            <button id="btnAddMail" class="btn red" type="button">
                                                <i class="fa fa-plus-circle"></i>
                                            </button>
                                            <label for="btnAddMail"></label>
                                        </div>


                                    </div>

                                    <div class="row" style="padding-left: 5px;padding-right: 5px;">

                                        <table data-toggle="table"
                                               class="table table-striped table-bordered table-hover order-column"
                                               id="tableMail">
                                            <thead>
                                            <tr>
                                                <th data-halign="center" data-align="left"> Mail</th>
                                                <th data-halign="center" data-align="left"> Type</th>
                                                <th data-halign="center" data-align="right"> Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->

                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box red" id="portleAccesAnnuaire">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gear"></i>Accès Annuaire
                                    </div>
                                    <div class="tools">
                                        <a title="" data-original-title="" href="javascript:;" class="expand"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body portlet-collapsed">

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Actif </label>
                                                <div class="checkbox checkbox-success">
                                                    <input id="ccACTIF" class="styled" type="checkbox">
                                                    <label for="ccACTIF"></label>
                                                </div>
                                            </div>
                                        </div>
<<<<<<< HEAD
=======
                                        <div class="col-md-4">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Visible </label>
                                                <div class="checkbox checkbox-success">
                                                    <input id="ccVISIBLE" class="styled" type="checkbox">
                                                    <label for="ccVISIBLE"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group form-md-checkboxes">
                                                <label>Accès Annuaire </label>
                                                <div class="md-checkbox-inline">
                                                    <div class="checkbox checkbox-success">
                                                        <input id="ccACCES" class="styled" type="checkbox">
                                                        <label for="ccACCES"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
>>>>>>> debug8
                                    </div>
                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->

                            <div class="alert alert-info complement-footer note" style="left: 10px;">
                            </div>

                            <div class="portlet box green-sharp">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-sticky-note-o"></i>Notes Complémentaires
                                    </div>
                                    <div class="tools">
                                    </div>
                                </div>
                                <div class="portlet-body">

                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="form-group form-md-line-input">
                                                <textarea id="ztNotes" style="resize: none !important"
                                                          class="form-control" placeholder="Saisir vos notes ici"
                                                          rows="5"></textarea>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>

                            <div class="alert alert-info historique note" style="left: 10px;">
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnCopieData" class="btn red-mint btn-outline">
                    </button>
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fermer</button>
                    <button type="button" class="btn" id="btnAction"></button>
                </div>
            </div>
        </div>

    </div>
    <!-- END Modal  ADRESSES -->

    <!-- START Modal LISTE POLES -->
    <div id="formListPole" style="display: none;">
        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
        <div class="modal-dialog modal-full">
            <div class="modal-content">
                <div class="modal-header bg-grey">
                    <div class="row">
                        <div class="col-md-12" id="prec">
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box red">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cogs"></i>Gestion des Pôles
                                    </div>
                                    <div class="actions">
                                        <a href="javascript:;" class="btn btn-default btn-sm" id="btnAddPole">
                                            <i class="fa fa-plus"></i> Ajout </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table data-toggle="table"
                                           class="table table-striped table-bordered table-hover order-column"
                                           id="tablePole">
                                        <thead>
                                        <tr>
                                            <th data-halign="center" data-align="left"> Code</th>
                                            <th data-halign="center" data-align="left"> Nom</th>
                                            <th data-halign="center" data-align="left"> Type</th>
                                            <th data-halign="center" data-align="left"> Ville</th>
                                            <!--<th data-halign="center" data-align="middle"> Statut</th>-->
                                            <th data-halign="center" data-align="right"> Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button id="btnClosePoles" type="button" class="btn dark btn-outline" data-dismiss="modal">Fermer
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal LISTE POLES -->

    <!-- START Modal LISTE ETABLISSEMENTS -->
    <div id="formListEtabl" style="display: none;">
        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
        <div class="modal-dialog modal-full">
            <div class="modal-content">
                <div class="modal-header bg-grey">
                    <div class="row">
                        <div class="col-md-12" id="prec">
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box blue-steel">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cogs"></i>Gestion des Établissements
                                    </div>
                                    <div class="actions">
                                        <a href="javascript:;" class="btn btn-default btn-sm" id="btnAddEtabl">
                                            <i class="fa fa-plus"></i> Ajout </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table data-toggle="table"
                                           class="table table-striped table-bordered table-hover order-column"
                                           id="tableEtabl">
                                        <thead>
                                        <tr>
                                            <th data-halign="center" data-align="left"> Code</th>
                                            <th data-halign="center" data-align="left"> Nom</th>
                                            <th data-halign="center" data-align="left"> Secteur</th>
                                            <th data-halign="center" data-align="left"> Secteur SMS</th>
                                            <th data-halign="center" data-align="left"> Adresse</th>
                                            <th data-halign="center" data-align="left"> Ville</th>
                                            <!--<th data-halign="center" data-align="middle"> Statut</th>-->
                                            <th data-halign="center" data-align="right"> Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button id="btnCloseEtabl" type="button" class="btn dark btn-outline" data-dismiss="modal">Fermer
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal LISTE ETABLISSEMENTS -->

    <!-- START Modal ANTENNES -->
    <div id="formListAntenne" style="display: none;">
        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
        <div class="modal-dialog modal-full">
            <div class="modal-content">
                <div class="modal-header bg-grey">
                    <div class="row">
                        <div class="col-md-12" id="prec">
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box blue-steel">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cogs"></i>Gestion des Antennes
                                    </div>
                                    <div class="actions">
                                        <a href="javascript:;" class="btn btn-default btn-sm" id="btnAddAntenne">
                                            <i class="fa fa-plus"></i> Ajout </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table data-toggle="table"
                                           class="table table-striped table-bordered table-hover order-column"
                                           id="tableAntenne">
                                        <thead>
                                        <tr>
                                            <th data-halign="center" data-align="left"> Nom</th>
                                            <th data-halign="center" data-align="left"> Adresse</th>
                                            <th data-halign="center" data-align="left"> Ville</th>
                                            <th data-halign="center" data-align="right"> Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button id="btnCloseAntenne" type="button" class="btn dark btn-outline" data-dismiss="modal">Fermer
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal  ANTENNES -->

    <!-- START Modal LISTE ADRESSES ACCUEIL -->
    <div id="formListAdr" style="display: none;">
        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
        <div class="modal-dialog modal-full">
            <div class="modal-content">
                <div class="modal-header bg-grey">
                    <div class="row">
                        <div class="col-md-12" id="prec">
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box green-sharp">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cogs"></i>Gestion des Adresses d'Accueil
                                    </div>
                                    <div class="actions">
                                        <a href="javascript:;" class="btn btn-default btn-sm" id="btnAddAdr">
                                            <i class="fa fa-plus"></i> Ajout </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table data-toggle="table"
                                           class="table table-striped table-bordered table-hover order-column"
                                           id="tableAdr">
                                        <thead>
                                        <tr>
                                            <th data-halign="center" data-align="left"> Nom</th>
                                            <th data-halign="center" data-align="left"> Type</th>
                                            <th data-halign="center" data-align="left"> Rue</th>
                                            <th data-halign="center" data-align="left"> Ville</th>
                                            <th data-halign="center" data-align="right"> Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button id="btnCloseAdresse" type="button" class="btn dark btn-outline" data-dismiss="modal">Fermer
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal LISTE ADRESSES ACCUEIL -->

    <!-- START Modal PERSONNELS -->
    <div id="formListPersonnel" style="display: none;">
        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
        <div class="modal-dialog modal-full">
            <div class="modal-content">
                <div class="modal-header bg-grey">
                    <div class="row">
                        <div class="col-md-12" id="prec">
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box blue-steel">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-users"></i>Gestion du Personnel
                                    </div>
                                    <div class="actions">
                                        <a href="javascript:;" class="btn btn-default btn-sm" id="btnAddPersonnel">
                                            <i class="fa fa-plus"></i> Ajout </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table data-toggle="table"
                                           class="table table-striped table-bordered table-hover order-column"
                                           id="tablePersonnel">
                                        <thead>
                                        <tr>
                                            <th data-halign="center" data-align="left"> Nom</th>
                                            <th data-halign="center" data-align="left"> Prénom</th>
                                            <th data-halign="center" data-align="left"> Service</th>
                                            <th data-halign="center" data-align="left"> Fonction</th>
                                            <th data-halign="center" data-align="left"> Ville</th>
                                            <th data-halign="center" data-align="right"> Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button id="btnClosePersonnel" type="button" class="btn dark btn-outline" data-dismiss="modal">
                        Fermer
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal  PERSONNELS -->


    <!-- END Liste de Modals -->


    <!-- START Dialogue d'exécution Level 0 -->
    <div class="modal fade" id="dialogExec" data-level="" aria-hidden="true" data-backdrop="static"
         data-keyboard="false">
    </div>
    <!-- END Dialogue d'exécution Level 0 -->

    <!-- START Dialogue d'exécution Level 0 -->
    <div class="modal fade dialog-exec" id="dialogExecLev0" aria-hidden="true" data-backdrop="static"
         data-keyboard="false">
    </div>
    <!-- END Dialogue d'exécution Level 0 -->

    <!-- START Dialogue d'exécution Level 1 -->
    <div class="modal fade dialog-exec" id="dialogExecLev1" aria-hidden="true" data-backdrop="static"
         data-keyboard="false">
    </div>
    <!-- END Dialogue d'exécution Level 1 -->

    <!-- START Dialogue d'exécution Level 2 -->
    <div class="modal fade dialog-exec" id="dialogExecLev2" aria-hidden="true" data-backdrop="static"
         data-keyboard="false">
    </div>
    <!-- END Dialogue d'exécution Level 2 -->

    <!-- START Dialogue d'exécution Level 3 -->
    <div class="modal fade dialog-exec" id="dialogExecLev3" aria-hidden="true" data-backdrop="static"
         data-keyboard="false">
    </div>
    <!-- END Dialogue d'exécution Level 3 -->

    <!-- START Dialogue d'exécution Level 4 -->
    <div class="modal fade dialog-exec" id="dialogExecLev4" aria-hidden="true" data-backdrop="static"
         data-keyboard="false">
    </div>
    <!-- END Dialogue d'exécution Level 4 -->

    <!-- START Dialogue d'exécution Level 5 -->
    <div class="modal fade dialog-exec" id="dialogExecLev5" aria-hidden="true" data-backdrop="static"
         data-keyboard="false">
    </div>
    <!-- END Dialogue d'exécution Level 5 -->

    <!-- START Dialogue d'exécution Level 9 -->
    <div class="modal fade dialog-exec" id="dialogExecLev9" aria-hidden="true" data-backdrop="static"
         data-keyboard="false">
    </div>
    <!-- END Dialogue d'exécution Level 9 -->

@endsection
