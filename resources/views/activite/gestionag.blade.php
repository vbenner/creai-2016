@extends('layouts.app')
@section('title', $titre)
@section('subtitle', $subtitre)

@section('script')

    <script src="{{ URL::asset('/scripts/activite/gestionag.js') }}"></script>

@endsection
@section('content')

    <div class="row">

        <div class="col-md-12 col-sm-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box red">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>Gestion des {!! $subtitre !!} </div>
                    <div class="actions">
                        <a href="javascript:;" class="btn btn-default btn-sm" id="btnAdd">
                            <i class="fa fa-plus"></i> Ajout </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <table data-toggle="table"
                           class="table table-striped table-bordered table-hover order-column"
                           id="table">
                        <thead>
                        <tr>
                            <th data-halign="center" data-align="left"> Année</th>
                            <th data-halign="center" data-align="left"> Date</th>
                            <th data-halign="center" data-align="left"> Note</th>
                            <th data-halign="center" data-align="right"> Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <!-- START Dialogue d'exécution -->
    <div class="modal fade dialog-exec" id="dialogExec" aria-hidden="true" data-backdrop="static"
         data-keyboard="false">
    </div>
    <!-- END Dialogue d'exécution -->

    <!-- START Modal ASSEMBLEE GENERALE -->
    <div id="formAG" style="display: none;">
        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header caption">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title bold uppercase"></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group form-md-line-input">
                                <input class="form-control text-uppercase required" id="ztAnneeAG"
                                       type="text" data-inputmask="'mask': '9{4}'">
                                <label for="ztAnneeAG">Saisir l'année <span
                                            class="font-red">*</span></label>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group form-md-line-input form-md-floating-label has-success">
                                <input class="form-control date-picker required"
                                       id="ztDateAG" readonly type="text">
                                <label for="ztDateAG">Date AG <span
                                            class="font-red">*</span></label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group form-md-line-input">
                                <textarea id="ztNotes" style="resize: none !important"
                                          class="form-control" placeholder="Saisir vos notes ici"
                                          rows="5"></textarea>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fermer</button>
                    <button type="button" class="btn" id="btnAction"></button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal ASSEMBLEE GENERALE -->

    <!-- END Liste de Modals -->

@endsection
