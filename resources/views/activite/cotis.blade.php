@extends('layouts.app')
@section('title', $titre)
@section('subtitle', $subtitre)

@section('script')

    <script src="{{ URL::asset('/scripts/activite/cotis.js') }}"></script>

@endsection
@section('content')
    <div class="row">

        <div class="col-md-12 col-sm-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box red">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>Préparation d'une Campagne de {!! $subtitre !!} </div>
                    <div class="actions">
                        <a href="javascript:;" class="btn btn-default btn-sm" id="btnSelect">
                            <i class="fa fa-check-square-o"></i>/<i class="fa fa-square-o"></i> Sélectionner </a>
                        <a href="javascript:;" class="btn btn-default btn-sm" id="btnCampagne" {!! $status !!}>
                            <i class="fa fa-send-o"></i> {!! $btn !!}</a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12" style="padding-bottom: 15px;">
                            Légende =
                            <span class="label label-primary"> Personne Physique </span>&nbsp;
                            <span class="label label-info"> Organisme </span>&nbsp;
                            <span class="label label-success"> Personnel </span>&nbsp;
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table id="tableCotisant" style="width: 100%" class="table table-striped table-bordered table-hover no-footer">
                            <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" id="annee" name="annee" value="{!! $annee !!}">
                            <thead>
                            <tr>
                                <th colspan="6"
                                    style="
                                    text-transform: uppercase;
                                    font-size:16px;
                                    width:100%;
                                    background-color: #EAEFF3 !important;
                                ">
                                    <strong>Sélection des Cotisants</strong>
                                </th>
                            </tr>
                            <tr>
                                <th style="width:5%;"></th>
                                <th style="width:40%;">Nom</th>
                                <th style="width:5%;">Type</th>
                                <th style="width:5%;"></th>
                                <th style="width:40%;">Nom</th>
                                <th style="width:5%;">Type</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <!-- START Portlet Attente Règlement -->
    <div class="row">

        <div class="col-md-12 col-sm-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box blue-sharp">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-money"></i>Traitement d'une Campagne de {!! $subtitre !!}
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12" style="padding-bottom: 15px;">
                            Légende =
                            <span class="label label-primary"> Personne Physique </span>&nbsp;
                            <span class="label label-info"> Organisme </span>&nbsp;
                            <span class="label label-success"> Personnel </span>&nbsp;
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table id="tableCotisations" style="width: 100%" class="table table-striped table-bordered table-hover no-footer">
                                <thead>
                                <tr>
                                    <th style="width:45%;">Nom</th>
                                    <th style="width:10%;"><small>Type Cotisation</small></th>
                                    <th style="width:5%;"><small>Montant</small></th>
                                    <th style="width:10%;">Date</th>
                                    <th style="width:5%;">Type</th>
                                    <th style="width:5%;">Références</th>
                                    <th style="width:5%;">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <!-- END Portlet Attente Règlement -->

    <!-- START Dialogue d'exécution -->
    <div class="modal fade dialog-exec" id="dialogExec" aria-hidden="true" data-backdrop="static"
         data-keyboard="false">
    </div>
    <!-- END Dialogue d'exécution -->

    <!-- END Liste de Modals -->

@endsection
