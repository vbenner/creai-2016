<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReglementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reglements', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('campagne_id')->unsigned()->nullable();
            $table->foreign('campagne_id')->references('id')->on('campagneags');

            $table->integer('oo_id')->unsigned()->nullable();
            $table->foreign('oo_id')->references('id')->on('organismes');

            $table->integer('pp_id')->unsigned()->nullable();
            $table->foreign('pp_id')->references('id')->on('personnes_physiques');

            $table->integer('pe_id')->unsigned()->nullable();
            $table->foreign('pe_id')->references('id')->on('personnels');

            $table->integer('type_cotisation_id')->unsigned()->nullable();
            $table->foreign('type_cotisation_id')->references('id')->on('type_cotisations');

            $table->integer('type_contribution_id')->unsigned()->nullable();
            $table->foreign('type_contribution_id')->references('id')->on('type_contributions');

            $table->integer('type_reglement_id')->unsigned()->nullable();
            $table->foreign('type_reglement_id')->references('id')->on('type_reglements');

            $table->decimal('montant', 10, 2);
            $table->date('date');
            $table->string('reference');

            /** -----------------------------------------------------------------------------------
             * Bloc commun
             */
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reglements');
    }
}
