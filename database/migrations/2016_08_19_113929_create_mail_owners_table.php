<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailOwnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mail_owners', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('pp_id')->unsigned()->nullable();
            $table->foreign('pp_id')->references('id')->on('personnes_physiques');

            $table->integer('mail_id')->unsigned()->nullable();
            $table->foreign('mail_id')->references('id')->on('mails');

            $table->text('notes');

            /** -----------------------------------------------------------------------------------
             * Bloc commun
             */
            $table->boolean('active')->default(1);
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mail_owners');
    }
}
