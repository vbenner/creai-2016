<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsTelephonesAndMailsEt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('telephone_owners', function($table) {
            $table->integer('et_id')->unsigned()->nullable();
            $table->foreign('et_id')->references('id')->on('etablissements');
        });

        Schema::table('mail_owners', function($table) {
            $table->integer('et_id')->unsigned()->nullable();
            $table->foreign('et_id')->references('id')->on('etablissements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
