<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteCascadeOnEtablissementModaliteDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('etablissement_modalite_details', function (Blueprint $table) {
            $table->dropForeign(['etablissement_modalite_id']);
            $table->foreign('etablissement_modalite_id')
                ->references('id')->on('etablissement_modalites')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('etablissement_modalite_details', function (Blueprint $table) {
            //
        });
    }
}
