<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldPersoToTelOwner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('telephone_owners', function (Blueprint $table) {
            $table->integer('pe_id')->unsigned()->nullable()->after('an_id');
            $table->foreign('pe_id')->references('id')->on('personnels');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('telephone_owners', function (Blueprint $table) {
            //
        });
    }
}
