<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldAntenneIdToAdressesAccueil extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adresses_accueil', function (Blueprint $table) {
            $table->integer('antenne_id')->unsigned()->nullable()->after('etablissement_id');
            $table->foreign('antenne_id')->references('id')->on('antennes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adresses_accueil', function (Blueprint $table) {
            //
        });
    }
}
