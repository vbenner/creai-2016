<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAntenneModaliteAccueilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('antennes_modalite_accueil', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('antenne_id')->unsigned()->nullable();
            $table->foreign('antenne_id')->references('id')->on('antennes');

            $table->integer('type_hebergement_id')->unsigned()->nullable();
            $table->foreign('type_hebergement_id')->references('id')->on('modalite_accueil');

            /** -----------------------------------------------------------------------------------
             * Bloc commun
             */
            $table->boolean('active')->default(1);
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('antennes_modalite_accueil');
    }
}
