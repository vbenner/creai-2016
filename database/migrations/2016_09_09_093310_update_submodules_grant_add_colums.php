<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSubmodulesGrantAddColums extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('submodules_grants', function (Blueprint $table) {
            $table->tinyInteger('grant')->default(1)->after('user_id');
            $table->tinyInteger('add')->default(1)->after('grant');
            $table->tinyInteger('edit')->default(1)->after('add');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('submodules_grants', function (Blueprint $table) {
            //
        });
    }
}
