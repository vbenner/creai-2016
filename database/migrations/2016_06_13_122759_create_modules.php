<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bloc_id')->unsigned()->index();
            $table->foreign('bloc_id')->references('id')->on('blocs');
            $table->string('libelle', 100);
            $table->string('logo', 100);
            $table->integer('ordre')->unsigned();
            $table->string('path', 100);
            $table->boolean('has_sub_module');
            $table->boolean('accordeon');

            /** -----------------------------------------------------------------------------------
             * Bloc commun
             */
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('modules');
    }
}
