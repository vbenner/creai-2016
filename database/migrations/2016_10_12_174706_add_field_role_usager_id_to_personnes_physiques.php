<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldRoleUsagerIdToPersonnesPhysiques extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('personnes_physiques', function (Blueprint $table) {
            $table->integer('type_role_usager_id')->unsigned()->nullable()->after('usager');
            $table->foreign('type_role_usager_id')->references('id')->on('type_role_usager');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('personnes_physiques', function (Blueprint $table) {
            //
        });
    }
}
