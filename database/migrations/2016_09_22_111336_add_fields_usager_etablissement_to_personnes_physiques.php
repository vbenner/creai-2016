<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsUsagerEtablissementToPersonnesPhysiques extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('personnes_physiques', function (Blueprint $table) {

            $table->boolean('usager')->after('prenom');
            $table->integer('etablissement_id')->unsigned()->nullable();
            $table->foreign('etablissement_id')->references('id')->on('etablissements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('personnes_physiques', function (Blueprint $table) {
            //
        });
    }
}
