<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleGrantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules_grants', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('module_id');
            $table->unsignedInteger('user_id');

            $table->foreign('module_id', 'modules_grants_fk1')->references('id')->on('modules');
            $table->foreign('user_id', 'modules_grants_fk2')->references('id')->on('users');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('modules_grants');
    }
}
