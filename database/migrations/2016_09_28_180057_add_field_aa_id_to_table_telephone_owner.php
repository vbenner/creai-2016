<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldAaIdToTableTelephoneOwner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('telephone_owners', function (Blueprint $table) {
            $table->integer('aa_id')->unsigned()->nullable()->after('et_id');
            $table->foreign('aa_id')->references('id')->on('adresses_accueil');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('telephone_owners', function (Blueprint $table) {
            //
        });
    }
}
