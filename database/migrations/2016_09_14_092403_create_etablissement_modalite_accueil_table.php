<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEtablissementModaliteAccueilTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('etablissements_modalite_accueil', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('etablissement_id')->unsigned()->nullable();
            $table->foreign('etablissement_id')->references('id')->on('etablissements');

            $table->integer('type_hebergement_id')->unsigned()->nullable();
            $table->foreign('type_hebergement_id')->references('id')->on('modalite_accueil');

            /** -----------------------------------------------------------------------------------
             * Bloc commun
             */
            $table->boolean('active')->default(1);
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('etablissements_modalite_accueil');
    }
}
