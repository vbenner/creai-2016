<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEtablissementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('etablissements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_interne');

            $table->string('nom');
            $table->string('sigle');

            $table->string('finess');

            $table->string('arrete_pref');
            $table->date('date_arrete');

            $table->date('date_ouverture');
            $table->boolean('projet');

            $table->integer('type_secteur_id')->unsigned()->nullable();
            $table->foreign('type_secteur_id')->references('id')->on('type_secteurs');

            $table->integer('type_secteur_ms_id')->unsigned()->nullable();
            $table->foreign('type_secteur_ms_id')->references('id')->on('type_secteurs_ms');

            $table->integer('type_public_id')->unsigned()->nullable();
            $table->foreign('type_public_id')->references('id')->on('type_publics');

            $table->integer('type_etablissement_id')->unsigned()->nullable();
            $table->foreign('type_etablissement_id')->references('id')->on('type_etablissements');

            $table->string('adr1');
            $table->string('adr2');
            $table->string('adr3');
            $table->string('cp');
            $table->string('ville');
            $table->integer('pays_id')->unsigned()->nullable();
            $table->foreign('pays_id')->references('id')->on('pays');

            $table->string('web');

            $table->boolean('bi');
            $table->integer('bi_reception_id')->unsigned()->nullable();
            $table->foreign('bi_reception_id')->references('id')->on('type_receptions');

            $table->boolean('cf');
            $table->integer('cf_reception_id')->unsigned()->nullable();
            $table->foreign('cf_reception_id')->references('id')->on('type_receptions');

            $table->boolean('nl');

            $table->boolean('isCONTRIBUANT');

            $table->integer('nb_places');
            $table->integer('age');

            $table->integer('oo_id')->unsigned()->nullable();
            $table->foreign('oo_id')->references('id')->on('organismes');

            $table->integer('po_id')->unsigned()->nullable();
            $table->foreign('po_id')->references('id')->on('poles');

            $table->string('notes');
            $table->boolean('visible');
            $table->boolean('acces');

            $table->text('history');

            /** -----------------------------------------------------------------------------------
             * Bloc commun
             */
            $table->boolean('active')->default(1);
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('etablissements');
    }
}
