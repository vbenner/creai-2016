<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldHandicapToTypeSecteurMs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('type_secteurs_ms', function (Blueprint $table) {
            $table->boolean('handicap')->default(0)->after('libelle');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('type_secteurs_ms', function (Blueprint $table) {
            //
        });
    }
}
