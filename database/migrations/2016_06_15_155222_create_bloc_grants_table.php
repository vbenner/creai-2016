<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlocGrantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blocs_grants', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('bloc_id');
            $table->unsignedInteger('user_id');

            $table->foreign('bloc_id', 'blocs_grants_fk1')->references('id')->on('blocs');
            $table->foreign('user_id', 'blocs_grants_fk2')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blocs_grants');
    }
}
