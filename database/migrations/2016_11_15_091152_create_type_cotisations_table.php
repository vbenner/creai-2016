<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypeCotisationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('type_cotisations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('libelle');
            $table->decimal('montant', 10, 2);

            /** -----------------------------------------------------------------------------------
             * Bloc commun
             */
            $table->boolean('active')->default(1);
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('type_cotisations');
    }
}
