<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypeEtablissementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('type_etablissements', function (Blueprint $table) {

            $table->increments('id');
            $table->string('libelle');
            $table->string('sigle');

            $table->integer('type_secteur_id')->unsigned()->nullable();
            $table->foreign('type_secteur_id')->references('id')->on('type_secteurs');

            $table->integer('type_secteur_ms_id')->unsigned()->nullable();
            $table->foreign('type_secteur_ms_id')->references('id')->on('type_secteurs_ms');

            $table->integer('type_age_id')->unsigned()->nullable();
            $table->foreign('type_age_id')->references('id')->on('type_ages');

            /** -----------------------------------------------------------------------------------
             * Bloc commun
             */
            $table->boolean('active')->default(1);
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('type_etablissements');
    }
}
