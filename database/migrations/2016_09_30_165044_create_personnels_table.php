<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonnelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personnels', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('civilite_id')->unsigned()->nullable();
            $table->foreign('civilite_id')->references('id')->on('civilites');
            $table->string('nom');
            $table->string('prenom');

            /** -----------------------------------------------------------------------------------
             * A qui peut être rattaché cet utilisateur ?
             */
            $table->integer('oo_id')->unsigned()->nullable();
            $table->foreign('oo_id')->references('id')->on('organismes');

            $table->integer('po_id')->unsigned()->nullable();
            $table->foreign('po_id')->references('id')->on('poles');

            $table->integer('et_id')->unsigned()->nullable();
            $table->foreign('et_id')->references('id')->on('etablissements');

            $table->integer('an_id')->unsigned()->nullable();
            $table->foreign('an_id')->references('id')->on('antennes');

            $table->string('adr1');
            $table->string('adr2');
            $table->string('adr3');
            $table->string('cp');
            $table->string('ville');
            $table->integer('pays_id')->unsigned()->nullable();
            $table->foreign('pays_id')->references('id')->on('pays');

            $table->string('service');

            $table->integer('type_organisme_id')->unsigned()->nullable();
            $table->foreign('type_organisme_id')->references('id')->on('type_organismes');

            $table->integer('fonction_id')->unsigned()->nullable();
            $table->foreign('fonction_id')->references('id')->on('fonctions');
            $table->string('notes_fonctions');

            $table->boolean('nl')->default(1);
            $table->boolean('voeux')->default(1);

            $table->boolean('ag');
            $table->integer('role_ag_id')->unsigned()->nullable();
            $table->foreign('role_ag_id')->references('id')->on('type_roles_ag');

            $table->boolean('cf');
            $table->integer('cf_reception_id')->unsigned()->nullable();
            $table->foreign('cf_reception_id')->references('id')->on('type_receptions');

            $table->boolean('buro');
            $table->integer('role_buro_id')->unsigned()->nullable();
            $table->foreign('role_buro_id')->references('id')->on('type_roles_bureau');

            $table->boolean('ca');
            $table->integer('role_ca_id')->unsigned()->nullable();
            $table->foreign('role_ca_id')->references('id')->on('type_roles_ca');

            $table->boolean('bi');
            $table->integer('bi_reception_id')->unsigned()->nullable();
            $table->foreign('bi_reception_id')->references('id')->on('type_receptions');

            /** ---------------------------
             *
             */
            $table->boolean('ctc')->comment = "Comité Technique Consultatif";
            $table->boolean('cc')->comment = "Correspondant Commission";

            $table->integer('clone_id')->unsigned()->nullable();
            $table->foreign('clone_id')->references('id')->on('personnels');

            $table->text('note');

            /** -----------------------------------------------------------------------------------
             * Bloc commun
             */
            $table->boolean('active')->default(1);
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        return;
        Schema::drop('personnels');
    }
}
