<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailTypeOrganismesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_type_organismes', function (Blueprint $table) {

            $table->increments('id');
            $table->string('libelle');

            $table->integer('type_organisme_id')->unsigned()->nullable();
            $table->foreign('type_organisme_id')->references('id')->on('type_organismes');

            $table->text('note');

            /** -----------------------------------------------------------------------------------
             * Bloc commun
             */
            $table->boolean('active')->default(1);
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detail_type_organismes', function (Blueprint $table) {
            //
        });
    }
}
