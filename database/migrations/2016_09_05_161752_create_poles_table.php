<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('poles', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('organisme_id')->unsigned()->nullable();
            $table->foreign('organisme_id')->references('id')->on('organismes');

            $table->string('nom');

            $table->integer('type_pole_id')->unsigned()->nullable();
            $table->foreign('type_pole_id')->references('id')->on('type_poles');
            $table->integer('type_public_id')->unsigned()->nullable();
            $table->foreign('type_public_id')->references('id')->on('type_publics');

            $table->integer('nb_places');
            $table->integer('age');
            $table->string('adr1');
            $table->string('adr2');
            $table->string('adr3');
            $table->string('cp');
            $table->string('ville');
            $table->integer('pays_id')->unsigned()->nullable();
            $table->foreign('pays_id')->references('id')->on('pays');

            $table->string('web');

            $table->boolean('bi');
            $table->integer('bi_reception_id')->unsigned()->nullable();
            $table->foreign('bi_reception_id')->references('id')->on('type_receptions');

            $table->boolean('nl');

            $table->boolean('cf');
            $table->integer('cf_reception_id')->unsigned()->nullable();
            $table->foreign('cf_reception_id')->references('id')->on('type_receptions');

            $table->boolean('isCONTRIBUANT');

            $table->string('notes');
            $table->boolean('visible');
            $table->boolean('acces');

            /** -----------------------------------------------------------------------------------
             * Bloc commun
             */
            $table->boolean('active')->default(1);
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('poles');
    }
}
