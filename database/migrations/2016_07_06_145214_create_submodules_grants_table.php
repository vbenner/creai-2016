<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubmodulesGrantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('submodules_grants', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('submodule_id');
            $table->unsignedInteger('user_id');

            $table->foreign('submodule_id', 'submodules_grants_fk1')->references('id')->on('submodules');
            $table->foreign('user_id', 'submodules_grants_fk2')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('submodules_grants');
    }
}
