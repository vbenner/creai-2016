<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEtablissementModaliteDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('etablissement_modalite_details', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('etablissement_modalite_id')->unsigned()->nullable();
            $table->foreign('etablissement_modalite_id')->references('id')->on('etablissement_modalites');

            $table->integer('modalite_accueil_id')->unsigned()->nullable();
            $table->foreign('modalite_accueil_id')->references('id')->on('modalite_accueil');

            $table->integer('type_public_id')->unsigned()->nullable();
            $table->foreign('type_public_id')->references('id')->on('type_publics');

            $table->integer('detail_type_public_id')->unsigned()->nullable();
            $table->foreign('detail_type_public_id')->references('id')->on('detail_type_publics');

            $table->integer('nb_places')->unsigned()->default(0);

            /** -----------------------------------------------------------------------------------
             * Bloc commun
             */
            $table->boolean('active')->default(1);
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('etablissement_modalite_details');
    }
}
