<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldAnIdToMailOwners extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mail_owners', function (Blueprint $table) {
            $table->integer('an_id')->unsigned()->nullable()->after('aa_id');
            $table->foreign('an_id')->references('id')->on('antennes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mail_owners', function (Blueprint $table) {
            //
        });
    }
}
