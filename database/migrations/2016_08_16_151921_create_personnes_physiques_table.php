<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonnesPhysiquesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personnes_physiques', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_interne');

            $table->integer('civilite_id')->unsigned()->nullable();
            $table->foreign('civilite_id')->references('id')->on('civilites');
            $table->string('nom');
            $table->string('prenom');

            $table->string('adr1');
            $table->string('adr2');
            $table->string('adr3');
            $table->string('cp');
            $table->string('ville');
            $table->integer('pays_id')->unsigned()->nullable();
            $table->foreign('pays_id')->references('id')->on('pays');

            $table->boolean('voeux');
            $table->boolean('nl');

            $table->boolean('ag');
            $table->integer('role_ag_id')->unsigned()->nullable();
            $table->foreign('role_ag_id')->references('id')->on('type_roles_ag');

            $table->boolean('cf');
            $table->integer('cf_reception_id')->unsigned()->nullable();
            $table->foreign('cf_reception_id')->references('id')->on('type_receptions');

            $table->boolean('buro');
            $table->integer('role_buro_id')->unsigned()->nullable();
            $table->foreign('role_buro_id')->references('id')->on('type_roles_bureau');

            $table->boolean('ca');
            $table->integer('role_ca_id')->unsigned()->nullable();
            $table->foreign('role_ca_id')->references('id')->on('type_roles_ca');

            $table->boolean('bi');
            $table->integer('bi_reception_id')->unsigned()->nullable();
            $table->foreign('bi_reception_id')->references('id')->on('type_receptions');

            $table->boolean('cotis');

            $table->string('notes');
            $table->text('history');

            /** -----------------------------------------------------------------------------------
             * Bloc commun
             */
            $table->boolean('active')->default(1);
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('personnes_physiques');
    }
}
