<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssembleesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assemblees', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('annee');
            $table->date('date_ag');
            $table->string('notes');

            /** -----------------------------------------------------------------------------------
             * Bloc commun
             */
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('assemblees');
    }
}
