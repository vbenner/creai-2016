<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdressesAccueilTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adresses_accueil', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('etablissement_id')->unsigned()->nullable();
            $table->foreign('etablissement_id')->references('id')->on('etablissements');

            $table->integer('type_public_id')->unsigned()->nullable();
            $table->foreign('type_public_id')->references('id')->on('type_publics');

            $table->integer('detail_type_public_id')->unsigned()->nullable();
            $table->foreign('detail_type_public_id')->references('id')->on('detail_type_publics');

            $table->integer('nb_places');
            $table->string('age');

            $table->string('adr1');
            $table->string('adr2');
            $table->string('adr3');
            $table->string('cp');
            $table->string('ville');
            $table->integer('pays_id')->unsigned()->nullable();
            $table->foreign('pays_id')->references('id')->on('pays');

            $table->string('notes');

            /** -----------------------------------------------------------------------------------
             * Bloc commun
             */
            $table->boolean('active')->default(1);
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adresses_accueil', function (Blueprint $table) {
            //
        });
    }
}
