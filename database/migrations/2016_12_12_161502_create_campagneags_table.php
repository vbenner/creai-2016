<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampagneagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campagneags', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('assemblee_id')->unsigned()->nullable();
            $table->foreign('assemblee_id')->references('id')->on('assemblees');

            /** -----------------------------------------------------------------------------------
             * Bloc commun
             */
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('campagneags');
    }
}
