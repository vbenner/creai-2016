<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampagneagCotisantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campagneag_cotisants', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('campagneags_id')->unsigned()->nullable();
            $table->foreign('campagneags_id')->references('id')->on('campagneags');

            $table->integer('pp_id')->unsigned()->nullable();
            $table->foreign('pp_id')->references('id')->on('personnes_physiques');

            $table->integer('oo_id')->unsigned()->nullable();
            $table->foreign('oo_id')->references('id')->on('organismes');

            $table->integer('pe_id')->unsigned()->nullable();
            $table->foreign('pe_id')->references('id')->on('personnels');

            /** -----------------------------------------------------------------------------------
             * Bloc commun
             */
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('campagneag_cotisants');
    }
}
