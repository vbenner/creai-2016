<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offres', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('annee');
            $table->integer('nb');
            $table->text('notes');
            $table->dateTime('date');

            /** -----------------------------------------------------------------------------------
             * A qui peut être rattaché cet utilisateur ?
             */
            $table->integer('oo_id')->unsigned()->nullable();
            $table->foreign('oo_id')->references('id')->on('organismes');

            $table->integer('po_id')->unsigned()->nullable();
            $table->foreign('po_id')->references('id')->on('poles');

            $table->integer('et_id')->unsigned()->nullable();
            $table->foreign('et_id')->references('id')->on('etablissements');

            $table->integer('an_id')->unsigned()->nullable();
            $table->foreign('an_id')->references('id')->on('antennes');

            /** -----------------------------------------------------------------------------------
             * Bloc commun
             */
            $table->boolean('active')->default(1);
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('offres');
    }
}
