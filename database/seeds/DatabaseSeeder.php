<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /** ---------------------------------------------------------------------------------------
         * CHECK FOREIGN_KEY_CHECKS --> Désactivation
         */
        Model::unguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->populateCivilite();
        $this->populatePays();
        $this->populateTypesTelephones();
        $this->populateTypesRolesBureau();
        $this->populateTypesMail();
        $this->populateTypesRolesCA();
        $this->populateTypesRolesAG();
        $this->populateTypesSecteur();
        $this->populateTypesPole();
        $this->populateTypesAge();
        $this->populateTypesStatutOrganisme();
        $this->populateTypesPublic();
        $this->populateDetailTypesPublic();
        $this->populateTypesOrganisme();
        $this->populateDetailTypesOrganisme();
        $this->populateTypesSecteurMS();
        $this->populateTypesReception();
        $this->populateModaliteAccueil();
        $this->populateTypeEtablissement();

        /** ---------------------------------------------------------------------------------------
         * CHECK FOREIGN_KEY_CHECKS --> Réactivation
         */
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }

    public function populateCivilite() {
        $model = 'App\Civilite';
        $items = [
            ['libelle' => 'M', 'user_id' => 1],
            ['libelle' => 'MME', 'user_id' => 1],
        ];

        $model::truncate();
        foreach($items as $item){
            $model::create($item);
        }
    }

    public function populatePays() {

        $model = 'App\Pays';
        $items = [
            ['libelle' => 'FRANCE', 'user_id' => 1],
            ['libelle' => 'ITALIE', 'user_id' => 1],
            ['libelle' => 'ALLEMAGNE', 'user_id' => 1],
            ['libelle' => 'SUISSE', 'user_id' => 1],
            ['libelle' => 'BELGIQUE', 'user_id' => 1],
        ];
        $model::truncate();
        foreach($items as $item){
            $model::create($item);
        }
    }

    public function populateTypesRolesBureau() {
        $model = 'App\TypeRoleBureau';
        $items = [
            ['libelle' => 'PRÉSIDENT', 'ordre' => 1, 'user_id' => 1],
            ['libelle' => 'VICE PRÉSIDENT', 'ordre' => 2, 'user_id' => 1],
            ['libelle' => 'SECRÉTAIRE GÉNÉRAL', 'ordre' => 3, 'user_id' => 1],
            ['libelle' => 'SECRÉTAIRE GÉNÉRAL ADJOINT', 'ordre' => 4, 'user_id' => 1],
            ['libelle' => 'TRÉSORIER', 'ordre' => 5, 'user_id' => 1],
            ['libelle' => 'TRÉSORIER ADJOINT', 'ordre' => 6, 'user_id' => 1],
            ['libelle' => 'MEMBRE À TITRE DÉLIBÉRATIF', 'ordre' => 7, 'user_id' => 1],
            ['libelle' => 'MEMBRE À TITRE CONSULTATIF', 'ordre' => 8, 'user_id' => 1],
        ];
        $model::truncate();
        foreach($items as $item){
            $model::create($item);
        }
    }

    public function populateTypesMail() {
        $model = 'App\TypeMail';
        $items = [
            ['libelle' => 'PERSONNEL', 'user_id' => 1],
            ['libelle' => 'PROFESSIONNEL', 'user_id' => 1],
        ];
        $model::truncate();
        foreach($items as $item){
            $model::create($item);
        }
    }

    public function populateTypesTelephones() {
        $model = 'App\TypeTel';
        $items = [
            ['libelle' => 'STANDARD', 'user_id' => 1],
            ['libelle' => 'LIGNE DIRECTE', 'user_id' => 1],
            ['libelle' => 'FAX', 'user_id' => 1],
            ['libelle' => 'MOBILE', 'user_id' => 1],
            ['libelle' => 'PERSONNEL', 'user_id' => 1],
            ['libelle' => 'PROFESSIONNEL', 'user_id' => 1],
        ];
        $model::truncate();
        foreach($items as $item){
            $model::create($item);
        }
    }

    public function populateTypesRolesCA() {
        $model = 'App\TypeRoleCA';
        $items = [
            ['libelle' => 'TITULAIRE', 'user_id' => 1],
            ['libelle' => strtoupper('Membre À titre consultatif'), 'user_id' => 1],
            ['libelle' => strtoupper('autre'), 'user_id' => 1],
        ];
        $model::truncate();
        foreach($items as $item){
            $model::create($item);
        }
    }

    public function populateTypesRolesAG() {

        $model = 'App\TypeRoleAG';
        $items = [
            ['libelle' => 'PERSONNE MORALE', 'user_id' => 1],
            ['libelle' => 'PERSONNE PHYSIQUE', 'user_id' => 1],
            ['libelle' => 'PERSONNE PHYSIQUE MEMBRE D\'HONNEUR', 'user_id' => 1],
            ['libelle' => 'MEMBRE À TITRE CONSULTATIF', 'user_id' => 1],
            ['libelle' => 'PERSONNE PHYSIQUE QUALIFIÉE', 'user_id' => 1],
            ['libelle' => 'REPRÉSENTANTS ÉLUS', 'user_id' => 1],
            ['libelle' => 'AUTRE', 'user_id' => 1],
        ];
        $model::truncate();
        foreach($items as $item){
            $model::create($item);
        }
    }

    public function populateTypesSecteur() {

        $model = 'App\TypeSecteur';
        $items = [
            ['libelle' => 'SANITAIRE', 'isSecteurMS'=> 0, 'user_id' => 1],
            ['libelle' => 'SOCIAL ET MEDICO-SOCIAL', 'isSecteurMS'=> 1, 'user_id' => 1],
        ];
        $model::truncate();
        foreach($items as $item){
            $model::create($item);
        }
    }

    public function populateTypesPole() {

        $model = 'App\TypePole';
        $items = [
            ['libelle' => 'DISPOSITIF / RÉSEAU', 'user_id' => 1],
            ['libelle' => 'PÔLE / SECTEUR', 'user_id' => 1],
        ];
        $model::truncate();
        foreach($items as $item){
            $model::create($item);
        }
    }

    public function populateTypesAge() {

        $model = 'App\TypeAge';
        $items = [
            ['libelle' => 'ENFANTS ET ADOLESCENTS', 'user_id' => 1],
            ['libelle' => 'ADULTES', 'user_id' => 1],
            ['libelle' => 'ADULTES, ENFANTS ET ADOLESCENTS', 'user_id' => 1],
        ];
        $model::truncate();
        foreach($items as $item){
            $model::create($item);
        }
    }

    public function populateTypesOrganisme() {

        $model = 'App\TypeOrganisme';
        $items = [
            ['libelle' => 'ASSOCIATION GESTIONNAIRE', 'isOG' => 1, 'isPOLE' => 1, 'isGROUP' => 1, 'user_id' => 1],
            ['libelle' => 'ADMINISTRATION / COLLECTIVITÉ', 'isOG' => 1, 'isPOLE' => 1, 'isGROUP' => 0, 'user_id' => 1],
            ['libelle' => 'ASSOCIATION USAGERS / FAMILLE', 'isOG' => 1, 'isPOLE' => 1, 'isGROUP' => 1, 'user_id' => 1],
            ['libelle' => 'GROUPEMENT', 'isOG' => 1, 'isPOLE' => 1, 'isGROUP' => 1, 'user_id' => 1],
            ['libelle' => 'CREAI', 'isOG' => 0, 'isPOLE' => 0, 'isGROUP' => 0, 'user_id' => 1],
            ['libelle' => 'AUTRE', 'isOG' => 0, 'isPOLE' => 0, 'isGROUP' => 0, 'user_id' => 1],
        ];
        $model::truncate();
        foreach($items as $item){
            $model::create($item);
        }
    }

    public function populateDetailTypesOrganisme() {

        $model = 'App\DetailTypeOrganisme';
        $items = [
            ['libelle' => 'CENTRE DE FORMATION', 'type_organisme_id'=> 6, 'user_id' => 1],
            ['libelle' => 'ENTREPRISE INSERTION', 'type_organisme_id'=> 6, 'user_id' => 1],
        ];
        $model::truncate();
        foreach($items as $item){
            $model::create($item);
        }
    }

    public function populateTypesStatutOrganisme() {

        $model = 'App\TypeStatutOrganisme';
        $items = [
            ['libelle' => 'ETAT', 'sigle' => '', 'user_id' => 1],
            ['libelle' => 'COLLECTIVITÉ TERRITORIALE', 'sigle' => '', 'user_id' => 1],
            ['libelle' => 'ÉTABLISSEMENT PUBLIC DÉPARTEMENTAL D\'HOSPITALISATION', 'sigle' => 'EPDH', 'user_id' => 1],
            ['libelle' => 'ÉTABLISSEMENT PUBLIC INTERCOMMUNAL D\'HOSPITALISATION', 'sigle' => 'EPIH', 'user_id' => 1],
            ['libelle' => 'CENTRE COMMUNAL D\'ACTION SOCIALE', 'sigle' => 'CCAS', 'user_id' => 1],
            ['libelle' => 'ÉTABLISSEMENT SOCIAL ET MÉDICO-SOCIAL DÉPARTEMENTAL', 'sigle' => '', 'user_id' => 1],
            ['libelle' => 'ÉTABLISSEMENT SOCIAL ET MÉDICO-SOCIAL COMMUNAL', 'sigle' => '', 'user_id' => 1],
            ['libelle' => 'ÉTABLISSEMENT SOCIAL ET MÉDICO-SOCIAL', 'sigle' => '', 'user_id' => 1],
            ['libelle' => 'GROUPEMENT D\'INTÉRÊT PUBLIC', 'sigle' => 'GIP', 'user_id' => 1],
            ['libelle' => 'GROUPEMENT DE COOPÉRATION SOCIALE ET MÉDICO SOCIALE PUBLIC', 'sigle' => 'GCSMS Public', 'user_id' => 1],
            ['libelle' => 'GROUPEMENT DE COOPÉRATION SANITAIRE PUBLIC', 'sigle' => 'GCS Public', 'user_id' => 1],
            ['libelle' => 'AUTRE ÉTABLISSEMENT PUBLIC À CARACTÈRE ADMINISTRATIF', 'sigle' => '', 'user_id' => 1],
            ['libelle' => 'SOCIÉTÉ MUTUALISTE', 'sigle' => '', 'user_id' => 1],
            ['libelle' => 'ASSOCIATION LOI 1901 NON RECONNUE D\'UTILITÉ PUBLIQUE', 'sigle' => '', 'user_id' => 1],
            ['libelle' => 'ASSOCIATION LOI 1901 RECONNUE D\'UTILITÉ PUBLIQUE', 'sigle' => '', 'user_id' => 1],
            ['libelle' => 'FONDATION', 'sigle' => '', 'user_id' => 1],
            ['libelle' => 'GROUPEMENT DE COOPÉRATION SOCIALE ET MÉDICO SOCIALE PRIVÉ', 'sigle' => 'GCSMS Privé', 'user_id' => 1],
            ['libelle' => 'GROUPEMENT DE COOPÉRATION SANITAIRE PRIVÉ', 'sigle' => 'GCS Privé', 'user_id' => 1],
            ['libelle' => 'ORGANISME PRIVÉ À CARACTÈRE COMMERCIAL', 'sigle' => '', 'user_id' => 1],
            ['libelle' => 'AUTRE', 'sigle' => '', 'user_id' => 1],
        ];
        $model::truncate();
        foreach($items as $item){
            $model::create($item);
        }
    }

    public function populateTypesPublic()
    {

        $model = 'App\TypePublic';
        $items = [
            ['libelle' => 'TOUS TYPES DE DÉFICIENCES', 'user_id' => 1],
            ['libelle' => 'DÉFICIENCE INTELLECTUELLE', 'user_id' => 1],
            ['libelle' => 'TROUBLES DU COMPORTEMENT', 'user_id' => 1],
            ['libelle' => 'DÉFICIENCE AUDITIVE', 'user_id' => 1],
            ['libelle' => 'DÉFICIENCE VISUELLE', 'user_id' => 1],
            ['libelle' => 'SURDI-CÉCITÉ', 'user_id' => 1],
            ['libelle' => 'DÉFICIENCE MOTRICE', 'user_id' => 1],
            ['libelle' => 'POLYHANDICAP', 'user_id' => 1],
            ['libelle' => 'PERSONNES AGÉES', 'user_id' => 1],
            ['libelle' => 'ENFANTS ET ADOLESCENTS (AIDE SOCIALE JUSTICE)', 'user_id' => 1],
            ['libelle' => 'ADULTES EN DIFFICULTÉ D\'INSERTION SOCIALE', 'user_id' => 1],
            ['libelle' => 'FAMILLES EN DIFFICULTÉ D\'INSERTION SOCIALE', 'user_id' => 1],
            ['libelle' => 'PERSONNES ET FAMILLES PROTÉGÉES', 'user_id' => 1],
            ['libelle' => 'TROUBLES PSYCHOPATHOLOGIQUES', 'user_id' => 1],
            ['libelle' => 'EPILEPSIE', 'user_id' => 1],
            ['libelle' => 'MALADIE INVALIDANTE DE LONGUE DURÉE', 'user_id' => 1],
            ['libelle' => 'PERSONNES AYANT DES PRATIQUES ADDICTIVES', 'user_id' => 1],
            ['libelle' => 'AUTRE', 'user_id' => 1],
        ];
        $model::truncate();
        foreach ($items as $item) {
            $model::create($item);
        }
    }

    public function populateDetailTypesPublic() {

        $model = 'App\DetailTypePublic';
        $items = [
            ['libelle' => 'DÉFICIENCE INTELLECTUELLE (SAI)', 'type_publics_id' => 2, 'user_id' => 1],
            ['libelle' => 'RETARD MENTAL PROFOND ET SÉVÈRE', 'type_publics_id' => 2, 'user_id' => 1],
            ['libelle' => 'RETARD MENTAL MOYEN', 'type_publics_id' => 2, 'user_id' => 1],
            ['libelle' => 'RETARD MENTAL LÉGER', 'type_publics_id' => 2, 'user_id' => 1],
            ['libelle' => 'AUTRES DÉFICIENCES DE L\’INTELLIGENCE', 'type_publics_id' => 2, 'user_id' => 1],

            ['libelle' => 'TROUBLES DU CARACTÈRE ET DU COMPORTEMENT', 'type_publics_id' => 3, 'user_id' => 1],
            ['libelle' => 'DÉFICIENCE INTERMITTENTE DE LA CONSCIENCE', 'type_publics_id' => 3, 'user_id' => 1],
            ['libelle' => 'DÉFICIENCE GRAVE DE LA COMMUNICATION', 'type_publics_id' => 3, 'user_id' => 1],
            ['libelle' => 'DÉFICIENCE GRAVE DU PSYCHISME', 'type_publics_id' => 3, 'user_id' => 1],
            ['libelle' => 'DÉFICIENCE DU PSYCHISME (SAI)', 'type_publics_id' => 3, 'user_id' => 1],
            ['libelle' => 'AUTRE TROUBLE DU PSYCHISME', 'type_publics_id' => 3, 'user_id' => 1],

            ['libelle' => 'DÉFICIENCE AUDITIVE', 'type_publics_id' => 4, 'user_id' => 1],
            ['libelle' => 'DÉFICIENCE AUDITIVE AVEC TROUBLES ASSOCIÉS', 'type_publics_id' => 4, 'user_id' => 1],

            ['libelle' => 'DÉFICIENCE VISUELLE (SAI)', 'type_publics_id' => 5, 'user_id' => 1],
            ['libelle' => 'DÉFICIENCE VISUELLE AVEC TROUBLES ASSOCIÉS', 'type_publics_id' => 5, 'user_id' => 1],

            ['libelle' => 'SURDI-CÉCITÉ AVEC OU SANS TROUBLES ASSOCIÉS', 'type_publics_id' => 6, 'user_id' => 1],

            ['libelle' => 'DÉFICIENCE MOTRICE SANS TROUBLES ASSOCIÉS', 'type_publics_id' => 7, 'user_id' => 1],
            ['libelle' => 'DÉFICIENCE MOTRICE AVEC TROUBLES ASSOCIÉS', 'type_publics_id' => 7, 'user_id' => 1],

            ['libelle' => 'PERSONNES AGÉES (SAI)', 'type_publics_id' => 9, 'user_id' => 1],
            ['libelle' => 'PERSONNES AGÉES AUTONOMES', 'type_publics_id' => 9, 'user_id' => 1],
            ['libelle' => 'PERSONNES HANDICAPÉES VIEILLISSANTES', 'type_publics_id' => 9, 'user_id' => 1],
            ['libelle' => 'PERSONNES AGÉES DÉPENDANTES', 'type_publics_id' => 9, 'user_id' => 1],

            ['libelle' => 'ENFANTS, ADOLESCENTS ASE ET JUSTICE (SAI)', 'type_publics_id' => 10, 'user_id' => 1],
            ['libelle' => 'ENFANTS D\'AGE PRÉSCOLAIRE ASE 0 À 6 ANS', 'type_publics_id' => 10, 'user_id' => 1],
            ['libelle' => 'ENFANTS D\'AGE SCOLAIRE ASE 6 À 16 ANS', 'type_publics_id' => 10, 'user_id' => 1],
            ['libelle' => 'ADOLESCENTS ET JEUNES MAJEURS ASE 13 À 21 ANS', 'type_publics_id' => 10, 'user_id' => 1],

            ['libelle' => 'ADULTES EN DIFFICULTÉ D\'INSERTION SOCIALE (SAI)', 'type_publics_id' => 11, 'user_id' => 1],
            ['libelle' => 'JEUNES ADULTES EN DIFFICULTÉ', 'type_publics_id' => 11, 'user_id' => 1],
            ['libelle' => 'FEMMES SEULES EN DIFFICULTÉ', 'type_publics_id' => 11, 'user_id' => 1],
            ['libelle' => 'PERSONNES AVEC PROBLÈMES PSYCHIQUES', 'type_publics_id' => 11, 'user_id' => 1],
            ['libelle' => 'FEMMES VICTIMES DE VIOLENCE', 'type_publics_id' => 11, 'user_id' => 1],
            ['libelle' => 'PERSONNES SANS DOMICILE', 'type_publics_id' => 11, 'user_id' => 1],
            ['libelle' => 'AUTRE', 'type_publics_id' => 11, 'user_id' => 1],

            ['libelle' => 'FAMILLES EN DIFFICULTÉ OU SANS LOGEMENT', 'type_publics_id' => 12, 'user_id' => 1],
            ['libelle' => 'PERSONNES SEULES EN DIFFICULTÉS AVEC ENFANT', 'type_publics_id' => 12, 'user_id' => 1],
            ['libelle' => 'FAMILLES EN DIFFICULTÉ ET/OU FEMMES ISOLÉES', 'type_publics_id' => 12, 'user_id' => 1],
            ['libelle' => 'PERSONNES ET FAMILLES DEMANDEURS D\'ASILE', 'type_publics_id' => 12, 'user_id' => 1],
            ['libelle' => 'AUTRE', 'type_publics_id' => 12, 'user_id' => 1],

            ['libelle' => 'MAJEURS PROTÉGÉS', 'type_publics_id' => 13, 'user_id' => 1],
            ['libelle' => 'AUTRE', 'type_publics_id' => 13, 'user_id' => 1],

            ['libelle' => 'TROUBLES PSYCHOPATHOLOGIQUES (SAI)', 'type_publics_id' => 14, 'user_id' => 1],

            ['libelle' => 'EPILEPSIE', 'type_publics_id' => 15, 'user_id' => 1],

            ['libelle' => 'CÉRÉBRO LÉSÉS', 'type_publics_id' => 16, 'user_id' => 1],
            ['libelle' => 'AUTISTES', 'type_publics_id' => 16, 'user_id' => 1],
        ];
        $model::truncate();
        foreach($items as $item){
            $model::create($item);
        }
    }

    public function populateTypesSecteurMS() {

        $model = 'App\TypeSecteurMS';
        $items = [
            ['libelle' => 'HANDICAP', 'handicap' => 0, 'user_id' => 1],
            ['libelle' => 'DIFFICULTÉ SOCIALE', 'handicap' => 1, 'user_id' => 1],
            ['libelle' => 'GÉRONTOLOGIE', 'handicap' => 1, 'user_id' => 1],
            ['libelle' => 'AUTRE SITUATION', 'handicap' => 1, 'user_id' => 1],

        ];
        $model::truncate();
        foreach($items as $item){
            $model::create($item);
        }
    }

    public function populateTypesReception() {

        $model = 'App\TypeReception';
        $items = [
            ['libelle' => 'POSTAL', 'user_id' => 1],
            ['libelle' => 'MAIL', 'user_id' => 1],
            ['libelle' => 'POSTAL + MAIL', 'user_id' => 1],

        ];
        $model::truncate();
        foreach($items as $item){
            $model::create($item);
        }
    }

    public function populateModaliteAccueil() {

        $model = 'App\ModaliteAccueil';
        $items = [
            ['libelle' => 'ACCUEIL DE JOUR', 'MS' => 0, 'user_id' => 1],
            ['libelle' => 'PRESTATION EN MILIEU ORDINAIRE', 'MS' => 0, 'user_id' => 1],
            ['libelle' => 'TRAITEMENT ET CURE AMBULATOIRE', 'MS' => 0, 'user_id' => 1],
            ['libelle' => 'HOSPITALISATION COMPLÈTE', 'MS' => 0, 'user_id' => 1],
            ['libelle' => 'HOSPITALISATION DE JOUR', 'MS' => 0, 'user_id' => 1],

            ['libelle' => 'HÉBERGEMENT COMPLET / INTERNAT', 'MS' => 1, 'user_id' => 1],
            ['libelle' => 'SEMI INTERNAT', 'MS' => 1, 'user_id' => 1],
            ['libelle' => 'EXTERNAT', 'MS' => 1, 'user_id' => 1],
            ['libelle' => 'HÉBERGEMENT DE NUIT ÉCLATÉ', 'MS' => 1, 'user_id' => 1],
            ['libelle' => 'ACCUEIL DE JOUR', 'MS' => 1, 'user_id' => 1],
            ['libelle' => 'PRESTATION EN MILIEU ORDINAIRE', 'MS' => 1, 'user_id' => 1],
            ['libelle' => 'TRAITEMENT ET CURE AMBULATOIRE', 'MS' => 1, 'user_id' => 1],
            ['libelle' => 'FAMILLE D\'ACCUEIL', 'MS' => 1, 'user_id' => 1],
            ['libelle' => 'HÉBERGEMENT TEMPORAIRE', 'MS' => 1, 'user_id' => 1],
            ['libelle' => 'HÉBERGEMENT OU ACCUEIL D\'URGENCE', 'MS' => 1, 'user_id' => 1],


        ];
        $model::truncate();
        foreach($items as $item){
            $model::create($item);
        }
    }

    public function populateTypeEtablissement() {

        $model = 'App\TypeEtablissement';
        $items = [
            ['libelle' => 'INSTITUT MÉDICO-EDUCATIF', 'sigle' => 'IME',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 1, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'ETABLISSEMENT POUR ENFANTS OU ADOLESCENTS POLYHANDICAPÉS', 'sigle' => '',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 1, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'INSTITUT THÉRAPEUTIQUE ÉDUCATIF ET PÉDAGOGIQUE', 'sigle' => 'ITEP',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 1, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'ETABLISSEMENT POUR DÉFICIENT MOTEUR', 'sigle' => 'IEM',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 1, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'INSTITUT POUR DÉFICIENTS VISUELS', 'sigle' => '',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 1, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'INSTITUT POUR DÉFICIENTS AUDITIFS', 'sigle' => '',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 1, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'INSTITUT D\'EDUCATION SENSORIELLE SOURD/AVEUGLE', 'sigle' => '',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 1, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'ETABLISSEMENT D\'ACCUEIL TEMPORAIRE D\'ENFANTS HANDICAPÉS ', 'sigle' => '',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 1, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'FOYER HÉBERGEMENT ENFANTS ET ADOLESCENTS HANDICAPÉS', 'sigle' => '',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 1, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'SERVICE D\'ÉDUCATION SPÉCIALE ET DE SOINS À DOMICILE', 'sigle' => 'SESAD',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 1, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'CENTRE MÉDICO-PSYCHO-PÉDAGOGIQUE', 'sigle' => 'CMPP',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 1, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'CENTRE ACTION MÉDICO-SOCIALE PRÉCOCE', 'sigle' => 'CAMSP',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 1, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'FOYER HÉBERGEMENT ADULTES HANDICAPÉS', 'sigle' => 'FH',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 1, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'FOYER D\'ACCUEIL POLYVALENT POUR ADULTES HANDICAPÉS', 'sigle' => '',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 1, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'MAISON D\'ACCUEIL SPÉCIALISÉE', 'sigle' => 'MAS',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 1, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'FOYER DE VIE POUR ADULTES HANDICAPÉS', 'sigle' => 'FV',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 1, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'ETABLISSEMENT D\'ACCUEIL TEMPORAIRE POUR ADULTES HANDICAPÉS', 'sigle' => '',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 1, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'FOYER D\'ACCUEIL MÉDICALISÉ POUR ADULTES HANDICAPÉS', 'sigle' => 'FAM',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 1, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'ETABLISSEMENT ET SERVICE D\'AIDE PAR LE TRAVAIL', 'sigle' => 'ESAT',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 1, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'ENTREPRISE ADAPTÉE', 'sigle' => 'EA',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 1, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'CENTRE DE PRÉ ORIENTATION POUR HANDICAPÉS', 'sigle' => '',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 1, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'CENTRE RÉÉDUCATION PROFESSIONNELLE ', 'sigle' => '',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 1, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'ETABLISSEMENT EXPÉRIMENTAL POUR ADULTES HANDICAPÉS', 'sigle' => '',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 1, 'type_age_id' => 2, 'user_id' => 1],

            ['libelle' => 'SERVICE D\'ACCOMPAGNEMENT MÉDICO-SOCIAL ADULTES HANDICAPÉS', 'sigle' => 'SAMSAH',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 1, 'type_age_id' => 2, 'user_id' => 1],

            ['libelle' => 'SERVICE D\'ACCOMPAGNEMENT À LA VIE SOCIALE', 'sigle' => 'SAVS',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 1, 'type_age_id' => 2, 'user_id' => 1],

            ['libelle' => 'UNITÉ D\'EVALUATION ET DE RÉENTRAINEMENT ET D\'ORIENTATION SOCIO PROFESSIONNELLE', 'sigle' => 'UEROS',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 1, 'type_age_id' => 2, 'user_id' => 1],



            ['libelle' => 'ETABLISSEMENT ACCUEIL MÈRE-ENFANT', 'sigle' => '',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 2, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'POUPONIÈRE À CARACTÈRE SOCIAL', 'sigle' => '',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 2, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'FOYER DE L\'ENFANCE', 'sigle' => '',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 2, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'VILLAGE D\'ENFANTS', 'sigle' => '',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 2, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'MAISON D\'ENFANTS À CARATÈRE SOCIAL', 'sigle' => 'MECS',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 2, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'SERVICE D\'ACTION EDUCATIVE EN MILIEU OUVERT', 'sigle' => 'AEMO',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 2, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'ETABLISSEMENT EXPÉRIMENTAL ENFANCE PROTÉGÉE', 'sigle' => '',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 2, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'SERVICE D\'ENQUÊTES SOCIALES', 'sigle' => '',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 2, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'LIEUX DE VIE', 'sigle' => '',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 2, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'FOYER D\'ACTION EDUCATIVE', 'sigle' => 'FAE',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 2, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'SERVICE EDUCATIF AUPRÈS DES TRIBUNAUX', 'sigle' => 'SEAT',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 2, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'CENTRE D\'ACTION EDUCATIVE', 'sigle' => 'CAE',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 2, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'CENTRE DE PLACEMENT FAMILIAL SOCIO EDUCATIF', 'sigle' => '',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 2, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'AUTRE', 'sigle' => '',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 2, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'CENTRE D\'HÉBERGEMENT ET DE RÉINSERTION SOCIALE', 'sigle' => 'CHRS',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 2, 'type_age_id' => 2, 'user_id' => 1],

            ['libelle' => 'CENTRE D\'ACCUEIL DEMANDEURS D\'ASILE', 'sigle' => 'CADA',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 2, 'type_age_id' => 2, 'user_id' => 1],

            ['libelle' => 'FOYER DE JEUNES TRAVAILLEURS', 'sigle' => 'FJT',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 2, 'type_age_id' => 2, 'user_id' => 1],

            ['libelle' => 'RÉSIDENCE SOCIALE', 'sigle' => '',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 2, 'type_age_id' => 2, 'user_id' => 1],

            ['libelle' => 'MAISON RELAIS - PENSIONS DE FAMILLE', 'sigle' => '',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 2, 'type_age_id' => 2, 'user_id' => 1],

            ['libelle' => 'LIEUX DE VIE', 'sigle' => '',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 2, 'type_age_id' => 2, 'user_id' => 1],

            ['libelle' => 'SERVICE TUTELLE PRESTATION SOCIALE ', 'sigle' => '',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 2, 'type_age_id' => 2, 'user_id' => 1],

            ['libelle' => 'SERVICE MANDATAIRE JUDICIAIRE À LA PROTECTION DES MAJEURS', 'sigle' => 'MJPM',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 2, 'type_age_id' => 2, 'user_id' => 1],

            ['libelle' => 'CLUB ÉQUIPE PRÉVENTION', 'sigle' => '',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 2, 'type_age_id' => 2, 'user_id' => 1],

            ['libelle' => 'AUTRE', 'sigle' => '',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 2, 'type_age_id' => 2, 'user_id' => 1],

            ['libelle' => 'ETABLISSEMENT D\'HÉBERGEMENT POUR PERSONNES AGÉES DÉPENDANTES', 'sigle' => 'EHPAD',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 3, 'type_age_id' => 2, 'user_id' => 1],

            ['libelle' => 'RÉSIDENCE AUTONOMIE', 'sigle' => '',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 3, 'type_age_id' => 2, 'user_id' => 1],

            ['libelle' => 'ETABLISSEMENT D\'HÉBERGEMENT POUR PERSONNES AGÉES', 'sigle' => 'EHPA',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 3, 'type_age_id' => 2, 'user_id' => 1],

            ['libelle' => 'CENTRE LOCAUX INFORMATION COORDINATION', 'sigle' => 'CLIC',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 3, 'type_age_id' => 2, 'user_id' => 1],

            ['libelle' => 'AUTRE', 'sigle' => '',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 3, 'type_age_id' => 2, 'user_id' => 1],

            ['libelle' => 'CENTRE DE SOINS ACCOMPAGNEMENT PRÉVENTION ADDICTOLOGIE', 'sigle' => 'CSAPA',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 4, 'type_age_id' => 2, 'user_id' => 1],

            ['libelle' => 'CENTRE ACCUEIL/ACCOMPAGNEMENT À LA RÉDUCTION DES RISQUES DE L\'USAGE DE DROGUES', 'sigle' => 'CAARUD',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 4, 'type_age_id' => 2, 'user_id' => 1],

            ['libelle' => 'SERVICE DE SOINS INFIMIERS À DOMICILE', 'sigle' => 'SSIAD',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 4, 'type_age_id' => 2, 'user_id' => 1],

            ['libelle' => 'SERVICE POLYVALENT AIDE ET SOINS A DOMICILE', 'sigle' => 'SPASAD',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 4, 'type_age_id' => 2, 'user_id' => 1],

            ['libelle' => 'CENTRE DE RESSOURCES', 'sigle' => '',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 4, 'type_age_id' => 2, 'user_id' => 1],

            ['libelle' => 'LITS HALTE SOINS SANTÉ', 'sigle' => 'LHSS',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 4, 'type_age_id' => 2, 'user_id' => 1],

            ['libelle' => 'AUTRE', 'sigle' => '',
                'type_secteur_id' => 2, 'type_secteur_ms_id' => 4, 'type_age_id' => 2, 'user_id' => 1],


            ['libelle' => 'CENTRE MÉDICO-PSYCHOLOGIQUE', 'sigle' => 'CMP',
                'type_secteur_id' => 1, 'type_secteur_ms_id' => null, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'CENTRE D\'ACCUEIL THÉRAPEUTIQUE À TEMPS PARTIEL', 'sigle' => 'CATTP',
                'type_secteur_id' => 1, 'type_secteur_ms_id' => null, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'AUTRE', 'sigle' => '',
                'type_secteur_id' => 1, 'type_secteur_ms_id' => null, 'type_age_id' => 1, 'user_id' => 1],

            ['libelle' => 'CENTRE MÉDICO-PSYCHOLOGIQUE', 'sigle' => 'CMP',
                'type_secteur_id' => 1, 'type_secteur_ms_id' => null, 'type_age_id' => 2, 'user_id' => 1],

            ['libelle' => 'CENTRE D\'ACCUEIL THÉRAPEUTIQUE À TEMPS PARTIEL', 'sigle' => 'CATTP',
                'type_secteur_id' => 1, 'type_secteur_ms_id' => null, 'type_age_id' => 2, 'user_id' => 1],

            ['libelle' => 'APPARTEMENT THÉRAPEUTIQUE', 'sigle' => '',
                'type_secteur_id' => 1, 'type_secteur_ms_id' => null, 'type_age_id' => 2, 'user_id' => 1],

            ['libelle' => 'AUTRE', 'sigle' => '',
                'type_secteur_id' => 1, 'type_secteur_ms_id' => null, 'type_age_id' => 2, 'user_id' => 1],

            ['libelle' => 'CENTRE HOSPITALIER', 'sigle' => 'CH',
                'type_secteur_id' => 1, 'type_secteur_ms_id' => null, 'type_age_id' => 3, 'user_id' => 1],

            ['libelle' => 'CENTRE HOPSITALIER SPÉCIALISÉ', 'sigle' => 'CHS',
                'type_secteur_id' => 1, 'type_secteur_ms_id' => null, 'type_age_id' => 3, 'user_id' => 1],

            ['libelle' => 'SOINS DE SUITE ET DE RÉADAPTATION', 'sigle' => 'SSR',
                'type_secteur_id' => 1, 'type_secteur_ms_id' => null, 'type_age_id' => 3, 'user_id' => 1],

            ['libelle' => 'MAISON DE SANTÉ', 'sigle' => '',
                'type_secteur_id' => 1, 'type_secteur_ms_id' => null, 'type_age_id' => 3, 'user_id' => 1],

            ['libelle' => 'CENTRES DE SANTÉ', 'sigle' => '',
                'type_secteur_id' => 1, 'type_secteur_ms_id' => null, 'type_age_id' => 3, 'user_id' => 1],

            ['libelle' => 'AUTRE', 'sigle' => '',
                'type_secteur_id' => 1, 'type_secteur_ms_id' => null, 'type_age_id' => 3, 'user_id' => 1],

        ];
        $model::truncate();
        foreach($items as $item){
            $model::create($item);
        }
    }

}
