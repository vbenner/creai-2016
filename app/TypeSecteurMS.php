<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeSecteurMS extends Model
{
    /**
     * @return array
     */
    protected $table = 'type_secteurs_ms';
    protected $fillable = ['libelle', 'handicap', 'active', 'user_id'];

}
