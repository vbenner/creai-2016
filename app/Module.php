<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{

    protected $table = 'modules';

    /** -------------------------------------------------------------------------------------------
     * Un module appartient à 1 et 1 seul BLOC
     */
    public function bloc() {
        return $this->belongsTo(Bloc::class);
    }
    
    public function submodules() {
        return $this->hasMany(Submodule::class);
    }
}
