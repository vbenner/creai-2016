<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeStatutOrganisme extends Model
{
    /**
     * @return array
     */
    protected $table = 'type_statuts_organismes';
    protected $fillable = ['libelle', 'sigle', 'active', 'user_id'];

}
