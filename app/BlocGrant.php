<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlocGrant extends Model
{
    protected $table = 'blocs_grants';
    protected $fillable = [
        'bloc_id', 'user_id', 'grant', 'edit', 'add'
    ];

    public function blocs() {
        return $this->belongsTo(Bloc::class, 'bloc_id');
    }

    public function users() {
        return $this->belongsTo(User::class, 'user_id');
    }

}
