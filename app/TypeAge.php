<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeAge extends Model
{
    /**
     * @return array
     */
    protected $table = 'type_ages';
    protected $fillable = ['libelle', 'active', 'user_id'];
}
