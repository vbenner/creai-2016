<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeMail extends Model
{
    /**
     * @return array
     */
    protected $table = 'type_mails';
    protected $fillable = ['libelle', 'defaut', 'active', 'user_id'];

}
