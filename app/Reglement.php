<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reglement extends Model
{

    /** -------------------------------------------------------------------------------------------
     * @return array
     */
    protected $table = 'reglements';
    protected $fillable = [
        'campagne_id', 'oo_id', 'pp_id', 'pe_id',
        'type_cotisation_id', 'type_contribution_id', 'type_reglement_id',
        'montant', 'date', 'reference', 'user_id',
    ];

    public function typeCotisation()
    {
        return $this->belongsTo('App\TypeCotisation');
    }

    public function typeContribution()
    {
        return $this->belongsTo('App\TypeContribution');
    }

    public function typeReglement()
    {
        return $this->belongsTo(TypeReglement::class, 'type_reglement_id');
    }
}
