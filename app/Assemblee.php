<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assemblee extends Model
{
    protected $table = 'assemblees';
    protected $fillable = [
        'annee', 'date_ag', 'notes'
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

}
