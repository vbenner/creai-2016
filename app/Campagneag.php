<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campagneag extends Model
{
    protected $table = 'campagneags';
    protected $fillable = [
        'assemblee_id', 'user_id'
    ];

    public function assemblee() {
        return $this->belongsTo(Assemblee::class, 'assemblee_id');
    }

}
