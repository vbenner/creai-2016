<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeRoleBureau extends Model
{
    /**
     * @return array
     */
    protected $table = 'type_roles_bureau';
    protected $fillable = ['libelle', 'ordre', 'active', 'user_id'];

}
