<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Civilite extends Model
{
    /**
     * @return array
     */
    protected $table = 'civilites';
    protected $fillable = ['libelle', 'active', 'user_id'];

}
