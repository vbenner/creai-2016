<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//Route::resource('/annuaire/et/{id?}', 'EtablissementController@show');

Route::group(['middleware' => 'auth'], function(){

    Route::get('/download/{type}/{filename}', 'DownloadController@downloadFile');

    Route::get('/', 'DashboardController@index');
    Route::get('dashboard', 'DashboardController@index');
    Route::get('home', 'DashboardController@index');

    /** -------------------------------------------------------------------------------------------
     * EXPLOITATION -> Gestion des MODULES et SOUS-MODULES
     */

    Route::group(['prefix' => 'activite'], function() {
        Route::resource('ag', 'AssembleeController');
        Route::resource('cotis', 'CotisationController');
        Route::post('validcotis/{type}/{id}', 'CotisationController@validCotis');
        Route::post('filter/ariane', 'FiltreController@getAriane');
        #Route::get('cotis', 'CotisationController');
    });

    Route::group(['prefix' => 'annuaire'], function() {

        /** -------------------------------------------------------------------------------------------
         * Switch + Grants
         */
        Route::get('pp/switch/{id}/edit', 'PersonnePhysiqueController@switchStatut');
        Route::get('oo/switch/{id}/edit', 'OrganismeController@switchStatut');
        Route::get('po/switch/{id}/edit', 'PoleController@switchStatut');
        Route::get('et/switch/{id}/edit', 'EtablissementController@switchStatut');
        Route::get('ea/switch/{id}/edit', 'AdresseAccueilController@switchStatut');

        /** -------------------------------------------------------------------------------------------
         * LOGOS
         */
        Route::post('oo/logo', 'OrganismeController@uploadLogo');
        Route::post('oo/logo/del', 'OrganismeController@deleteLogo');

        Route::resource('detail_modalite', 'EtablissementModaliteDetailController');
        Route::resource('etablissement_modalites', 'EtablissementModaliteController');

        /** -------------------------------------------------------------------------------------------
         * Resful
         */
        Route::resource('/', 'OrganismeController');
        Route::resource('pp', 'PersonnePhysiqueController');
        Route::resource('oo', 'OrganismeController');
        Route::resource('po', 'PoleController');
        Route::resource('et', 'EtablissementController');
        Route::resource('aa', 'AdresseAccueilController');
        Route::resource('an', 'AntenneController');
        Route::resource('pe', 'PersonnelController');
        Route::resource('telephone', 'TelephoneController');
        Route::resource('mail', 'MailController');
        Route::resource('offre', 'OffreController');

    });

    Route::group(['prefix' => 'datacopy'], function () {
        Route::resource('oo', 'OrganismeController@getData');
        Route::resource('po', 'PoleController@getData');
        Route::resource('et', 'EtablissementController@getData');
        Route::resource('an', 'AntenneController@getData');
    });

    /** -------------------------------------------------------------------------------------------
     * PARAMS -> Gestion des MODULES et SOUS-MODULES
     */
    Route::group(['prefix' => 'param'], function() {

        Route::put('users/grants/{type}/{field}/{id}/{user}/{value}', 'UserController@updateGrant');

        Route::resource('civilite', 'CiviliteController');
        Route::resource('pays', 'PaysController');
        Route::resource('fonction', 'FonctionController');
        Route::resource('typetel', 'TypeTelController');
        Route::resource('typerolebureau', 'TypeRoleBureauController');
        Route::resource('typemail', 'TypeMailController');
        Route::resource('typeroleag', 'TypeRoleAGController');
        Route::resource('typeroleca', 'TypeRoleCAController');
        Route::resource('typeroleusager', 'TypeRoleUsagerController');
        Route::resource('typesecteur', 'TypeSecteurController');
        Route::resource('typepole', 'TypePoleController');
        Route::resource('typeage', 'TypeAgeController');
        Route::resource('typestatutorganisme', 'TypeStatutOrganismeController');
        Route::resource('typepublic', 'TypePublicController');
        Route::resource('detailtypepublic', 'DetailTypePublicController');
        Route::resource('detailtypeorganisme', 'DetailTypeOrganismeController');
        Route::resource('typeorganisme', 'TypeOrganismeController');
        Route::resource('typeetablissement', 'TypeEtablissementController');
        Route::resource('typesecteurms', 'TypeSecteurMSController');
        Route::resource('typereception', 'TypeReceptionController');
        Route::resource('modaliteaccueil', 'ModaliteAccueilController');
        Route::resource('user', 'UserController');
        Route::resource('typecotisation', 'TypeCotisationController');
        Route::resource('typecontribution', 'TypeContributionController');
        Route::resource('typereglement', 'TypeReglementController');
    });

    /** -------------------------------------------------------------------------------------------
     * Gestion des DATATABLES
     */
    Route::group(['prefix' => 'datatable'], function() {

        Route::post('filtreoo', 'FiltreController@listDatatableOO');

        Route::get('civilite', 'CiviliteController@listDatatable');
        Route::get('fonction', 'FonctionController@listDatatable');
        Route::get('pays', 'PaysController@listDatatable');
        Route::get('typetel', 'TypeTelController@listDatatable');
        Route::get('typerolebureau', 'TypeRoleBureauController@listDatatable');
        Route::get('typemail', 'TypeMailController@listDatatable');
        Route::get('typeroleag', 'TypeRoleAGController@listDatatable');
        Route::get('typeroleca', 'TypeRoleCAController@listDatatable');
        Route::get('typeroleusager', 'TypeRoleUsagerController@listDatatable');
        Route::get('typesecteur', 'TypeSecteurController@listDatatable');
        Route::get('typepole', 'TypePoleController@listDatatable');
        Route::get('typeage', 'TypeAgeController@listDatatable');
        Route::get('typestatutorganisme', 'TypeStatutOrganismeController@listDatatable');
        Route::get('typepublic', 'TypePublicController@listDatatable');
        Route::get('detailtypepublic/{id}', 'DetailTypePublicController@listDatatable');
        Route::get('detailtypeorganisme/{id}', 'DetailTypeOrganismeController@listDatatable');
        Route::get('typeorganisme', 'TypeOrganismeController@listDatatable');
        Route::get('typeetablissement', 'TypeEtablissementController@listDatatable');
        Route::get('typesecteurms', 'TypeSecteurMSController@listDatatable');
        Route::get('typereception', 'TypeReceptionController@listDatatable');
        Route::get('modaliteaccueil', 'ModaliteAccueilController@listDatatable');
        Route::get('user', 'UserController@listDatatable');
        Route::get('typecotisation', 'TypeCotisationController@listDatatable');
        Route::get('typecontribution', 'TypeContributionController@listDatatable');
        Route::get('typereglement', 'TypeReglementController@listDatatable');
        Route::get('user/grants/{id}', 'UserController@listGrants');

        /** ---------------------------------------------------------------------------------------
         * Attention, simplification des routes
         * pp = PersonnePhysique
         * oo = Organisme
         * po = Pôle
         * et = Etablissement
         * aa = Adresse Accueil
         * an = Antenne
         */
        Route::get('pp', 'PersonnePhysiqueController@listDatatable');
        Route::get('pp/mails/{id}', 'MailController@listeMailsPP');
        Route::get('pp/telephones/{id}', 'TelephoneController@listeTelsPP');

        Route::get('oo', 'OrganismeController@listDatatable');
        Route::get('oo/mails/{id}', 'MailController@listeMailsOO');
        Route::get('oo/telephones/{id}', 'TelephoneController@listeTelsOO');
        Route::get('oo/offres/{id}', 'OffreController@listeOffresOO');

        Route::get('po/{id}', 'PoleController@listDatatable');
        Route::get('po/mails/{id}', 'MailController@listeMailsPO');
        Route::get('po/telephones/{id}', 'TelephoneController@listeTelsPO');
        Route::get('po/offres/{id}', 'OffreController@listeOffresPO');

        // ----------------------------------------------------------------------------------------
        // On gère la route des téléphones avant celle des types !
        Route::get('et/telephones/{id}', 'TelephoneController@listeTelsET');
        Route::get('et/mails/{id}', 'MailController@listeMailsET');
        Route::get('et/offres/{id}', 'OffreController@listeOffresET');
        Route::get('et/{type}/{id}', 'EtablissementController@listDatatable');

        // ----------------------------------------------------------------------------------------
        // AA = ADRESSE ACCUEIL
        // On gère la route des téléphones avant celle des types !
        Route::get('aa/telephones/{id}', 'TelephoneController@listeTelsAA');
        Route::get('aa/mails/{id}', 'MailController@listeMailsAA');
        Route::get('aa/{type}/{id}', 'AdresseAccueilController@listDatatable');

        // ----------------------------------------------------------------------------------------
        // AN = ANTENNES
        // On gère la route des téléphones avant celle des types !
        Route::get('an/telephones/{id}', 'TelephoneController@listeTelsAN');
        Route::get('an/mails/{id}', 'MailController@listeMailsAN');
        Route::get('an/offres/{id}', 'OffreController@listeOffresAN');
        Route::get('an/{id}', 'AntenneController@listDatatable');

        // ----------------------------------------------------------------------------------------
        // PE = PERSONNEL
        // On gère la route des téléphones avant celle des types !
        Route::get('pe/telephones/{id}', 'TelephoneController@listeTelsPE');
        Route::get('pe/mails/{id}', 'MailController@listeMailsPE');
        Route::get('pe/{type}/{id}', 'PersonnelController@listDatatable');

        // ----------------------------------------------------------------------------------------
        // AG = ASSEMBLEE GENERALE
        Route::get('ag', 'AssembleeController@listDatatable');

        // ----------------------------------------------------------------------------------------
        // COTIS = COTISANTS
        Route::get('cotis/{annee}', 'CotisationController@listDatatable');
        Route::get('cotisres/{annee}', 'CotisationController@listDatatableRes');
    });

    Route::group(['prefix' => 'list'], function() {
        Route::get('civilite', 'CiviliteController@listZL');
        Route::get('fonction', 'FonctionController@listZL');
        Route::get('pays', 'PaysController@listZL');
        Route::get('reception', 'TypeReceptionController@listZL');
        Route::get('role', 'RoleController@listZL');

        Route::get('typeorganisme', 'TypeOrganismeController@listZL');
        Route::get('typestatutorganisme', 'TypeStatutOrganismeController@listZL');

        Route::get('typecotisation', 'TypeCotisationController@listZL');
        Route::get('typereglement', 'TypeReglementController@listZL');

        Route::get('typepole', 'TypePoleController@listZL');
        Route::get('typeetablissement', 'TypeEtablissementController@listZL');
        Route::get('typepublic', 'TypePublicController@listZL');
        Route::get('detailtypepublic/', 'DetailTypePublicController@listZLAll');
        Route::get('detailtypepublic/{id}', 'DetailTypePublicController@listZL');
        Route::get('detailtypeorganisme/{id}', 'DetailTypeOrganismeController@listZL');

        Route::get('typeage', 'TypeAgeController@listZL');
        Route::get('modaliteaccueil', 'ModaliteAccueilController@listZLAll');
        Route::get('modaliteaccueil/{type}', 'ModaliteAccueilController@listZL');
        Route::get('modaliteaccueilEtablissement/{id}', 'ModaliteAccueilController@listZLEtablissement');
        Route::get('typesecteur', 'TypeSecteurController@listZL');
        Route::get('typesecteurms', 'TypeSecteurMSController@listZL');

        Route::get('roleag', 'TypeRoleAGController@listZL');
        Route::get('rolebureau', 'TypeRoleBureauController@listZL');
        Route::get('roleca', 'TypeRoleCAController@listZL');
        Route::get('roleusager', 'TypeRoleUsagerController@listZL');
        Route::get('typetel', 'TypeTelController@listZL');
        Route::get('typemail', 'TypeMailController@listZL');

        Route::get('etablissements', 'EtablissementController@listZL');
        Route::get('poles/{organisme}', 'PoleController@listZL');
    });

});


Route::auth();

