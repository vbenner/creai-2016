<?php

namespace App\Http\Controllers;

use App\TypeCotisation;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use App\Http\Requests;

class TypeCotisationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('params.typecotisation', [
            'titre' => 'Paramétrage',
            'subtitre' => 'Types de Cotisations',
            'sidebar' => $this->getSidebar()//json_decode(json_encode($tabBlocs))
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $type = TypeCotisation::find($id);
        return Response::json($type);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $type = TypeCotisation::firstOrNew([
            'libelle' => $request->pLibelle
        ]);
        $type->montant = $request->pMontant;
        $type->libre = $request->pLibre;
        $type->user_id = Auth::user()->id;
        $type->save();
        return Response::json($type->id);    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $type = TypeCotisation::find($id);
        $type->active = $request->pValue;
        $type->user_id = Auth::user()->id;
        $type->save();
        return Response::json($id);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /** ---------------------------------------------------------------------------------------
         * Recherche d'un enregistrement identique avec un autre ID
         */
        $type = TypeCotisation::where('libelle', 'like', $request->pLibelle)
            ->where('id', '<>', $id)
            ->first();
        if ($type === null) {
            $type = TypeCotisation::find($id);
            $type->libelle = $request->pLibelle;
            $type->montant = $request->pMontant;
            $type->libre = $request->pLibre;
            $type->user_id = Auth::user()->id;
            $type->save();
            return Response::json($id);
        } else {
            return Response::json(-1);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listDatatable() {
        $types = TypeCotisation::all();

        /** @var Initialisation du tableau de sortie $result */
        $result = ['aaData' => []];

        /** @var On parcourt les logs $infoPatch */
        foreach($types as $type){


            /** -----------------------------------------------------------------------------------
             * Données
             */
            $id = $type->id;
            $libelle = $type->libelle;

            /** -----------------------------------------------------------------------------------
             * Actions
             */
            $libre =  $this->buildSwitch($id, $type->libre);
            $active =  $this->buildSwitch($id, $type->active);
            $action = '<a class="modif btn btn-xs blue" data-id="'.$id.'"><i class="fa fa-edit"></i></a>';
            $result['aaData'][] = [
                $libelle,
                $libre,
                $active,
                $action
            ];
        }

        return Response::json($result);
    }

    public function listZL() {

        $cotisations = TypeCotisation::where('active', '=', 1)->orderBy('id')->get();
        $result = array();
        foreach($cotisations as $cotisation){
            array_push($result, array(
                'id'   => $cotisation->id,
                'libelle'  => $cotisation->libelle,
                'libre'  => $cotisation->libre,
                'montant'  => $cotisation->montant,
            ));
        }
        return Response::json(array('LISTE' => $result));
    }

}
