<?php

namespace App\Http\Controllers;

use App\EtablissementModaliteDetail;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class EtablissementModaliteDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $record = new EtablissementModaliteDetail();
        $record->etablissement_modalite_id = $request->pID_Detail;
        $record->modalite_accueil_id = $request->pID_Mod;
        $record->type_public_id = $request->pID_Pub;
        $record->detail_type_public_id = ($request->pID_PubDet != 0 ? $request->pID_PubDet : null);
        $record->nb_places = $request->pNB;
        $record->active = 1;
        $record->user_id = Auth::user()->id;
        $record->save();

        return Response::json($record->id);    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        EtablissementModaliteDetail::find($id)->delete();
    }
}
