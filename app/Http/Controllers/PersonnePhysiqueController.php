<?php

namespace App\Http\Controllers;

use App\CampagneagCotisant;
use App\Reglement;
use App\Telephone;
use App\TelephoneOwner;
use App\Mail;
use App\MailOwner;
use App\TypeTel;
use App\User;
use App\Civilite;
use App\Pays;
use App\PersonnePhysique;
use App\Http\Controllers\MailOwnerController;
use App\Http\Controllers\TelephoneController;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;

class PersonnePhysiqueController extends Controller
{

    /**
     * @return array
     */
    /*
    protected $table = 'personnes_physiques';
    protected $fillable = ['id_interne', 'civilite_id', 'nom', 'prenom',
        'usager', 'etablissement_id', 'type_role_usager_id',
        'adr1', 'adr2', 'adr3', 'cp', 'ville', 'pays_id',
        'voeux', 'nl', 'ag', 'role_ag_id',
        'cf', 'cf_reception_id',
        'buro', 'role_buro_id', 'ca', 'role_ca_id',
        'bi', 'bi_reception_id',
        'cotis', 'notes', 'user_id', 'history',
        'active', 'visible', 'acces',
        'user_id'];
    */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('annuaire.pp', [
            'titre' => 'Annuaire',
            'subtitre' => 'Personnes Physiques / Usagers',
            'sidebar' => $this->getSidebar()//json_decode(json_encode($tabBlocs))
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        /** -----------------------------------------------------------------------------------
         * Données de base
         */
        $pp = PersonnePhysique::firstOrNew([
            'nom' => $request->pNom,
            'prenom' => $request->pPrenom,
            'id_interne' => $request->pCode
        ]);
        $pp->usager = $request->pUsager;
        $pp->etablissement_id = ($request->pEtablissement == '' ? null : $request->pEtablissement);
        $pp->type_role_usager_id = ($request->pTypeUsager == '' ? null : $request->pTypeUsager);
        $pp->civilite_id = ($request->pCivilite != '' ? $request->pCivilite : null);
        $pp->adr1 = $request->pAdr1;
        $pp->adr2 = $request->pAdr2;
        $pp->adr3 = $request->pAdr3;
        $pp->cp = $request->pCP;
        $pp->ville = $request->pVille;
        $pp->pays_id = ($request->pPays == '' ? null : $request->pPays);

        $pp->active = $request->pVisible;
        $pp->visible = $request->pVisible;
        $pp->acces = $request->pAcces;

        $pp->voeux = $request->pVoeux;
        $pp->nl = $request->pNL;
        $pp->ag = $request->pAG;
        $pp->role_ag_id = ($request->pAG_Role == '' ? null : $request->pAG_Role);
        $pp->cf = $request->pCF;
        $pp->cf_reception_id = ($request->pCF_Recep == '' ? null : $request->pCF_Recep);
        $pp->buro = $request->pBU;
        $pp->role_buro_id = ($request->pBU_Role == '' ? null : $request->pBU_Role);
        $pp->ca = $request->pCA;
        $pp->role_ca_id = ($request->pCA_Role == '' ? null : $request->pCA_Role);
        $pp->bi = $request->pBI;
        $pp->bi_reception_id = ($request->pBI_Recep == '' ? null : $request->pBI_Recep);
        $pp->cotis = $request->pCOTIS;
        $pp->notes = $request->pNotes;
        $pp->user_id = Auth::user()->id;
        $pp->history = '';

        $pp->save();

        /** -----------------------------------------------------------------------------------
         * Traitement des mails et des téléphones
         * Pour chaque téléphone, on va faire une première vérification dans la table
         *
         */
        if (isset($request->pTels)) {
            foreach ($request->pTels as $key => $val) {
                $num = $val[0];
                $type = $val[1];
                $tel = Telephone::where('numero', '=', $num)
                    ->first();
                if ($tel === null) {
                    $tel = new Telephone();
                    $tel->numero = $num;
                    $tel->typetel_id = $type;
                    $tel->user_id = Auth::user()->id;
                    $tel->save();
                } else {
                    $tel->typetel_id = $type;
                    $tel->user_id = Auth::user()->id;
                    $tel->save();
                }
                $owner = new TelephoneOwner();
                $owner->telephone_id = $tel->id;
                $owner->pp_id = $pp->id;
                $owner->user_id = Auth::user()->id;
                $owner->save();
            }
        }

        if (isset($request->pMails)) {
            foreach ($request->pMails as $key => $val) {
                $adr = $val[0];
                $type = $val[1];
                $mail = Mail::where('mail', '=', $adr)
                    ->first();
                if ($mail === null) {
                    $mail = new Mail();
                    $mail->mail = $adr;
                    $mail->typemail_id = $type;
                    $mail->user_id = Auth::user()->id;
                    $mail->save();
                } else {
                    $mail->typemail_id = $type;
                    $mail->user_id = Auth::user()->id;
                    $mail->save();
                }
                $owner = new MailOwner();
                $owner->mail_id = $mail->id;
                $owner->user_id = Auth::user()->id;
                $owner->pp_id = $pp->id;
                $owner->save();
            }
        }
        return Response::json($pp->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pp = PersonnePhysique::find($id);

        /** ---------------------------------------------------------------------------------------
         * On prend aussi la liste des téléphones
         */
        $listeTels = DB::table('telephones')
            ->join('type_tels', 'type_tels.id', '=', 'telephones.typetel_id')
            ->join('telephone_owners', 'telephones.id', '=', 'telephone_owners.telephone_id')
            ->join('personnes_physiques', 'personnes_physiques.id', '=', 'telephone_owners.pp_id')
            ->select('numero', 'type_tels.id AS TYPE_ID',
                'type_tels.libelle', 'telephone_owners.pp_id AS PP_ID',
                'telephone_owners.ID AS OWN_ID', 'telephone_owners.notes')
            ->where('pp_id', '=', $id)
            ->get();

        /** ---------------------------------------------------------------------------------------
         * On prend aussi la liste des mails
         */
        $listeMails = DB::table('mails')
            ->join('type_mails', 'type_mails.id', '=', 'mails.typemail_id')
            ->join('mail_owners', 'mails.id', '=', 'mail_owners.mail_id')
            ->join('personnes_physiques', 'personnes_physiques.id', '=', 'mail_owners.pp_id')
            ->select('mail', 'type_mails.id AS TYPE_ID',
                'type_mails.libelle', 'mail_owners.pp_id AS PP_ID',
                'mail_owners.ID AS OWN_ID')
            ->where('pp_id', '=', $id)
            ->get();

        /** ---------------------------------------------------------------------------------------
         * On va récupérer toutes les COTISATIONS
         */
        $listeCotisations = [];
        $campagnes = CampagneagCotisant::where('pp_id', '=', $pp->id)->get();
        foreach ($campagnes as $campagne) {

            /** -----------------------------------------------------------------------------------
             * Y'a-'il des règlements ?
             */
            $reglement = Reglement::where('campagne_id', '=', $campagne->id)->get()->first();

            #echo '<pre>';
            #echo 'ID'.$campagne->id.'<br/><br/>';
            #print_r($reglement);
            #echo '</pre>';
            #die();

            $detailReglement = [];
            if ($reglement != null) {
                $detailReglement = Array (
                    'DATE' => Carbon::createFromFormat('Y-m-d', $reglement->date)->format('d/m/Y'),
                    'TYPE_COTISATION' => $reglement->typeCotisation->libelle,
                    'MONTANT' => $reglement->montant,
                    'TYPE_REGLEMENT' => $reglement->typeReglement->libelle,
                    'REFERENCE' => $reglement->reference,
                );
            }
            $listeCotisations [] = Array (
                'ANNEE' => $campagne->campagne->assemblee->annee,
                'REGLEMENT' => array(
                    'DETAIL' => $detailReglement
                )
            );

        }

        return Response::json(
            array(
                'PP' => $pp,
                'TEL' => $listeTels,
                'MAIL' => $listeMails,
                'COTISATIONS' => $listeCotisations,
                'UPDATED_DATE' => Carbon::createFromFormat('Y-m-d H:i:s', $pp->created_at)->format('d/m/Y'),
                'UPDATED_BY' => $pp->user->name
            ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        echo $id;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pp = PersonnePhysique::find($id);

        $pp->civilite_id = ($request->pCivilite != '' ? $request->pCivilite : null);
        $pp->usager = $request->pUsager;
        $pp->etablissement_id = ($request->pEtablissement == '' ? null : $request->pEtablissement);
        $pp->type_role_usager_id = ($request->pTypeUsager == '' ? null : $request->pTypeUsager);

        $pp->adr1 = $request->pAdr1;
        $pp->adr2 = $request->pAdr2;
        $pp->adr3 = $request->pAdr3;
        $pp->cp = $request->pCP;
        $pp->ville = $request->pVille;
        $pp->active = $request->pVisible;
        $pp->visible = $request->pVisible;
        $pp->acces = $request->pAcces;

        $pp->pays_id = ($request->pPays == '' ? null : $request->pPays);
        $pp->voeux = $request->pVoeux;
        $pp->nl = $request->pNL;
        $pp->ag = $request->pAG;
        $pp->role_ag_id = ($request->pAG_Role == '' ? null : $request->pAG_Role);
        $pp->cf = $request->pCF;
        $pp->cf_reception_id = ($request->pCF_Recep == '' ? null : $request->pCF_Recep);
        $pp->buro = $request->pBU;
        $pp->role_buro_id = ($request->pBU_Role == '' ? null : $request->pBU_Role);
        $pp->ca = $request->pCA;
        $pp->role_ca_id = ($request->pCA_Role == '' ? null : $request->pCA_Role);
        $pp->bi = $request->pBI;
        $pp->bi_reception_id = ($request->pBI_Recep == '' ? null : $request->pBI_Recep);
        $pp->cotis = $request->pCOTIS;
        $pp->notes = $request->pNotes;
        $pp->user_id = Auth::user()->id;
        $pp->history = '';

        $pp->save();
        return Response::json($pp->id);

        /** ---------------------------------------------------------------------------------------
         * TELEPHONES
         */
        if (isset($request->pTels)) {
            /** -----------------------------------------------------------------------------------
             * On crée les téléphones
             */
            foreach ($request->pTels as $key => $val) {
                $num = $val[0];
                $type = $val[1];
                $tel = Telephone::where('numero', '=', $num)
                    ->first();
                if ($tel === null) {
                    $tel = new Telephone();
                    $tel->numero = $num;
                    $tel->typetel_id = $type;
                    $tel->user_id = Auth::user()->id;
                    $tel->save();
                } else {
                    $tel->typetel_id = $type;
                    $tel->user_id = Auth::user()->id;
                    $tel->save();
                }
                /** -------------------------------------------------------------------------------
                 * Et on les affecte
                 */
                $owner = TelephoneOwner::firstOrNew([
                    'pp_id' => $pp->id,
                    'telephone_id' => $tel->id
                ]);
                $owner->save();
            }
        }

        /** -----------------------------------------------------------------------------------
         * MAILS
         */
        if (isset($request->pMails)) {
            /** -----------------------------------------------------------------------------------
             * On crée les téléphones
             */
            foreach ($request->pMails as $key => $val) {
                $adr = $val[0];
                $type = $val[1];
                $mail = Mail::where('mail', '=', $adr)
                    ->first();
                if ($mail === null) {
                    $mail = new Mail();
                    $mail->mail = $adr;
                    $mail->typemail_id = $type;
                    $mail->user_id = Auth::user()->id;
                    $mail->save();
                } else {
                    $mail->typemail_id = $type;
                    $mail->user_id = Auth::user()->id;
                    $mail->save();
                }
                /** -------------------------------------------------------------------------------
                 * Et on les affecte
                 */
                $owner = MailOwner::firstOrNew([
                    'pp_id' => $pp->id,
                    'mail_id' => $mail->id
                ]);
                $owner->save();
            }
        }

        return Response::json($pp->id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listDatatable() {
        $pps = PersonnePhysique::all();

        /** @var Initialisation du tableau de sortie $result */
        $result = ['aaData' => []];

        /** @var On parcourt les logs $infoPatch */
        foreach($pps as $pp){


            /** -----------------------------------------------------------------------------------
             * Données de base
             */
            $id = $pp->id;
            $nom = $pp->nom;
            $prenom = $pp->prenom;
            $cp = $pp->cp;
            $ville = $pp->ville;
            $dpt = substr($pp->cp, 0, 2);
            if ($dpt != '') {
                $ville.= ' ('.$dpt.')';
            }

            $cotis = $pp->cotis;
            $usager = $pp->usager;

            $civilite = Civilite::find($pp->civilite_id);
            if (! is_null($civilite)) {
                $nom.= ' ('.$civilite->libelle.')';
            }
            $nom.= ($cotis == 1 ? '&nbsp;<span class="label label-success"> cotisant </span>' : '');
            $nom.= ($usager == 1 ? '&nbsp;<span class="label label-warning"> usager </span>' : '');

            /** -----------------------------------------------------------------------------------
             * ADD-IN
             */
            $addin = '<a data-toggle="tooltip" title="ACTIF" class="pp-actif btn btn-xs '.($pp->active == 1 ? 'green-seagreen' : 'red-sunglo').'" data-id="'.$id.'" data-statut="'.$pp->active.'"><i class="fa fa-thumbs-o-up"></i></a>';
            $addin.= '<a data-toggle="tooltip" title="VISIBLE" class="pp-visible btn btn-xs '.($pp->visible == 1 ? 'green-seagreen' : 'red-sunglo').'" data-id="'.$id.'" data-statut="'.$pp->visible.'"><i class="fa fa-search"></i></a>';
            $addin.= '<a data-toggle="tooltip" title="ACCES WEB" class="pp-acces btn btn-xs '.($pp->acces == 1 ? 'green-seagreen' : 'red-sunglo').'" data-id="'.$id.'" data-statut="'.$pp->acces.'"><i class="fa fa-internet-explorer"></i></a>';

            /** -----------------------------------------------------------------------------------
             * Actions
             */
            $action = '<a class="telephone btn btn-xs green-seagreen" data-id="'.$id.'"><i class="fa fa-phone"></i></a>';
            $action .= '<a class="mail btn btn-xs purple-plum" data-id="'.$id.'"><i class="fa fa-send"></i></a>';
            $action .= '<a class="activite btn btn-xs blue-dark" data-id="'.$id.'"><i class="fa fa-group"></i></a>';
            $action .= '<a class="modif btn btn-xs blue" data-id="'.$id.'"><i class="fa fa-edit"></i></a>';


            /** -----------------------------------------------------------------------------------
             * Actions
             */
            //$countAC = Activite::where('antenne_id', $antenne->id)->count();

            $action = '<a class="visu btn btn-xs blue" data-id="'.$id.'"><i class="fa fa-file-text-o"></i></a>';
            $actions = [];
            $actions[] = '<a class="telephone" data-id="'.$id.'"><i class="fa fa-phone"></i>Téléphones / Fax</a>';
            $actions[] .= '<a class="mail" data-id="'.$id.'"><i class="fa fa-send"></i>Mails</a>';
            $actions[] .= '<a class="activites" data-id="'.$id.'"><i class="fa fa-group"></i>Activités</a>';
            $actions[] .= '';
            $actions[] .= '<a class="modif" data-id="'.$id.'"><i class="fa fa-edit"></i>Modifier</a>';

            /** -----------------------------------------------------------------------------------
             * BOUTONS
             */
            $buttons = '
            <div class="btn-group">
                <div class="dropdown">
                    <button class="btn btn-xs dropdown-toggle" data-toggle="dropdown">Actions <span class="caret"></span>
                        <!--<i class="fa fa-angle-down"></i>-->
                    </button>
                    <ul class="dropdown-menu pull-right">';
            foreach ($actions as $unBtn) {
                if ($unBtn == '') {
                    $buttons.='<li class="divider"></li>';
                } else {
                    $buttons.='<li>'.$unBtn.'</li>';
                }
            }
            $buttons.= '</ul></div></div>';

            $result['aaData'][] = [
                $nom,
                $prenom,
                $ville,
                //$addin,
                $action.$buttons
            ];
        }

        return Response::json($result);
    }

    public function switchStatut(Request $request, $id) {
        $pp = PersonnePhysique::find($id);
        switch ($request->pType) {
            case 'actif':
                $pp->active = $request->pValue;
                break;
            case 'visible':
                $pp->visible = $request->pValue;
                break;
            case 'acces':
                $pp->acces = $request->pValue;
                break;
        }
        $pp->save();
    }
}
