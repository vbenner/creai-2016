<?php

namespace App\Http\Controllers;

use App\Mail;
use App\MailOwner;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;

class MailController extends Controller
{
    protected $table = 'mails';
    protected $fillable = ['mail', 'typemail_id', 'active', 'user_id'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /** ---------------------------------------------------------------------------------------
         * On récupère les infos du téléphone
         */
        $mail = Mail::firstOrNew([
            'mail' => $request->pMail
        ]);
        $mail->user_id = Auth::user()->id;
        $mail->typemail_id = $request->pTypeMail;
        $mail->save();

        /** ---------------------------------------------------------------------------------------
         * Et le owner qui va bien
         */
        $owner = new MailOwner();
        switch($request->pType) {
            case 'pp':
                $owner->pp_id = $request->pID;
                break;
            case 'oo':
                $owner->oo_id = $request->pID;
                break;
            case 'po':
                $owner->po_id = $request->pID;
                break;
            case 'et':
                $owner->et_id = $request->pID;
                break;
            case 'aa':
                $owner->aa_id = $request->pID;
                break;
            case 'an':
                $owner->an_id = $request->pID;
                break;
            case 'pe':
                $owner->pe_id = $request->pID;
                break;
        }
        $owner->mail_id = $mail->id;
        $owner->user_id = Auth::user()->id;
        $owner->save();


        return Response::json(array(
            'ID' => $owner->id,
        ));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        /** ---------------------------------------------------------------------------------------
         * si le paramètre pPrefere est passé
         *
         * 1 - on récupère la fk et on clean tous les owners de cette fk
         * 2 - on set à prefere = 1 la ligne de l'id
         */
        if (isset($request->pPrefere)) {

            /** -----------------------------------------------------------------------------------
             * 1
             */
            $owner = MailOwner::find($id);
            $cleanid = $owner->{$request->pType.'_id'};

            $rows = MailOwner::where($request->pType.'_id', '=', $cleanid)->get();
            foreach ($rows as $row) {
                $row->prefere = 0;
                $row->save();
            }
            /** -----------------------------------------------------------------------------------
             * 2
             */
            $owner->prefere = $request->pPrefere;
            $owner->save();
            return Response::json(
                array('success', 1
                ));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $owner = MailOwner::find($id);
        $owner->notes = $request->input('pValue');
        $owner->user_id = Auth::user()->id;
        $owner->save();
        return Response::json(
            array('success', 1
            ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tel = MailOwner::find($id)->delete();
        return Response::json(
            array('res' => 1)
        );
    }

    public function listeMailsPP($id) {
        return $this->listeMails($id, 'pp');
    }

    public function listeMailsOO($id) {
        return $this->listeMails($id, 'oo');
    }

    public function listeMailsPO($id) {
        return $this->listeMails($id, 'po');
    }

    public function listeMailsET($id) {
        return $this->listeMails($id, 'et');
    }

    public function listeMailsAA($id) {
        return $this->listeMails($id, 'aa');
    }

    public function listeMailsAN($id) {
        return $this->listeMails($id, 'an');
    }

    public function listeMailsPE($id) {
        return $this->listeMails($id, 'pe');
    }

}
