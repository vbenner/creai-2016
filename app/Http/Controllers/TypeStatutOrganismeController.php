<?php

namespace App\Http\Controllers;

use App\TypeStatutOrganisme;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class TypeStatutOrganismeController extends Controller
{

    protected $table = 'type_statuts_organismes';
    protected $fillable = ['libelle', 'sigle', 'active', 'user_id'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('params.typestatutorganisme', [
            'titre' => 'Paramétrage',
            'subtitre' => 'Types de Statuts d\'Organismes',
            'sidebar' => $this->getSidebar()//json_decode(json_encode($tabBlocs))
        ]);
    }

    public function listZL() {
        $typestatutorganismes = TypeStatutOrganisme::where('active', '=', 1)->orderBy('libelle')->get();
        $result = array();
        foreach($typestatutorganismes as $typestatutorganisme){
            array_push($result, array(
                'id'   => $typestatutorganisme->id,
                'libelle'  => $typestatutorganisme->libelle,
            ));
        }
        return Response::json(array('LISTE' => $result));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $type = TypeStatutOrganisme::find($id);
        return Response::json($type);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $type = TypeStatutOrganisme::firstOrNew([
            'libelle' => $request->pLibelle
        ]);
        $type->sigle = $request->pSigle;
        $type->user_id = Auth::user()->id;
        $type->save();
        return Response::json($type->id);    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $type = TypeStatutOrganisme::find($id);
        $type->active = $request->pValue;
        $type->user_id = Auth::user()->id;
        $type->save();
        return Response::json($id);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /** ---------------------------------------------------------------------------------------
         * Recherche d'un enregistrement identique avec un autre ID
         */
        $type = TypeStatutOrganisme::where('libelle', 'like', $request->pLibelle)
            ->where('id', '<>', $id)
            ->first();
        if ($type === null) {
            $type = TypeStatutOrganisme::find($id);
            $type->libelle = $request->pLibelle;
            $type->sigle = $request->pSigle;
            $type->user_id = Auth::user()->id;
            $type->save();
            return Response::json($id);
        } else {
            return Response::json(-1);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listDatatable() {
        $types = TypeStatutOrganisme::all();

        /** @var Initialisation du tableau de sortie $result */
        $result = ['aaData' => []];

        /** @var On parcourt les logs $infoPatch */
        foreach($types as $type){


            /** -----------------------------------------------------------------------------------
             * Données
             */
            $id = $type->id;
            $libelle = $type->libelle;
            $sigle = $type->sigle;

            /** -----------------------------------------------------------------------------------
             * Actions
             */
            $active =  $this->buildSwitch($id, $type->active);
            $action = '<a class="modif btn btn-xs blue" data-id="'.$id.'"><i class="fa fa-edit"></i></a>';
            $result['aaData'][] = [
                $libelle,
                $sigle,
                $active,
                $action
            ];
        }

        return Response::json($result);
    }
}
