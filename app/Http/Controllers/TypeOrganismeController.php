<?php

namespace App\Http\Controllers;

use App\DetailTypeOrganisme;
use App\TypeOrganisme;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class TypeOrganismeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('params.typeorganisme', [
            'titre' => 'Paramétrage',
            'subtitre' => 'Types d\'Organismes',
            'sidebar' => $this->getSidebar()//json_decode(json_encode($tabBlocs))
        ]);
    }

    public function listZL() {
        $typeorganismes = TypeOrganisme::where('active', '=', 1)->orderBy('libelle')->get();
        $result = array();
        foreach($typeorganismes as $typeorganisme){
            array_push($result, array(
                'id'   => $typeorganisme->id,
                'libelle'  => $typeorganisme->libelle,
                'forceog'  => $typeorganisme->forceOG,
            ));
        }
        return Response::json(array('LISTE' => $result));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $type = TypeOrganisme::find($id);
        return Response::json($type);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $type = TypeOrganisme::firstOrNew([
            'libelle' => $request->pLibelle,
            'isOG' => $request->pOG,
            'isPOLE' => $request->pPOLE,
            'isGROUP' => $request->pGROUP,
            'forceOG' => $request->pFORCE,
        ]);
        $type->user_id = Auth::user()->id;
        $type->save();
        return Response::json($type->id);    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $type = TypeOrganisme::find($id);
        $type->active = $request->pValue;
        $type->user_id = Auth::user()->id;
        $type->save();
        return Response::json($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /** ---------------------------------------------------------------------------------------
         * Recherche d'un enregistrement identique avec un autre ID
         */
        $type = TypeOrganisme::where('libelle', 'like', $request->pLibelle)
            ->where('id', '<>', $id)
            ->first();
        if ($type === null) {
            $type = TypeOrganisme::find($id);
            $type->libelle = $request->pLibelle;
            $type->isOG = $request->pOG;
            $type->isPOLE = $request->pPOLE;
            $type->isGROUP = $request->pGROUP;
            $type->forceOG = $request->pFORCE;
            $type->user_id = Auth::user()->id;
            $type->save();
            return Response::json($id);
        } else {
            return Response::json(-1);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listDatatable() {
        $types = TypeOrganisme::all();

        /** @var Initialisation du tableau de sortie $result */
        $result = ['aaData' => []];

        /** @var On parcourt les logs $infoPatch */
        foreach($types as $type){


            /** -----------------------------------------------------------------------------------
             * Données
             */
            $id = $type->id;
            $libelle = $type->libelle;
            $isOG = '<span class="label '.($type->isOG ? 'label-success' : 'label-danger').'"> '.($type->isOG ? 'OUI' : 'NON').'</span>';
            $isPOLE = '<span class="label '.($type->isPOLE ? 'label-success' : 'label-danger').'"> '.($type->isPOLE ? 'OUI' : 'NON').'</span>';
            $isGROUP = '<span class="label '.($type->isGROUP ? 'label-success' : 'label-danger').'"> '.($type->isGROUP ? 'OUI' : 'NON').'</span>';

            /** -----------------------------------------------------------------------------------
             * Actions
             */
            /** -----------------------------------------------------------------------------------
             * Détail ?
             */
            $addin = '';
            $count = DetailTypeOrganisme::where('type_organisme_id', $type->id)->count();
            $addin .= '<a class="detail btn btn-xs blue-steel" data-id="'.$id.'"><i class="fa fa-plus"></i> DETAIL '.($count != 0 ? '('.$count.')' : '').'</a>';

            $active =  $this->buildSwitch($id, $type->active);
            $action = '<a class="modif btn btn-xs blue" data-id="'.$id.'"><i class="fa fa-edit"></i></a>';
            $result['aaData'][] = [
                $libelle,
                $isOG,
                $isPOLE,
                $isGROUP,
                $active,
                $addin.$action
            ];
        }

        return Response::json($result);
    }
}
