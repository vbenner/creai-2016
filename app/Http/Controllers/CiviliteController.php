<?php

namespace App\Http\Controllers;

use App\Civilite;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class CiviliteController extends Controller
{

    protected $table = 'civilites';
    protected $fillable = array('libelle', 'active', 'user_id');

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('params.civilite', [
            'titre' => 'Paramétrage',
            'subtitre' => 'Civilités',
            'sidebar' => $this->getSidebar()//json_decode(json_encode($tabBlocs))
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$input = $_REQUEST
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $civilite = Civilite::firstOrNew([
            'libelle' => $request->pLibelle
        ]);
        $civilite->user_id = Auth::user()->id;
        $civilite->save();
        return Response::json($civilite->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $civilite = Civilite::find($id);
        return Response::json($civilite);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $civilite = Civilite::find($id);
        $civilite->active = $request->pValue;
        $civilite->user_id = Auth::user()->id;
        $civilite->save();
        return Response::json($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /** ---------------------------------------------------------------------------------------
         * Recherche d'un enregistrement identique avec un autre ID
         */
        $civilite = Civilite::where('libelle', 'like', $request->pLibelle)
            ->where('id', '<>', $id)
            ->first();
        if ($civilite === null) {
            $civilite = Civilite::find($id);
            $civilite->libelle = $request->pLibelle;
            $civilite->user_id = Auth::user()->id;
            $civilite->save();
            return Response::json($id);
        } else {
            return Response::json(-1);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listZL() {
        $civilites = Civilite::where('active', '=', 1)->orderBy('libelle')->get();
        $result = array();
        foreach($civilites as $civilite){
            array_push($result, array(
                'id'   => $civilite->id,
                'libelle'  => $civilite->libelle,
            ));
        }
        return Response::json(array('LISTE' => $result));

    }

    public function listDatatable() {
        $civilites = Civilite::all();

        /** @var Initialisation du tableau de sortie $result */
        $result = ['aaData' => []];

        /** @var On parcourt les logs $infoPatch */
        foreach($civilites as $civilite){


            /** -----------------------------------------------------------------------------------
             * Données
             */
            $id = $civilite->id;
            $libelle = $civilite->libelle;

            /** -----------------------------------------------------------------------------------
             * Actions
             */
            $active =  $this->buildSwitch($id, $civilite->active);
            $action = '<a class="modif btn btn-xs blue" data-id="'.$id.'"><i class="fa fa-edit"></i></a>';
            $result['aaData'][] = [
                $libelle,
                $active,
                $action
            ];
        }

        return Response::json($result);
    }
}
