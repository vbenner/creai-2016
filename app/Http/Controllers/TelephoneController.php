<?php

namespace App\Http\Controllers;

use App\Telephone;
use App\TelephoneOwner;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;

class TelephoneController extends Controller
{
    protected $table = 'telephones';
    protected $fillable = ['numero', 'typetel_id', 'active', 'user_id'];

    /** -------------------------------------------------------------------------------------------
     * Clear tous les téléphones SANS aucune affectation dans la table des owners
     */
    public function clear($oldTels) {

        foreach ($oldTels as $oldTel) {
            $count = TelephoneOwner::where('telephone_id', '=', $oldTel->telephone_id)->count();
            if ($count == 0) {
                Telephone::find($oldTel->telephone_id)->delete();
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(array $data)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /** ---------------------------------------------------------------------------------------
         * On récupère les infos du téléphone
         */
        $cleanNum = $this->cleanPhone( $request->pNum );

        $tel = Telephone::firstOrNew([
            'numero' => $cleanNum
        ]);
        $tel->typetel_id = $request->pTypeTel;
        $tel->user_id = Auth::user()->id;
        $tel->save();

        /** ---------------------------------------------------------------------------------------
         * Et le owner qui va bien
         * Attention à l'affectation !
         */
        $owner = new TelephoneOwner();
        if (isset($request->pType)) {
            switch ($request->pType) {
                case 'pp':
                    $owner->pp_id = $request->pID;
                    break;
                case 'oo':
                    $owner->oo_id = $request->pID;
                    break;
                case 'po':
                    $owner->po_id = $request->pID;
                    break;
                case 'et':
                    $owner->et_id = $request->pID;
                    break;
                case 'aa':
                    $owner->aa_id = $request->pID;
                    break;
                case 'an':
                    $owner->an_id = $request->pID;
                    break;
                case 'pe':
                    $owner->pe_id = $request->pID;
                    break;
            }
        }
        $owner->telephone_id = $tel->id;
        $owner->user_id = Auth::user()->id;
        $owner->save();

        return Response::json(array(
            'ID' => $owner->id
        ));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        /** ---------------------------------------------------------------------------------------
         * si le paramètre pPrefere est passé
         *
         * 1 - on récupère la fk et on clean tous les owners de cette fk
         * 2 - on set à prefere = 1 la ligne de l'id
         */
        if (isset($request->pPrefere)) {

            /** -----------------------------------------------------------------------------------
             * 1
             */
            $owner = TelephoneOwner::find($id);
            $cleanid = $owner->{$request->pType.'_id'};

            $rows = TelephoneOwner::where($request->pType.'_id', '=', $cleanid)->get();
            foreach ($rows as $row) {
                $row->prefere = 0;
                $row->save();
            }
            /** -----------------------------------------------------------------------------------
             * 2
             */
            $owner->prefere = $request->pPrefere;
            $owner->save();
            return Response::json(
                array('success', 1
                ));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        /** ---------------------------------------------------------------------------------------
         *
         */
        $owner = TelephoneOwner::find($id);
        $owner->notes = $request->input('pValue');
        $owner->user_id = Auth::user()->id;
        $owner->save();
        return Response::json(
            array('success', 1
            ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tel = TelephoneOwner::find($id)->delete();
        return Response::json(
            array('res' => 1)
        );
    }

    public function listeTelsPP($id) {
        return $this->listeTels($id, 'pp');
    }

    public function listeTelsOO($id) {
        return $this->listeTels($id, 'oo');
    }

    public function listeTelsPO($id) {
        return $this->listeTels($id, 'po');
    }

    public function listeTelsET($id) {
        return $this->listeTels($id, 'et');
    }

    public function listeTelsAA($id) {
        return $this->listeTels($id, 'aa');
    }

    public function listeTelsAN($id) {
        return $this->listeTels($id, 'an');
    }

    public function listeTelsPE($id) {
        return $this->listeTels($id, 'pe');
    }

    public function setInfo(Request $request, $id) {

    }
}
