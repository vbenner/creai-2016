<?php

namespace App\Http\Controllers;

use App\DetailTypePublic;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class DetailTypePublicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $type = DetailTypePublic::firstOrNew([
            'libelle' => $request->pLibelle,
            'type_publics_id' => $request->pIdType,
        ]);
        $type->note = $request->pNote;
        $type->user_id = Auth::user()->id;
        $type->save();
        return Response::json($type->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $type = DetailTypePublic::find($id);
        return Response::json($type);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $type = DetailTypePublic::find($id);
        $type->active = $request->pValue;
        $type->user_id = Auth::user()->id;
        $type->save();
        return Response::json($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /** ---------------------------------------------------------------------------------------
         * Recherche d'un enregistrement identique avec un autre ID
         */
        $type = DetailTypePublic::where('libelle', 'like', $request->pLibelle)
            ->where('id', '<>', $id)
            ->where('type_publics_id', '=', $request->pType)
            ->first();
        if ($type === null) {
            $type = DetailTypePublic::find($id);
            $type->libelle = $request->pLibelle;
            $type->user_id = Auth::user()->id;
            $type->save();
            return Response::json($id);
        } else {
            return Response::json(-1);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listDatatable($id) {
        $types = DetailTypePublic::
        where('type_publics_id', $id)
            ->get();

        /** @var Initialisation du tableau de sortie $result */
        $result = ['aaData' => []];

        /** @var On parcourt les logs $infoPatch */
        foreach($types as $type){


            /** -----------------------------------------------------------------------------------
             * Données
             */
            $id = $type->id;
            $libelle = $type->libelle;
            $note = $type->note;

            /** -----------------------------------------------------------------------------------
             * Actions
             */
            $active =  $this->buildSwitch($id, $type->active);
            $action = '<a class="modif btn btn-xs blue" data-id="'.$id.'"><i class="fa fa-edit"></i></a>';
            $result['aaData'][] = [
                $libelle,
                $active,
                $note,
                $action
            ];
        }

        return Response::json($result);
    }

    public function listZL($id) {

        $details = DetailTypePublic::
        where('active', '=', 1)
            ->where('type_publics_id', '=', $id)
            ->orderBy('id')
            ->get();
        $result = array();
        foreach($details as $detail){
            array_push($result, array(
                'id'   => $detail->id,
                'libelle'  => $detail->libelle,
            ));
        }
        return Response::json(array('LISTE' => $result));
    }

    public function listZLAll() {

        $details = DetailTypePublic::
<<<<<<< HEAD
            where('active', '=', 1)
=======
        where('active', '=', 1)
>>>>>>> debug8
            ->orderBy('id')
            ->get();
        $result = array();
        foreach($details as $detail){
            array_push($result, array(
                'id'   => $detail->id,
                'libelle'  => $detail->libelle,
            ));
        }
        return Response::json(array('LISTE' => $result));
    }
<<<<<<< HEAD
}
=======
}
>>>>>>> debug8
