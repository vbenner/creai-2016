<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Response;

class DownloadController extends Controller
{
    public function downloadFile ($type, $fileName) {

        $dir = storage_path().'/'.$type.'/';
        return Response::download($dir . $fileName);
    }

}
