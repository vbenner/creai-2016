<?php

namespace App\Http\Controllers;

use App\Mail;
use App\MailOwner;
use App\Personnel;
use App\Telephone;
use App\TelephoneOwner;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;

class PersonnelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        /** -----------------------------------------------------------------------------------
         * Données de base - Pour le personnel, on fait toujous un CREATE ! On gérera les
         * clônes plus tard
         */
        $pe = new Personnel;
        $pe->civilite_id = ($request->pCivilite != '' ? $request->pCivilite : null);
        $pe->nom = $request->pNom;
        $pe->prenom = $request->pPrenom;

        /** -----------------------------------------------------------------------------------
         * A qui le rattacher ?
         */
        $pe->oo_id = ($request->pOO != '' ? $request->pOO : null);
        $pe->po_id = ($request->pPO != '' ? $request->pPO : null);
        $pe->et_id = ($request->pET != '' ? $request->pET : null);
        $pe->an_id = ($request->pAN != '' ? $request->pAN : null);

        $pe->fonction_id = ($request->pFonction != '' ? $request->pFonction : null);

        $pe->service = $request->pService;
        $pe->notes_fonctions = $request->pComplementFontion;

        $pe->adr1 = $request->pAdr1;
        $pe->adr2 = $request->pAdr2;
        $pe->adr3 = $request->pAdr3;
        $pe->cp = $request->pCP;
        $pe->ville = $request->pVille;
        $pe->pays_id = ($request->pPays == '' ? null : $request->pPays);

        $pe->note = $request->pNotes;

        $pe->voeux = $request->pVoeux;
        $pe->nl = $request->pNL;

        $pe->cotis = $request->pCOTIS;
        $pe->bi = $request->pBI;
        $pe->bi_reception_id = ($request->pBI_Recep != '' ? $request->pBI_Recep : null);

        $pe->cf = $request->pCF;
        $pe->cf_reception_id = ($request->pCF_Recep != '' ? $request->pCF_Recep : null);

        $pe->ag = $request->pAG;
        $pe->role_ag_id = ($request->pAG_Role != '' ? $request->pAG_Role : null);

        $pe->buro = $request->pBU;
        $pe->role_buro_id = ($request->pBU_Role != '' ? $request->pBU_Role : null);
        $pe->ca = $request->pCA;
        $pe->role_ca_id = ($request->pCA_Role != '' ? $request->pCA_Role : null);

        $pe->ctc = $request->pCTC;
        $pe->cc = $request->pCC;

        $pe->active = $request->pActif;
        $pe->visible = $request->pVisible;
        $pe->acces = $request->pAcces;

        $pe->user_id = Auth::user()->id;
        $pe->save();

        /** -----------------------------------------------------------------------------------
         * Traitement des mails et des téléphones
         * Pour chaque téléphone, on va faire une première vérification dans la table
         *
         */
        if (isset($request->pTels)) {
            foreach ($request->pTels as $key => $val) {
                $num = $val[0];
                $prefere = $val[1];
                $type = $val[2];
                $tel = Telephone::where('numero', '=', $num)
                    ->first();
                if ($tel === null) {
                    $tel = new Telephone();
                    $tel->numero = $num;
                    $tel->typetel_id = $type;
                    $tel->user_id = Auth::user()->id;
                    $tel->save();
                } else {
                    $tel->typetel_id = $type;
                    $tel->user_id = Auth::user()->id;
                    $tel->save();
                }
                $owner = new TelephoneOwner();
                $owner->telephone_id = $tel->id;
                $owner->prefere = $prefere;
                $owner->pe_id = $pe->id;
                $owner->user_id = Auth::user()->id;
                $owner->save();
            }
        }

        if (isset($request->pMails)) {
            foreach ($request->pMails as $key => $val) {
                $adr = $val[0];
                $prefere = $val[1];
                $type = $val[2];
                $mail = Mail::where('mail', '=', $adr)
                    ->first();
                if ($mail === null) {
                    $mail = new Mail();
                    $mail->mail = $adr;
                    $mail->typemail_id = $type;
                    $mail->user_id = Auth::user()->id;
                    $mail->save();
                } else {
                    $mail->typemail_id = $type;
                    $mail->user_id = Auth::user()->id;
                    $mail->save();
                }
                $owner = new MailOwner();
                $owner->mail_id = $mail->id;
                $owner->prefere = $prefere;
                $owner->user_id = Auth::user()->id;
                $owner->pe_id = $pe->id;
                $owner->save();
            }
        }
        return Response::json($pe->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pe = Personnel::find($id);

        /** ---------------------------------------------------------------------------------------
         * On prend aussi la liste des téléphones
         */
        $listeTels = DB::table('telephones')
            ->join('type_tels', 'type_tels.id', '=', 'telephones.typetel_id')
            ->join('telephone_owners', 'telephones.id', '=', 'telephone_owners.telephone_id')
            ->join('personnels', 'personnels.id', '=', 'telephone_owners.pe_id')
            ->select('numero', 'type_tels.id AS TYPE_ID', 'prefere',
                'type_tels.libelle', 'telephone_owners.pe_id AS PE_ID',
                'telephone_owners.ID AS OWN_ID', 'telephone_owners.notes')
            ->where('pe_id', '=', $id)
            ->get();

        /** ---------------------------------------------------------------------------------------
         * On prend aussi la liste des mails
         */
        $listeMails = DB::table('mails')
            ->join('type_mails', 'type_mails.id', '=', 'mails.typemail_id')
            ->join('mail_owners', 'mails.id', '=', 'mail_owners.mail_id')
            ->join('personnels', 'personnels.id', '=', 'mail_owners.pe_id')
            ->select('mail', 'type_mails.id AS TYPE_ID', 'prefere',
                'type_mails.libelle', 'mail_owners.pe_id AS PE_ID',
                'mail_owners.ID AS OWN_ID')
            ->where('pe_id', '=', $id)
            ->get();

        return Response::json(
            array(
                'PE' => $pe,
                'TEL' => $listeTels,
                'MAIL' => $listeMails,
                'UPDATED_DATE' => Carbon::createFromFormat('Y-m-d H:i:s', $pe->created_at)->format('d/m/Y'),
                'UPDATED_BY' => $pe->user->name
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pe = Personnel::find($id);

        $pe->civilite_id = ($request->pCivilite != '' ? $request->pCivilite : null);
        $pe->nom = $request->pNom;
        $pe->prenom = $request->pPrenom;

        $pe->fonction_id = ($request->pFonction != '' ? $request->pFonction : null);

        $pe->service = $request->pService;
        $pe->notes_fonctions = $request->pComplementFontion;

        $pe->adr1 = $request->pAdr1;
        $pe->adr2 = $request->pAdr2;
        $pe->adr3 = $request->pAdr3;
        $pe->cp = $request->pCP;
        $pe->ville = $request->pVille;
        $pe->pays_id = ($request->pPays == '' ? null : $request->pPays);

        $pe->note = $request->pNotes;

        $pe->voeux = $request->pVoeux;
        $pe->nl = $request->pNL;

        $pe->cotis = $request->pCOTIS;
        $pe->bi = $request->pBI;
        $pe->bi_reception_id = ($request->pBI_Recep != '' ? $request->pBI_Recep : null);

        $pe->cf = $request->pCF;
        $pe->cf_reception_id = ($request->pCF_Recep != '' ? $request->pCF_Recep : null);

        $pe->ag = $request->pAG;
        $pe->role_ag_id = ($request->pAG_Role != '' ? $request->pAG_Role : null);

        $pe->buro = $request->pBU;
        $pe->role_buro_id = ($request->pBU_Role != '' ? $request->pBU_Role : null);
        $pe->ca = $request->pCA;
        $pe->role_ca_id = ($request->pCA_Role != '' ? $request->pCA_Role : null);

        $pe->ctc = $request->pCTC;
        $pe->cc = $request->pCC;

        $pe->active = $request->pActif;
        $pe->visible = $request->pVisible;
        $pe->acces = $request->pAcces;

        $pe->user_id = Auth::user()->id;
        $pe->save();

        /** ---------------------------------------------------------------------------------------
         * TELEPHONES
         */
        if (isset($request->pTels)) {
            /** -----------------------------------------------------------------------------------
             * On crée les téléphones
             */
            foreach ($request->pTels as $key => $val) {
                $num = $val[0];
                $prefere = $val[1];
                $type = $val[2];
                $tel = Telephone::where('numero', '=', $num)
                    ->first();
                if ($tel === null) {
                    $tel = new Telephone();
                    $tel->numero = $num;
                    $tel->typetel_id = $type;
                    $tel->user_id = Auth::user()->id;
                    $tel->save();
                } else {
                    $tel->typetel_id = $type;
                    $tel->user_id = Auth::user()->id;
                    $tel->save();
                }
                /** -------------------------------------------------------------------------------
                 * Et on les affecte
                 */
                $owner = TelephoneOwner::firstOrNew([
                    'pe_id' => $pe->id,
                    'prefere' => $prefere,
                    'telephone_id' => $tel->id
                ]);
                $owner->save();
            }
        }

        /** -----------------------------------------------------------------------------------
         * MAILS
         */
        if (isset($request->pMails)) {
            /** -----------------------------------------------------------------------------------
             * On crée les téléphones
             */
            foreach ($request->pMails as $key => $val) {
                $adr = $val[0];
                $prefere = $val[1];
                $type = $val[2];
                $mail = Mail::where('mail', '=', $adr)
                    ->first();
                if ($mail === null) {
                    $mail = new Mail();
                    $mail->mail = $adr;
                    $mail->typemail_id = $type;
                    $mail->user_id = Auth::user()->id;
                    $mail->save();
                } else {
                    $mail->typemail_id = $type;
                    $mail->user_id = Auth::user()->id;
                    $mail->save();
                }
                /** -------------------------------------------------------------------------------
                 * Et on les affecte
                 */
                $owner = MailOwner::firstOrNew([
                    'pp_id' => $pe->id,
                    'prefere' => $prefere,
                    'mail_id' => $mail->id
                ]);
                $owner->save();
            }
        }

        return Response::json($pe->id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listDatatable($type, $id) {

        $personnels = Personnel::where($type.'_id', '=', $id)
            ->get();

        /** @var Initialisation du tableau de sortie $result */
        $result = ['aaData' => []];

        /** @var On parcourt les logs $infoPatch */
        foreach($personnels as $personnel){


            /** -----------------------------------------------------------------------------------
             * Données de base
             */
            $id = $personnel->id;
            $nom = $personnel->nom;
            $prenom = $personnel->prenom;
            $service = $personnel->service;
            $fonction = ($personnel->fonction_id != null ? $personnel->fonction->libelle : '');
            $ville = $personnel->ville;
            $dpt = substr($personnel->cp, 0, 2);

            /** -----------------------------------------------------------------------------------
             * Données de base
             */
            $ville.= ' ('.$dpt.')';

            /** -----------------------------------------------------------------------------------
             * Actions
             */
            $action = '<a class="visu btn btn-xs blue" data-id="'.$id.'"><i class="fa fa-file-text-o"></i></a>';
            $actions = [];
            $actions[] = '<a class="telephone" data-id="'.$id.'"><i class="fa fa-phone"></i>Téléphones / Fax</a>';
            $actions[] .= '<a class="mail" data-id="'.$id.'"><i class="fa fa-send"></i>Mails</a>';
            $actions[] .= '<a class="modif" data-id="'.$id.'"><i class="fa fa-edit"></i>Modifier</a>';

            /** -----------------------------------------------------------------------------------
             * BOUTONS
             */
            $buttons = '
            <div class="btn-group">
                <div class="dropdown">
                    <button class="btn btn-xs dropdown-toggle" data-toggle="dropdown">Actions <span class="caret"></span>
                        <!--<i class="fa fa-angle-down"></i>-->
                    </button>
                    <ul class="dropdown-menu pull-right">';
            foreach ($actions as $unBtn) {
                if ($unBtn == '') {
                    $buttons.='<li class="divider"></li>';
                } else {
                    $buttons.='<li>'.$unBtn.'</li>';
                }
            }
            $buttons.= '</ul></div></div>';

            $result['aaData'][] = [
                $nom,
                $prenom,
                $service,
                $fonction,
                $ville,
                $action.$buttons
            ];
        }

        return Response::json($result);
    }
}
