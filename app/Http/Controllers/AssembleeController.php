<?php

namespace App\Http\Controllers;

use App\Assemblee;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;

class AssembleeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('activite.gestionag', [
            'titre' => 'Activité',
            'subtitre' => 'Assemblées Générales',
            'sidebar' => $this->getSidebar()//json_decode(json_encode($tabBlocs))
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $assemblee = Assemblee::firstOrNew([
            'annee' => $request->pAnnee
        ]);
        $assemblee->notes = $request->pNotes;
        $assemblee->date_ag = Carbon::createFromFormat('d/m/Y', $request->pDate);
        $assemblee->user_id = Auth::user()->id;
        $assemblee->save();
        return Response::json($assemblee->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ag = Assemblee::find($id);
        $ag->date_ag = ($ag->date_ag != '0000-00-00' ? Carbon::createFromFormat('Y-m-d', $ag->date_ag)->format('d/m/Y') : '');
        return Response::json(
            array(
                'AG' => $ag,
                'UPDATED_DATE' => Carbon::createFromFormat('Y-m-d H:i:s', $ag->created_at)->format('d/m/Y'),
                'UPDATED_BY' => $ag->user->name
            ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ag = Assemblee::find($id);
        $ag->notes = $request->pNotes;
        $ag->annee = $request->pAnnee;
        $ag->date_ag = Carbon::createFromFormat('d/m/Y', $request->pDate);
        $ag->user_id = Auth::user()->id;
        $ag->save();

        return Response::json($ag->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listDatatable() {
        $ags = Assemblee::all();

        /** @var Initialisation du tableau de sortie $result */
        $result = ['aaData' => []];

        /** @var On parcourt les logs $infoPatch */
        foreach($ags as $ag){


            /** -----------------------------------------------------------------------------------
             * Données
             */
            $id = $ag->id;
            $annee = $ag->annee;
            $dateag = ($ag->date_ag != '' && $ag->date_ag != '0000-00-00' ? Carbon::createFromFormat('Y-m-d', $ag->date_ag)->format('d/m/Y') : '');
            $notes = substr($ag->notes, 0, 500);
            $notes = wordwrap($notes, 100, '<br/>', true).(strlen($ag->notes) > strlen($notes) ? '...' : '');

            /** -----------------------------------------------------------------------------------
             * Actions
             */
            $action = '<a class="visu btn btn-xs blue" data-id="'.$id.'"><i class="fa fa-file-text-o"></i></a>';
            $actions = [];
            $actions [0][] = '<a class="modif" data-id="'.$id.'"><i class="fa fa-edit"></i>Modifier</a>';

            /** -----------------------------------------------------------------------------------
             * Build Button
             */
            $buttons = $this->buildButtons($actions, 'down');

            $result['aaData'][] = [
                $annee,
                $dateag,
                $notes,
                $action.$buttons
            ];
        }

        return Response::json($result);
    }
}
