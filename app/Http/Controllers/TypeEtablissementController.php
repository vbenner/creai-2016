<?php

namespace App\Http\Controllers;

use App\TypeEtablissement;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class TypeEtablissementController extends Controller
{

    protected $table = 'type_etablissements';
    protected $fillable = ['libelle', 'sigle', 'type_secteur_id',
        'type_secteur_ms_id', 'type_age_id', 'active',
        'user_id'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('params.typeetablissement', [
            'titre' => 'Paramétrage',
            'subtitre' => 'Types d\'Établissements',
            'sidebar' => $this->getSidebar()//json_decode(json_encode($tabBlocs))
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $type = TypeEtablissement::firstOrNew([
            'libelle' => $request->pLibelle,
        ]);

        $type->sigle = $request->pSigle;
        $type->type_secteur_id = $request->pSecteur;
        $type->type_secteur_ms_id  = ($request->pSecteurMS != '' ? $request->pSecteurMS : null);
        $type->type_age_id  = $request->pAge;
        $type->active  = 1;
        $type->user_id = Auth::user()->id;
        $type->save();
        return Response::json($type->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $type = TypeEtablissement::find($id);
        return Response::json($type);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $type = TypeEtablissement::find($id);
        $type->active = $request->pValue;
        $type->user_id = Auth::user()->id;
        $type->save();
        return Response::json($id);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /** ---------------------------------------------------------------------------------------
         * Recherche d'un enregistrement identique avec un autre ID
         */
        $type = TypeEtablissement::where('libelle', 'like', $request->pLibelle)
            ->where('id', '<>', $id)
            ->first();
        if ($type === null) {
            $type = TypeEtablissement::find($id);
            $type->libelle = $request->pLibelle;
            $type->sigle = $request->pSigle;
            $type->type_secteur_id = $request->pSecteur;
            $type->type_secteur_ms_id  = ($request->pSecteurMS != '' ? $request->pSecteurMS : null);
            $type->type_age_id  = $request->pAge;
            $type->save();
            return Response::json($id);
        } else {
            return Response::json(-1);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listDatatable() {
        $types = TypeEtablissement::all();

        /** @var Initialisation du tableau de sortie $result */
        $result = ['aaData' => []];

        /** @var On parcourt les logs $infoPatch */
        foreach($types as $type){


            /** -----------------------------------------------------------------------------------
             * Données
             */
            $id = $type->id;
            $libelle = $type->libelle;
            $secteur = $type->typeSecteur->libelle;
            $secteurms = ($type->type_secteur_ms_id != null ? $type->typeSecteurMS->libelle : '');
            $age = $type->typeAge->libelle;

            /** -----------------------------------------------------------------------------------
             * Actions
             */
            $active =  $this->buildSwitch($id, $type->active);
            $action = '<a class="modif btn btn-xs blue" data-id="'.$id.'"><i class="fa fa-edit"></i></a>';
            $result['aaData'][] = [
                $libelle,
                $secteur,
                $secteurms,
                $age,
                $active,
                $action
            ];
        }

        return Response::json($result);
    }

    public function listZL(Request $request) {

        /** ---------------------------------------------------------------------------------------
         * Le type d'établissement est filtré par 3 variables,
         */
        if ($request->pSecteurMS != 0) {
            $types = TypeEtablissement::
                where('active', '=', 1)
                ->where('type_secteur_id', '=', $request->pSecteur)
                ->where('type_secteur_ms_id', '=', $request->pSecteurMS)
                ->where('type_age_id', '=', $request->pAge)
                ->orderBy('libelle')->get();
        } else {
            if ($request->has('pSecteurMS')) {
                $types = TypeEtablissement::
                where('active', '=', 1)
                    ->where('type_secteur_id', '=', $request->pSecteur)
                    ->where('type_age_id', '=', $request->pAge)
                    ->orderBy('libelle')->get();
            } else {
                $types = TypeEtablissement::
                where('active', '=', 1)
                    ->orderBy('libelle')->get();
            }
        }
        $result = array();
        foreach($types as $type){
            array_push($result, array(
                'id'   => $type->id,
                'libelle'  => $type->libelle,
            ));
        }
        return Response::json(array('LISTE' => $result));
    }
}
