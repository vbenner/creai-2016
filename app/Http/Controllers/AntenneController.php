<?php

namespace App\Http\Controllers;

use App\AdresseAccueil;
use App\Antenne;
use App\AntenneModaliteAccueil;
use App\Mail;
use App\MailOwner;
use App\Offres;
use App\Personnel;
use App\Telephone;
use App\TelephoneOwner;
use Illuminate\Http\Request;

use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class AntenneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /** -----------------------------------------------------------------------------------
         * Données de base
         *
         */
        $antenne = Antenne::firstOrNew([
            'nom' => $request->pNom,
            'etablissement_id' => ($request->pET == '' ? null : $request->pET),
        ]);

        $antenne->nom = $request->pNom;
        $antenne->finess = $request->pFiness;
        $antenne->arrete_pref = $request->pArrete;

        if ($request->pDateArrete != '') {
            $dateArrete = Carbon::createFromFormat('d/m/Y', $request->pDateArrete);
            $antenne->date_arrete = $dateArrete->format('Y-m-d');
        }
        if ($request->pOpening != '') {
            $dateOpening = Carbon::createFromFormat('d/m/Y', $request->pOpening);
            $antenne->date_ouverture = $dateOpening->format('Y-m-d');
        }
        $antenne->isCONTRIBUANT = $request->pContrib;

        $antenne->adr1 = $request->pAdr1;
        $antenne->adr2 = $request->pAdr2;
        $antenne->adr3 = $request->pAdr3;
        $antenne->cp = $request->pCP;
        $antenne->ville = $request->pVille;
        $antenne->pays_id = ($request->pPays == '' ? null : $request->pPays);

        $antenne->notes = $request->pNotes;

        $antenne->user_id = Auth::user()->id;
        $antenne->active = $request->pActif;
        $antenne->visible = $request->pVisible;
        $antenne->acces = $request->pAcces;

        $antenne->save();

        /** ---------------------------------------------------------------------------------------
         * Type AGE : array
         */
        if (isset($request->pModaliteAccueil)) {
            foreach($request->pModaliteAccueil as $modaliteAccueil) {
                $type = AntenneModaliteAccueil::firstOrNew([
                    'antenne_id' => $antenne->id,
                    'type_hebergement_id' => $modaliteAccueil,
                ]);
                $type->active = 1;
                $type->user_id = Auth::user()->id;
                $type->save();
            }
        }

        /** ---------------------------------------------------------------------------------------
         * Traitement des mails et des téléphones
         * Pour chaque téléphone, on va faire une première vérification dans la table
         *
         */
        if (isset($request->pTels)) {
            foreach ($request->pTels as $key => $val) {
                $num = $val[0];
                $prefere = $val[1];
                $type = $val[2];

                /** -------------------------------------------------------------------------------
                 * Il se peut que le téléphone existe déjà !
                 */
                $tel = Telephone::where('numero', '=', $num)
                    ->first();
                if ($tel === null) {
                    $tel = new Telephone();
                    $tel->numero = $num;
                    $tel->typetel_id = $type;
                    $tel->user_id = Auth::user()->id;
                } else {
                    $tel->typetel_id = $type;
                    $tel->user_id = Auth::user()->id;
                    $tel->save();
                }
                $tel->save();

                $owner = new TelephoneOwner();
                $owner->telephone_id = $tel->id;
                $owner->prefere = $prefere;
                $owner->an_id = $antenne->id;
                $owner->user_id = Auth::user()->id;
                $owner->save();

            }
        }

        if (isset($request->pMails)) {
            foreach ($request->pMails as $key => $val) {
                $adr = $val[0];
                $prefere = $val[1];
                $type = $val[2];
                $mail = Mail::where('mail', '=', $adr)
                    ->first();
                if ($mail === null) {
                    $mail = new Mail();
                    $mail->mail = $adr;
                    $mail->typemail_id = $type;
                    $mail->user_id = Auth::user()->id;
                    $mail->save();
                } else {
                    $mail->typemail_id = $type;
                    $mail->user_id = Auth::user()->id;
                    $mail->save();
                }
                $owner = new MailOwner();
                $owner->mail_id = $mail->id;
                $owner->prefere = $prefere;
                $owner->user_id = Auth::user()->id;
                $owner->an_id = $antenne->id;
                $owner->save();
            }
        }
        return Response::json($antenne->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $antenne = Antenne::find($id);

        /** ---------------------------------------------------------------------------------------
         * On prend aussi la liste des téléphones
         */
        $listeTels = DB::table('telephones')
            ->join('type_tels', 'type_tels.id', '=', 'telephones.typetel_id')
            ->join('telephone_owners', 'telephones.id', '=', 'telephone_owners.telephone_id')
            ->join('antennes', 'antennes.id', '=', 'telephone_owners.an_id')
            ->select('numero', 'type_tels.id AS TYPE_ID', 'prefere',
                'type_tels.libelle', 'telephone_owners.an_id AS AN_ID',
                'telephone_owners.ID AS OWN_ID', 'telephone_owners.notes')
            ->where('an_id', '=', $id)
            ->get();

        /** ---------------------------------------------------------------------------------------
         * On prend aussi la liste des mails
         */
        $listeMails = DB::table('mails')
            ->join('type_mails', 'type_mails.id', '=', 'mails.typemail_id')
            ->join('mail_owners', 'mails.id', '=', 'mail_owners.mail_id')
            ->join('antennes', 'antennes.id', '=', 'mail_owners.an_id')
            ->select('mail', 'type_mails.id AS TYPE_ID', 'prefere',
                'type_mails.libelle', 'mail_owners.an_id AS AN_ID',
                'mail_owners.ID AS OWN_ID')
            ->where('an_id', '=', $id)
            ->get();

        /** ---------------------------------------------------------------------------------------
         * On prend aussi la liste des types d'hébergements
         */
        $listeHebergements = DB::table('antennes_modalite_accueil')
            ->select('type_hebergement_id AS TYPE_ID')
            ->where('antenne_id', '=', $id)
            ->get();

        /** ---------------------------------------------------------------------------------------
         * Une petite transformation des dates
         */
        $antenne->date_arrete = ($antenne->date_arrete != '0000-00-00' ? Carbon::createFromFormat('Y-m-d', $antenne->date_arrete)->format('d/m/Y') : '');
        $antenne->date_ouverture = ($antenne->date_ouverture != '0000-00-00' ? Carbon::createFromFormat('Y-m-d', $antenne->date_ouverture)->format('d/m/Y') : '');
        return Response::json(
            array(
                'AN' => $antenne,
                'TEL' => $listeTels,
                'MAIL' => $listeMails,
                'HEBERGEMENT' => $listeHebergements,
                'UPDATED_DATE' => Carbon::createFromFormat('Y-m-d H:i:s', $antenne->created_at)->format('d/m/Y'),
                'UPDATED_BY' => $antenne->user->name
            ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /** -----------------------------------------------------------------------------------
         * Données de base
         *
         */
        $antenne = Antenne::find($id);

        $antenne->nom = $request->pNom;
        $antenne->finess = $request->pFiness;
        $antenne->arrete_pref = $request->pArrete;

        if ($request->pDateArrete != '') {
            $dateArrete = Carbon::createFromFormat('d/m/Y', $request->pDateArrete);
            $antenne->date_arrete = $dateArrete->format('Y-m-d');
        }
        if ($request->pOpening != '') {
            $dateOpening = Carbon::createFromFormat('d/m/Y', $request->pOpening);
            $antenne->date_ouverture = $dateOpening->format('Y-m-d');
        }
        $antenne->isCONTRIBUANT = $request->pContrib;

        $antenne->adr1 = $request->pAdr1;
        $antenne->adr2 = $request->pAdr2;
        $antenne->adr3 = $request->pAdr3;
        $antenne->cp = $request->pCP;
        $antenne->ville = $request->pVille;
        $antenne->pays_id = ($request->pPays == '' ? null : $request->pPays);

        $antenne->notes = $request->pNotes;

        $antenne->user_id = Auth::user()->id;
        $antenne->active = $request->pActif;
        $antenne->visible = $request->pVisible;
        $antenne->acces = $request->pAcces;

        $antenne->save();

        /** ---------------------------------------------------------------------------------------
         * Type AGE : array - Annule et remplace
         */
        DB::table('antennes_modalite_accueil')
            ->where('antenne_id', '=', $antenne->id)
            ->delete();
        if (isset($request->pModaliteAccueil)) {
            foreach($request->pModaliteAccueil as $modalite) {
                $type = AntenneModaliteAccueil::firstOrNew([
                    'antenne_id' => $antenne->id,
                    'type_hebergement_id' => $modalite,
                ]);
                $type->active = 1;
                $type->user_id = Auth::user()->id;
                $type->save();
            }
        }

        /** ---------------------------------------------------------------------------------------
         * Traitement des mails et des téléphones
         * Pour chaque téléphone, on va faire une première vérification dans la table
         *
         */
        if (isset($request->pTels)) {
            foreach ($request->pTels as $key => $val) {
                $num = $val[0];
                $prefere = $val[1];
                $type = $val[2];

                /** -------------------------------------------------------------------------------
                 * Il se peut que le téléphone existe déjà !
                 */
                $exist = false;
                $tel = Telephone::where('numero', '=', $num)
                    ->first();
                if ($tel === null) {
                    $tel = new Telephone();
                    $tel->numero = $num;
                    $tel->typetel_id = $type;
                    $tel->user_id = Auth::user()->id;
                } else {
                    $exist = true;
                    $tel->typetel_id = $type;
                    $tel->user_id = Auth::user()->id;
                    $tel->save();
                }
                $tel->save();

                $owner = new TelephoneOwner();
                $owner->telephone_id = $tel->id;
                $owner->prefere = $prefere;
                $owner->an_id = $antenne->id;
                $owner->user_id = Auth::user()->id;
                $owner->save();

            }
        }

        if (isset($request->pMails)) {
            foreach ($request->pMails as $key => $val) {
                $adr = $val[0];
                $prefere = $val[1];
                $type = $val[2];

                /** -------------------------------------------------------------------------------
                 * Il se peut que le téléphone existe déjà !
                 */
                $exist = false;
                $mail = Mail::where('mail', '=', $adr)
                    ->first();
                if ($mail === null) {
                    $mail = new Mail();
                    $mail->mail = $adr;
                    $mail->typemail_id = $type;
                    $mail->user_id = Auth::user()->id;
                } else {
                    $exist = true;
                    $mail->typemail_id = $type;
                    $mail->user_id = Auth::user()->id;
                    $mail->save();
                }
                $mail->save();

                $owner = new MailOwner();
                $owner->mail_id = $mail->id;
                $owner->prefere = $prefere;
                $owner->an_id = $antenne->id;
                $owner->user_id = Auth::user()->id;
                $owner->save();

            }
        }

        return Response::json($antenne->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /** -------------------------------------------------------------------------------------------
     *
     * Récupère la liste des antennes d'un établissement
     *
     * @param $idPO
     * @return mixed
     */
    public function listDatatable ($id) {

        $antennes = Antenne::where('etablissement_id', '=', $id)
            ->get();

        /** @var Initialisation du tableau de sortie $result */
        $result = ['aaData' => []];

        /** @var On parcourt les logs $infoPatch */
        foreach($antennes as $antenne){


            /** -----------------------------------------------------------------------------------
             * Données de base
             */
            $id = $antenne->id;
            $nom = $antenne->nom;
            $adr1 = $antenne->adr1;
            $cp = $antenne->cp;
            $ville = $antenne->ville;
            $dpt = substr($antenne->cp, 0, 2);

            /** -----------------------------------------------------------------------------------
             * Données de base
             */
            $ville.= ' ('.$dpt.')';

            /** -----------------------------------------------------------------------------------
             * Actions
             */
            $countAA = AdresseAccueil::where('antenne_id', $antenne->id)->count();
            $countPE = Personnel::where('an_id', $antenne->id)->count();
            $countO = Offres::where('an_id', $antenne->id)->count();

            $action = '<a class="visu btn btn-xs blue" data-id="'.$id.'"><i class="fa fa-file-text-o"></i></a>';
            $actions = [];
            $actions[0][] = '<a class="adresse font-green-meadow" data-id="'.$id.'"><i class="fa fa-send-o font-green-meadow"></i> Adresses Accueil'.($countAA != 0 ? ' <span class="badge badge-danger"> '.$countAA.' </span>' : '').'</a>';
            $actions[0][] = '<a class="personnel font-purple-soft" data-id="' . $id . '"><i class="fa fa-users font-purple-soft"></i> Personnel ' . ($countPE != 0 ? ' <span class="badge badge-danger"> '.$countPE.' </span>' : '') . '</a>';
            $actions[1][] = '<a class="telephone" data-id="'.$id.'"><i class="fa fa-phone"></i>Téléphones / Fax</a>';
            $actions[1][] .= '<a class="mail" data-id="'.$id.'"><i class="fa fa-send"></i>Mails</a>';
            $actions[1][] = '<a class="offres" data-id="'.$id.'"><i class="fa fa-sticky-note-o"></i>Offres d\'Emploi '.($countO != 0 ? ' <span class="badge badge-danger"> '.$countO.' </span>' : '').'</a>';
            $actions[0][] .= '<a class="modif" data-id="'.$id.'"><i class="fa fa-edit"></i>Modifier</a>';


            /** -----------------------------------------------------------------------------------
             * Build Button
             */
            $buttons = $this->buildButtons($actions, 'down');


            $result['aaData'][] = [
                '<span data-nom="'.$antenne->nom.'">'. $nom .'</span>',
                $adr1,
                $ville,
//                $quick,
                $action.$buttons
            ];
        }

        return Response::json($result);
    }

    public function getData($id) {

        $listeModalites = array();
        $listes = AntenneModaliteAccueil::where('antenne_id', '=', $id)
            ->get();
        if ($listes != null) {
            foreach($listes as $item){
                $listeModalites[] = $item->type_hebergement_id;
            }
        }
        $an = Antenne::find($id);

        $dateArrete = ($an->date_arrete != '' ? Carbon::createFromFormat('Y-m-d', $an->date_arrete) : '');
        $dateOuverture = ($an->date_ouverture != '' ? Carbon::createFromFormat('Y-m-d', $an->date_ouverture) : '');

        return Response::json(array(
            'FINESS' => $an->finess,
            'ARRETE' => $an->arrete_pref,
            'DATE_ARRETE' => $dateArrete->format('d/m/Y'),
            'DATE_OUVERTURE' => $dateOuverture->format('d/m/Y'),
            'ADR1' => ($an->adr1 === null ? '' : $an->adr1),
            'ADR2' => ($an->adr2 === null ? '' : $an->adr2),
            'ADR3' => ($an->adr3 === null ? '' : $an->adr3),
            'CP' => ($an->cp === null ? '' : $an->cp),
            'VILLE' => ($an->ville === null ? '' : $an->ville),
            'PAYS' => ($an->pays_id === null ? '' : $an->pays_id),

            // Pour les données suivantes, on remonte à l'établissement
            'TYPE_PUBLIC' => ($an->etablissement->type_public_id === null ? '' : $an->etablissement->type_public_id),
            'DETAIL_TYPE_PUBLIC' => ($an->etablissement->detail_type_public_id === null ? '' : $an->etablissement->detail_type_public_id),
            'NB_PLACES' => $an->etablissement->nb_places ,
            'AGE' => $an->etablissement->age ,
        ));
    }

}
