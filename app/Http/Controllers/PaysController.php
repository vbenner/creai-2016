<?php

namespace App\Http\Controllers;

use App\Pays;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class PaysController extends Controller
{

    protected $table = 'pays';
    protected $fillable = array('libelle', 'active', 'user_id');

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('params.pays', [
            'titre' => 'Paramétrage',
            'subtitre' => 'Pays',
            'sidebar' => $this->getSidebar()//json_decode(json_encode($tabBlocs))
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pays = Pays::firstOrNew([
            'libelle' => $request->pLibelle
        ]);
        $pays->user_id = Auth::user()->id;
        $pays->save();
        return Response::json($pays->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pays = Pays::find($id);
        return Response::json($pays);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $pays = Pays::find($id);
        $pays->active = $request->pValue;
        $pays->user_id = Auth::user()->id;
        $pays->save();
        return Response::json($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /** ---------------------------------------------------------------------------------------
         * Recherche d'un enregistrement identique avec un autre ID
         */
        $pays = Pays::where('libelle', 'like', $request->pLibelle)
            ->where('id', '<>', $id)
            ->first();
        if ($pays === null) {
            $pays = Pays::find($id);
            $pays->libelle = $request->pLibelle;
            $pays->user_id = Auth::user()->id;
            $pays->save();
            return Response::json($id);
        } else {
            return Response::json(-1);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listZL() {
        $payss = Pays::where('active', '=', 1)->orderBy('libelle')->get();
        $result = array();
        foreach($payss as $pays){
            array_push($result, array(
                'id'   => $pays->id,
                'libelle'  => $pays->libelle,
            ));
        }
        return Response::json(array('LISTE' => $result));
    }

    public function listDatatable() {
        $payss = Pays::all();

        /** @var Initialisation du tableau de sortie $result */
        $result = ['aaData' => []];

        /** @var On parcourt les logs $infoPatch */
        foreach($payss as $pays){


            /** -----------------------------------------------------------------------------------
             * Données
             */
            $id = $pays->id;
            $libelle = $pays->libelle;

            /** -----------------------------------------------------------------------------------
             * Actions
             */
            $active =  $this->buildSwitch($id, $pays->active);
            $action = '<a class="modif btn btn-xs blue" data-id="'.$id.'"><i class="fa fa-edit"></i></a>';
            $result['aaData'][] = [
                $libelle,
                $active,
                $action
            ];
        }

        return Response::json($result);
    }
}
