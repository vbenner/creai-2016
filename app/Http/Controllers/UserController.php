<?php

namespace App\Http\Controllers;

use App\Bloc;
use App\BlocGrant;
use App\Mail;
use App\Module;
use App\ModuleGrant;
use App\Submodule;
use App\SubmoduleGrant;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Hash;
class UserController extends Controller
{
    protected $table = 'users';
    protected $fillable = ['name', 'email', 'password', 'active', 'protected'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('params.user', [
            'titre' => 'Paramétrage',
            'subtitre' => 'Utilisateurs',
            'sidebar' => $this->getSidebar()//json_decode(json_encode($tabBlocs))
        ]);    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /** ---------------------------------------------------------------------------------------
         * On ne peut pas créer d'utilisateur SI un mail existe déjà
         */
        $user = User::where('email', '=', $request->pMail)->first();
        if ($user != null) {
            return Response::json(-1);
        }

        /** ---------------------------------------------------------------------------------------
         * On génère un nouvel utilisateur
         */
        $user = new User;
        $user->email = $request->pMail;
        $user->name = $request->pNom;
        $user->role_id = $request->pRole;

        /** ---------------------------------------------------------------------------------------
         * Et un mot de passe costaud
         */
        $random = str_random(10);
        $hashed_random_password = Hash::make($random);
        $user->password = $hashed_random_password;
        $user->save();

        /** -----------------------------------------------------------------------------------
         * On envoie le mail
         */
        \Illuminate\Support\Facades\Mail::send('emails.inscription',[
            'nom' => $user->name,
            'login' => $user->email,
            'password' => $random,
        ], function ($message){
            $message->subject('Inscription Nouvel Annuaire CREAI');
            $message->to("vbenner@pageup.fr");
        });

        return Response::json($user->id);


        /** ---------------------------------------------------------------------------------------
         * Envoi du mail
         */
        return Response::json($user->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return Response::json($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $user = User::find($id);
        $user->active = $request->pValue;
        $user->save();
        return Response::json($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        /** ---------------------------------------------------------------------------------------
         * Recherche d'un enregistrement identique avec un autre ID
         */
        $user = User::where('email', '=', $request->pMail)
            ->where('id', '<>', $id)
            ->first();
        if ($user === null) {
            $user = User::find($id);
            $user->name = $request->pNom;
            $user->role_id = $request->pRole;
            $user->save();

            /** -----------------------------------------------------------------------------------
             * C'est fini
             */
            return Response::json($id);
        } else {
            return Response::json(-1);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listDatatable() {
        $users = User::all();

        /** @var Initialisation du tableau de sortie $result */
        $result = ['aaData' => []];

        /** @var On parcourt les logs $infoPatch */
        foreach($users as $user){


            /** -----------------------------------------------------------------------------------
             * Données
             */
            $id = $user->id;
            $name = $user->name;
            $email = $user->email;
            $role = $user->role->name;
            $roleCode = $user->role->code;
            switch ($roleCode) {
                case 'RO':
                case 'AD':
                    $role = '<span class="label label-danger"> '.$role. ' </span>';
                    break;
                case 'DI':
                case 'SD':
                    $role = '<span class="label label-primary"> '.$role. ' </span>';
                    break;
                case 'SE':
                    $role = '<span class="label label-info"> '.$role. ' </span>';
                    break;
                case 'CT':
                    $role = '<span class="label label-success"> '.$role. ' </span>';
                    break;
                case 'ST':
                    $role = '<span class="label label-default"> '.$role. ' </span>';
                    break;
            }
            /** -----------------------------------------------------------------------------------
             * Données
             */

            /** -----------------------------------------------------------------------------------
             * Actions
             */
            $active =  $this->buildSwitch($id, $user->active);

            /** -----------------------------------------------------------------------------------
             * Droits d'Accès
             *
             * On va comparer le nombre de sous-modules AVEC le nombre de lignes où apparaît
             * l'utilisateur courant.
             */
            $sm = DB::table('submodules')->where('active', '=', 1)->count();
            $droits = DB::table('submodules_grants')->where('user_id', '=', $id)->count();
            $color = 'grey-silver';
            if ($droits == 0) {
                $color = 'red';
            } elseif ($sm == $droits) {
                $color = 'green-haze';
            } elseif ($sm < $droits) {
                $color = 'red-mint';
            }
            $acces = '<a class="acces btn btn-xs '.$color.'" data-id="'.$id.'"><i class="fa fa-sliders"></i> Accès '.'</a>';

            /** -----------------------------------------------------------------------------------
             * Actions
             */
            $action = '<a class="modif btn btn-xs blue" data-id="'.$id.'"><i class="fa fa-edit"></i></a>';
            $result['aaData'][] = [
                $name,
                $email,
                ($user->protected == 1 ? '' : $active),
                $role,
                ($user->protected == 1 && Auth::user()->id != 1 ? '' : $acces.' '.$action)
            ];
        }

        return Response::json($result);
    }

    public function listGrants($id) {

        /** ---------------------------------------------------------------------------------------
         * On récupère la liste des BLOCS
         */
        $res = [];
        $blocs = Bloc::where('active', '=', 1)->get();
        foreach ($blocs as $bloc) {

            /** -----------------------------------------------------------------------------------
             * On récupère maintenant la liste des MODULES associés à ces BLOCS
             */
            $modules = Module::where('active', '=', 1)
                ->where('bloc_id', '=', $bloc->id)
                ->get();
            $tabModules = [];
            foreach ($modules as $module) {


                /** -------------------------------------------------------------------------------
                 * Et on récupère enfin la liste des SOUS-MODULES associés à ces MODULES
                 */
                $subModules = Submodule::where('active', '=', 1)
                    ->where('module_id', '=', $module->id)
                    ->get();
                $tabSubModules = [];

                foreach ($subModules as $subModule) {

                    /** ---------------------------------------------------------------------------
                     * Si, par hasard, il n'y a pas de correspondance entre SUBMODULES
                     * et SUMODULES_GRANTS alors on va créer une nouvelle ligne
                     **/
                    $grant = SubmoduleGrant::where('submodule_id', '=', $subModule->id)
                        ->where('user_id', '=', $id)
                        ->first();

                    if ($grant == null) {
                        $new = new SubmoduleGrant();
                        $new->submodule_id = $subModule->id;
                        $new->user_id = $id;
                        $new->grant = 0;
                        $new->add = 0;
                        $new->edit = 0;
                        $new->save();
                    }
                    $tabSubModules['SUBMODULE'][] = array(
                        'ID' => $subModule->id,
                        'NAME' => $subModule->libelle,
                        'GRANT' => ($grant == null ? 0 : $grant->grant),
                        'ADD' => ($grant == null ? 0 : $grant->add),
                        'EDIT' => ($grant == null ? 0 : $grant->edit),
                    );
                }

                /** -------------------------------------------------------------------------------
                 * Si, par hasard, il n'y a pas de correspondance entre BLOCS et BLOCS_GRANTS
                 * alors on va créer une nouvelle ligne
                 **/
                $grant = ModuleGrant::where('module_id', '=', $module->id)
                    ->where('user_id', '=', $id)
                    ->first();
                if ($grant == null) {
                    $new = new ModuleGrant();
                    $new->module_id = $module->id;
                    $new->user_id = $id;
                    $new->grant = 0;
                    $new->add = 0;
                    $new->edit = 0;
                    $new->save();
                }

                $tabModules['MODULE'][] = array(
                    'ID' => $module->id,
                    'NAME' => $module->libelle,
                    'GRANT' => ($grant == null ? 0 : $grant->grant),
                    'ADD' => ($grant == null ? 0 : $grant->add),
                    'EDIT' => ($grant == null ? 0 : $grant->edit),
                    'SUBMODULES' => $tabSubModules
                );
            }

            /** -----------------------------------------------------------------------------------
             * Si, par hasard, il n'y a pas de correspondance entre BLOCS et BLOCS_GRANTS
             * alors on va créer une nouvelle ligne
             **/
            $grant = BlocGrant::where('bloc_id', '=', $bloc->id)
                ->where('user_id', '=', $id)
                ->first();
            if ($grant == null) {
                $new = new BlocGrant();
                $new->bloc_id = $bloc->id;
                $new->user_id = $id;
                $new->grant = 0;
                $new->add = 0;
                $new->edit = 0;
                $new->save();
            }
            $res['BLOC'][] = array(
                'ID' => $bloc->id,
                'NAME' => $bloc->libelle,
                'GRANT' => ($grant == null ? 0 : $grant->grant),
                'ADD' => ($grant == null ? 0 : $grant->add),
                'EDIT' => ($grant == null ? 0 : $grant->edit),
                'MODULES' => $tabModules
            );
        }
        return Response::json($res);
    }

    public function updateGrant($type, $field, $id, $user, $value) {
        switch ($type) {
            case 'bloc':
                $what = BlocGrant::where('bloc_id', '=', $id)
                    ->where('user_id', '=', $user)
                    ->first();
                break;
            case 'module':
                $what = ModuleGrant::where('module_id', '=', $id)
                    ->where('user_id', '=', $user)
                    ->first();
                break;
            case 'submodule':
                $what = SubmoduleGrant::where('submodule_id', '=', $id)
                    ->where('user_id', '=', $user)
                    ->first();
                break;
        }
        $what->{$field} = $value;
        $what->save();
        echo $type.', '.$field;
    }
}
