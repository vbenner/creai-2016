<?php

namespace App\Http\Controllers;

use App\Etablissement;
use App\ModaliteAccueil;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class ModaliteAccueilController extends Controller
{
    protected $table = 'modalite_accueil';
    protected $fillable = ['libelle', 'MS', 'note', 'active', 'user_id'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('params.modaliteaccueil', [
            'titre' => 'Paramétrage',
            'subtitre' => 'Modalités d\'Accueil',
            'sidebar' => $this->getSidebar()//json_decode(json_encode($tabBlocs))
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $type = ModaliteAccueil::find($id);
        return Response::json($type);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $type = ModaliteAccueil::firstOrNew([
            'libelle' => $request->pLibelle
        ]);
        $type->MS = $request->pMS;
        $type->note = $request->pNote;
        $type->user_id = Auth::user()->id;
        $type->save();
        return Response::json($type->id);    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $type = ModaliteAccueil::find($id);
        $type->active = $request->pValue;
        $type->user_id = Auth::user()->id;
        $type->save();
        return Response::json($id);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /** ---------------------------------------------------------------------------------------
         * Recherche d'un enregistrement identique avec un autre ID
         */
        $type = ModaliteAccueil::where('libelle', 'like', $request->pLibelle)
            ->where('id', '<>', $id)
            ->first();
        if ($type === null) {
            $type = ModaliteAccueil::find($id);
            $type->libelle = $request->pLibelle;
            $type->MS = $request->pMS;
            $type->note = $request->pNote;
            $type->user_id = Auth::user()->id;
            $type->save();
            return Response::json($id);
        } else {
            return Response::json(-1);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listDatatable() {
        $types = ModaliteAccueil::all();

        /** @var Initialisation du tableau de sortie $result */
        $result = ['aaData' => []];

        /** @var On parcourt les logs $infoPatch */
        foreach($types as $type){


            /** -----------------------------------------------------------------------------------
             * Données
             */
            $id = $type->id;
            $libelle = $type->libelle;

            $ms = '<span class="label '.($type->MS ? 'label-success' : 'label-danger').'"> '.($type->MS ? 'OUI' : 'NON').'</span>';

            /** -----------------------------------------------------------------------------------
             * Actions
             */
            $active =  $this->buildSwitch($id, $type->active);
            $action = '<a class="modif btn btn-xs blue" data-id="'.$id.'"><i class="fa fa-edit"></i></a>';
            $result['aaData'][] = [
                $libelle,
                $ms,
                $active,
                $action
            ];
        }

        return Response::json($result);
    }

    public function listZL($filter)
    {
        $types = ModaliteAccueil::where('active', '=', 1)
            ->where('MS', '=', $filter)
            ->orderBy('libelle')->get();
        $result = array();
        foreach ($types as $type) {
            array_push($result, array(
                'id' => $type->id,
                'libelle' => $type->libelle,
            ));
        }
        return Response::json(array('LISTE' => $result));
    }

    public function listZLAll()
    {
        $types = ModaliteAccueil::where('active', '=', 1)
            ->orderBy('libelle')
            ->select('libelle')
            ->distinct()
            ->get();
        $result = array();
        foreach ($types as $type) {
            array_push($result, array(
                'libelle' => $type->libelle,
            ));
        }
        return Response::json(array('LISTE' => $result));
    }

    public function listZLEtablissement($id)
    {
        /** ---------------------------------------------------------------------------------------
         * L'établissement donne le MS
         */
        $etablissement = Etablissement::find($id);
        $ms = $etablissement->typeSecteur->isSecteurMS;
        return $this->listZL($ms);
    }
}