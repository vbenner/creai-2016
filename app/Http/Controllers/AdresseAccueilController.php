<?php

namespace App\Http\Controllers;

use App\AdresseAccueil;

use App\Antenne;
use App\Mail;
use App\MailOwner;
use App\Telephone;
use App\TelephoneOwner;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class AdresseAccueilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /** -----------------------------------------------------------------------------------
         * Données de base
         *
         * pET: idET,
         * pAN: idAN,
         */
        $adresse = AdresseAccueil::firstOrNew([
            'nom' => $request->pNom,
            'etablissement_id' => ($request->pET == '' ? null : $request->pET),
            'antenne_id' => ($request->pAN == '' ? null : $request->pAN),
        ]);
        $adresse->type_public_id = ($request->pTypePublic == '' ? null : $request->pTypePublic);
        $adresse->detail_type_public_id = ($request->pDetailTypePublic == '' ? null : $request->pDetailTypePublic);
        $adresse->nb_places = $request->pNbPlaces;
        $adresse->age = $request->pAge;

        $adresse->adr1 = $request->pAdr1;
        $adresse->adr2 = $request->pAdr2;
        $adresse->adr3 = $request->pAdr3;
        $adresse->cp = $request->pCP;
        $adresse->ville = $request->pVille;
        $adresse->pays_id = ($request->pPays == '' ? null : $request->pPays);

        $adresse->notes = $request->pNotes;
        $adresse->user_id = Auth::user()->id;
        $adresse->active = $request->pActif;

        $adresse->save();

        /** ---------------------------------------------------------------------------------------
         * Traitement des mails et des téléphones
         * Pour chaque téléphone, on va faire une première vérification dans la table
         *
         */
        if (isset($request->pTels)) {
            foreach ($request->pTels as $key => $val) {
                $num = $val[0];
                $prefere = $val[1];
                $type = $val[2];

                /** -------------------------------------------------------------------------------
                 * Il se peut que le téléphone existe déjà !
                 */
                $exist = false;
                $tel = Telephone::where('numero', '=', $num)
                    ->first();
                if ($tel === null) {
                    $tel = new Telephone();
                    $tel->numero = $num;
                    $tel->typetel_id = $type;
                    $tel->user_id = Auth::user()->id;
                } else {
                    $exist = true;
                    $tel->typetel_id = $type;
                    $tel->user_id = Auth::user()->id;
                    $tel->save();
                }
                $tel->save();

                $owner = new TelephoneOwner();
                $owner->telephone_id = $tel->id;
                $owner->prefere = $prefere;
                $owner->aa_id = $adresse->id;
                $owner->user_id = Auth::user()->id;
                $owner->save();

            }
        }

        if (isset($request->pMails)) {
            foreach ($request->pMails as $key => $val) {
                $adr = $val[0];
                $prefere = $val[1];
                $type = $val[2];
                $mail = Mail::where('mail', '=', $adr)
                    ->first();
                if ($mail === null) {
                    $mail = new Mail();
                    $mail->mail = $adr;
                    $mail->typemail_id = $type;
                    $mail->user_id = Auth::user()->id;
                    $mail->save();
                } else {
                    $mail->typemail_id = $type;
                    $mail->user_id = Auth::user()->id;
                    $mail->save();
                }
                $owner = new MailOwner();
                $owner->mail_id = $mail->id;
                $owner->prefere = $prefere;
                $owner->user_id = Auth::user()->id;
                $owner->aa_id = $adresse->id;
                $owner->save();
            }
        }
        return Response::json($adresse->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $aa = AdresseAccueil::find($id);

        /** ---------------------------------------------------------------------------------------
         * On prend aussi la liste des téléphones
         */
        $listeTels = DB::table('telephones')
            ->join('type_tels', 'type_tels.id', '=', 'telephones.typetel_id')
            ->join('telephone_owners', 'telephones.id', '=', 'telephone_owners.telephone_id')
            ->join('adresses_accueil', 'adresses_accueil.id', '=', 'telephone_owners.aa_id')
            ->select('numero', 'type_tels.id AS TYPE_ID', 'prefere',
                'type_tels.libelle', 'telephone_owners.aa_id AS AA_ID',
                'telephone_owners.ID AS OWN_ID', 'telephone_owners.notes')
            ->where('aa_id', '=', $id)
            ->get();

        /** ---------------------------------------------------------------------------------------
         * On prend aussi la liste des mails
         */
        $listeMails = DB::table('mails')
            ->join('type_mails', 'type_mails.id', '=', 'mails.typemail_id')
            ->join('mail_owners', 'mails.id', '=', 'mail_owners.mail_id')
            ->join('adresses_accueil', 'adresses_accueil.id', '=', 'mail_owners.aa_id')
            ->select('mail', 'type_mails.id AS TYPE_ID', 'prefere',
                'type_mails.libelle', 'mail_owners.aa_id AS AA_ID',
                'mail_owners.ID AS OWN_ID')
            ->where('aa_id', '=', $id)
            ->get();

        /** ---------------------------------------------------------------------------------------
         * On va désactiver le logo POUR le SHOW (sans faire de save();
         */
        return Response::json(
            array(
                'AA' => $aa,
                'TEL' => $listeTels,
                'MAIL' => $listeMails,
                'UPDATED_DATE' => Carbon::createFromFormat('Y-m-d H:i:s', $aa->created_at)->format('d/m/Y'),
                'UPDATED_BY' => $aa->user->name
            ));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $adr = AdresseAccueil::find($id);

        $adr->nom = $request->pNom;
        $adr->type_public_id = ($request->pTypePublic == '' ? null : $request->pTypePublic);
        $adr->detail_type_public_id = ($request->pDetailTypePublic == '' ? null : $request->pDetailTypePublic);

        $adr->nb_places = $request->pNbPlaces;
        $adr->age = $request->pAge;

        $adr->adr1 = $request->pAdr1;
        $adr->adr2 = $request->pAdr2;
        $adr->adr3 = $request->pAdr3;
        $adr->cp = $request->pCP;
        $adr->ville = $request->pVille;
        $adr->pays_id = ($request->pPays == '' ? null : $request->pPays);

        $adr->notes = $request->pNotes;

        $adr->user_id = Auth::user()->id;
        $adr->active = $request->pActif;

        $adr->save();

        /** ---------------------------------------------------------------------------------------
         * Traitement des mails et des téléphones
         * Pour chaque téléphone, on va faire une première vérification dans la table
         *
         */
        if (isset($request->pTels)) {
            foreach ($request->pTels as $key => $val) {
                $num = $val[0];
                $prefere = $val[1];
                $type = $val[2];

                /** -------------------------------------------------------------------------------
                 * Il se peut que le téléphone existe déjà !
                 */
                $exist = false;
                $tel = Telephone::where('numero', '=', $num)
                    ->first();
                if ($tel === null) {
                    $tel = new Telephone();
                    $tel->numero = $num;
                    $tel->typetel_id = $type;
                    $tel->user_id = Auth::user()->id;
                } else {
                    $exist = true;
                    $tel->typetel_id = $type;
                    $tel->user_id = Auth::user()->id;
                    $tel->save();
                }
                $tel->save();

                $owner = new TelephoneOwner();
                $owner->telephone_id = $tel->id;
                $owner->prefere = $prefere;
                $owner->aa_id = $adr->id;
                $owner->user_id = Auth::user()->id;
                $owner->save();

            }
        }

        if (isset($request->pMails)) {
            foreach ($request->pMails as $key => $val) {
                $adr = $val[0];
                $prefere = $val[1];
                $type = $val[2];

                /** -------------------------------------------------------------------------------
                 * Il se peut que le téléphone existe déjà !
                 */
                $exist = false;
                $mail = Mail::where('mail', '=', $adr)
                    ->first();
                if ($mail === null) {
                    $mail = new Mail();
                    $mail->mail = $adr;
                    $mail->typemail_id = $type;
                    $mail->user_id = Auth::user()->id;
                } else {
                    $exist = true;
                    $mail->typemail_id = $type;
                    $mail->user_id = Auth::user()->id;
                    $mail->save();
                }
                $mail->save();

                $owner = new MailOwner();
                $owner->mail_id = $mail->id;
                $owner->prefere = $prefere;
                $owner->aa_id = $adr->id;
                $owner->user_id = Auth::user()->id;
                $owner->save();

            }
        }

        return Response::json($adr->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /** -------------------------------------------------------------------------------------------
     *
     * Récupère la liste des adresses d'accueil d'un établissement ou d'une antenne
     *
     * @param $idPO
     * @return mixed
     */
    public function listDatatable ($type, $id) {

        switch ($type) {
            case 'et':
                $adresses = AdresseAccueil::where('etablissement_id', '=', $id)
                    ->get();
                break;
            case 'an':
                $adresses = AdresseAccueil::where('antenne_id', '=', $id)
                    ->get();
                break;
        }

        /** @var Initialisation du tableau de sortie $result */
        $result = ['aaData' => []];

        /** @var On parcourt les logs $infoPatch */
        foreach($adresses as $adresse){


            /** -----------------------------------------------------------------------------------
             * Données de base
             */
            $id = $adresse->id;

            $nom = $adresse->nom;
            $typePublic = ($adresse->type_public_id != null ? $adresse->typePublic->libelle : '');
            $detailTypePublic = ($adresse->detail_type_public_id != null ? $adresse->detailTypePublic->libelle : '');
            $adr1 = $adresse->adr1;
            $cp = $adresse->cp;
            $ville = $adresse->ville;
            $dpt = substr($adresse->cp, 0, 2);

            /** -----------------------------------------------------------------------------------
             * Données de base
             */
            $ville.= ' ('.$dpt.')';

            /** -----------------------------------------------------------------------------------
             * Actions
             */
            $action = '<a class="visu btn btn-xs blue" data-id="'.$id.'"><i class="fa fa-file-text-o"></i></a>';
            $actions=[];
            $actions[0][] = '<a class="telephone" data-id="'.$id.'"><i class="fa fa-phone"></i>Téléphones / Fax</a>';
            $actions[0][] .= '<a class="mail" data-id="'.$id.'"><i class="fa fa-send"></i>Mails</a>';
            $actions[0][] .= '<a class="modif" data-id="'.$id.'"><i class="fa fa-edit"></i>Modifier</a>';

            /** -----------------------------------------------------------------------------------
             * Build Button
             */
            $buttons = $this->buildButtons($actions, 'down');


            $result['aaData'][] = [
                $nom,
                $typePublic.'<br/>'.$detailTypePublic,
                $adr1,
                $ville,
                $action.$buttons
            ];
        }

        return Response::json($result);
    }

}
