<?php

namespace App\Http\Controllers;

use App\Antenne;
use App\Bloc;
use App\BlocGrant;
use App\Etablissement;
use App\Module;
use App\ModuleGrant;
use App\Organisme;
use App\Pole;
use App\Submodule;
use App\SubmoduleGrant;
use App\User;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;


    /** -------------------------------------------------------------------------------------------
     * Build Active / Inactive
     */
    public function buildSwitch($id, $status)
    {

        if ($status == 1) {
            return '<span style="font-size: 0px;">1</span><input type="checkbox" id="' . $id . '" class="make-switch" data-on-color="success" data-on-text="Oui" data-off-color="danger" data-off-text="Non" data-size="mini" checked />';
        } else {
            return '<span style="font-size: 0px;">0</span><input type="checkbox" id="' . $id . '" class="make-switch" data-on-color="success" data-on-text="Oui" data-off-color="danger" data-off-text="Non" data-size="mini" />';
        }

    }

    public function getSidebar()
    {

        /** ---------------------------------------------------------------------------------------
         * Liste des blocs autorisés
         *
         * bloc_granted + inner bloc (WHERE bloc.active = 1)
         */
        $sidebar = '';
        $treeSidebar = json_decode($this->getTreeSidebar());

        #echo '<pre>';
        #print_r($treeSidebar);
        #echo '</pre>';
        #die();
        #return;

        foreach ($treeSidebar as $bloc) {

            #echo '<pre>';
            #print_r($bloc);
            #echo '</pre>';
            #die();
            #return;

            if ($bloc->LIB != '') {
                $sidebar .= '<li class="heading">';
            } else {

            }

            if ($bloc->LIB != '') {
                $sidebar .= '<h3 class="uppercase">' . $bloc->LIB . '</h3>';
            } else {

            }

            foreach ($bloc->MODULES as $module) {
                $sidebar .= '<li class="nav-item">';
                $sidebar .= '<a class="nav-link nav-toggle" href="' . $module->PATH . '">';
                $sidebar .= '<i class="fa ' . $module->LOGO . '"></i>';
                $sidebar .= '<span class="title">' . $module->LIB . '</span>';
                if ($module->SUBMODULES != null) {
                    $sidebar .= '<span class="arrow" ></span ></a>';
                    $sidebar .= '<ul class="sub-menu" style = "display: none;" >';
                    foreach ($module->SUBMODULES as $submodule) {
                        $sidebar .= '<li class="nav-item">';
                        $sidebar .= '<a class="nav-link" data-href="" href="/' . $module->PATH . '/' . $submodule->PATH . '" >' . $submodule->LIB . '</a>';
                        $sidebar .= '</li >';
                    }
                    $sidebar .= '</ul >';
                } else {
                    $sidebar .= '</a>';
                }
                $sidebar .= '</li>';
            }

            #if ($bloc->LIB != '') {
            #    $sidebar .= '</li>';
            #} else {
            #
            #}
        }

        #die();
        #echo $sidebar;
        #die();

        return ($sidebar);

        /*
                @foreach($bloc->MODULES as $module)
                    <li class="nav-item start active">
                        <a class="nav-link nav-toggle" href="{{$module->PATH}}">
                            <i class="fa {{$module->LOGO}}"></i>
                            <span class="title">{{$module->LIB}}</span>
    @if($module->SUBMODULES != null)
                                <span class="arrow"></span>

                                <ul class="sub-menu" style="display: none;">
        @foreach($module->SUBMODULES as $submodule)
                                        <li class="nav-item ">
                                            <a class="nav-link" href="">{{$submodule->LIB}}</a>
                                        </li>
    @endforeach

                                </ul>

    @endif
                        </a>
                    </li>

    @endforeach
                    </li>
    @endforeach
*/


    }

    public function buildButtons($tab, $sens) {

        #print_r($tab);
        #echo $sens;
        #die();
        /** ---------------------------------------------------------------------------------------
         * Combien de colonnes ? On en fait, on n'en gère qu final que 2, donc le code est
         * un peu faussé...
         */
        $dimension = count($tab);
        $multi = ($dimension > 1 ? 'multi-column columns-'.$dimension : '');
        $uls = '';
        $buttons = '
            <div class="btn-group drop'.$sens.'">
                <div class="dropdown">
                    <button class="btn btn-xs dropdown-toggle" data-toggle="dropdown">Actions <span class="caret"></span>
                    </button>';
        if ($dimension>1) {
            $buttons.='<ul class="dropdown-menu multi-column columns-'.$dimension.' pull-right">';
            $buttons.='<div class="row" style="margin-left: 0px; margin-right: -1px;">';
            for($i=0; $i < $dimension; $i++) {
                $uls .= '<div class="col-sm-6" style="padding-left: 0px; padding-right: 0px;">';
                $uls .= '<ul class="multi-column-dropdown">';
                foreach ($tab[$i] as $item) {
                    if ($item == '') {
                        $uls.='<li class="divider"></li>';
                    } else {
                        $uls.='<li>'.$item.'</li>';
                    }
                }
                $uls .= '</ul></div>';
            }
            $buttons .= $uls.'</div>';
            $buttons .= '</ul>';
        } else {
            $uls .= '<ul class="dropdown-menu pull-right">';
            foreach ($tab[0] as $item) {
                if ($item == '') {
                    $uls.='<li class="divider"></li>';
                } else {
                    $uls.='<li>'.$item.'</li>';
                }
            }
            $uls .= '</ul>';
            $buttons.= $uls;
        }
        $buttons .= '</div></div>';

        return $buttons;
    }

    /** -------------------------------------------------------------------------------------------
     * Cette fonction retourne le MENU
     */
    public function getTreeSidebar()
    {

        /** ---------------------------------------------------------------------------------------
         * Liste des blocs autorisés
         *
         * bloc_granted + inner bloc (WHERE bloc.active = 1)
         */

        $blocsGranted = BlocGrant::where('user_id', Auth::user()->id)
            ->join('blocs', 'blocs.id', '=', 'blocs_grants.bloc_id')
            ->where('grant', '=', 1)
            ->orderBy('ordre', 'asc')
            ->get();

        //$blocsGranted = BlocGrant::where('user_id', Auth::user()->id)->blocs()->orderBy('ordre', 'asc')->get();

        #echo '<pre>';
        #print_r($blocsGranted);
        #echo '</pre>';
        $i = 1;
        foreach ($blocsGranted as $blocGranted) {

            $tabModules = array();

            /** -----------------------------------------------------------------------------------
             * Pour chaque BLOC autorisé, on va regarder quels sont les MODULES autorisés pour
             * lesquels le USER est autorisé
             *
             * On va faire simple, on regarde juste la liste des modules et, pour chacun, on ira
             * compter le nombre d'occurences dans la table modules_gran
             */
            $modulesGranted = ModuleGrant::where('user_id', Auth::user()->id)
                ->join('modules', 'modules.id', '=', 'modules_grants.module_id')
                ->where('grant', '=', 1)
                ->orderBy('ordre', 'asc')
                ->get();

            #$profiles = Profile::whereHas('user', function ($q) use ($id) {
            #    $q->where('users.id', $id);
            #})->get();

            foreach ($modulesGranted as $moduleGranted) {

                $tabSubmodules = array();

                /** -------------------------------------------------------------------------------
                 * Et pour chaque MODULE AUTORISE, on regarde ses SOUS-MODULES autorisés
                 */
                if ($moduleGranted->modules->bloc_id == $blocGranted->bloc_id &&
                    $moduleGranted->modules->active == 1
                ) {

                    if ($moduleGranted->modules->has_sub_module == 1) {

                        /** ---------------------------------------------------------------------------
                         * On récupère maintenant les SOUS-MODULES de cet USER
                         *
                         */
                        $submodulesGranted = SubmoduleGrant::where(
                            'user_id', Auth::user()->id)
                            ->join('submodules', 'submodules.id', '=', 'submodules_grants.submodule_id')
                            ->where('grant', '=', 1)
                            ->get();
                        foreach ($submodulesGranted as $submoduleGranted) {

                            if ($submoduleGranted->submodules->active == 1 &&
                                $submoduleGranted->submodules->module_id == $moduleGranted->module_id
                            ) {

                                $tabSubmodules[] = array(
                                    'ID' => $submoduleGranted->submodules->id,
                                    'LIB' => $submoduleGranted->submodules->libelle,
                                    'PATH' => $submoduleGranted->submodules->cheminSousModule
                                );

                            }
                        }

                        $tabModules[] = array(
                            'ID' => $moduleGranted->modules->id,
                            'LIB' => $moduleGranted->modules->libelle,
                            'LOGO' => $moduleGranted->modules->logo,
                            'PATH' => $moduleGranted->modules->path,
                            'ACCORDEON' => $moduleGranted->modules->accordeon,
                            'SUBMODULES' => $tabSubmodules
                        );

                    } else {

                        $tabModules[] = array(
                            'ID' => $moduleGranted->modules->id,
                            'LIB' => $moduleGranted->modules->libelle,
                            'LOGO' => $moduleGranted->modules->logo,
                            'PATH' => $moduleGranted->modules->path,
                            'ACCORDEON' => $moduleGranted->modules->accordeon,
                            'SUBMODULES' => null
                        );

                    }
                }


                /** -------------------------------------------------------------------------------
                 * Si le module est AUTORISE, on va regarder la liste des SOUS-MODULES qui eux
                 * mêmes sont autorisés.
                 */
                #if ($isModuleAutorised != 0) {
                #} else {
                #    echo $i++.' - '.$module->libelle.' - '.' OUI '.' pour '.Auth::user()->name."<br/>";
                #}
            }

            $tabBlocs[] = array(
                'ID' => $blocGranted->blocs->id,
                'LIB' => $blocGranted->blocs->libelle,
                'MODULES' => $tabModules
            );

        }

        #$tabBlocs = array(
        #    'ID' => 1,
        #    'LIB' => 'TOTO'
        #);
        #echo '<pre>';
        #var_dump(json_decode(json_encode($tabBlocs)));
        #echo '</pre>';
        #return;

        #echo '<pre>';
        #print_r(
        #    $tabBlocs
        #);
        #echo '</pre>';
        #return;
        #Auth::user()->blocs;
        #
        #print_r($tabBlocs);
        #die();
        return json_encode($tabBlocs);

    }

    public function listeTels($id, $type)
    {

        $table = '';
        $key = '';
        switch ($type) {
            case 'pp':
                $table = 'personnes_physiques';
                $key = 'pp_id';
                $alias = 'ID';
                break;
            case 'oo':
                $table = 'organismes';
                $key = 'oo_id';
                $alias = 'ID';
                break;
            case 'po':
                $table = 'poles';
                $key = 'po_id';
                $alias = 'ID';
                break;
            case 'et':
                $table = 'etablissements';
                $key = 'et_id';
                $alias = 'ID';
                break;
            case 'aa':
                $table = 'adresses_accueil';
                $key = 'aa_id';
                $alias = 'ID';
                break;
            case 'an':
                $table = 'antennes';
                $key = 'an_id';
                $alias = 'ID';
                break;
            case 'pe':
                $table = 'personnels';
                $key = 'pe_id';
                $alias = 'ID';
                break;
        }
        $listeTels = DB::table('telephones')
            ->join('type_tels', 'type_tels.id', '=', 'telephones.typetel_id')
            ->join('telephone_owners', 'telephones.id', '=', 'telephone_owners.telephone_id')
            ->join($table, $table . '.id', '=', 'telephone_owners.' . $key)
            ->select('numero AS NUM', 'type_tels.id AS TYPE_ID',
                'type_tels.libelle', 'telephone_owners.' . $key . ' AS ' . $alias,
                'telephone_owners.id AS OWN_ID', 'telephone_owners.notes', 'prefere')
            ->where($key, '=', $id)
            ->get();
        return Response::json(
            array(
                'TEL' => $listeTels
            ));
    }

    public function listeMails($id, $type) {
        switch ($type) {
            case 'pp':
                $table = 'personnes_physiques';
                $key = 'pp_id';
                $alias = 'ID';
                break;
            case 'oo':
                $table = 'organismes';
                $key = 'oo_id';
                $alias = 'ID';
                break;
            case 'po':
                $table = 'poles';
                $key = 'po_id';
                $alias = 'ID';
                break;
            case 'et':
                $table = 'etablissements';
                $key = 'et_id';
                $alias = 'ID';
                break;
            case 'aa':
                $table = 'adresses_accueil';
                $key = 'aa_id';
                $alias = 'ID';
                break;
            case 'an':
                $table = 'antennes';
                $key = 'an_id';
                $alias = 'ID';
                break;
            case 'pe':
                $table = 'personnels';
                $key = 'pe_id';
                $alias = 'ID';
                break;
        }

        $listeMails = DB::table('mails')
            ->join('type_mails', 'type_mails.id', '=', 'mails.typemail_id')
            ->join('mail_owners', 'mails.id', '=', 'mail_owners.mail_id')
            ->join($table, $table.'.id', '=', 'mail_owners.'.$key)
            ->select('mail', 'type_mails.id AS TYPE_ID',
                'type_mails.libelle', 'mail_owners.'.$key.' AS '.$alias,
                'mail_owners.ID AS OWN_ID', 'mail_owners.notes', 'prefere')
            ->where($key, '=', $id)
            ->get();
        return Response::json(
            array(
                'MAIL' => $listeMails
            ));
    }

    public function listeOffres($id, $type) {
        switch ($type) {
            case 'oo':
                $key = 'oo_id';
                $alias = 'ID';
                break;
            case 'po':
                $key = 'po_id';
                $alias = 'ID';
                break;
            case 'et':
                $key = 'et_id';
                $alias = 'ID';
                break;
            case 'an':
                $key = 'an_id';
                $alias = 'ID';
                break;
        }

        $listeOffres = array();
        $liste = DB::table('offres')
            ->select('id' , 'annee', 'nb', 'date_offre', 'notes')
            ->where($key, '=', $id)
            ->get();
        foreach ($liste as $item){
            $item->date_offre = Carbon::createFromFormat('Y-m-d H:i:s', $item->date_offre)->format('d/m/Y');
            $listeOffres [] = $item;
        }
        return Response::json(
            array(
                'OFFRE' => $listeOffres
            ));
    }

    public function getNbProjets() {
        $nb = Etablissement::where('projet', 1)
            ->where('active', 1)
            ->count();
        return $nb;
    }

    public function getNbOrganismes() {
        $nb = Organisme::where('active', 1)
            ->count();
        return $nb;
    }

    public function getNbPoles() {
        $nb = Pole::where('active', 1)
            ->count();
        return $nb;
    }

    public function getNbEtablissements() {
        $nb = Etablissement::where('active', 1)
            ->count();
        return $nb;
    }

    public function getNbAntennes() {
        $nb = Antenne::where('active', 1)
            ->count();
        return $nb;
    }

    public function cleanPhone($num)
    {

        $cleanNum = str_replace('.', ' ', $num);
        $cleanNum = str_replace(',', ' ', $cleanNum);
        $cleanNum = str_replace('-', ' ', $cleanNum);
        $cleanNum = str_replace('\'', ' ', $cleanNum);
        $cleanNum = str_replace('/', ' ', $cleanNum);
        $cleanNum = str_replace('#', ' ', $cleanNum);
        while (strpos($cleanNum, '  ')) {
            $cleanNum = str_replace('  ', ' ', $cleanNum);
        }

        return trim($cleanNum);
    }
}
