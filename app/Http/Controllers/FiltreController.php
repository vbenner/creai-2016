<?php

namespace App\Http\Controllers;

use App\AdresseAccueil;
use App\Antenne;
use App\Etablissement;
use App\Organisme;
use App\Personnel;
use App\Pole;
use App\TypePublic;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;

class FiltreController extends Controller
{

    // --------------------------------------------------------------------------------------------
    // Création des variables de classe
    // --------------------------------------------------------------------------------------------
    protected $inOO;
    protected $inPO;
    protected $inET;
    protected $inAN;
    protected $inAA;
    protected $inPE;
    protected $nom;
    protected $adresse;
    protected $cp;
    protected $ville;
    protected $finess;
    protected $code;
<<<<<<< HEAD

    protected $sigle;
    protected $nbplaces;

=======
>>>>>>> debug8
    protected $service;
    protected $typeetablissement;
    protected $typesecteur;
    protected $typesecteursms;
    protected $trancheage;
    protected $typepublic;
    protected $detailtypepublic;
    protected $typemodalite;
    protected $typeorganisme;
    protected $fonction;

<<<<<<< HEAD
    protected $nl;
=======
>>>>>>> debug8

    /** -------------------------------------------------------------------------------------------
     * @param Request $request
     */
    public function listDatatableOO(Request $request)
    {

        /** ---------------------------------------------------------------------------------------
         * Les paramètres reçus sont passés en POST
         */
        $this->inOO = Input::get('pOO', null);
        $this->inPO = Input::get('pPO', null);
        $this->inET = Input::get('pET', null);
        $this->inAN = Input::get('pAN', null);
        $this->inAA = Input::get('pAA', null);
        $this->inPE = Input::get('pPE', null);

        $this->nom = Input::get('pNom', null);
        $this->adresse = Input::get('pAdresse', null);
        $this->cp = Input::get('pCP', null);
        $this->ville = Input::get('pVille', null);
        $this->finess = Input::get('pFiness', null);
        $this->code = Input::get('pCode', null);
<<<<<<< HEAD

        $this->sigle = Input::get('pSigle', null);
        $this->nbplaces = Input::get('pNbPlaces', null);

=======
>>>>>>> debug8
        $this->service = Input::get('pService', null);
        $this->typeetablissement = Input::get('pTypeEtablissement', null);
        $this->typesecteur = Input::get('pTypeSecteur', null);
        $this->typesecteursms = Input::get('pTypeSecteurSMS', null);
        $this->trancheage = Input::get('pTrancheAge', null);
        $this->typepublic = Input::get('pTypePublic', null);
        $this->detailtypepublic = Input::get('pDetailTypePublic', null);

        $this->typemodalite = Input::get('pFiltreModalite', null);
        $this->typeorganisme = Input::get('pFiltreTypeOrganisme', null);
        $this->fonction = Input::get('pFiltreFonction', null);
<<<<<<< HEAD

        $this->nl == Input::get('pNL', null);
=======
>>>>>>> debug8

        /** ---------------------------------------------------------------------------------------
         * La recherche va se faire, dans l'ordre suivant
         * OO / PO / ET / AN / AA / PEL
         */
        $result = ['aaData' => []];

        if ($this->inOO) {
            $this->getResultsFiltre('oo', $result);
        }
        if ($this->inPO) {
            $this->getResultsFiltre('po', $result);
        }
        if ($this->inET) {
            $this->getResultsFiltre('et', $result);
        }
        if ($this->inAN) {
            $this->getResultsFiltre('an', $result);
        }
        if ($this->inAA) {
            $this->getResultsFiltre('aa', $result);
        }
        if ($this->inPE) {
            $this->getResultsFiltre('pe', $result);
        }

        return Response::json($result);

    }

    public function getResultsFiltre($route, &$result)
    {
        switch ($route) {
            case 'oo':
                $this->getResultOrganismeFiltre('oo', $result);
                break;
            case 'po':
                $this->getResultPoleFiltre('po', $result);
                break;
            case 'et':
                $this->getResultEtablissementFiltre('et', $result);
                break;
            case 'an':
                $this->getResultAntenneFiltre('an', $result);
                break;
            case 'aa':
                $this->getResultAdresseFiltre('aa', $result);
                break;
            case 'pe':
                $this->getResultPersonnelFiltre('pe', $result);
                break;
        }
    }

    /** -------------------------------------------------------------------------------------------
     */
    public function getResultOrganismeFiltre($short, &$result)
    {

        /** ---------------------------------------------------------------------------------------
         * On va construire la requête bloc / bloc
         */
        $table = DB::table('organismes');

        if ($this->code != null) {
            $table->where(function ($query) {
                $query->orWhere('id_interne', 'LIKE', '%'.$this->code.'%');
                $query->orWhere('id', '=', $this->code);
            });
        }

        if ($this->nom != null) {
            $table->where('nom', 'like', '%' . $this->nom . '%');
        }
        if ($this->adresse != null) {
            $table->where(function ($query) {
                $query->orWhere('adr1', 'like', '%' . $this->adresse . '%');
                $query->orWhere('adr2', 'like', '%' . $this->adresse . '%');
                $query->orWhere('adr3', 'like', '%' . $this->adresse . '%');
            });
        }
        if ($this->cp != null) {
            $table->where('cp', 'like', '%' . $this->cp . '%');
<<<<<<< HEAD
        }
        if ($this->ville != null) {
            $table->where('ville', 'like', '%' . $this->ville . '%');
        }
        if ($this->finess != null) {
            $table->where('finess', 'like', '%' . $this->finess . '%');
        }
        if ($this->sigle != null) {
            $table->where('sigle', 'like', '%' . $this->sigle . '%');
        }
        if ($this->nl != -1) {
            $table->where('nl', '=', $this->nl);
=======
        }
        if ($this->ville != '') {
            $table->where('ville', 'like', '%' . $this->ville . '%');
        }
        if ($this->finess != '') {
            $table->where('finess', 'like', '%' . $this->finess . '%');
>>>>>>> debug8
        }

        if (is_array($this->typeorganisme)) {
            $table->where(function ($query) {
                foreach ($this->typeorganisme as $type) {
                    $query->orWhere('type_organisme_id', '=', $type);
                }
            });
        }


        /** ---------------------------------------------------------------------------------------
         * Si on n'a pas de filtre, donc pas de where, autant partir !
         */
        if (strpos($table->toSql(), 'where') === false) {
            return false;
        }

        /** ---------------------------------------------------------------------------------------
         * Et donc, on tableautise le résultat
         */
        $liste = $table->get();
        foreach ($liste as $item) {
            $id = $item->id;
            $code = '<strong>' . strtoupper($short) . $item->id . '</strong>';
            $code .= ($item->id_interne != '' ? ' (' . $item->id_interne . ')' : '');
            $type = Organisme::find($item->id)->typeOrganisme->libelle;
            $detail = ($item->detail_type_organisme_id != 0 ? Organisme::find($item->id)->detailTypeOrganisme->libelle : '');
            $cp = $item->cp;
            $dpt = substr($cp, 0, 2);
            $ville = ($dpt != '' ? $item->ville . ' (' . $dpt . ')' : $item->ville);

            $action = '<a class="visu btn btn-xs blue" data-type="' . $short . '" data-id="' . $id . '"><i class="fa fa-file-text-o"></i></a>';
            $action .= '<a class="update btn btn-xs red" data-type="' . $short . '" data-id="' . $id . '"><i class="fa fa-pencil"></i></a>';
            $result['aaData'][] = [
                '<span class="label label-primary">' . $short . '</span>',
                $code,
                '<span data-nom="' . $item->nom . '">' . $item->nom . '</span>',
                ($detail != '' ? $detail : $type),
                $ville,
                $action
            ];
        }
    }

    /** -------------------------------------------------------------------------------------------
     */
    public function getResultPoleFiltre($short, &$result)
    {

        /** ---------------------------------------------------------------------------------------
         * On va construire la requête bloc / bloc
         */
        $table = DB::table('poles');

<<<<<<< HEAD
        if ($this->nom != null) {
            $table->where('nom', 'like', '%' . $this->nom . '%');
        }
        if ($this->adresse != null) {
=======
        if ($this->nom != '') {
            $table->where('nom', 'like', '%' . $this->nom . '%');
        }
        if ($this->adresse != '') {
>>>>>>> debug8
            $table->where(function ($query) {
                $query->orWhere('adr1', 'like', '%' . $this->adresse . '%');
                $query->orWhere('adr2', 'like', '%' . $this->adresse . '%');
                $query->orWhere('adr3', 'like', '%' . $this->adresse . '%');
            });
        }
<<<<<<< HEAD
        if ($this->cp != null) {
            $table->where('cp', 'like', '%' . $this->cp . '%');
        }
        if ($this->ville != null) {
            $table->where('ville', 'like', '%' . $this->ville . '%');
        }
        if ($this->sigle != null) {
            $table->where('sigle', 'like', '%' . $this->sigle . '%');
=======
        if ($this->cp != '') {
            $table->where('cp', 'like', '%' . $this->cp . '%');
        }
        if ($this->ville != '') {
            $table->where('ville', 'like', '%' . $this->ville . '%');
>>>>>>> debug8
        }

        /** ---------------------------------------------------------------------------------------
         * Si on n'a pas de filtre, donc pas de where, autant partir !
         */
        if (strpos($table->toSql(), 'where') === false) {
            return false;
        }

        /** ---------------------------------------------------------------------------------------
         * Et donc, on tableautise le résultat
         */
        $liste = $table->get();
        foreach ($liste as $item) {
            $id = $item->id;
            $code = '<strong>' . strtoupper($short) . $item->id . '</strong>';
            $type = Pole::find($item->id)->typePole->libelle;
            $cp = $item->cp;
            $dpt = substr($cp, 0, 2);
            $ville = ($dpt != '' ? $item->ville . ' (' . $dpt . ')' : $item->ville);

            $action = '<a class="visu btn btn-xs blue" data-type="' . $short . '" data-id="' . $id . '"><i class="fa fa-file-text-o"></i></a>';
            $action .= '<a class="update btn btn-xs red" data-type="' . $short . '" data-id="' . $id . '"><i class="fa fa-pencil"></i></a>';
            $result['aaData'][] = [
                '<span class="label bg-red-sunglo">' . $short . '</span>',
                $code,
                '<span data-nom="' . $item->nom . '">' . $item->nom . '</span>',
                $type,
                $ville,
                $action
            ];
        }
    }

    /** -------------------------------------------------------------------------------------------
     */
    public function getResultEtablissementFiltre($short, &$result)
    {

        /** ---------------------------------------------------------------------------------------
         * On va construire la requête bloc / bloc
         */
        $table = DB::table('etablissements');

        if ($this->code != null) {
            $table->where(function ($query) {
                $query->orWhere('id_interne', 'LIKE', '%'.$this->code.'%');
                $query->orWhere('id', '=', $this->code);
            });
<<<<<<< HEAD
        }

        if ($this->nom != null) {
            $table->where('nom', 'like', '%' . $this->nom . '%');
        }
        if ($this->adresse != null) {
=======
        }

        if ($this->nom != '') {
            $table->where('nom', 'like', '%' . $this->nom . '%');
        }
        if ($this->adresse != '') {
>>>>>>> debug8
            $table->where(function ($query) {
                $query->orWhere('adr1', 'like', '%' . $this->adresse . '%');
                $query->orWhere('adr2', 'like', '%' . $this->adresse . '%');
                $query->orWhere('adr3', 'like', '%' . $this->adresse . '%');
            });
        }
<<<<<<< HEAD
        if ($this->cp != null) {
            $table->where('cp', 'like', '%' . $this->cp . '%');
        }
        if ($this->ville != null) {
            $table->where('ville', 'like', '%' . $this->ville . '%');
        }
        if ($this->finess != null) {
            $table->where('finess', 'like', '%' . $this->finess . '%');
        }
        if ($this->sigle != null) {
            $table->where('sigle', 'like', '%' . $this->sigle . '%');
=======
        if ($this->cp != '') {
            $table->where('cp', 'like', '%' . $this->cp . '%');
        }
        if ($this->ville != '') {
            $table->where('ville', 'like', '%' . $this->ville . '%');
        }
        if ($this->finess != '') {
            $table->where('finess', 'like', '%' . $this->finess . '%');
>>>>>>> debug8
        }

        /** ---------------------------------------------------------------------------------------
         * Plus compliqué - le type d'établissement car il faut passer par une jointure
         */
        if (is_array($this->typeetablissement)) {
            $table->join('etablissement_modalites', 'etablissements.id', '=', 'etablissement_modalites.etablissement_id');
            $table->where(function ($query) {
                foreach ($this->typeetablissement as $type) {
                    $query->orWhere('type_etablissement_id', '=', $type);
                }
            });
        }

        /** ---------------------------------------------------------------------------------------
         * Encore plus compliqué - le type de public car il faut passer par une jointure et
         * (éventuellement une autre si on n'a pas de join sur etablissement_modalites)
         */
        if (is_array($this->typepublic)) {

            /** -----------------------------------------------------------------------------------
             * On regarde si on est lié à la table etablissement_modalites
             */
            if (strpos($table->toSql(), 'etablissement_modalites') === false) {
                $table->join('etablissement_modalites', 'etablissements.id', '=', 'etablissement_modalites.etablissement_id');
            }
            /** -----------------------------------------------------------------------------------
             * Et dans tous les cas, on se branche sur la table
             */
            $table->join('etablissement_modalite_details', 'etablissement_modalites.id', '=', 'etablissement_modalite_details.etablissement_modalite_id');
            $table->where(function ($query) {
                foreach ($this->typepublic as $type) {
                    $query->orWhere('type_public_id', '=', $type);
                }
            });
        }

        /** ---------------------------------------------------------------------------------------
         * Aussi compliqué mais avec une nuance, on recherche la modalité sur un 'libelle'
         */
        if (is_array($this->typemodalite)) {

            /** -----------------------------------------------------------------------------------
             * On regarde si on est lié à la table etablissement_modalites
             */
            if (strpos($table->toSql(), 'etablissement_modalites') === false) {
                $table->join('etablissement_modalites', 'etablissements.id', '=', 'etablissement_modalites.etablissement_id');
            }
            /** -----------------------------------------------------------------------------------
             * On regarde si on est lié à la table etablissement_modalite_details
             */
            if (strpos($table->toSql(), 'etablissement_modalite_details') === false) {
                $table->join('etablissement_modalite_details', 'etablissement_modalites.id', '=', 'etablissement_modalite_details.etablissement_modalite_id');
            }
            $table->join('modalite_accueil', 'modalite_accueil.id', '=', 'etablissement_modalite_details.modalite_accueil_id');
            $table->where(function ($query) {
                foreach ($this->typemodalite as $type) {
                    $query->orWhere('modalite_accueil.libelle', 'like', $type);
                }
            });
        }

        /** ---------------------------------------------------------------------------------------
         * Et Encore plus compliqué - le détail type de public car il faut passer par une
         * jointure et (éventuellement deux autres si on n'a pas de join sur etablissement_modalites
         * ni etablissement_modalite_details)
         */
        if (is_array($this->detailtypepublic)) {

            /** -----------------------------------------------------------------------------------
             * On regarde si on est lié à la table etablissement_modalites
             */
            if (strpos($table->toSql(), 'etablissement_modalites') === false) {
                $table->join('etablissement_modalites', 'etablissements.id', '=', 'etablissement_modalites.etablissement_id');
            }

            /** -----------------------------------------------------------------------------------
             * On regarde si on est lié à la table etablissement_modalite_details
             */
            if (strpos($table->toSql(), 'etablissement_modalite_details') === false) {
                $table->join('etablissement_modalite_details', 'etablissement_modalites.id', '=', 'etablissement_modalite_details.etablissement_modalite_id');
            }

            /** -----------------------------------------------------------------------------------
             * Et dans tous les cas, on se branche sur la table
             */
            $table->where(function ($query) {
                foreach ($this->detailtypepublic as $type) {
                    $query->orWhere('detail_type_public_id', '=', $type);
                }
            });
        }

        /** ---------------------------------------------------------------------------------------
         * Facile : le type de secteur
         */
        if (is_array($this->typesecteur)) {
            $table->where(function ($query) {
                foreach ($this->typesecteur as $type) {
                    if ($type != '') {
                        $query->orWhere('type_secteur_id', '=', $type);
                    }
<<<<<<< HEAD
                }
            });
        }

        /** ---------------------------------------------------------------------------------------
         * Facile : le type de secteur SMS
         */
        if (is_array($this->typesecteursms)) {
            $table->where(function ($query) {
                foreach ($this->typesecteursms as $type) {
                    if ($type != '') {
                        $query->orWhere('type_secteur_ms_id', '=', $type);
                    }
                }
            });
        }

        /** ---------------------------------------------------------------------------------------
         * Facile : le type de secteur SMS
         */
        if (is_array($this->trancheage)) {
            $table->where(function ($query) {
                foreach ($this->trancheage as $type) {
                    if ($type != '') {
                        $query->orWhere('type_age_id', '=', $type);
                    }
=======
>>>>>>> debug8
                }
            });
        }


        /** ---------------------------------------------------------------------------------------
<<<<<<< HEAD
         * Si on n'a pas de filtre, donc pas de where, autant partir !
         */
=======
         * Facile : le type de secteur SMS
         */
        if (is_array($this->typesecteursms)) {
            $table->where(function ($query) {
                foreach ($this->typesecteursms as $type) {
                    if ($type != '') {
                        $query->orWhere('type_secteur_ms_id', '=', $type);
                    }
                }
            });
        }

        /** ---------------------------------------------------------------------------------------
         * Facile : le type de secteur SMS
         */
        if (is_array($this->trancheage)) {
            $table->where(function ($query) {
                foreach ($this->trancheage as $type) {
                    if ($type != '') {
                        $query->orWhere('type_age_id', '=', $type);
                    }
                }
            });
        }


        /** ---------------------------------------------------------------------------------------
         * Si on n'a pas de filtre, donc pas de where, autant partir !
         */
>>>>>>> debug8
        #echo $table->toSql();
        #die();
        if (strpos($table->toSql(), 'where') === false) {
            return false;
        }

        $liste = $table->select('etablissements.*')->distinct()->get();
        foreach ($liste as $item) {
            $id = $item->id;
            $code = '<strong>' . strtoupper($short) . $item->id . '</strong>';
            $code .= ($item->id_interne != '' ? ' (' . $item->id_interne . ')' : '');
            $type = ($item->type_secteur_id != null ? Etablissement::find($item->id)->typeSecteur->libelle : '');
            $cp = $item->cp;
            $dpt = substr($cp, 0, 2);
            $ville = ($dpt != '' ? $item->ville . ' (' . $dpt . ')' : $item->ville);

            $action = '<a class="visu btn btn-xs blue" data-type="' . $short . '" data-id="' . $id . '"><i class="fa fa-file-text-o"></i></a>';
            $action .= '<a class="update btn btn-xs red" data-type="' . $short . '" data-id="' . $id . '"><i class="fa fa-pencil"></i></a>';
            $result['aaData'][] = [
                '<span class="label bg-green-seagreen">' . $short . '</span>',
                $code,
                '<span data-nom="' . $item->nom . '">' . $item->nom . '</span>',
                $type,
                $ville,
                $action
            ];
        }
    }

    /** -------------------------------------------------------------------------------------------
     */
    public function getResultAntenneFiltre($short, &$result)
    {

        /** ---------------------------------------------------------------------------------------
         * On va construire la requête bloc / bloc
         */
        $table = DB::table('antennes');

        if ($this->nom != '') {
            $table->where('nom', 'like', '%' . $this->nom . '%');
        }
        if ($this->adresse != '') {
            $table->where(function ($query)  {
                $query->orWhere('adr1', 'like', '%' . $this->adresse . '%');
                $query->orWhere('adr2', 'like', '%' . $this->adresse . '%');
                $query->orWhere('adr3', 'like', '%' . $this->adresse . '%');
            });
        }
        if ($this->cp != '') {
            $table->where('cp', 'like', '%' . $this->cp . '%');
        }
        if ($this->ville != '') {
            $table->where('ville', 'like', '%' . $this->ville . '%');
        }
        if ($this->finess != '') {
            $table->where('finess', 'like', '%' . $this->finess . '%');
        }

        /** ---------------------------------------------------------------------------------------
         * Si on n'a pas de filtre, donc pas de where, autant partir !
         */
        if (strpos($table->toSql(), 'where') === false) {
            return false;
        }

        $liste = $table->get();
        foreach ($liste as $item) {
            $id = $item->id;
            $code = '<strong>' . strtoupper($short) . $item->id . '</strong>';
            $type = '';
            $cp = $item->cp;
            $dpt = substr($cp, 0, 2);
            $ville = ($dpt != '' ? $item->ville . ' (' . $dpt . ')' : $item->ville);

            $action = '<a class="visu btn btn-xs blue" data-type="' . $short . '" data-id="' . $id . '"><i class="fa fa-file-text-o"></i></a>';
            $action .= '<a class="update btn btn-xs red" data-type="' . $short . '" data-id="' . $id . '"><i class="fa fa-pencil"></i></a>';
            $result['aaData'][] = [
                '<span class="label bg-yellow-casablanca">' . $short . '</span>',
                $code,
                '<span data-nom="' . $item->nom . '">' . $item->nom . '</span>',
                $type,
                $ville,
                $action
            ];
        }
    }

    /** -------------------------------------------------------------------------------------------
     */
    public function getResultAdresseFiltre($short, &$result)
    {

        /** ---------------------------------------------------------------------------------------
         * On va construire la requête bloc / bloc
         */
        $table = DB::table('adresses_accueil');

        if ($this->nom != '') {
            $table->where('nom', 'like', '%' . $this->nom . '%');
        }
        if ($this->adresse != '') {
            $table->where(function ($query)  {
                $query->orWhere('adr1', 'like', '%' . $this->adresse . '%');
                $query->orWhere('adr2', 'like', '%' . $this->adresse . '%');
                $query->orWhere('adr3', 'like', '%' . $this->adresse . '%');
            });
        }
        if ($this->cp != '') {
            $table->where('cp', 'like', '%' . $this->cp . '%');
        }
        if ($this->ville != '') {
            $table->where('ville', 'like', '%' . $this->ville . '%');
        }

        /** ---------------------------------------------------------------------------------------
         * Type de public
         */
        if (is_array($this->typepublic)) {
            $table->where(function ($query) {
                foreach ($this->typepublic as $type) {
                    $query->orWhere('type_public_id', '=', $type);
                }
            });
        }

        /** ---------------------------------------------------------------------------------------
         * Détail de Type de public
         */
        if (is_array($this->detailtypepublic)) {
            $table->where(function ($query) {
                foreach ($this->detailtypepublic as $type) {
                    $query->orWhere('detail_type_public_id', '=', $type);
                }
            });
        }

        /** ---------------------------------------------------------------------------------------
         * Si on n'a pas de filtre, donc pas de where, autant partir !
         */
        if (strpos($table->toSql(), 'where') === false) {
            return false;
        }

        $liste = $table->get();
        foreach ($liste as $item) {
            $id = $item->id;
            $code = '<strong>' . strtoupper($short) . $item->id . '</strong>';
            $type = ($item->type_public_id != null ? TypePublic::find($item->type_public_id)->libelle : '');
            $cp = $item->cp;
            $dpt = substr($cp, 0, 2);
            $ville = ($dpt != '' ? $item->ville . ' (' . $dpt . ')' : $item->ville);

            $action = '<a class="visu btn btn-xs blue" data-type="' . $short . '" data-id="' . $id . '"><i class="fa fa-file-text-o"></i></a>';
            $action .= '<a class="update btn btn-xs red" data-type="' . $short . '" data-id="' . $id . '"><i class="fa fa-pencil"></i></a>';
            $result['aaData'][] = [
                '<span class="label bg-purple-plum">' . $short . '</span>',
                $code,
                '<span data-nom="' . $item->nom . '">' . $item->nom . '</span>',
                $type,
                $ville,
                $action
            ];
        }
    }

    /** -------------------------------------------------------------------------------------------
     */
    public function getResultPersonnelFiltre($short, &$result)
    {

        /** ---------------------------------------------------------------------------------------
         * On va construire la requête bloc / bloc
         */
        $table = DB::table('personnels');

        if ($this->nom != '') {
            $table->where('nom', 'like', '%' . $this->nom . '%');
        }
        if ($this->adresse != '') {
            $table->where(function ($query)  {
                $query->orWhere('adr1', 'like', '%' . $this->adresse . '%');
                $query->orWhere('adr2', 'like', '%' . $this->adresse . '%');
                $query->orWhere('adr3', 'like', '%' . $this->adresse . '%');
            });
        }
        if ($this->cp != '') {
            $table->where('cp', 'like', '%' . $this->cp . '%');
        }
        if ($this->ville != '') {
            $table->where('ville', 'like', '%' . $this->ville . '%');
        }
        if ($this->service != '') {
            $table->where('service', 'like', '%' . $this->service . '%');
        }
        /** ---------------------------------------------------------------------------------------
         * Facile : la fonction
         */
        if (is_array($this->fonction)) {
            $table->where(function ($query) {
                foreach ($this->fonction as $type) {
                    if ($type != '') {
                        $query->orWhere('fonction_id', '=', $type);
                    }
                }
            });
        }
        /** ---------------------------------------------------------------------------------------
         * Si on n'a pas de filtre, donc pas de where, autant partir !
         */
        if (strpos($table->toSql(), 'where') === false) {
            return false;
        }

        $liste = $table->get();
        foreach ($liste as $item) {
            $id = $item->id;
            $code = '<strong>' . strtoupper($short) . $item->id . '</strong>';
            $type = '';
            $cp = $item->cp;
            $dpt = substr($cp, 0, 2);
            $ville = ($dpt != '' ? $item->ville . ' (' . $dpt . ')' : $item->ville);

            $action = '<a class="visu btn btn-xs blue" data-type="' . $short . '" data-id="' . $id . '"><i class="fa fa-file-text-o"></i></a>';
            $action .= '<a class="update btn btn-xs red" data-type="' . $short . '" data-id="' . $id . '"><i class="fa fa-pencil"></i></a>';
            $result['aaData'][] = [
                '<span class="label bg-yellow">' . $short . '</span>',
                $code,
                '<span data-nom="' . $item->nom . '">' . $item->nom.' '.$item->prenom . '</span>',
                $type,
                $ville,
                $action
            ];
        }
    }

<<<<<<< HEAD
    /** -------------------------------------------------------------------------------------------
     */
    public function getAriane(Request $request) {

        /** -------------------------------------------------------------------------
         * Selon le schéma suivant :
         *
         * ORGANISME --> POLE --> ETABLISSEMENT --> ANTENNE --> ADRESSE
         * ORGANISME --> ETABLISSEMENT --> ANTENNE --> ADRESSE
         *
         */
        $ariane = [];
        $type = $request->pType;
        $id = $request->pId;
        if ($type == 'po') {
            $this->getPerePO($id, $ariane);
        } elseif ($type == 'et') {
            $this->getPereET($id, $ariane);
        } elseif ($type == 'an') {
            $this->getPereAN($id, $ariane);
        } elseif ($type == 'aa') {
            $this->getPereAA($id, $ariane);
        } elseif ($type == 'pe') {
            $this->getPerePE($id, $ariane);
        }

        return Response::json(
            $ariane
        );
    }

    /** -------------------------------------------------------------------------------------------
     * On récupère le père d'un PERSONNEL
     * --> l'AA est lié à un ETABLISSEMENT ou une ANTENNE
     */
    protected function getPerePE($id, &$ariane) {
        $pe = Personnel::find($id);
        if ($pe->oo_id != null) {
            $ariane[] = $pe->organisme->nom;
        }
        if ($pe->po_id != null) {
            $this->getPerePO($pe->po_id, $ariane);
            $ariane[] = $pe->pole->nom;
        }
        if ($pe->et_id != null) {
            $this->getPereET($pe->et_id, $ariane);
            $ariane[] = $pe->etablissement->nom;
        }
        if ($pe->an_id != null) {
            $this->getPereAN($pe->an_id, $ariane);
            $ariane[] = $pe->antenne->nom;
        }
    }

    /** -------------------------------------------------------------------------------------------
     * On récupère le père d'une ADRESSE D'ACCUEIL
     * --> l'AA est lié à un ETABLISSEMENT ou une ANTENNE
     */
    protected function getPereAA($id, &$ariane) {
        $aa = AdresseAccueil::find($id);
        if ($aa->etablissement_id != null) {
            $this->getPereET($aa->etablissement_id, $ariane);
            $ariane[] = $aa->etablissement->nom;
        }
        if ($aa->antenne_id != null) {
            $this->getPereAN($aa->antenne_id, $ariane);
            $ariane[] = $aa->antenne->nom;
        }
    }

    /** -------------------------------------------------------------------------------------------
     */
    protected function getPereAN($id, &$ariane) {
        $an = Antenne::find($id);
        $this->getPereET($an->etablissement_id, $ariane);
        $ariane[] = $an->etablissement->nom;
    }

    /** -------------------------------------------------------------------------------------------
     */
    protected function getPereET($id, &$ariane) {
        $et = Etablissement::find($id);
        if ($et->oo_id != null) {
            $ariane[] = $et->organisme->nom;
        }
        if ($et->po_id != null) {
            $this->getPerePO($et->po_id, $ariane);
            $ariane[] = $et->pole->nom;
        }
    }

    /** -------------------------------------------------------------------------------------------
     */
    protected function getPerePO($id, &$ariane) {
        $po = Pole::find($id);
        $ariane[] = $po->organisme->nom;
    }
}
=======
}
>>>>>>> debug8
