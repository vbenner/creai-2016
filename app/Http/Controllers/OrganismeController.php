<?php

namespace App\Http\Controllers;

use App\Etablissement;
use App\Offres;
use App\Organisme;
use App\Personnel;
use App\Pole;
use App\Telephone;
use App\TelephoneOwner;
use App\Mail;
use App\MailOwner;
use App\TypeTel;
use App\User;
use App\Pays;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;

class OrganismeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('annuaire.oo', [
            'titre' => 'Annuaire',
            'subtitre' => 'Organismes',
            'sidebar' => $this->getSidebar()//json_decode(json_encode($tabBlocs))
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        /** -----------------------------------------------------------------------------------
         * Données de base
         */
        $oo = Organisme::firstOrNew([
            'nom' => $request->pNom,
            'sigle' => $request->pSigle,
            'cp' => $request->pCP,
        ]);
        $oo->id_interne = $request->pCode;
        $oo->isGEST = $request->pGestionnaire;
        $oo->finess = $request->pFiness;
        $oo->siret = $request->pSiret;
        $oo->iban = $request->pIban;
        $oo->bic = $request->pBic;
        $oo->type_organisme_id = ($request->pTypeo == '' ? null : $request->pTypeo);
        $oo->detail_type_organisme_id = ($request->pDetailo == '' ? null : $request->pDetailo);
        $oo->parrain = $request->pParrain;
        $oo->type_statut_id = $request->pStatuto;
        $oo->adr1 = $request->pAdr1;
        $oo->adr2 = $request->pAdr2;
        $oo->adr3 = $request->pAdr3;
        //$oo->cp = $request->pCP;
        $oo->ville = $request->pVille;
        $oo->pays_id = ($request->pPays == '' ? null : $request->pPays);
        $oo->web = $request->pWeb;
        $oo->bi = $request->pBI;
        $oo->bi_reception_id = ($request->pBI_recep == '' ? null : $request->pBI_recep);
        $oo->nl = $request->pNl;
        $oo->cotis = $request->pCotis;
        $oo->cf = $request->pCF;
        $oo->cf_reception_id = ($request->pCF_recep == '' ? null : $request->pCF_recep);
        $oo->notes = $request->pNotes;
        $oo->user_id = Auth::user()->id;

        $oo->active = $request->pActif;
        $oo->visible = $request->pVisible;
        $oo->acces = $request->pAcces;

        $oo->save();

        /** ---------------------------------------------------------------------------------------
         * Traitement des mails et des téléphones
         * Pour chaque téléphone, on va faire une première vérification dans la table
         *
         */
        if (isset($request->pTels)) {
            foreach ($request->pTels as $key => $val) {
                $num = $val[0];
                $prefere = $val[1];
                $type = $val[2];

                /** -------------------------------------------------------------------------------
                 * Il se peut que le téléphone existe déjà !
                 */
                $exist = false;
                $tel = Telephone::where('numero', '=', $num)
                    ->first();
                if ($tel === null) {
                    $tel = new Telephone();
                    $tel->numero = $num;
                    $tel->typetel_id = $type;
                    $tel->user_id = Auth::user()->id;
                } else {
                    $exist = true;
                    $tel->typetel_id = $type;
                    $tel->user_id = Auth::user()->id;
                    $tel->save();
                }
                $tel->save();

                $owner = new TelephoneOwner();
                $owner->telephone_id = $tel->id;
                $owner->prefere = $prefere;
                $owner->oo_id = $oo->id;
                $owner->user_id = Auth::user()->id;
                $owner->save();

            }
        }

        if (isset($request->pMails)) {
            foreach ($request->pMails as $key => $val) {
                $adr = $val[0];
                $prefere = $val[1];
                $type = $val[2];
                $mail = Mail::where('mail', '=', $adr)
                    ->first();
                if ($mail === null) {
                    $mail = new Mail();
                    $mail->mail = $adr;
                    $mail->typemail_id = $type;
                    $mail->user_id = Auth::user()->id;
                    $mail->save();
                } else {
                    $mail->typemail_id = $type;
                    $mail->user_id = Auth::user()->id;
                    $mail->save();
                }
                $owner = new MailOwner();
                $owner->mail_id = $mail->id;
                $owner->prefere = $prefere;
                $owner->user_id = Auth::user()->id;
                $owner->oo_id = $oo->id;
                $owner->save();
            }
        }
        return Response::json($oo->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $oo = Organisme::find($id);

        /** ---------------------------------------------------------------------------------------
         * On prend aussi la liste des téléphones
         */
        $listeTels = DB::table('telephones')
            ->join('type_tels', 'type_tels.id', '=', 'telephones.typetel_id')
            ->join('telephone_owners', 'telephones.id', '=', 'telephone_owners.telephone_id')
            ->join('organismes', 'organismes.id', '=', 'telephone_owners.oo_id')
            ->select('numero', 'type_tels.id AS TYPE_ID', 'prefere',
                'type_tels.libelle', 'telephone_owners.oo_id AS OO_ID',
                'telephone_owners.ID AS OWN_ID', 'telephone_owners.notes')
            ->where('oo_id', '=', $id)
            ->get();

        /** ---------------------------------------------------------------------------------------
         * On prend aussi la liste des mails
         */
        $listeMails = DB::table('mails')
            ->join('type_mails', 'type_mails.id', '=', 'mails.typemail_id')
            ->join('mail_owners', 'mails.id', '=', 'mail_owners.mail_id')
            ->join('organismes', 'organismes.id', '=', 'mail_owners.oo_id')
            ->select('mail', 'type_mails.id AS TYPE_ID', 'prefere',
                'type_mails.libelle', 'mail_owners.oo_id AS OO_ID',
                'mail_owners.ID AS OWN_ID')
            ->where('oo_id', '=', $id)
            ->get();

        /** ---------------------------------------------------------------------------------------
         * On va désactiver le logo POUR le SHOW (sans faire de save();
         */
        $oo->logo = '';
        return Response::json(
            array(
                'OO' => $oo,
                'LOGO' => base64_decode($oo->logo),
                'TEL' => $listeTels,
                'MAIL' => $listeMails,
                'UPDATED_DATE' => Carbon::createFromFormat('Y-m-d H:i:s', $oo->created_at)->format('d/m/Y'),
                'UPDATED_BY' => $oo->user->name
            ));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $oo = Organisme::find($id);

        $oo->nom = $request->pNom;
        $oo->sigle = str_replace('.', '', $request->pSigle);
        $oo->id_interne = $request->pCode;
        $oo->isGEST = $request->pGestionnaire;
        $oo->finess = $request->pFiness;
        $oo->siret = $request->pSiret;
        $oo->iban = $request->pIban;
        $oo->bic = $request->pBic;
        $oo->type_organisme_id = ($request->pTypeo == '' ? null : $request->pTypeo);
        $oo->detail_type_organisme_id = ($request->pDetailo == '' ? null : $request->pDetailo);
        $oo->parrain = $request->pParrain;
        $oo->type_statut_id = $request->pStatuto;
        $oo->adr1 = $request->pAdr1;
        $oo->adr2 = $request->pAdr2;
        $oo->adr3 = $request->pAdr3;
        $oo->cp = $request->pCP;
        $oo->ville = $request->pVille;
        $oo->pays_id = ($request->pPays == '' ? null : $request->pPays);
        $oo->web = $request->pWeb;
        $oo->bi = $request->pBI;
        $oo->bi_reception_id = ($request->pBI_recep == '' ? null : $request->pBI_recep);
        $oo->nl = $request->pNl;
        $oo->cotis = $request->pCotis;
        $oo->cf = $request->pCF;
        $oo->cf_reception_id = ($request->pCF_recep == '' ? null : $request->pCF_recep);
        $oo->notes = $request->pNotes;
        $oo->user_id = Auth::user()->id;
        $oo->active = $request->pActif;
        $oo->visible = $request->pVisible;
        $oo->acces = $request->pAcces;
        $oo->save();

        /** ---------------------------------------------------------------------------------------
         * TELEPHONES
         */
        if (isset($request->pTels)) {
            /** -----------------------------------------------------------------------------------
             * On crée les téléphones
             */
            foreach ($request->pTels as $key => $val) {
                $num = $val[0];
                $prefere = $val[1];
                $type = $val[2];
                $tel = Telephone::where('numero', '=', $num)
                    ->first();
                if ($tel === null) {
                    $tel = new Telephone();
                    $tel->numero = $num;
                    $tel->typetel_id = $type;
                    $tel->user_id = Auth::user()->id;
                    $tel->save();
                } else {
                    $tel->typetel_id = $type;
                    $tel->user_id = Auth::user()->id;
                    $tel->save();
                }
                /** -------------------------------------------------------------------------------
                 * Et on les affecte
                 */
                $owner = TelephoneOwner::firstOrNew([
                    'oo_id' => $oo->id,
                    'prefere' => $prefere,
                    'telephone_id' => $tel->id
                ]);
                $owner->save();
            }
        }

        /** -----------------------------------------------------------------------------------
         * MAILS
         */
        if (isset($request->pMails)) {
            /** -----------------------------------------------------------------------------------
             * On crée les téléphones
             */
            foreach ($request->pMails as $key => $val) {
                $adr = $val[0];
                $prefere = $val[1];
                $type = $val[2];
                $mail = Mail::where('mail', '=', $adr)
                    ->first();
                if ($mail === null) {
                    $mail = new Mail();
                    $mail->mail = $adr;
                    $mail->typemail_id = $type;
                    $mail->user_id = Auth::user()->id;
                    $mail->save();
                } else {
                    $mail->typemail_id = $type;
                    $mail->user_id = Auth::user()->id;
                    $mail->save();
                }
                /** -------------------------------------------------------------------------------
                 * Et on les affecte
                 */
                $owner = MailOwner::firstOrNew([
                    'oo_id' => $oo->id,
                    'prefere' => $prefere,
                    'mail_id' => $mail->id
                ]);
                $owner->save();
            }
        }

        return Response::json($oo->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listDatatable() {

        $organismes = Organisme::all();

        /** @var Initialisation du tableau de sortie $result */
        $result = ['aaData' => []];

        /** @var On parcourt les logs $infoPatch */
        foreach($organismes as $organisme){


            /** -----------------------------------------------------------------------------------
             * Données de base
             */
            $id = $organisme->id;
            $code = '<strong>'.'OO'.$organisme->id.'</strong>';
            $code.= ($organisme->id_interne != '' ? ' ('.$organisme->id_interne.')' : '');
            $nom = ($organisme->isGEST ? '<span class="fa fa-briefcase font-blue-steel"> </span> ' : '').$organisme->nom;
            $type = $organisme->typeOrganisme->libelle;
            $detail = ($organisme->detail_type_organisme_id != 0 ? $organisme->detailTypeOrganisme->libelle : '');
            $statut = $organisme->typeStatutOrganisme->libelle;
            $cp = $organisme->cp;
            $dpt = substr($organisme->cp, 0, 2);
            $ville = ($dpt != '' ? $organisme->ville . ' ('.$dpt.')' : $organisme->ville);
            $actif = $organisme->active;
            if (! is_null($organisme->sigle)) {
                $nom.= ' <span class="label label-info">'.$organisme->sigle.'</span>';
            }

            /** -----------------------------------------------------------------------------------
             * ADD-IN - QUICK
             */
            $quick = '<a data-toggle="tooltip" title="ACTIF" class="oo-actif btn btn-xs '.($organisme->active == 1 ? 'green-seagreen' : 'red-sunglo').'" data-id="'.$id.'" data-statut="'.$organisme->active.'"><i class="fa fa-thumbs-o-up"></i></a>';
            $quick.= '<a data-toggle="tooltip" title="VISIBLE" class="oo-visible btn btn-xs '.($organisme->visible == 1 ? 'green-seagreen' : 'red-sunglo').'" data-id="'.$id.'" data-statut="'.$organisme->visible.'"><i class="fa fa-search"></i></a>';
            $quick.= '<a data-toggle="tooltip" title="ACCES WEB" class="oo-acces btn btn-xs '.($organisme->acces == 1 ? 'green-seagreen' : 'red-sunglo').'" data-id="'.$id.'" data-statut="'.$organisme->acces.'"><i class="fa fa-internet-explorer"></i></a>';

            /** -----------------------------------------------------------------------------------
             * Les actions vont dans un tableau
             */
            $actions = [];

            /** -----------------------------------------------------------------------------------
             * Actions complémentaires
             * POLE si besoin mais ETALISSEMENT dans tous les cas
             */
            if ($organisme->typeOrganisme->isPOLE == 1) {
                $count = Pole::where('organisme_id', $organisme->id)->count();
                $actions[0][] = '<a class="pole font-red-intense" data-id="'.$id.'"><i class="fa fa-building-o font-red-intense"></i> Pôles '.($count != 0 ? '<span class="badge badge-danger"> '.$count.' </span>' : '').'</a>';
            }

            /** -----------------------------------------------------------------------------------
             * Si un organisme n'est pas gestionnaire, on ne peut pas ajouter d'établissement
             */
            if ($organisme->isGEST != 0) {
                $count = Etablissement::where('oo_id', $organisme->id)
                    ->where('po_id', '=', null)
                    ->count();
                $actions[0][] = '<a class="etabl font-blue-steel" data-id="' . $id . '"><i class="fa fa-bed font-blue-steel"></i> Établissements ' . ($count != 0 ? '<span class="badge badge-danger"> '.$count.' </span>' : '') . '</a>';
            }

            /** -----------------------------------------------------------------------------------
             * Affichage du Personnel
             */
            $countP = Personnel::where('oo_id', $organisme->id)->count();
            $countO = Offres::where('oo_id', $organisme->id)->count();
            $actions[0][] = '<a class="personnel font-purple-soft" data-id="' . $id . '"><i class="fa fa-users font-purple-soft"></i> Personnel ' . ($countP != 0 ? '<span class="badge badge-danger"> '.$countP.' </span>' : '') . '</a>';

            /** -----------------------------------------------------------------------------------
             * Actions
             */
            if ($organisme->logo == '') {
                $actions[1][] = '<a class="logo" data-id="'.$id.'"><i class="fa fa-file-image-o">Logo</i></a>';
            } else {
                $actions[1][] = '<a class="logo" data-id="'.$id.'"><i class="fa fa-file-image-o"></i>Logo</a>';
            }

            $actions[1][] = '<a class="telephone" data-id="'.$id.'"><i class="fa fa-phone"></i>Téléphones / Fax</a>';
            $actions[1][] = '<a class="mail" data-id="'.$id.'"><i class="fa fa-send"></i>Mails</a>';
            $actions[1][] = '<a class="offres" data-id="'.$id.'"><i class="fa fa-sticky-note-o"></i>Offres d\'Emploi '.($countO != 0 ? '(' . $countO . ')' : '').'</a>';
            $actions[0][] = '<a class="modif" data-id="'.$id.'"><i class="fa fa-edit"></i>Modifier</a>';

            /** -----------------------------------------------------------------------------------
             * Build Button
             */
            $buttons = $this->buildButtons($actions, 'down');

            /** -----------------------------------------------------------------------------------
             * Actions
             */
            $action = '<a class="tree btn btn-xs red-sunglo" data-id="'.$id.'"><i class="fa fa-code-fork fa-rotate-90"></i></a>';
            $action .= '<a class="visu btn btn-xs blue" data-id="'.$id.'"><i class="fa fa-file-text-o"></i></a>';
            $result['aaData'][] = [
                $code,
                '<span data-nom="'.$organisme->nom.'" class="'.($actif == 1 ? 'font-blue' : 'font-grey-cascade').'">'.$nom.'</span>',
                ($detail!= '' ? $detail : $type),
                $ville,
                //$quick,
                //$addin.$action.$buttons
                $action.$buttons
            ];
        }

        return Response::json($result);
    }

    public function listeTelsOO($id) {
        return $this->listeTels($id, 'oo');
    }

    public function getData($id) {
        $oo = Organisme::find($id);
        return Response::json(array(
            'ADR1' => ($oo->adr1 === null ? '' : $oo->adr1),
            'ADR2' => ($oo->adr2 === null ? '' : $oo->adr2),
            'ADR3' => ($oo->adr3 === null ? '' : $oo->adr3),
            'CP' => ($oo->cp === null ? '' : $oo->cp),
            'VILLE' => ($oo->ville === null ? '' : $oo->ville),
            'PAYS' => ($oo->pays_id === null ? '' : $oo->pays_id),
            'NL' => $oo->nl,
            'BI' => $oo->bi,
            'RECEP_BI' => $oo->bi_reception_id,
            'CF' => $oo->cf,
            'RECEP_CF' => $oo->cf_reception_id,
        ));
    }

    public function deleteLogo(Request $request) {
        $oo = Organisme::find($_POST['pId']);
        $oo->logo = '';
        $oo->save();
        return Response::json(1);
    }

    public function uploadLogo(Request $request) {

        if (isset($_FILES['file_data']['name']) && ($_FILES['file_data']['size'] > 0)) {

            $fichierLogo = $_FILES['file_data'];
            $infoFichier = finfo_open(FILEINFO_MIME_TYPE);
            $formatEnvoi = finfo_file($infoFichier, $fichierLogo['tmp_name']);

            switch ($_FILES['file_data']['type']) {
                case 'image/jpeg':
                    $source = imagecreatefromjpeg($fichierLogo['tmp_name']);
                    break;
                case 'image/png':
                    $source = imagecreatefrompng($fichierLogo['tmp_name']);
                    break;
            }

            /** -----------------------------------------------------------------------------------
             * Eventuellement, on convertit (ou on resize)
             */
            @unlink('TEMP.png');


            /** -----------------------------------------------------------------------------------
             * On va maximiser la largeur
             * Pour se faire, on va faire un petit ratio
             */
            $largeur_source = imagesx($source);
            $hauteur_source = imagesy($source);

            $maxWidth = 350;
            if ($largeur_source > $maxWidth) {
                $ratio = $largeur_source/$maxWidth;
            } else {
                $ratio = 1;
            }

            $destination = imagecreatetruecolor($maxWidth, round($hauteur_source / $ratio, 0));
            $largeur_destination = imagesx($destination);
            $hauteur_destination = imagesy($destination);

            imagecopyresampled($destination, $source, 0, 0, 0, 0, $largeur_destination, $hauteur_destination, $largeur_source, $hauteur_source);

            imagepng($destination, 'TEMP.png');
            $image64 = $this->file2base64('TEMP.png');

            $oo = Organisme::find($_POST['pId']);
            $oo->logo = $image64;
            $oo->save();

            return Response::json(
                array('data' => $image64)
            );

        } else {

            return Response::json(-1);

        }
    }

    public function file2base64($source)
    {
        $bin_data_source = fread(fopen($source, "r"), filesize($source));
        $b64_data_source = base64_encode($bin_data_source);
        return $b64_data_source;
    }

    public function switchStatut(Request $request, $id) {
        $oo = Organisme::find($id);
        switch ($request->pType) {
            case 'actif':
                $oo->active = $request->pValue;
                break;
            case 'visible':
                $oo->visible = $request->pValue;
                break;
            case 'acces':
                $oo->acces = $request->pValue;
                break;
        }
        $oo->save();
    }

}