<?php

namespace App\Http\Controllers;

use App\Civilite;
use App\Fonction;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class FonctionController extends Controller
{

    protected $table = 'fonctions';
    protected $fillable = array('libelle', 'active', 'user_id');

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('params.fonction', [
            'titre' => 'Paramétrage',
            'subtitre' => 'Fonctions',
            'sidebar' => $this->getSidebar()//json_decode(json_encode($tabBlocs))
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fonction = Fonction::firstOrNew([
            'libelle' => $request->pLibelle
        ]);
        $fonction->user_id = Auth::user()->id;
        $fonction->save();
        return Response::json($fonction->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $fonction = Fonction::find($id);
        return Response::json($fonction);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $fonction = Fonction::find($id);
        $fonction->active = $request->pValue;
        $fonction->user_id = Auth::user()->id;
        $fonction->save();
        return Response::json($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /** ---------------------------------------------------------------------------------------
         * Recherche d'un enregistrement identique avec un autre ID
         */
        $fonction = Fonction::where('libelle', 'like', $request->pLibelle)
            ->where('id', '<>', $id)
            ->first();
        if ($fonction === null) {
            $fonction = Fonction::find($id);
            $fonction->libelle = $request->pLibelle;
            $fonction->user_id = Auth::user()->id;
            $fonction->save();
            return Response::json($id);
        } else {
            return Response::json(-1);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listZL() {
        $fonctions = Fonction::where('active', '=', 1)->orderBy('libelle')->get();
        $result = array();
        foreach($fonctions as $fonction){
            array_push($result, array(
                'id'   => $fonction->id,
                'libelle'  => $fonction->libelle,
            ));
        }
        return Response::json(array('LISTE' => $result));

    }

    public function listDatatable() {
        $fonctions = Fonction::all();

        /** @var Initialisation du tableau de sortie $result */
        $result = ['aaData' => []];

        /** @var On parcourt les logs $infoPatch */
        foreach($fonctions as $fonction){


            /** -----------------------------------------------------------------------------------
             * Données
             */
            $id = $fonction->id;
            $libelle = $fonction->libelle;

            /** -----------------------------------------------------------------------------------
             * Actions
             */
            $active =  $this->buildSwitch($id, $fonction->active);
            $action = '<a class="modif btn btn-xs blue" data-id="'.$id.'"><i class="fa fa-edit"></i></a>';
            $result['aaData'][] = [
                $libelle,
                $active,
                $action
            ];
        }

        return Response::json($result);
    }

}
