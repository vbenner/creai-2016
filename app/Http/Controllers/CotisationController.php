<?php

namespace App\Http\Controllers;

use App\MailOwner;
use App\Reglement;
use Maatwebsite\Excel\Facades\Excel;

use App\Assemblee;
use App\Campagneag;
use App\CampagneagCotisant;
use App\Organisme;
use App\Personnel;
use App\PersonnePhysique;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class CotisationController extends Controller
{
    private $curYear;
    private $curCampagne;

    public function __construct()
    {
        $this->curYear = null;
        $this->curCampagne = null;
    }

    public function index()
    {
        /** ---------------------------------------------------------------------------------------
         * On récupère la liste des CAMPAGNES
         *
         */
        $this->curYear = null;
        $this->curCampagne = null;

        $btn = '';
        $campagne = Campagneag::all()->last();

        if ($campagne == null) {
            $assemblee = Assemblee::where('date_ag', '>=', Carbon::now()->format('Y-m-d'))
                ->orderBy('annee')
                ->get()->first();

            if ($assemblee == null) {
                $btn = 'PAS  DE CAMPAGNE';
                $annee = '';
                $status = 'disabled="disabled"';
            } else {
                $btn = 'Campagne ' . $assemblee->annee;
                $annee = $assemblee->annee;
                $status = '';
            }
        } else {
            $btn = 'Campagne ' . $campagne->assemblee->annee;
            $annee = $campagne->assemblee->annee;
            $this->curYear = $annee;
            $this->curCampagne = $campagne->id;
            $status = '';
        }

        return view('activite.cotis', [
            'titre' => 'Suivi',
            'btn' => $btn,
            'annee' => $annee,
            'status' => $status,
            'subtitre' => 'Cotisations',
            'sidebar' => $this->getSidebar()//json_decode(json_encode($tabBlocs))
        ]);
    }

    public function store(Request $request)
    {

        /** ---------------------------------------------------------------------------------------
         * Le but est de créer un fichier .EXCEL
         */
        $extention = 'xls';
        $subDirectory = 'excel';

        #return 'Laravel.xls';

        #return;

        /** ---------------------------------------------------------------------------------------
         * On récupère dans un premier temps l'année pour connaître la campagne
         */
        $ag = Assemblee::where('annee', '=', $request->pAnnee)->get()->first();

        $id = $ag->id;
        $campagne = Campagneag::where('assemblee_id', '=', $id)->get()->first();
        if ($campagne == null) {
            $campagne = new Campagneag;
            $campagne->assemblee_id = $id;
            $campagne->user_id = Auth::user()->id;
            $campagne->save();
        }

        /** ---------------------------------------------------------------------------------------
         * Avec cette campagne, on va rajouter les identifiants des COTISANTS
         */
        $listeCotisant = array ();
        foreach ($request->pTab as $item) {
            $cotisant = new CampagneagCotisant();
            $cotisant->{$item['type'] . '_id'} = $item['id'];
            $cotisant->campagneags_id = $id;
            $cotisant->user_id = Auth::user()->id;
            $cotisant->save();

            /** -----------------------------------------------------------------------------------
             * On récupère donc maintenant les données de la table, en fonction du type
             */
            $record = [];
            switch ($item['type']) {
                case 'pp':
                    $finalMail = '';
                    $get = PersonnePhysique::find($item['id']);
                    $mails = MailOwner::where('pp_id', '=', $item['id'])->get();
                    foreach ($mails as $mail) {
                        $adr = $mail->mail->mail;
                        if ($mail->prefere == 1) {
                            $finalMail = $adr;
                        } else {
                            if ($finalMail == '') {
                                $finalMail = $adr;
                            }
                        }
                    }
                    $record = array (
                        $get->id,
                        ($get->civilite_id != null ? $get->civilite->libelle : ''),
                        $get->nom.' '.$get->prenom,
                        $get->adr1,
                        $get->adr2,
                        $get->adr3,
                        $get->cp,
                        $get->ville,
                        ($get->pays_id != null ? $get->pays->libelle : ''),
                        $finalMail,
                        $get->bi == 1 ? 'OUI' : 'NON',
                        $get->bi_reception_id != null ? $get->typeReceptionBI->libelle : '',
                        $get->role_buro_id != null ? $get->typeRoleBuro->libelle : '',
                        $get->role_ca_id != null ? $get->typeRoleCA->libelle : '',
                        $get->role_ag_id != null ? $get->typeRoleAG->libelle : '',
                    );
                    break;
                case 'oo':
                    $finalMail = '';
                    $get = Organisme::find($item['id']);
                    $mails = MailOwner::where('oo_id', '=', $item['id'])->get();
                    foreach ($mails as $mail) {
                        $adr = $mail->mail->mail;
                        if ($mail->prefere == 1) {
                            $finalMail = $adr;
                        } else {
                            if ($finalMail == '') {
                                $finalMail = $adr;
                            }
                        }
                    }
                    $record = array (
                        $get->id,
                        '',
                        $get->nom,
                        $get->adr1,
                        $get->adr2,
                        $get->adr3,
                        $get->cp,
                        $get->ville,
                        ($get->pays_id != null ? $get->pays->libelle : ''),
                        $finalMail,
                        $get->bi == 1 ? 'OUI' : 'NON',
                        $get->bi_reception_id != null ? $get->typeReceptionBI->libelle : '',
                        '',
                        '',
                        '',
                    );
                    break;
                case 'pe':
                    $finalMail = '';
                    $get = Personnel::find($item['id']);
                    $mails = MailOwner::where('oo_id', '=', $item['id'])->get();
                    foreach ($mails as $mail) {
                        $adr = $mail->mail->mail;
                        if ($mail->prefere == 1) {
                            $finalMail = $adr;
                        } else {
                            if ($finalMail == '') {
                                $finalMail = $adr;
                            }
                        }
                    }
                    $record = array (
                        $get->id,
                        ($get->civilite_id != null ? $get->civilite->libelle : ''),
                        $get->nom,
                        $get->adr1,
                        $get->adr2,
                        $get->adr3,
                        $get->cp,
                        $get->ville,
                        ($get->pays_id != null ? $get->pays->libelle : ''),
                        $finalMail,
                        $get->bi == 1 ? 'OUI' : 'NON',
                        $get->bi_reception_id != null ? $get->typeReceptionBI->libelle : '',
                        $get->role_buro_id != null ? $get->typeRoleBuro->libelle : '',
                        $get->role_ca_id != null ? $get->typeRoleCA->libelle : '',
                        $get->role_ag_id != null ? $get->typeRoleAG->libelle : '',
                    );
                    break;
            }

            $listeCotisant[] = $record;
        }

        /** ---------------------------------------------------------------------------------------
         * Le fichier est créé
         */
        $fileName = 'Campagne_Cotisation_'.$campagne->assemblee->annee;
        $file = Excel::create($fileName, function ($excel) use ($listeCotisant) {

            /** -----------------------------------------------------------------------------------
             * On ajoute la feuille des ADRESSES
             */
            $excel->sheet('Adresses', function ($sheet) use ($listeCotisant) {

                $sheet->setOrientation('portrait');

                /** -------------------------------------------------------------------------------
                 * On définit les entêtes
                 */
                $head = array(
                    'ID',
                    'CIVILITE',
                    'NOM',
                    'ADRESSE 1',
                    'ADRESSE 2',
                    'ADRESSE 3',
                    'CP',
                    'VILLE',
                    'PAYS',
                    'EMAIL',
                    'INSCRIPTION BI',
                    'RECEPTION BI',
                    'ROLE BUREAU',
                    'ROLE CA',
                    'ROLE AG',

                );

                /** -------------------------------------------------------------------------------
                 * Entête
                 */
                $i = 1;
                $sheet->appendRow($i++, //array(
                    $head
                );

                /** -------------------------------------------------------------------------------
                 * Body
                 */
                foreach ($listeCotisant as $uncotisant) {
                    $sheet->appendRow($i++, //array(
                        $uncotisant
                    );
                }
            });
        });

        $file->store($extention, storage_path() . '/' . $subDirectory);
        return $subDirectory . '/' . $fileName . '.' . $extention;

    }

    public function listDatatable($annee)
    {

        /** ---------------------------------------------------------------------------------------
         * On va créer de toute pièce le table_body
         */

        /** ---------------------------------------------------------------------------------------
         * Avec l'année, on récupère l'id de la campagne de cette annnee ?
         */
        $assemblee = Assemblee::where('annee', '=', $annee)->first();
        if ($assemblee == null) {
            return '<tr><td colspan="6" style="text-align: center;font-size: 18px;">MERCI DE DEFINIR LA DATE DE L\'AG</td></tr>';
        }

        /** ---------------------------------------------------------------------------------------
         * Est-ce que cela correspond à une campagne?
         */
        $idCampagne = null;
        $campagne = Campagneag::where('assemblee_id', '=', $assemblee->id)->first();
        if ($campagne != null) {
            $idCampagne = $campagne->id;
        }

        /** ---------------------------------------------------------------------------------------
         * iCol vaudra 0 ou 1
         * cc est l'id de la case à cocher
         */
        $iCol = 0;
        $cc = 1;

        /** ---------------------------------------------------------------------------------------
         * On va récupérer les cotisants dans les tables suivantes :
         * PERSONNES PHYSIQUES
         * On vérifie qu'il n'est pas déjà dans la campagne ?
         */
        $table = '';
        $personnesPhysiques = PersonnePhysique::all()->sortBy('nom');
        foreach ($personnesPhysiques as $pp) {

            $checkVisible = true;
            if (!is_null($idCampagne)) {
                $isCampagne = CampagneagCotisant::where([
                    ['pp_id', '=', $pp->id],
                    ['campagneags_id', '=', $idCampagne]
                ])->first();
                if ($isCampagne != null) {
                    $checkVisible = false;
                }
            }

            $str = $pp->nom . ' ' . $pp->prenom . ($pp->cp != '' ? ' (' . substr($pp->cp, 0, 2) . ') ' : '');
            $tmp = $this->addCell(1 - $iCol, 'primary', $cc, $str, 'pp', $pp->id, $pp->cotis, $checkVisible);
            if ($tmp != '') {
                $table .= $tmp;
                $cc++;
                $iCol = 1 - $iCol;
            }

        }

        $organismes = Organisme::all()->sortBy('nom');
        foreach ($organismes as $oo) {

            $checkVisible = true;
            if (!is_null($idCampagne)) {
                $isCampagne = CampagneagCotisant::where([
                    ['oo_id', '=', $oo->id],
                    ['campagneags_id', '=', $idCampagne]
                ])->first();
                if ($isCampagne != null) {
                    $checkVisible = false;
                }
            }
            $str = $oo->nom . ' ' . $oo->sigle . ($oo->cp != '' ? ' (' . substr($oo->cp, 0, 2) . ') ' : '');
            $tmp = $this->addCell(1 - $iCol, 'info', $cc, $str, 'oo', $oo->id, $oo->cotis, $checkVisible);
            if ($tmp != '') {
                $table .= $tmp;
                $cc++;
                $iCol = 1 - $iCol;
            }
        }

        $personnels = Personnel::all()->sortBy('nom');


        foreach ($personnels as $pp) {

            $checkVisible = true;
            if (!is_null($idCampagne)) {
                $isCampagne = CampagneagCotisant::where([
                    ['pe_id', '=', $pp->id],
                    ['campagneags_id', '=', $idCampagne]
                ])->first();
                if ($isCampagne != null) {
                    $checkVisible = false;
                }
            }
            $str = $pp->nom . ' ' . $pp->prenom . ($pp->cp != '' ? ' (' . substr($pp->cp, 0, 2) . ') ' : '');
            $tmp = $this->addCell(1 - $iCol, 'primary', $cc, $str, 'pp', $pp->id, $pp->cotis, $checkVisible);
            if ($tmp != '') {
                $table .= $tmp;
                $cc++;
                $iCol = 1 - $iCol;
            }
        }

        /* ----------------------------------------------------------------------------------------
         * Pour le dernier, on termine quand même le </tr>
         */
        if ($iCol == 1) {
            $table .= '<td colspan="3"></td></tr>';
        }

        return ($table);
    }

    function addCell($deb, $color, $id, $str, $type, $dataId, $cotis = 1, $hasCC = 1)
    {

        $nonCotisant = '
        style="
            color:red;
            font-style:italic;
        "
        ';
        $cell =
            '<td>' .
            '<div class="checkbox checkbox-' . $color . '">' .
            ($hasCC ?
                '<input id="ccCOTIS' . $id . '" class="styled" type="checkbox" data-type="' . $type . '" data-id="' . $dataId . '">' .
                '<label for="ccCOTIS' . $id . '">' . '</label>'
                : '') .
            '</div>' .
            '</td>' .
            '<td ' . ($cotis ? '' : $nonCotisant) . '>' . $str . '</td>' .
            '<td><span class="label label-' . $color . ' uppercase">' . $type . '</span></td>';

        return ($hasCC ? ($deb == 1 ? '<tr>' . $cell : $cell . '</tr>') : '');

    }

    /** -------------------------------------------------------------------------------------------
     * Permet de renvoyer un datatable des personnes n'ayant pas de Règlement
     * @param $annee
     * @return mixed
     */
    public function listDatatableRes($annee)
    {

        /** ---------------------------------------------------------------------------------------
         * On récupère dans un premier temps l'année pour connaître la campagne
         */
        $ag = Assemblee::where('annee', '=', $annee)->get()->first();

        /** ---------------------------------------------------------------------------------------
         * Assemblée donne Campagne
         */
        $campagne = Campagneag::where('assemblee_id', '=', $ag->id)->get()->first();

        /** @var Initialisation du tableau de sortie $result */
        $result = ['aaData' => []];
        if ($campagne == null) {
        } else {

            /** -----------------------------------------------------------------------------------
             * Avec ce CAMPAGNE_ID, on va pouvoir regarder CEUX qui on une COTISATION en ATTENTE
             */
            $id = $campagne->id;

            /** -----------------------------------------------------------------------------------
             * On traite, dans l'ordre PP / OO / PE
             */
            $cotisans = CampagneagCotisant::where('campagneags_id', '=', $id)->
            where('pp_id', '<>', null)->get();
            foreach ($cotisans as $cotisan) {

                /** -------------------------------------------------------------------------------
                 * Cette Personne Physique a-t-elle payé ?
                 */
                $reglement = Reglement::where('campagne_id', '=', $id)
                    ->where('pp_id', '=', $cotisan->id)
                    ->get()->first();
                #echo '<pre>';
                #print_r(($reglement));
                #echo '</pre>';
                if ($reglement == null) {
                    $pp = $cotisan->listePP;
                    $nom = $pp->nom . ' ' . $pp->prenom . ($pp->cp != '' ? ' (' . substr($pp->cp, 0, 2) . ') ' : '');
                    $select = '<select class="type_cotisation"></select>';
                    $montant = '<div class="form-group">
                            <input class="form-control montant" placeholder="" type="text" readonly>
                            </div>';
                    $date = '<div class="form-group">
                             <input class="form-control date-picker" placeholder="" type="text" 
                                readonly style="font-size:12px;">
                         </div>';
                    $select2 = '<select class="type_reglement"></select>';
                    $reference = '<div class="form-group">
                                  <input class="form-control reference" placeholder="" type="text">
                              </div>';
                    $actions = '
                <a class="valid btn btn-xs green-haze" data-type="pp" data-campagne-id="' . $cotisan->id . '" data-id="' . $pp->id . '">
                    <i class="fa fa-check"></i>
                </a>
                ';

                    $result['aaData'][] = [
                        $nom . ' ' . '<span class="label label-primary uppercase">pp</span>',
                        $select,
                        $montant,
                        $date,
                        $select2,
                        $reference,
                        $actions,
                    ];
                }
            }

            $cotisans = CampagneagCotisant::where('campagneags_id', '=', $id)->
            where('oo_id', '<>', null)->get();
            foreach ($cotisans as $cotisan) {
                $pp = $cotisan->listeOO;
                $nom = $pp->nom . ' ' . $pp->sigle . ($pp->cp != '' ? ' (' . substr($pp->cp, 0, 2) . ') ' : '');
                $select = '<select class="type_cotisation"></select>';
                $montant = '<div class="form-group">
                            <input class="form-control montant" placeholder="" type="text" readonly>
                            </div>';
                $date = '<div class="form-group">
                             <input class="form-control date-picker" placeholder="" type="text" 
                                readonly style="font-size:12px;">
                         </div>';
                $select2 = '<select class="type_reglement"></select>';
                $reference = '<div class="form-group">
                                  <input class="form-control reference" placeholder="" type="text">
                              </div>';
                $actions = '
                <a class="valid btn btn-xs green-haze" data-type="pp" data-campagne-id="' . $cotisan->id . '" data-id="' . $pp->id . '">
                    <i class="fa fa-check"></i>
                </a>
                ';

                $result['aaData'][] = [
                    $nom . ' ' . '<span class="label label-info uppercase">oo</span>',
                    $select,
                    $montant,
                    $date,
                    $select2,
                    $reference,
                    $actions,
                ];
            }

            $cotisans = CampagneagCotisant::where('campagneags_id', '=', $id)->
            where('pe_id', '<>', null)->get();
            foreach ($cotisans as $cotisan) {

                /** -------------------------------------------------------------------------------
                 * Ce Personnel a-t-il payé ?
                 */
                $reglement = Reglement::where('campagne_id', '=', $id)
                    ->where('pe_id', '=', $cotisan->id)
                    ->get()->first();
                if ($reglement == null) {
                    $pp = $cotisan->listePE;
                    $nom = $pp->nom . ' ' . $pp->prenom . ($pp->cp != '' ? ' (' . substr($pp->cp, 0, 2) . ') ' : '');
                    $select = '<select class="type_cotisation"></select>';
                    $montant = '<div class="form-group">
                            <input class="form-control montant" placeholder="" type="text" readonly>
                            </div>';
                    $date = '<div class="form-group">
                             <input class="form-control date-picker" placeholder="" type="text" 
                                readonly style="font-size:12px;">
                         </div>';
                    $select2 = '<select class="type_reglement"></select>';
                    $reference = '<div class="form-group">
                                  <input class="form-control reference" placeholder="" type="text">
                              </div>';
                    $actions = '
                <a class="valid btn btn-xs green-haze" data-type="pp" data-campagne-id="' . $cotisan->id . '" data-id="' . $pp->id . '">
                    <i class="fa fa-check"></i>
                </a>
                ';

                    $result['aaData'][] = [
                        $nom . ' ' . '<span class="label label-success uppercase">pp</span>',
                        $select,
                        $montant,
                        $date,
                        $select2,
                        $reference,
                        $actions,
                    ];
                }
            }

        }

        return Response::json($result);

    }

    /** -------------------------------------------------------------------------------------------
     * @param $type : type de cotisant oo / pp ou pe
     * Respectivement Organisme, Personne Physique ou Personnel
     * @param $id : id de ce type
     * @param Request $request
     * @return mixed
     */
    public function validCotis($type, $id, Request $request)
    {

        /** ---------------------------------------------------------------------------------------
         * On crée le règlement
         */
        $reglement = new Reglement();
        $reglement->campagne_id = $request->pCampagneId;
        $reglement->{$type . '_id'} = $id;
        $reglement->type_cotisation_id = $request->pTypeCotisation;
        $reglement->montant = $request->pMontant;

        $reglement->date = Carbon::createFromFormat('d/m/Y', $request->pDate);

        $reglement->type_cotisation_id = $request->pTypeCotisation;
        $reglement->type_reglement_id = $request->pTypeReglement;
        $reglement->reference = $request->pReference;
        $reglement->save();

        /** ---------------------------------------------------------------------------------------
         * Et du coup, la personne devient obligatoirement COTISANT
         */
        switch ($type) {
            case 'oo':
                $oo = Organisme::find($id);
                $oo->cotis = 1;
                $oo->save();
                break;
            case 'pp':
                $pp = PersonnePhysique::find($id);
                $pp->cotis = 1;
                $pp->save();
                break;
            case 'pe':
                $pp = Personnel::find($id);
                $pp->cotis = 1;
                $pp->save();
                break;
        }

        return Response::json($reglement->id);

    }
}