<?php

namespace App\Http\Controllers;

use App\Offres;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use App\Http\Requests;

class OffreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $offre = new Offres();


        $offre->annee = $request->pAnnee;
        $offre->nb = $request->pNb;
        $offre->notes = strtoupper($request->pNotes);
        $offre->date_offre = Carbon::createFromFormat('d/m/Y', $request->pDate);
        $offre->user_id = Auth::user()->id;
        $offre->{$request->pType.'_id'} = $request->pId;
        $offre->save();

        return Response::json(array(
            'ID' => $offre->id
        ));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $offre = Offres::find($id)->delete();
        return Response::json(
            array('res' => 1)
        );
    }

    public function listeOffresOO($id) {
        return $this->listeOffres($id, 'oo');
    }

    public function listeOffresPO($id) {
        return $this->listeOffres($id, 'po');
    }

    public function listeOffresET($id) {
        return $this->listeOffres($id, 'et');
    }

    public function listOffresAN($id) {
        return $this->listeOffres($id, 'an');
    }

}
