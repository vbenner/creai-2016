<?php

namespace App\Http\Controllers;

use App\AdresseAccueil;
use App\Antenne;
use App\Etablissement;
use App\EtablissementModalite;
use App\EtablissementModaliteDetail;
use App\EtablissementTypeAge;
use App\EtablissementModaliteAccueil;
use App\Mail;
use App\MailOwner;
use App\Offres;
use App\Personnel;
use App\Pole;
use App\Telephone;
use App\TelephoneOwner;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class EtablissementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /** -----------------------------------------------------------------------------------
         * Données de base
         *
         */
        $etab = Etablissement::firstOrNew([
            'nom' => $request->pNom,
            'oo_id' => ($request->pOO == '' ? null : $request->pOO),
            'po_id' => ($request->pPO == '' ? null : $request->pPO),
        ]);

        $etab->sigle = $request->pSigle;
        $etab->adr1 = $request->pAdr1;
        $etab->adr2 = $request->pAdr2;
        $etab->adr3 = $request->pAdr3;
        $etab->cp = $request->pCp;
        $etab->ville = $request->pVille;
        $etab->pays_id = ($request->pPays == '' ? null : $request->pPays);

        /** -----------------------------------------------------------------------------------
         * Caractéristiques Établissement / Service
         */
        $etab->nbplaces = $request->pNbPlaces;
        $etab->dispositif = $request->pDispositif;
        $etab->type_secteur_id = ($request->pSecteur == '' ? null : $request->pSecteur);
        $etab->type_secteur_ms_id = ($request->pSecteurSms == '' ? null : $request->pSecteurSms);
        $etab->handicap = $request->pHandicap;
        $etab->type_age_id = ($request->pTypeAge == '' ? null : $request->pTypeAge);

        $etab->arrete_pref = $request->pArrete;
        if ($request->pDateArrete != '') {
            $dateArrete = Carbon::createFromFormat('d/m/Y', $request->pDateArrete);
            $etab->date_arrete = $dateArrete->format('Y-m-d');
        }
        $etab->age = $request->pAge;
        $etab->finess = $request->pFiness;
        $etab->projet = $request->pProjet;
        if ($request->pOpening != '') {
            $dateOpening = Carbon::createFromFormat('d/m/Y', $request->pOpening);
            $etab->date_ouverture = $dateOpening->format('Y-m-d');
        }

        $etab->bi = $request->pBI;
        $etab->bi_reception_id = ($request->pBI_recep == '' ? null : $request->pBI_recep);
        $etab->nl = $request->pNL;
        $etab->isCONTRIBUANT = $request->pContrib;
        $etab->cf = $request->pCF;
        $etab->cf_reception_id = ($request->pCF_recep == '' ? null : $request->pCF_recep);
        $etab->id_interne = $request->pCode;

        $etab->web = $request->pWeb;
        $etab->notes = $request->pNotes;
        $etab->notesTypePublic = $request->pNotes_public;

        $etab->user_id = Auth::user()->id;
        $etab->active = $request->pActif;
        $etab->visible = $request->pVisible;
        $etab->acces = $request->pAcces;

        $etab->save();

        /** ---------------------------------------------------------------------------------------
         * Type AGE : array
         */
        if (isset($request->pModaliteAccueuil)) {
            if (is_array($request->pModaliteAccueuil) && count($request->pModaliteAccueuil) != 0) {
                foreach($request->pModaliteAccueuil as $modaliteAccueil) {
                    $type = EtablissementModaliteAccueil::firstOrNew([
                        'etablissement_id' => $etab->id,
                        'type_hebergement_id' => $modaliteAccueil,
                    ]);
                    $type->active = 1;
                    $type->user_id = Auth::user()->id;
                    $type->save();
                }
            }
        }

        /** ---------------------------------------------------------------------------------------
         * Traitement des mails et des téléphones
         * Pour chaque téléphone, on va faire une première vérification dans la table
         *
         */
        if (isset($request->pTels)) {
            foreach ($request->pTels as $key => $val) {
                $num = $val[0];
                $prefere = $val[1];
                $type = $val[2];

                /** -------------------------------------------------------------------------------
                 * Il se peut que le téléphone existe déjà !
                 */
                $exist = false;
                $tel = Telephone::where('numero', '=', $num)
                    ->first();
                if ($tel === null) {
                    $tel = new Telephone();
                    $tel->numero = $num;
                    $tel->typetel_id = $type;
                    $tel->user_id = Auth::user()->id;
                } else {
                    $exist = true;
                    $tel->typetel_id = $type;
                    $tel->user_id = Auth::user()->id;
                    $tel->save();
                }
                $tel->save();

                $owner = new TelephoneOwner();
                $owner->telephone_id = $tel->id;
                $owner->prefere = $prefere;
                $owner->et_id = $etab->id;
                $owner->user_id = Auth::user()->id;
                $owner->save();

            }
        }

        if (isset($request->pMails)) {
            foreach ($request->pMails as $key => $val) {
                $adr = $val[0];
                $prefere = $val[1];
                $type = $val[2];
                $mail = Mail::where('mail', '=', $adr)
                    ->first();
                if ($mail === null) {
                    $mail = new Mail();
                    $mail->mail = $adr;
                    $mail->typemail_id = $type;
                    $mail->user_id = Auth::user()->id;
                    $mail->save();
                } else {
                    $mail->typemail_id = $type;
                    $mail->user_id = Auth::user()->id;
                    $mail->save();
                }
                $owner = new MailOwner();
                $owner->mail_id = $mail->id;
                $owner->prefere = $prefere;
                $owner->user_id = Auth::user()->id;
                $owner->et_id = $etab->id;
                $owner->save();
            }
        }

        /** ---------------------------------------------------------------------------------------
         * Traitement des modalites
         * En mode STORE, rien n'existe !
         */
        if (isset($request->pModalites)) {
            foreach ($request->pModalites as $key => $val) {

                /** -------------------------------------------------------------------------------
                 * On crée l'EtablissementModalité
                 */
                $modalite = new EtablissementModalite();
                $modalite->etablissement_id = $etab->id;
                $modalite->type_etablissement_id = $val['id'];
                $modalite->active = 1;
                $modalite->user_id = Auth::user()->id;
                $modalite->save();

                /** -------------------------------------------------------------------------------
                 * Et, éventuellement le détail !
                 */
                if (isset($val['detail'])) {
                    foreach ($val['detail'] as $key => $val) {

                        $tmp = (object)$val;

                        $detail = new EtablissementModaliteDetail();
                        $detail->etablissement_modalite_id = $modalite->id;
                        $detail->modalite_accueil_id = $tmp->modalite_id;
                        $detail->type_public_id = $tmp->type_public_id;
                        $detail->detail_type_public_id = ($tmp->detail_type_public_id != 0 ? $tmp->detail_type_public_id : null);
                        $detail->nb_places = $tmp->nb_places;
                        $detail->active = 1;
                        $detail->user_id = Auth::user()->id;
                        $detail->save();

                    }
                }
            }
        }
        return Response::json($etab->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $et = Etablissement::find($id);

        /** ---------------------------------------------------------------------------------------
         * On prend aussi la liste des téléphones
         */
        $listeTels = DB::table('telephones')
            ->join('type_tels', 'type_tels.id', '=', 'telephones.typetel_id')
            ->join('telephone_owners', 'telephones.id', '=', 'telephone_owners.telephone_id')
            ->join('etablissements', 'etablissements.id', '=', 'telephone_owners.et_id')
            ->select('numero', 'type_tels.id AS TYPE_ID', 'prefere',
                'type_tels.libelle', 'telephone_owners.et_id AS ET_ID',
                'telephone_owners.ID AS OWN_ID', 'telephone_owners.notes')
            ->where('et_id', '=', $id)
            ->get();

        /** ---------------------------------------------------------------------------------------
         * On prend aussi la liste des mails
         */
        $listeMails = DB::table('mails')
            ->join('type_mails', 'type_mails.id', '=', 'mails.typemail_id')
            ->join('mail_owners', 'mails.id', '=', 'mail_owners.mail_id')
            ->join('etablissements', 'etablissements.id', '=', 'mail_owners.et_id')
            ->select('mail', 'type_mails.id AS TYPE_ID', 'prefere',
                'type_mails.libelle', 'mail_owners.et_id AS ET_ID',
                'mail_owners.ID AS OWN_ID')
            ->where('et_id', '=', $id)
            ->get();

        /** ---------------------------------------------------------------------------------------
         * On prend aussi laliste des modalites et, pour chaque modalite, la liste des détails
         */
        $modalites = [];
        $listeModalite = DB::table('etablissement_modalites')
            ->join('type_etablissements', 'type_etablissements.id', '=', 'etablissement_modalites.type_etablissement_id')
            ->select('etablissement_modalites.id', 'type_etablissement_id', 'libelle')
            ->where('etablissement_id', '=', $id)
            ->get();

        foreach ($listeModalite as $key => $modalite) {
            $modalite = (object) $modalite;

            $listeModaliteDetail = DB::table('etablissement_modalite_details AS D')
                ->select('D.ID AS ID', 'D.nb_places AS NB',
                    'MOD.id AS MOD_ID', 'MOD.libelle AS MOD_LIB',
                    'PUB.id AS PUB_ID', 'PUB.libelle AS PUB_LIB',
                    'DET.id AS DET_ID', 'DET.libelle AS DET_LIB' )
                ->join('type_publics AS PUB', 'PUB.id', '=', 'D.type_public_id')
                ->leftJoin('detail_type_publics AS DET', 'DET.id', '=', 'D.detail_type_public_id')
                ->join('modalite_accueil AS MOD', 'MOD.id', '=', 'D.modalite_accueil_id')
                ->where('etablissement_modalite_id', '=', $modalite->id)
                ->get();
            $details = [];
            foreach ($listeModaliteDetail as $key2 => $detail) {
                $detail = (object) $detail;
                $details[] = array(
                    'ID' => $detail->ID,
                    'ID_MOD' => $detail->MOD_ID,
                    'TXT_MOD' => $detail->MOD_LIB,
                    'ID_PUB' => $detail->PUB_ID,
                    'TXT_PUB' => $detail->PUB_LIB,
                    'ID_DET' => ($detail->DET_ID != null ? $detail->DET_ID : 0),
                    'TXT_DET' => ($detail->DET_LIB != null ? $detail->DET_LIB : ''),
                    'NB' => $detail->NB,
                );
            }

            $modalites[] = array(
                'ID' => $modalite->id,
                'TXT' => $modalite->libelle,
                'ID_TYPE_ET' => $modalite->type_etablissement_id,
                'DETAIL' => $details
            );
        }

        /** ---------------------------------------------------------------------------------------
         * Une petite transformation des date
         */
        $et->date_arrete = ($et->date_arrete != '' && $et->date_arrete != '0000-00-00' ? Carbon::createFromFormat('Y-m-d', $et->date_arrete)->format('d/m/Y') : '');
        $et->date_ouverture = ($et->date_ouverture != '' && $et->date_ouverture != '0000-00-00' ? Carbon::createFromFormat('Y-m-d', $et->date_ouverture)->format('d/m/Y') : '');
        return Response::json(
            array(
                'ET' => $et,
                'TEL' => $listeTels,
                'MAIL' => $listeMails,
                'MODALITES' => $modalites,
                'UPDATED_DATE' => Carbon::createFromFormat('Y-m-d H:i:s', $et->created_at)->format('d/m/Y'),
                'UPDATED_BY' => $et->user->name
            ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $etab = Etablissement::find($id);

        $etab->nom = $request->pNom;

        /** ---------------------------------------------------------------------------------------
         * Si on supprime le POLE, on rattache à l'organisme
         */
        if ($request->pPO == '') {

            /** -----------------------------------------------------------------------------------
             * Quel est le père de ce pôle (celui déjà rattaché)
             */
            if ($etab->po_id != null) {
                $pole = Pole::find($etab->po_id);
                $etab->oo_id = $pole->organisme_id;
                $etab->po_id = null;
            }
        } else {
            $etab->po_id = $request->pPO;
        }

        $etab->sigle = $request->pSigle;
        $etab->adr1 = $request->pAdr1;
        $etab->adr2 = $request->pAdr2;
        $etab->adr3 = $request->pAdr3;
        $etab->cp = $request->pCp;
        $etab->ville = $request->pVille;
        $etab->pays_id = ($request->pPays == '' ? null : $request->pPays);

        /** -----------------------------------------------------------------------------------
         * Caractéristiques Établissement / Service
         */
        $etab->nbplaces = $request->pNbPlaces;
        $etab->dispositif = $request->pDispositif;
        $etab->type_secteur_id = ($request->pSecteur == '' ? null : $request->pSecteur);
        $etab->type_secteur_ms_id = ($request->pSecteurSms == '' ? null : $request->pSecteurSms);
        $etab->handicap = $request->pHandicap;
        $etab->type_age_id = ($request->pTypeAge == '' ? null : $request->pTypeAge);

        $etab->arrete_pref = $request->pArrete;
        if ($request->pDateArrete != '') {
            $dateArrete = Carbon::createFromFormat('d/m/Y', $request->pDateArrete);
            $etab->date_arrete = $dateArrete->format('Y-m-d');
        }
        $etab->age = $request->pAge;
        $etab->finess = $request->pFiness;
        $etab->projet = $request->pProjet;
        if ($request->pOpening != '') {
            $dateOpening = Carbon::createFromFormat('d/m/Y', $request->pOpening);
            $etab->date_ouverture = $dateOpening->format('Y-m-d');
        }

        $etab->bi = $request->pBI;
        $etab->bi_reception_id = ($request->pBI_recep == '' ? null : $request->pBI_recep);
        $etab->nl = $request->pNL;
        $etab->isCONTRIBUANT = $request->pContrib;
        $etab->cf = $request->pCF;
        $etab->cf_reception_id = ($request->pCF_recep == '' ? null : $request->pCF_recep);
        $etab->id_interne = $request->pCode;

        $etab->web = $request->pWeb;
        $etab->notes = $request->pNotes;
        $etab->notesTypePublic = $request->pNotes_public;

        $etab->user_id = Auth::user()->id;
        $etab->active = $request->pActif;
        $etab->visible = $request->pVisible;
        $etab->acces = $request->pAcces;

        $etab->save();

        /** ---------------------------------------------------------------------------------------
         * Traitement des mails et des téléphones
         * Pour chaque téléphone, on va faire une première vérification dans la table
         *
         */
        if (isset($request->pTels)) {
            foreach ($request->pTels as $key => $val) {
                $num = $val[0];
                $prefere = $val[1];
                $type = $val[2];

                /** -------------------------------------------------------------------------------
                 * Il se peut que le téléphone existe déjà !
                 */
                $exist = false;
                $tel = Telephone::where('numero', '=', $num)
                    ->first();
                if ($tel === null) {
                    $tel = new Telephone();
                    $tel->numero = $num;
                    $tel->typetel_id = $type;
                    $tel->user_id = Auth::user()->id;
                } else {
                    $exist = true;
                    $tel->typetel_id = $type;
                    $tel->user_id = Auth::user()->id;
                    $tel->save();
                }
                $tel->save();

                $owner = new TelephoneOwner();
                $owner->telephone_id = $tel->id;
                $owner->prefere = $prefere;
                $owner->et_id = $etab->id;
                $owner->user_id = Auth::user()->id;
                $owner->save();

            }
        }

        if (isset($request->pMails)) {
            foreach ($request->pMails as $key => $val) {
                $adr = $val[0];
                $prefere = $val[1];
                $type = $val[2];

                /** -------------------------------------------------------------------------------
                 * Il se peut que le téléphone existe déjà !
                 */
                $exist = false;
                $mail = Mail::where('mail', '=', $adr)
                    ->first();
                if ($mail === null) {
                    $mail = new Mail();
                    $mail->mail = $adr;
                    $mail->typemail_id = $type;
                    $mail->user_id = Auth::user()->id;
                } else {
                    $exist = true;
                    $mail->typemail_id = $type;
                    $mail->user_id = Auth::user()->id;
                    $mail->save();
                }
                $mail->save();

                $owner = new MailOwner();
                $owner->mail_id = $mail->id;
                $owner->prefere = $prefere;
                $owner->et_id = $etab->id;
                $owner->user_id = Auth::user()->id;
                $owner->save();

            }
        }

        return Response::json($etab->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /** -------------------------------------------------------------------------------------------
     *
     * Récupère la liste des établissements d'un pôle
     *
     * @param $idPO
     * @return mixed
     */
    public function listDatatable ($type, $id) {

        switch ($type) {
            case 'oo':
                $etablissements = Etablissement::where('oo_id', '=', $id)
                    ->where('po_id', '=', null)
                    ->get();
                break;
            case 'po':
                $etablissements = Etablissement::where('po_id', '=', $id)
                    ->get();
                break;
        }

        /** @var Initialisation du tableau de sortie $result */
        $result = ['aaData' => []];

        /** @var On parcourt les logs $infoPatch */
        foreach($etablissements as $etablissement){


            /** -----------------------------------------------------------------------------------
             * Données de base
             */
            $id = $etablissement->id;
            $code = '<strong>'.'PO'.$etablissement->id.'</strong>';
            $code.= ($etablissement->id_interne != '' ? ' ('.$etablissement->id_interne.')' : '');
            $nom = $etablissement->nom;
            $sigle = $etablissement->sigle;
            $adr1 = $etablissement->adr1;
            $adr2 = $etablissement->adr2;
            $adr3 = $etablissement->adr3;
            $type = ($etablissement->type_etablissement_id == null ? '' : $etablissement->typeEtablissement->libelle);
            $typeSecteur = $etablissement->typeSecteur->libelle;
            $typeSecteurMS = ($etablissement->type_secteur_ms_id == null ? '' : $etablissement->typeSecteurMS->libelle);
            $dpt = substr($etablissement->cp, 0, 2);
            $ville = $etablissement->ville;
            $dpt = substr($etablissement->cp, 0, 2);

            if (!($sigle =='')) {
                $nom.= ' <span class="label label-info"> '.$sigle.' </span>';
            }

            /** -----------------------------------------------------------------------------------
             * Ajout de pictos
             */
            if ($etablissement->type_secteur_id == 1) {
                $picto = '<span class="label label-info"> S </span>';
            } else {
                $picto = '<span class="label label-info"> SMS </span>';
            }
            /** -----------------------------------------------------------------------------------
             * Données de base
             */
            $ville.= ' ('.$dpt.')';

            /** -----------------------------------------------------------------------------------
             * ADD-IN - QUICK
             */
            $quick = '<a data-toggle="tooltip" title="ACTIF" class="et-actif btn btn-xs '.($etablissement->active == 1 ? 'green-seagreen' : 'red-sunglo').'" data-id="'.$id.'" data-statut="'.$etablissement->active.'"><i class="fa fa-thumbs-o-up"></i></a>';
            $quick.= '<a data-toggle="tooltip" title="VISIBLE" class="et-visible btn btn-xs '.($etablissement->visible == 1 ? 'green-seagreen' : 'red-sunglo').'" data-id="'.$id.'" data-statut="'.$etablissement->visible.'"><i class="fa fa-search"></i></a>';
            $quick.= '<a data-toggle="tooltip" title="ACCES WEB" class="et-acces btn btn-xs '.($etablissement->acces == 1 ? 'green-seagreen' : 'red-sunglo').'" data-id="'.$id.'" data-statut="'.$etablissement->acces.'"><i class="fa fa-internet-explorer"></i></a>';

            /** -----------------------------------------------------------------------------------
             * Actions complémentaires
             * ANTENNES
             */
            $addin = '';
            $addin .= '<a class="antenne btn btn-xs green-sharp" data-id="'.$id.'"><i class="fa fa-cubes"></i> ANTENNES</a>';
            $addin .= '<a class="personnel btn btn-xs purple-sharp" data-id="'.$id.'"><i class="fa fa-group"></i> PERSONNEL</a>';

            /** -----------------------------------------------------------------------------------
             * Actions
             */
            $action = '<a class="telephone btn btn-xs green-seagreen" data-id="'.$id.'"><i class="fa fa-phone"></i></a>';
            $action .= '<a class="mail btn btn-xs purple-plum" data-id="'.$id.'"><i class="fa fa-send"></i></a>';
            $action .= '<a class="modif btn btn-xs blue" data-id="'.$id.'"><i class="fa fa-edit"></i></a>';


            /** -----------------------------------------------------------------------------------
             * Actions
             */
            $countAA = AdresseAccueil::where('etablissement_id', $etablissement->id)->count();
            $countAN = Antenne::where('etablissement_id', $etablissement->id)->count();
            $countPE = Personnel::where('et_id', $etablissement->id)->count();
            $countO = Offres::where('et_id', $etablissement->id)->count();

            $action = '<a class="visu btn btn-xs blue" data-id="'.$id.'"><i class="fa fa-file-text-o"></i></a>';
            $actions = [];
            $actions[0][] = '<a class="adresse font-green-meadow" data-id="'.$id.'"><i class="fa fa-send-o font-green-meadow"></i> Adresses Accueil'.($countAA != 0 ? ' <span class="badge badge-danger"> '.$countAA.' </span>' : '').'</a>';
            $actions[0][] = '<a class="antenne" data-id="'.$id.'"><i class="fa fa-cubes"></i> Antennes'.($countAN != 0 ? ' <span class="badge badge-danger"> '.$countAN.' </span>' : '').'</a>';
            $actions[0][] = '<a class="personnel font-purple-soft" data-id="' . $id . '"><i class="fa fa-users font-purple-soft"></i> Personnel ' . ($countPE != 0 ? ' <span class="badge badge-danger"> '.$countPE.' </span>' : '') . '</a>';
            $actions[1][] = '<a class="telephone" data-id="'.$id.'"><i class="fa fa-phone"></i>Téléphones / Fax</a>';
            $actions[1][] .= '<a class="mail" data-id="'.$id.'"><i class="fa fa-send"></i>Mails</a>';
            $actions[1][] = '<a class="offres" data-id="'.$id.'"><i class="fa fa-sticky-note-o"></i>Offres d\'Emploi '.($countO != 0 ? ' <span class="badge badge-danger"> '.$countO.' </span>' : '').'</a>';
            $actions[0][] .= '';
            $actions[0][] .= '<a class="modif" data-id="'.$id.'"><i class="fa fa-edit"></i>Modifier</a>';

            /** -----------------------------------------------------------------------------------
             * Build Button
             */
            $buttons = $this->buildButtons($actions, 'down');


            $result['aaData'][] = [
                $code,
                '<span data-nom="'.$etablissement->nom.'">'. $nom .'</span>',
                $etablissement->typeSecteur->libelle,
                ($etablissement->typeSecteurMS != null ? $etablissement->typeSecteurMS->libelle : ''),
                $adr1.($adr2 != '' ? '<br/>'.$adr2 : '').($adr3 != '' ? '<br/>'.$adr3 : ''),
                $ville,
//                $quick,
                $action.$buttons
            ];
        }

        return Response::json($result);
    }

    public function switchStatut(Request $request, $id) {
        $et = Etablissement::find($id);
        switch ($request->pType) {
            case 'actif':
                $et->active = $request->pValue;
                break;
            case 'visible':
                $et->visible = $request->pValue;
                break;
            case 'acces':
                $et->acces = $request->pValue;
                break;
        }
        $et->save();
    }


    public function getData($id) {

        //$listeModalites = array();
        //$listes = EtablissementModaliteAccueil::where('etablissement_id', '=', $id)
        //    ->get();
        //if ($listes != null) {
        //    foreach($listes as $item){
        //        $listeModalites[] = $item->type_hebergement_id;
        //    }
        //}
        $et = Etablissement::find($id);

        $dateArrete = ($et->date_arrete != '' ? Carbon::createFromFormat('Y-m-d', $et->date_arrete) : '');
        $dateOuverture = ($et->date_ouverture != '' ? Carbon::createFromFormat('Y-m-d', $et->date_ouverture) : '');

        return Response::json(array(
            'FINESS' => $et->finess,
            'ARRETE' => $et->arrete_pref,
            'DATE_ARRETE' => $dateArrete->format('d/m/Y'),
            'DATE_OUVERTURE' => $dateOuverture->format('d/m/Y'),
            'NB_PLACES' => $et->nb_places ,
            'AGE' => $et->age ,
            'ADR1' => ($et->adr1 === null ? '' : $et->adr1),
            'ADR2' => ($et->adr2 === null ? '' : $et->adr2),
            'ADR3' => ($et->adr3 === null ? '' : $et->adr3),
            'CP' => ($et->cp === null ? '' : $et->cp),
            'VILLE' => ($et->ville === null ? '' : $et->ville),
            'PAYS' => ($et->pays_id === null ? '' : $et->pays_id),
            'TYPE_PUBLIC' => ($et->type_public_id === null ? '' : $et->type_public_id),
            'DETAIL_TYPE_PUBLIC' => ($et->detail_type_public_id === null ? '' : $et->detail_type_public_id),
            //'MODALITES' => $listeModalites,
        ));
    }

    public function listZL() {

        /** ---------------------------------------------------------------------------------------
         * Récupère la liste des ETABLISSEMENTS
         */
        $etablissements = Etablissement::orderBy('nom')
            ->get();
        $result = array();
        foreach($etablissements as $etablissement){
            array_push($result, array(
                'id'   => $etablissement->id,
                'libelle'  => $etablissement->nom.($etablissement->sigle != '' ? ' ('.$etablissement->sigle.')' : '').
                    ' - '.$etablissement->cp.' '.$etablissement->ville,
            ));
        }
        return Response::json(array('LISTE' => $result));
    }
}