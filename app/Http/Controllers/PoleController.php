<?php

namespace App\Http\Controllers;

use App\Etablissement;
use App\EtablissementModalite;
use App\EtablissementModaliteDetail;
use App\Mail;
use App\MailOwner;
use App\ModaliteAccueil;
use App\Offres;
use App\Personnel;
use App\Pole;
use App\Telephone;
use App\TelephoneOwner;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class PoleController extends Controller
{

    /*
    protected $table = 'poles';
    protected $fillable = ['organisme_id', 'nom', 'sigle', 'type_pole_id', 'type_public_id',
        'detail_type_public_id',
        'nb_places', 'age', 'adr1', 'adr2', 'adr3', 'cp', 'ville', 'pays_id',
        'web', 'bi', 'bi_reception_id', 'nl', 'cf', 'cf_reception_id',
        'isCONTRIBUANT', 'notes', 'visible', 'acces',
        'active', 'user_id'];
    */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /** -----------------------------------------------------------------------------------
         * Données de base
         */
        $po = Pole::firstOrNew([
            'nom' => $request->pNom,
            'organisme_id' => $request->pOO,
        ]);
        $po->sigle = $request->pSigle;
        $po->type_pole_id = ($request->pTypeP == '' ? null : $request->pTypeP);
        $po->type_public_id = ($request->pPublic == '' ? null : $request->pPublic);
        $po->detail_type_public_id = ($request->pDetail == '' ? null : $request->pDetail);
        $po->nb_places = $request->pNbPlaces;
        $po->age = $request->pAge;
        $po->adr1 = $request->pAdr1;
        $po->adr2 = $request->pAdr2;
        $po->adr3 = $request->pAdr3;
        $po->cp = $request->pCP;
        $po->ville = $request->pVille;
        $po->pays_id = ($request->pPays == '' ? null : $request->pPays);
        $po->web = $request->pWeb;
        $po->bi = $request->pBI;
        $po->bi_reception_id = ($request->pBI_recep == '' ? null : $request->pBI_recep);
        $po->nl = $request->pNl;
        $po->isCONTRIBUANT = $request->pContrib;
        $po->cf = $request->pCF;
        $po->cf_reception_id = ($request->pCF_recep == '' ? null : $request->pCF_recep);
        $po->notes = $request->pNotes;
        $po->user_id = Auth::user()->id;
        $po->active = $request->pActif;
        $po->visible = $request->pVisible;
        $po->acces = $request->pAcces;
        $po->save();

        /** ---------------------------------------------------------------------------------------
         * Traitement des mails et des téléphones
         * Pour chaque téléphone, on va faire une première vérification dans la table
         *
         */
        if (isset($request->pTels)) {
            foreach ($request->pTels as $key => $val) {
                $num = $val[0];
                $prefere = $val[1];
                $type = $val[2];

                /** -------------------------------------------------------------------------------
                 * Il se peut que le téléphone existe déjà !
                 */
                $exist = false;
                $tel = Telephone::where('numero', '=', $num)
                    ->first();
                if ($tel === null) {
                    $tel = new Telephone();
                    $tel->numero = $num;
                    $tel->typetel_id = $type;
                    $tel->user_id = Auth::user()->id;
                } else {
                    $exist = true;
                    $tel->typetel_id = $type;
                    $tel->user_id = Auth::user()->id;
                    $tel->save();
                }
                $tel->save();

                $owner = new TelephoneOwner();
                $owner->telephone_id = $tel->id;
                $owner->prefere = $prefere;
                $owner->po_id = $po->id;
                $owner->user_id = Auth::user()->id;
                $owner->save();

            }
        }

        if (isset($request->pMails)) {
            foreach ($request->pMails as $key => $val) {
                $adr = $val[0];
                $prefere = $val[1];
                $type = $val[2];
                $mail = Mail::where('mail', '=', $adr)
                    ->first();
                if ($mail === null) {
                    $mail = new Mail();
                    $mail->mail = $adr;
                    $mail->typemail_id = $type;
                    $mail->user_id = Auth::user()->id;
                    $mail->save();
                } else {
                    $mail->typemail_id = $type;
                    $mail->user_id = Auth::user()->id;
                    $mail->save();
                }
                $owner = new MailOwner();
                $owner->mail_id = $mail->id;
                $owner->prefere = $prefere;
                $owner->user_id = Auth::user()->id;
                $owner->po_id = $po->id;
                $owner->save();
            }
        }
        return Response::json($po->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $po = Pole::find($id);

        /** ---------------------------------------------------------------------------------------
         * On prend aussi la liste des téléphones
         */
        $listeTels = DB::table('telephones')
            ->join('type_tels', 'type_tels.id', '=', 'telephones.typetel_id')
            ->join('telephone_owners', 'telephones.id', '=', 'telephone_owners.telephone_id')
            ->join('poles', 'poles.id', '=', 'telephone_owners.po_id')
            ->select('numero', 'type_tels.id AS TYPE_ID', 'prefere',
                'type_tels.libelle', 'telephone_owners.po_id AS PO_ID',
                'telephone_owners.ID AS OWN_ID', 'telephone_owners.notes')
            ->where('po_id', '=', $id)
            ->get();

        /** ---------------------------------------------------------------------------------------
         * On prend aussi la liste des mails
         */
        $listeMails = DB::table('mails')
            ->join('type_mails', 'type_mails.id', '=', 'mails.typemail_id')
            ->join('mail_owners', 'mails.id', '=', 'mail_owners.mail_id')
            ->join('poles', 'poles.id', '=', 'mail_owners.po_id')
            ->select('mail', 'type_mails.id AS TYPE_ID', 'prefere',
                'type_mails.libelle', 'mail_owners.po_id AS PO_ID',
                'mail_owners.ID AS OWN_ID')
            ->where('po_id', '=', $id)
            ->get();

        /** ---------------------------------------------------------------------------------------
         * On récupère la liste des établissement liés à ce PÔLE pour en déduire :
         * 1 - Le Type de Public (on voit çà dans les modalités)
         * 2 - Les Tranches d'Age (directement dans établissement)
         * 3 - Le nombre de Places (dans les modalités également)
         */
        $typePublic = [];
        $trancheAge = [];
        $nbPlaces = 0;
        $etablissements = Etablissement::where('po_id', '=', $id)
            ->where('active', '=', 1)
            ->get();
        foreach ($etablissements as $et) {

            /** -----------------------------------------------------------------------------------
             * Pour chaque modalité, on récupère la liste des détails
             */
            $modalites = EtablissementModalite::where('etablissement_id', '=', $et->id)
                ->get();
            foreach ($modalites as $modalite) {

                $detailsModalites = EtablissementModaliteDetail::where('etablissement_modalite_id', '=', $modalite->id)
                    ->get();
                foreach ($detailsModalites as $detail) {
                    $typePublic[] = $detail->typePublic->libelle;
                    $nbPlaces += $detail->nb_places;
                }
            }
            $trancheAge[] = $et->typeAge->libelle;
        }

        $trancheAge =  array_unique ( $trancheAge );
        $typePublic =  array_unique ( $typePublic );

        return Response::json(
            array(
                'PO' => $po,
                'TEL' => $listeTels,
                'MAIL' => $listeMails,
                'TYPE_PUBLIC' => $typePublic,
                'TYPE_AGE' => $trancheAge,
                'NB_PLACES' => $nbPlaces,
                'UPDATED_DATE' => Carbon::createFromFormat('Y-m-d H:i:s', $po->created_at)->format('d/m/Y'),
                'UPDATED_BY' => $po->user->name
            ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $po = Pole::find($id);

        $po->nom=  $request->pNom;
        $po->sigle = $request->pSigle;
        $po->type_pole_id = ($request->pTypeP == '' ? null : $request->pTypeP);
        $po->type_public_id = ($request->pPublic == '' ? null : $request->pPublic);
        $po->detail_type_public_id = ($request->pDetail == '' ? null : $request->pDetail);
        $po->nb_places = $request->pNbPlaces;
        $po->age = $request->pAge;
        $po->adr1 = $request->pAdr1;
        $po->adr2 = $request->pAdr2;
        $po->adr3 = $request->pAdr3;
        $po->cp = $request->pCP;
        $po->ville = $request->pVille;
        $po->pays_id = ($request->pPays == '' ? null : $request->pPays);
        $po->web = $request->pWeb;
        $po->bi = $request->pBI;
        $po->bi_reception_id = ($request->pBI_recep == '' ? null : $request->pBI_recep);
        $po->nl = $request->pNl;
        $po->isCONTRIBUANT = $request->pContrib;
        $po->cf = $request->pCF;
        $po->cf_reception_id = ($request->pCF_recep == '' ? null : $request->pCF_recep);
        $po->notes = $request->pNotes;
        $po->user_id = Auth::user()->id;
        $po->active = $request->pActif;
        $po->visible = $request->pVisible;
        $po->acces = $request->pAcces;
        $po->save();

        /** ---------------------------------------------------------------------------------------
         * TELEPHONES
         */
        if (isset($request->pTels)) {
            /** -----------------------------------------------------------------------------------
             * On crée les téléphones
             */
            foreach ($request->pTels as $key => $val) {
                $num = $val[0];
                $prefere = $val[1];
                $type = $val[2];
                $tel = Telephone::where('numero', '=', $num)
                    ->first();
                if ($tel === null) {
                    $tel = new Telephone();
                    $tel->numero = $num;
                    $tel->typetel_id = $type;
                    $tel->user_id = Auth::user()->id;
                    $tel->save();
                } else {
                    $tel->typetel_id = $type;
                    $tel->user_id = Auth::user()->id;
                    $tel->save();
                }
                /** -------------------------------------------------------------------------------
                 * Et on les affecte
                 */
                $owner = TelephoneOwner::firstOrNew([
                    'po_id' => $po->id,
                    'prefere' => $prefere,
                    'telephone_id' => $tel->id
                ]);
                $owner->save();
            }
        }

        /** -----------------------------------------------------------------------------------
         * MAILS
         */
        if (isset($request->pMails)) {
            /** -----------------------------------------------------------------------------------
             * On crée les téléphones
             */
            foreach ($request->pMails as $key => $val) {
                $adr = $val[0];
                $prefere = $val[1];
                $type = $val[2];
                $mail = Mail::where('mail', '=', $adr)
                    ->first();
                if ($mail === null) {
                    $mail = new Mail();
                    $mail->mail = $adr;
                    $mail->typemail_id = $type;
                    $mail->user_id = Auth::user()->id;
                    $mail->save();
                } else {
                    $mail->typemail_id = $type;
                    $mail->user_id = Auth::user()->id;
                    $mail->save();
                }
                /** -------------------------------------------------------------------------------
                 * Et on les affecte
                 */
                $owner = MailOwner::firstOrNew([
                    'po_id' => $po->id,
                    'prefere' => $prefere,
                    'mail_id' => $mail->id
                ]);
                $owner->save();
            }
        }

        return Response::json($po->id);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listDatatable($idOO) {

        $poles = Pole::where('organisme_id', '=', $idOO)
            ->get();

        /** @var Initialisation du tableau de sortie $result */
        $result = ['aaData' => []];

        /** @var On parcourt les logs $infoPatch */
        foreach($poles as $pole){


            /** -----------------------------------------------------------------------------------
             * Données de base
             */
            $id = $pole->id;
            $code = '<strong>'.'PO'.$pole->id.'</strong>';
            $code.= ($pole->id_interne != '' ? ' ('.$pole->id_interne.')' : '');
            $nom = $pole->nom;
            $sigle = $pole->sigle;
            $type = $pole->typePole->libelle;
            $cp = $pole->cp;
            $ville = $pole->ville;
            $dpt = substr($pole->cp, 0, 2);

            if (!($sigle=='')) {
                $nom.= ' <span class="label label-info"> '.$sigle.' </span>';
            }

            /** -----------------------------------------------------------------------------------
             * Données de base
             */
            if ($dpt != '') {
                $ville.= ' ('.$dpt.')';
            }

            /** -----------------------------------------------------------------------------------
             * ADD-IN - QUICK
             */
            $quick = '<a data-toggle="tooltip" title="ACTIF" class="po-actif btn btn-xs '.($pole->active == 1 ? 'green-seagreen' : 'red-sunglo').'" data-id="'.$id.'" data-statut="'.$pole->active.'"><i class="fa fa-thumbs-o-up"></i></a>';
            $quick.= '<a data-toggle="tooltip" title="VISIBLE" class="po-visible btn btn-xs '.($pole->visible == 1 ? 'green-seagreen' : 'red-sunglo').'" data-id="'.$id.'" data-statut="'.$pole->visible.'"><i class="fa fa-search"></i></a>';
            $quick.= '<a data-toggle="tooltip" title="ACCES WEB" class="po-acces btn btn-xs '.($pole->acces == 1 ? 'green-seagreen' : 'red-sunglo').'" data-id="'.$id.'" data-statut="'.$pole->acces.'"><i class="fa fa-internet-explorer"></i></a>';

            /** -----------------------------------------------------------------------------------
             * Actions complémentaires
             * POLE
             */
            $addin = '';
            $count = Etablissement::where('po_id', $pole->id)->count();
            $countP = Personnel::where('po_id', $pole->id)->count();
            $countO = Offres::where('po_id', $pole->id)->count();


            /** -----------------------------------------------------------------------------------
             * Actions
             */
            $action = '<a class="visu btn btn-xs blue" data-id="'.$id.'"><i class="fa fa-file-text-o"></i></a>';
            $actions = [];
            $actions[0][] = '<a class="personnel font-purple-soft" data-id="' . $id . '"><i class="fa fa-users font-purple-soft"></i> Personnel ' . ($countP != 0 ? '<span class="badge badge-danger"> '.$countP.' </span>'  : '') . '</a>';
            $actions[0][] = '<a class="etabl font-blue-steel" data-id="' . $id . '"><i class="fa fa-bed font-blue-steel"></i> Établissements ' . ($count != 0 ? '<span class="badge badge-danger"> '.$count.' </span>' : '') . '</a>';
            $actions[1][] = '<a class="telephone" data-id="'.$id.'"><i class="fa fa-phone"></i>Téléphones / Fax</a>';
            $actions[1][] .= '<a class="mail" data-id="'.$id.'"><i class="fa fa-send"></i>Mails</a>';
            $actions[1][] = '<a class="offres" data-id="'.$id.'"><i class="fa fa-sticky-note-o"></i>Offres d\'Emploi '.($countO != 0 ? '<span class="badge badge-danger"> '.$countO.' </span>' : '').'</a>';
            $actions[0][] .= '<a class="modif" data-id="'.$id.'"><i class="fa fa-edit"></i>Modifier</a>';

            /** -----------------------------------------------------------------------------------
             * Build Button
             */
            $buttons = $this->buildButtons($actions, 'down');

            $result['aaData'][] = [
                $code,
                '<span data-nom="'.$pole->nom.'">'. $nom .'</span>',
                $type,
                $ville,
                //$quick,
                $action.$buttons
            ];
        }

        return Response::json($result);
    }

    public function getData($id) {
        $po = Pole::find($id);
        return Response::json(array(
            'ADR1' => ($po->adr1 === null ? '' : $po->adr1),
            'ADR2' => ($po->adr2 === null ? '' : $po->adr2),
            'ADR3' => ($po->adr3 === null ? '' : $po->adr3),
            'CP' => ($po->cp === null ? '' : $po->cp),
            'VILLE' => ($po->ville === null ? '' : $po->ville),
            'PAYS' => ($po->pays_id === null ? '' : $po->pays_id),
            'NL' => $po->nl,
            'BI' => $po->bi,
            'RECEP_BI' => $po->bi_reception_id,
            'CF' => $po->cf,
            'RECEP_CF' => $po->cf_reception_id,
            'CONTRIB' => $po->isCONTRIBUANT,
        ));
    }

    public function switchStatut(Request $request, $id) {
        $po = Pole::find($id);
        switch ($request->pType) {
            case 'actif':
                $po->active = $request->pValue;
                break;
            case 'visible':
                $po->visible = $request->pValue;
                break;
            case 'acces':
                $po->acces = $request->pValue;
                break;
        }
        $po->save();
    }

    public function listZL($filter) {

        /** ---------------------------------------------------------------------------------------
         * Récupère la liste des Pôles d'un ORGANISME
         */
        $poles = Pole::
        where('active', '=', 1)
            ->where('organisme_id', '=', $filter)
            ->orWhere('id', '=', $filter)
            ->orderBy('nom')->get();
        $result = array();
        foreach($poles as $pole){
            array_push($result, array(
                'id'   => $pole->id,
                'libelle'  => $pole->nom,
            ));
        }
        return Response::json(array('LISTE' => $result));
    }

}
