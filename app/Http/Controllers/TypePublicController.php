<?php

namespace App\Http\Controllers;

use App\DetailTypePublic;
use App\TypePublic;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class TypePublicController extends Controller
{

    protected $table = 'type_publics';
    protected $fillable = ['libelle', 'note', 'active', 'user_id'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('params.typepublic', [
            'titre' => 'Paramétrage',
            'subtitre' => 'Types de Public',
            'sidebar' => $this->getSidebar()//json_decode(json_encode($tabBlocs))
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $type = TypePublic::find($id);
        return Response::json($type);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $type = TypePublic::firstOrNew([
            'libelle' => $request->pLibelle,
            'note' => $request->pNote,
        ]);
        $type->user_id = Auth::user()->id;
        $type->save();
        return Response::json($type->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $type = TypePublic::find($id);
        $type->active = $request->pValue;
        $type->user_id = Auth::user()->id;
        $type->save();
        return Response::json($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /** ---------------------------------------------------------------------------------------
         * Recherche d'un enregistrement identique avec un autre ID
         */
        $type = TypePublic::where('libelle', 'like', $request->pLibelle)
            ->where('id', '<>', $id)
            ->first();
        if ($type === null) {
            $type = TypePublic::find($id);
            $type->libelle = $request->pLibelle;
            $type->note = $request->pNote;
            $type->user_id = Auth::user()->id;
            $type->save();
            return Response::json($id);
        } else {
            return Response::json(-1);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listDatatable() {
        $types = TypePublic::all();

        /** @var Initialisation du tableau de sortie $result */
        $result = ['aaData' => []];

        /** @var On parcourt les logs $infoPatch */
        foreach($types as $type){


            /** -----------------------------------------------------------------------------------
             * Données
             */
            $id = $type->id;
            $libelle = $type->libelle;
            $note = $type->note;

            /** -----------------------------------------------------------------------------------
             * Détail ?
             */
            $addin = '';
            $count = DetailTypePublic::where('type_publics_id', $type->id)->count();
            $addin .= '<a class="detail btn btn-xs blue-steel" data-id="'.$id.'"><i class="fa fa-plus"></i> DETAIL '.($count != 0 ? '('.$count.')' : '').'</a>';

            /** -----------------------------------------------------------------------------------
             * Actions
             */
            $active =  $this->buildSwitch($id, $type->active);
            $action = '<a class="modif btn btn-xs blue" data-id="'.$id.'"><i class="fa fa-edit"></i></a>';
            $result['aaData'][] = [
                $libelle,
                $note,
                $active,
                $addin.$action
            ];
        }

        return Response::json($result);
    }

    public function listZL() {

        $publics = TypePublic::where('active', '=', 1)->orderBy('id')->get();
        $result = array();
        foreach($publics as $public){
            array_push($result, array(
                'id'   => $public->id,
                'libelle'  => $public->libelle,
            ));
        }
        return Response::json(array('LISTE' => $result));
    }
}
