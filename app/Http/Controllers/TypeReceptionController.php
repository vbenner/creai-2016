<?php

namespace App\Http\Controllers;

use App\TypeReception;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class TypeReceptionController extends Controller
{

    protected $table = 'type_receptions';
    protected $fillable = ['libelle', 'active', 'user_id'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('params.typereception', [
            'titre' => 'Paramétrage',
            'subtitre' => 'Type de Réception',
            'sidebar' => $this->getSidebar()//json_decode(json_encode($tabBlocs))
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $type = TypeReception::find($id);
        return Response::json($type);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $type = TypeReception::firstOrNew([
            'libelle' => $request->pLibelle
        ]);
        $type->checkMail = $request->pCheck;
        $type->user_id = Auth::user()->id;
        $type->save();
        return Response::json($type->id);    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $type = TypeReception::find($id);
        $type->active = $request->pValue;
        $type->user_id = Auth::user()->id;
        $type->save();
        return Response::json($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /** ---------------------------------------------------------------------------------------
         * Recherche d'un enregistrement identique avec un autre ID
         */
        $type = TypeReception::where('libelle', 'like', $request->pLibelle)
            ->where('id', '<>', $id)
            ->first();
        if ($type === null) {
            $type = TypeReception::find($id);
            $type->libelle = $request->pLibelle;
            $type->checkMail = $request->pCheck;
            $type->user_id = Auth::user()->id;
            $type->save();
            return Response::json($id);
        } else {
            return Response::json(-1);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listZL() {
        $types = TypeReception::where('active', '=', 1)->orderBy('libelle')->get();
        $result = array();
        foreach($types as $type){
            array_push($result, array(
                'id'   => $type->id,
                'libelle'  => $type->libelle,
                'check' => $type->checkMail
            ));
        }
        return Response::json(array('LISTE' => $result));
    }

    public function listDatatable() {
        $types = TypeReception::all();

        /** @var Initialisation du tableau de sortie $result */
        $result = ['aaData' => []];

        /** @var On parcourt les logs $infoPatch */
        foreach($types as $type){


            /** -----------------------------------------------------------------------------------
             * Données
             */
            $id = $type->id;
            $libelle = $type->libelle;

            /** -----------------------------------------------------------------------------------
             * Actions
             */
            $active =  $this->buildSwitch($id, $type->active);
            $action = '<a class="modif btn btn-xs blue" data-id="'.$id.'"><i class="fa fa-edit"></i></a>';
            $result['aaData'][] = [
                $libelle,
                $active,
                $action
            ];
        }

        return Response::json($result);
    }
}
