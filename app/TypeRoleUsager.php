<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeRoleUsager extends Model
{
    /**
     * @return array
     */
    protected $table = 'type_role_usager';
    protected $fillable = ['libelle', 'active', 'user_id'];
}
