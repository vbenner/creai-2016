<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pays extends Model
{
    /** -------------------------------------------------------------------------------------------
     * @return array
     */
    protected $table = 'pays';
    protected $fillable = ['libelle', 'active', 'user_id'];

    /** -------------------------------------------------------------------------------------------
     * Dépendances
     */
    public function personnesphysiques() {
        return $this->hasMany(PersonnePhysique::class, 'pays_id');
    }

}