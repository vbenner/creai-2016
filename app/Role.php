<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $guarded = ['id'];
    protected $table = 'roles';
    protected $fillable = ['code' , 'name'];

    public function users() {
        return $this->hasMany(Users::class);
    }
}
