<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TelephoneOwner extends Model
{
    protected $table = 'telephone_owners';
    protected $fillable = [
        'pp_id', 'oo_id', 'po_id', 'et_id', 'aa_id', 'an_id', 'pe_id',
        'prefere', 'telephone_id', 'notes', 'active', 'user_id'];
}
