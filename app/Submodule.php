<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Submodule extends Model
{

    protected $table = 'submodules';

    public function module() {
        return $this->belongsTo(Module::class);
    }

}
