<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeOrganisme extends Model
{
    /**
     * @return array
     */
    protected $table = 'type_organismes';
    protected $fillable = ['libelle', 'isOG', 'isPOLE', 'isGROUP', 'forceOG', 'active', 'user_id'];

}
