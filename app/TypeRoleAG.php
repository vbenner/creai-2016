<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeRoleAG extends Model
{
    /**
     * @return array
     */
    protected $table = 'type_roles_ag';
    protected $fillable = ['libelle', 'active', 'user_id'];

}
