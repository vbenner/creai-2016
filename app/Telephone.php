<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Telephone extends Model
{
    protected $table = 'telephones';
    protected $fillable = ['numero', 'typetel_id', 'active', 'user_id'];
}
