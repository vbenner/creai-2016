<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampagneagCotisant extends Model
{
    protected $table = 'campagneag_cotisants';
    protected $fillable = [
        'campagneags_id', 'pp_id', 'oo_id', 'pe_id',
        'user_id'
    ];

    public function listePP() {
        return $this->belongsTo(PersonnePhysique::class, 'pp_id');
    }

    public function listeOO() {
        return $this->belongsTo(Organisme::class, 'oo_id');
    }

    public function listePE() {
        return $this->belongsTo(Personnel::class, 'pe_id');
    }

    public function campagne() {
        return $this->belongsTo(Campagneag::class, 'campagneags_id');
    }

}
