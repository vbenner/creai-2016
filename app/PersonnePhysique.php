<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonnePhysique extends Model
{
    /** -------------------------------------------------------------------------------------------
     * @return array
     */
    protected $table = 'personnes_physiques';
    protected $fillable = ['id_interne', 'civilite_id', 'nom', 'prenom',
        'usager', 'etablissement_id',
        'adr1', 'adr2', 'adr3', 'cp', 'ville', 'pays_id',
        'voeux', 'nl', 'ag', 'role_ag_id',
        'cf', 'cf_reception_id',
        'buro', 'role_buro_id', 'ca', 'role_ca_id',
        'bi', 'bi_reception_id',
        'cotis', 'notes', 'user_id', 'history',
        'active', 'visible', 'acces',
        'user_id'];

    /** -------------------------------------------------------------------------------------------
     * Dépendances
     */
    public function civilite() {
        return $this->belongsTo(Civilite::class, 'civilite_id');
    }
    public function pays() {
        return $this->belongsTo(Pays::class, 'pays_id');
    }
    public function typeRoleUsager() {
        return $this->belongsTo(TypeRoleUsager::class, 'type_role_usager_id');
    }
    public function typeReceptionBI() {
        return $this->belongsTo(TypeReception::class, 'bi_reception_id');
    }
    public function typeRoleBuro() {
        return $this->belongsTo(TypeRoleBureau::class, 'role_buro_id');
    }
    public function typeRoleCA() {
        return $this->belongsTo(TypeRoleCA::class, 'role_ca_id');
    }
    public function typeRoleAG() {
        return $this->belongsTo(TypeRoleAG::class, 'role_ag_id');
    }
    public function user() {
        return $this->belongsTo('App\User');
    }

}