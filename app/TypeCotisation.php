<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeCotisation extends Model
{
    protected $table = 'type_cotisations';
    protected $fillable = ['libelle', 'montant', 'libre', 'active',
        'user_id'];
}
