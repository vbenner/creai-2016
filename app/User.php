<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'protected', 'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role() {
        return $this->belongsTo(Role::class, 'role_id');
    }

    #public function modules(){
    #    return $this->belongsToMany(Module::class, 'modules_grants');
    #}

    //public function submodules(){
    //    return $this->belongsToMany(Module::class, 'modules_grants');
    //}

}
