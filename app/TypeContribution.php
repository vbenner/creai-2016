<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeContribution extends Model
{
    /**
     * @return array
     */
    protected $table = 'type_contributions';
    protected $fillable = ['libelle', 'sigle', 'active',
        'user_id'];
}
