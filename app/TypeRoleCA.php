<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeRoleCA extends Model
{
    /**
     * @return array
     */
    protected $table = 'type_roles_ca';
    protected $fillable = ['libelle', 'active', 'user_id'];

}
