<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeTel extends Model
{
    /**
     * @return array
     */
    protected $table = 'type_tels';
    protected $fillable = ['libelle', 'active', 'user_id'];

}
