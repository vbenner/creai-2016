<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Model;
class AdresseAccueil extends Model
{
    protected $table = 'adresses_accueil';
    protected $fillable = ['nom', 'etablissement_id', 'antenne_id',
        'type_public_id', 'detail_type_public_id',
        'adr1', 'adr2', 'adr3', 'cp',
        'ville', 'pays_id', 'notes',
        'active', 'user_id'
    ];

    public function antenne() {
        return $this->belongsTo(Antenne::class, 'antenne_id');
    }
    public function etablissement() {
        return $this->belongsTo(Etablissement::class, 'etablissement_id');
    }
    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function pays() {
        return $this->belongsTo(Pays::class, 'pays_id');
    }
    public function typePublic() {
        return $this->belongsTo(TypePublic::class, 'type_public_id');
    }
    public function detailTypePublic() {
        return $this->belongsTo(DetailTypePublic::class, 'detail_type_public_id');
    }
}
