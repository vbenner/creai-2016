<?php

namespace App;

use App\Http\Controllers\EtablissementModaliteDetailController;
use Illuminate\Database\Eloquent\Model;

class EtablissementModalite extends Model
{

    protected $table = 'etablissement_modalites';
    protected $fillable = ['etablissement_id', 'type_etablissement_id',
        'active', 'user_id'];

    public function typeEtablissement() {
        return $this->belongsTo(TypeEtablissement::class, 'type_etablissement_id');
    }

    public function modaliteDetail()
    {
        return $this->hasMany(EtablissementModaliteDetailController::class, 'etablissement_modalite_id');
    }
}
