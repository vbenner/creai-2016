<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeSecteur extends Model
{
    /**
     * @return array
     */
    protected $table = 'type_secteurs';
    protected $fillable = ['libelle', 'isSecteurMS', 'active', 'user_id'];

}
