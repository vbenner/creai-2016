<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Antenne extends Model
{

    protected $table = 'antennes';
    protected $fillable = ['nom', 'etablissement_id',
        'adr1', 'adr2', 'adr3', 'cp',
        'ville', 'pays_id',
        'finess', 'arrete_pref', 'date_arrete', 'date_ouverture', 'isCONTRIBUANT',
        'notes',
        'active', 'visible', 'acces', 'user_id'
    ];

    /** -------------------------------------------------------------------------------------------
     * Dépendances
     */
    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function pays() {
        return $this->belongsTo(Pays::class, 'pays_id');
    }
    public function etablissement() {
        return $this->belongsTo(Etablissement::class, 'etablissement_id');
    }
}
