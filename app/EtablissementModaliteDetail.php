<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EtablissementModaliteDetail extends Model
{
    protected $table = 'etablissement_modalite_details';
    protected $fillable = ['etablissement_modalite_id', 'modalite_accueil_id',
        'type_public_id', 'detail_type_public_id', 'nb_places',
        'active', 'user_id'];

    public function typePublic()
    {
        return $this->belongsTo(TypePublic::class, 'type_public_id');
    }

}

