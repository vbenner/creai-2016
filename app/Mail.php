<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mail extends Model
{
    protected $table = 'mails';
    protected $fillable = ['mail', 'typemail_id', 'active', 'user_id'];
}
