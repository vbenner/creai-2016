<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailTypePublic extends Model
{
    protected $table = 'detail_type_publics';
    protected $fillable = ['type_publics_id', 'libelle', 'note',
        'active', 'user_id'];

    public function detailTypePublic() {
        return $this->belongsTo(TypePublic::class, 'type_publics_id');
    }

}
