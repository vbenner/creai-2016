<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organisme extends Model
{

    /** -------------------------------------------------------------------------------------------
     * @return array
     */
    protected $table = 'organismes';
    protected $fillable = ['id_interne', 'isGEST', 'finess', 'siret',
        'iban', 'bic', 'type_organisme_id', 'detail_type_organisme_id',
        'parrain', 'type_statut_id', 'nom',
        'sigle', 'adr1', 'adr2', 'adr3', 'cp', 'ville', 'pays_id',
        'web', 'bi', 'bi_reception_id',
        'nl', 'cotis',
        'cf', 'cf_reception_id',
        'notes', 'visible', 'acces', 'history',
        'active', 'user_id'
    ];

    /** -------------------------------------------------------------------------------------------
     * Dépendances
     */
    public function pays() {
        return $this->belongsTo(Pays::class, 'pays_id');
    }
    public function typeOrganisme() {
        return $this->belongsTo(TypeOrganisme::class, 'type_organisme_id');
    }
    public function detailTypeOrganisme() {
        return $this->belongsTo(DetailTypeOrganisme::class, 'detail_type_organisme_id');
    }
    public function typeStatutOrganisme() {
        return $this->belongsTo(TypeStatutOrganisme::class, 'type_statut_id');
    }
    public function typeReceptionBI() {
        return $this->belongsTo(TypeReception::class, 'bi_reception_id');
    }
    public function typeRepeptionCF() {
        return $this->belongsTo(TypeReception::class, 'cf_reception_id');
    }
    public function user() {
        return $this->belongsTo('App\User');
    }

}
