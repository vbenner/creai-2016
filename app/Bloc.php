<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bloc extends Model
{

    /**
     * @return array
     */
    protected $table = 'blocs';

    public function blocsGrants(){
        return $this->hasMany(BlocGrant::class, 'blocs_grants');
    }

    public function modules() {
        return $this->hasMany(Module::class, 'modules');
    }
}
