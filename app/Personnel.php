<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Personnel extends Model
{
    /** -------------------------------------------------------------------------------------------
     * @return array
     */
    protected $table = 'personnels';
    protected $fillable = [
        'civilite_id', 'nom', 'prenom',
        'oo_id', 'po_id', 'et_id', 'an_id',
        'adr1', 'adr2', 'adr3', 'cp', 'ville', 'pays_id',
        'service', 'fonction_id', 'notes_fonctions',
        'nl', 'voeux',
        'ag', 'role_ag_id',
        'cf', 'cf_reception_id',
        'buro', 'role_buro_id',
        'ca', 'role_ca_id',
        'bi', 'bi_reception_id', 'cotis',
        'ctc', 'cc',
        'clone_id', 'note',
        'active', 'visible', 'acces', 'user_id'];

    /** -------------------------------------------------------------------------------------------
     * Dépendances
     */
    public function organisme() {
        return $this->belongsTo(Organisme::class, 'oo_id');
    }
    public function pole() {
        return $this->belongsTo(Pole::class, 'po_id');
    }
    public function etablissement() {
        return $this->belongsTo(Etablissement::class, 'et_id');
    }
    public function antenne() {
        return $this->belongsTo(Antenne::class, 'an_id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function civilite() {
        return $this->belongsTo(Civilite::class, 'civilite_id');
    }
    public function pays() {
        return $this->belongsTo(Pays::class, 'pays_id');
    }
    public function fonction() {
        return $this->belongsTo(Fonction::class, 'fonction_id');
    }
    public function typeReceptionCF() {
        return $this->belongsTo(TypeReception::class, 'cf_reception_id');
    }
    public function typeRoleBuro() {
        return $this->belongsTo(TypeRoleBureau::class, 'role_buro_id');
    }
    public function typeRoleCA() {
        return $this->belongsTo(TypeRoleCA::class, 'role_ca_id');
    }
    public function typeRoleAG() {
        return $this->belongsTo(TypeRoleAG::class, 'role_ag_id');
    }
    public function typeReceptionBI() {
        return $this->belongsTo(TypeReception::class, 'bi_reception_id');
    }

}