<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pole extends Model
{

    protected $table = 'poles';
    protected $fillable = ['organisme_id', 'nom', 'sigle', 'type_pole_id', 'type_public_id',
        'detail_type_public_id', 'notesTypePublic', 'nb_places', 'age',
        'adr1', 'adr2', 'adr3', 'cp', 'ville', 'pays_id',
        'web', 'bi', 'bi_reception_id', 'nl', 'cf', 'cf_reception_id',
        'isCONTRIBUANT', 'notes', 'visible', 'acces',
        'active', 'user_id'];

    /** -------------------------------------------------------------------------------------------
     * Dépendances
     */
    public function organisme() {
        return $this->belongsTo(Organisme::class, 'organisme_id');
    }
    public function pays() {
        return $this->belongsTo(Pays::class, 'pays_id');
    }
    public function typePole() {
        return $this->belongsTo(TypePole::class, 'type_pole_id');
    }
    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

}
