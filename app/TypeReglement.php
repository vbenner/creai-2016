<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeReglement extends Model
{
    /**
     * @return array
     */
    protected $table = 'type_reglements';
    protected $fillable = ['libelle', 'sigle', 'active',
        'user_id'];
}
