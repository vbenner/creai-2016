<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailOwner extends Model
{
    protected $table = 'mail_owners';
    protected $fillable = [
        'pp_id', 'oo_id', 'po_id', 'et_id', 'aa_id', 'an_id', 'pe_id',
        'prefere', 'mail_id', 'notes', 'active', 'user_id'];

    public function mail() {
        return $this->belongsTo(Mail::class, 'mail_id');
    }

}
