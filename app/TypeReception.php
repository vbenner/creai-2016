<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeReception extends Model
{
    /**
     * @return array
     */
    protected $table = 'type_receptions';
    protected $fillable = ['libelle', 'checkMail', 'active', 'user_id'];

}
