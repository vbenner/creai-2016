<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypePole extends Model
{
    /**
     * @return array
     */
    protected $table = 'type_poles';
    protected $fillable = ['libelle', 'active', 'user_id'];

}
