<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubmoduleGrant extends Model
{

    protected $table = 'submodules_grants';
    protected $fillable = [
        'submodule_id', 'user_id', 'grant', 'edit', 'add'
    ];

    public function submodules() {
        return $this->belongsTo(Submodule::class, 'submodule_id');
    }

    public function users() {
        return $this->belongsTo(User::class, 'user_id');
    }

    //
}
