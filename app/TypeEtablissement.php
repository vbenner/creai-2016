<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeEtablissement extends Model
{
    /**
     * @return array
     */
    protected $table = 'type_etablissements';
    protected $fillable = ['libelle', 'sigle', 'type_secteur_id',
        'type_secteur_ms_id', 'type_age_id', 'active',
        'user_id'];

    public function typeSecteur() {
        return $this->belongsTo(TypeSecteur::class, 'type_secteur_id');
    }

    public function typeSecteurMS() {
        return $this->belongsTo(TypeSecteurMS::class, 'type_secteur_ms_id');
    }

    public function typeAge() {
        return $this->belongsTo(TypeAge::class, 'type_age_id');
    }

}
