<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModuleGrant extends Model
{
    protected $table = 'modules_grants';
    protected $fillable = [
        'module_id', 'user_id', 'grant', 'edit', 'add'
    ];
    public function modules() {
        return $this->belongsTo(Module::class, 'module_id');
    }

    public function users() {
        return $this->belongsTo(User::class, 'user_id');
    }
 
    public function onlyActive() {
        if ($this->modules()->active == 1) {
            return $this->modules();
        }
    }
}
