<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypePublic extends Model
{
    /**
     * @return array
     */
    protected $table = 'type_publics';
    protected $fillable = ['libelle', 'active', 'note',
        'active', 'user_id'
    ];

}
