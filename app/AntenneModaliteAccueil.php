<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AntenneModaliteAccueil extends Model
{
    protected $table = 'antennes_modalite_accueil';
    protected $fillable = ['antenne_id', 'type_hebergement_id',
        'active', 'user_id'];

}
