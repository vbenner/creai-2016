<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModaliteAccueil extends Model
{
    /**
     * @return array
     */
    protected $table = 'modalite_accueil';
    protected $fillable = ['libelle', 'MS', 'note', 'active', 'user_id'];

}
