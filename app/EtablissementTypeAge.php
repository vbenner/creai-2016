<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EtablissementTypeAge extends Model
{
    protected $table = 'etablissements_type_ages';
    protected $fillable = ['etablissement_id', 'type_age_id',
        'active', 'user_id'];
}
