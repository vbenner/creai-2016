<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailTypeOrganisme extends Model
{
    protected $table = 'detail_type_organismes';
    protected $fillable = ['type_organisme_id', 'libelle', 'note', 'hasParrain',
        'active', 'user_id'];

    public function detailTypePublic() {
        return $this->belongsTo(TypePublic::class, 'type_publics_id');
    }
}
