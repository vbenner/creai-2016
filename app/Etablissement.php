<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Etablissement extends Model
{

    protected $table = 'etablissements';
    protected $fillable = ['id_interne', 'nom', 'sigle', 'finess', 'arrete_pref',
        'date_arrete', 'date_ouverture', 'projet',
        'type_secteur_id', 'type_secteur_ms_id',
        'handicap', 'dispositif',
        'notesTypePublic', 'type_etablissement_id',
        'type_age_id',
        'adr1', 'adr2', 'adr3', 'cp',
        'ville', 'pays_id', 'web', 'bi', 'bi_reception_id', 'cf',
        'cf_reception_id', 'nl', 'isCONTRIBUANT',
        'age', 'nbplaces', 'oo_id', 'po_id', 'notes', 'visible', 'acces',
        'history', 'active', 'user_id'
    ];

    /** -------------------------------------------------------------------------------------------
     *
     */
    #public function scopeModalitesEtablissement($query) {
    #    return $query->listeModalitesEtablissement;
    #}

    /** -------------------------------------------------------------------------------------------
     * Dépendances
     */
    public function organisme() {
        return $this->belongsTo(Organisme::class, 'oo_id');
    }
    public function pole() {
        return $this->belongsTo(Pole::class, 'po_id');
    }
    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function pays() {
        return $this->belongsTo(Pays::class, 'pays_id');
    }
    public function typeSecteur() {
        return $this->belongsTo(TypeSecteur::class, 'type_secteur_id');
    }
    public function typeSecteurMS() {
        return $this->belongsTo(TypeSecteurMS::class, 'type_secteur_ms_id');
    }
    public function typeEtablissement() {
        return $this->belongsTo(TypeEtablissement::class, 'type_etablissement_id');
    }
    public function typeAge() {
        return $this->belongsTo(TypeAge::class, 'type_age_id');
    }
    public function typeReceptionBI() {
        return $this->belongsTo(TypeReception::class, 'bi_reception_id');
    }
    public function typeReceptionCF() {
        return $this->belongsTo(TypeReception::class, 'cf_reception_id');
    }
    public function modalitesEtablissement() {
        return $this->hasMany(EtablissementModalite::class, 'etablissement_id');
    }
}
