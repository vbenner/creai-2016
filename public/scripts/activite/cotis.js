/** -----------------------------------------------------------------------------------------------
 *
 * @author : Vincent BENNER
 * @copyright : Page Up 2016
 * @version : 1.0.0
 *
 * @history : 1.0.0 - Release
 * @description : Gestion des COTISATIONS
 *
 */

/** -----------------------------------------------------------------------------------------------
 *
 */
var tableName = 'tableCotisations';
var table;
var route = 'cotis'
var routeResCotis = 'cotisres'
var edit = ' des Cotisations';
var rebound = 0;

/** -----------------------------------------------------------------------------------------------
 * JQuery start
 */
$(function () {

    ClassCOTIS.init();

});

/** -----------------------------------------------------------------------------------------------
 * @class : ASSEMBLEE GENERALE
 */
var ClassCOTIS = {

    /* --------------------------------------------------------------------------------------------
     * Init
     */
    init : function () {
        if ($('#annee').val() == '') {
            alert ('Définir la prochaine AG pour utiliser ce module.');
            return;
        }
        ClassCOTIS.initTable();
        ClassCOTIS.initDataTable();
        ClassCOTIS.manageActions();

    },

    /* --------------------------------------------------------------------------------------------
     * On récupère la table des tous les COTISANTS
     */
    initTable : function () {
        $('#tableCotisant tbody').empty();
        $.ajax({
            url: '/datatable/' + route + '/' + $('#annee').val(),
            type: 'GET',
            dataType: 'text',
            async: false,
            success: function (data) {
                $('#tableCotisant tbody').append(data);
            }
        });
    },

    /* --------------------------------------------------------------------------------------------
     * Init TABLE
     */
    initDataTable : function () {
        table = $('#' + tableName).DataTable({
            iDisplayLength:50,
            ajax : "/datatable/" + routeResCotis + '/' + $('#annee').val(),
            serverSide : false,
            autoWidth: false,
            columnDefs: [
                {className: "dt-body-left", "targets": [0, 1, 2, 3, 4, 5]},
                {className: "dt-body-right", "targets": [6]},
            ],
            columns : [
            ],
            lengthMenu : [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "Tous"]
            ],
            sPagingType     : "bootstrap_full_numbers",
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
            },
            initComplete : function (settingssettings) {
                //return;
                /** On change les largeurs par défaut des contrôles */
                var tableWrapper = $('#' + tableName + '_wrapper');
                var tableLength = $('#' + tableName + '_length');
                tableLength.css('width', '400px');
                tableWrapper.find('.dataTables_length select').
                    addClass("form-control input-inline").
                    select2();
                tableWrapper.find('.dataTables_filter input').addClass("form-control input-inline");
            },
            fnDrawCallback: function(oSettings, json) {
                if (rebound == 0) {
                    rebound = 1;
                    return true;
                }
                ClassCOTIS.initGeneric('typecotisation', '.type_cotisation', '');
                ClassCOTIS.initGeneric('typereglement', '.type_reglement', '');
                $('.date-picker').datepicker({
                    autoclose: true,
                    calendarWeeks: true,
                    format: 'dd/mm/yyyy',
                    language: 'fr',
                    today: true
                });

                $('.type_cotisation').on('change', function (event){
                    event.preventDefault();
                    var libre = $(this).select2().find(":selected").data("libre");
                    var montant = $(this).select2().find(":selected").data("montant");
                    if (libre == 1) {
                        $(this).closest('tr').find('.montant').prop('readonly', false);
                    } else {
                        $(this).closest('tr').find('.montant').prop('readonly', true);
                        $(this).closest('tr').find('.montant').val(montant);
                    }
                });

                $('.valid').confirmation({
                    btnOkLabel: 'OUI',
                    btnOkClass: 'green-haze btn-xs',
                    btnCancelLabel: 'NON',
                    placement: 'left',
                    title: 'Valider ?',
                    singleton: true,
                    popout: true,
                    onConfirm: function () {
                        //var pk = $(this).closest('tr');
                        var id = $(this).closest('tr').find('.valid').attr('data-id');
                        var campagne_id = $(this).closest('tr').find('.valid').attr('data-campagne-id');
                        var type_oo = $(this).closest('tr').find('.valid').attr('data-type');
                        var type_cotis = $(this).closest('tr').find('.type_cotisation').val();
                        var montant = $(this).closest('tr').find('.montant').val();
                        var date = $(this).closest('tr').find('.date-picker').val();
                        var type_reglement = $(this).closest('tr').find('.type_reglement').val();
                        var reference = $(this).closest('tr').find('.reference').val();
                        var campagne = $('#annee').val();

                        if (montant == '') {
                            toastr.warning('Le montant doit être saisi.', "Attention");
                            return false;
                        }
                        if (date == '') {
                            toastr.warning('La date doit être saisie.', "Attention");
                            return false;
                        }
                        if (type_reglement == '') {
                            toastr.warning('La type de règlement doit être saisi.', "Attention");
                            return false;
                        }

                        $.ajax({
                            url: '/activite/validcotis/' + type_oo + '/' + id,
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                _token: $('#token').val(),
                                pCampagneId : campagne_id,
                                pTypeCotisation :  type_cotis,
                                pMontant : montant,
                                pDate : date,
                                pTypeReglement : type_reglement,
                                pReference : reference,
                                pCampagne : campagne
                            },
                            async: false
                        }).complete(function (code_html, statut) {
                            table.ajax.reload();
                        });

                        //$(this).closest('tr').remove();

                    }
                });

            },
            rowCallback : function( row, data, index ) {

                var colAction   = 3;

                /** --------------------------------------------------------------------------------
                 * On checke la colonne ACTION - lien A
                 */
                $('td:eq(' + colAction + ')', row).children('a').bind('click', function () {
                    if ($(this).hasClass('visu')) {
                        ClassCOTIS.view($(this).attr('data-id'));
                    }
                });
                $('td:eq(' + colAction + ')', row).find('a').bind('click', function () {

                    if ($(this).hasClass('modif')) {
                        ClassCOTIS.update($(this).attr('data-id'));
                    }
                });

            }

        });


        return true;
    },

    manageActions: function () {

        /** ---------------------------------------------------------------------------------------
         * Ajout d'une PERSONNE PHYSIQUE
         */
        $('#btnSelect').click(function () {
            $('#tableCotisant :checkbox').each(function() {
               this.checked = !(this.checked);
            });
        });

        /** ---------------------------------------------------------------------------------------
         * Création d'une campagne avec TOUS les cotisants COCHÉS
         */
        $('#btnCampagne').click(function () {

            if ($(this).attr('disabled')) {
                return false;
            }
            var tab = Array();
            var compte = 0;
            $('#tableCotisant :checkbox').each(function() {
                if (this.checked) {
                    compte++;
                    var obj = {
                        'id': $(this).attr('data-id'),
                        'type': $(this).attr('data-type'),
                    };

                    tab.push(obj);
                }
            });
            if (compte == 0) {
                toastr.warning('Il faut sélectionner au moins un cotisant.', "Attention");
                return false;
            }

            $.ajax({
                url: '/activite/' + route,
                type: 'POST',
                async: false,
                data: {
                    _token : $('#token').val(),
                    pAnnee : $('#annee').val(),
                    pTab : tab
                },
                success: function(data){
                    ClassCOTIS.initTable();
                    table.ajax.reload();
                    window.location = '/download/' + data;
                }
            })
        });
    },

    initGeneric: function (route, zone, value) {

        /** ---------------------------------------------------------------------------------------
         * Dans tous les cas, on récupère la liste des ITEMS
         */
        $.ajax({
            url: '/list/' + route,
            type: 'GET',
            dataType: 'json',
            data: {
                //pFilter : filter
            },
            async: false,
            success: function (data) {
                var select = '';
                var zl = $(zone);
                zl.empty();
                if (data.LISTE.length > 0) {
                    if (data.LISTE.length > 1) {
                        zl.append('<option value=""' + '></option>');
                    }
                    $.each(data.LISTE, function (index, item) {
                        zl.append('<option ' +
                            ' value="' + item.id + '" ' + (item.id == value ? ' selected="selected" ' : '') +
                            ' data-libre = "' + item.libre + '" '+
                            ' data-montant = "' + item.montant + '"> ' + item.libelle +
                            '</option>');
                    });
                    zl.select2({
                        width: '100%',
                        allowClear: (data.LISTE.length > 1 ? true : false),
                        placeholder: ' ',
                        language: 'fr'
                    });
                } else {
                }
            }
        });
    },

}