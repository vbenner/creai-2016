/** -----------------------------------------------------------------------------------------------
 *
 * @author : Vincent BENNER
 * @copyright : Page Up 2016
 * @version : 1.0.0
 *
 * @history : 1.0.0 - Release
 * @description : Gestion des ASSEMBLEES GENERALES
 *
 */

/** -----------------------------------------------------------------------------------------------
 *
 */
var tableName = 'table';
var table;
var route = 'ag'
var edit = ' d\'une Assemblée Générale';

/** -----------------------------------------------------------------------------------------------
 * JQuery start
 */
$(function () {

    ClassAG.init();
    ClassAG.initDataTable();
    ClassAG.manageActions();

});

/** -----------------------------------------------------------------------------------------------
 * @class : ASSEMBLEE GENERALE
 */
var ClassAG = {

    /* --------------------------------------------------------------------------------------------
     * Init
     */
    init : function () {

    },

    /* --------------------------------------------------------------------------------------------
     * Init Datatables
     */
    initDataTable : function () {
        table = $('#' + tableName).DataTable({
            iDisplayLength:50,
            ajax : "/datatable/" + route,
            serverSide : false,
            autoWidth: false,
            columnDefs: [
                {className: "dt-body-left", "targets": [0, 1, 2, 3]},
                //{className: "dt-body-right", "targets": [3]},
            ],
            columns : [
                {orderable: true, width : 80},
                {orderable: true, width : 80},
                {orderable: true, width : null},
                {orderable: true, width : 110}
            ],
            lengthMenu : [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "Tous"]
            ],
            sPagingType     : "bootstrap_full_numbers",
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
            },
            initComplete : function (settingssettings) {
                //return;
                /** On change les largeurs par défaut des contrôles */
                var tableWrapper = $('#' + tableName + '_wrapper');
                var tableLength = $('#' + tableName + '_length');
                tableLength.css('width', '400px');
                tableWrapper.find('.dataTables_length select').
                    addClass("form-control input-inline").
                    select2();
                tableWrapper.find('.dataTables_filter input').addClass("form-control input-inline");
            },
            rowCallback : function( row, data, index ) {

                var colAction   = 3;

                /* --------------------------------------------------------------------------------
                 * On checke la colonne ACTION
                 */
                $('td:eq(2)', row).addClass("txtSmall");
                $('td:eq(' + colAction + ')', row).find('.dropdown-toggle').dropdown();

                /** --------------------------------------------------------------------------------
                 * On checke la colonne ACTION - lien A
                 */
                $('td:eq(' + colAction + ')', row).children('a').bind('click', function () {
                    if ($(this).hasClass('visu')) {
                        ClassAG.view($(this).attr('data-id'));
                    }
                });
                $('td:eq(' + colAction + ')', row).find('a').bind('click', function () {

                    if ($(this).hasClass('modif')) {
                        ClassAG.update($(this).attr('data-id'));
                    }
                });

            }

        });


        return true;
    },

    /* --------------------------------------------------------------------------------------------
     * Modal d'ajout d'un enregistrement
     */
    add: function () {

        /** -----------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        var modalName = '#dialogExec';
        var modal = $(modalName);
        modal.html($('#formAG').html());

        $(":input").inputmask();
        $(modalName + ' .date-picker').datepicker({
            autoclose: true,
            calendarWeeks: true,
            format: 'dd/mm/yyyy',
            language: 'fr',
            todayBtn: true,
            todayHighlight: true,

        });

        /** -----------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        $(modal).find('#btnAction').addClass('green').html('<i class="fa fa-plus-circle"></i> Ajouter');
        $(modal).find('.modal-header').addClass('font-green');
        $(modal).find('.modal-title').html('<i class="fa fa-plus-circle"></i> Ajout ' + edit);
        $(modal).modal('show');

        $(modalName + ' #btnAction').unbind().on('click', function () {

            /** -----------------------------------------------------------------------------------
             * Vérification des ZONES Mandatory
             */
            var oblig = true;
            $(modalName + ' .required').each(function () {
                if (!$(this).val().trim()) {
                    oblig = false;
                }
            });
            if (!oblig) {
                toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                return;
            }

            $(this).off().removeClass('green').addClass('grey-cascade').html(
                '<i class="fa fa-spin fa-spinner"></i> Patientez...'
            );

            var annee = $(modalName + ' #ztAnneeAG').val();
            var date = $(modalName + ' #ztDateAG').val();
            var notes = $('<p>' + $(modalName + ' #ztNotes').val() + '</p>').text();
            $.ajax({
                url: '/activite/' + route,
                type: 'POST',
                data: {
                    _token : $(modalName + ' #token').val(),
                    pAnnee : annee,
                    pDate : date,
                    pNotes : notes,
                },
                dataType: 'json',
                success: function(data){
                    result = true;
                    $(modal).modal('hide');
                    toastr.success('Saisie enregistrée', "Information");
                    table.ajax.reload();
                }
            });

        });

    },

    update: function (id) {

        /** -----------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        var modalName = '#dialogExec';
        var modal = $(modalName);
        modal.html($('#formAG').html());

        /** -----------------------------------------------------------------------------------
         * On récupère les infos de la civilite
         */
        $.ajax({
            url: '/activite/' + route + '/' + id,
            type: 'GET',
            dataType: 'json',
            success: function(data){

                /** -----------------------------------------------------------------------------------
                 * Chargement du template du modal dans le modal d'exécution
                 */
                $(modal).find('#btnAction').addClass('yellow-gold').html('<i class="fa fa-plus-circle"></i> Modifier');
                $(modal).find('.modal-header').addClass('font-yellow-casablanca');
                $(modal).find('.modal-title').html('<i class="fa fa-refresh"></i> Modification ' + edit);
                $(modal).modal('show');

                /** -----------------------------------------------------------------------------------
                 * Init des zones
                 */
                var annee = data.AG.annee;
                var date = data.AG.date_ag;
                var notes = data.AG.notes;
                $(modalName + ' #ztAnneeAG').val(annee).addClass('edited');
                $(modalName + ' #ztDateAG').val(date);
                $(modalName + ' #ztNotes').val(notes);

                $(":input").inputmask();
                $(modalName + ' .date-picker').datepicker({
                    autoclose: true,
                    calendarWeeks: true,
                    format: 'dd/mm/yyyy',
                    language: 'fr',
                    todayBtn: true,
                    todayHighlight: true,

                });

                /** -----------------------------------------------------------------------------------
                 * Bind de la modification
                 */
                $(modalName + ' #btnAction').unbind().on('click', function () {

                    /** -----------------------------------------------------------------------------------
                     * Vérification des ZONES Mandatory
                     */
                    var oblig = true;
                    $(modalName + ' .required').each(function () {
                        if (!$(this).val().trim()) {
                            oblig = false;
                        }
                    });
                    if (!oblig) {
                        toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                        return;
                    }

                    $(this).off().removeClass('yellow-gold').addClass('grey-cascade').html(
                        '<i class="fa fa-spin fa-spinner"></i> Patientez...'
                    );

                    var annee = $(modalName + ' #ztAnneeAG').val();
                    var date = $(modalName + ' #ztDateAG').val();
                    var notes = $('<p>' + $(modalName + ' #ztNotes').val() + '</p>').text();
                    $.ajax({
                        url: '/activite/' + route + '/' + id,
                        type: 'PUT',
                        data: {
                            _token : $(modalName + ' #token').val(),
                            pAnnee : annee,
                            pDate : date,
                            pNotes : notes,
                        },
                        dataType: 'json',
                        success: function(data){
                            if (data == -1) {
                                $(modal).modal('hide');
                                toastr.error('Modification non effectuée', "Attention");
                            } else {
                                $(modal).modal('hide');
                                toastr.info('Modification enregistrée', "Information");
                                table.ajax.reload();
                            }
                        }
                    });
                });
            }
        });
    },

    view: function (id) {

        /** -----------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        var modalName = '#dialogExec';
        var modal = $(modalName);
        modal.html($('#formAG').html());

        /** -----------------------------------------------------------------------------------
         * On récupère les infos de la civilite
         */
        $.ajax({
            url: '/activite/' + route + '/' + id,
            type: 'GET',
            dataType: 'json',
            success: function(data){

                /** -------------------------------------------------------------------------------
                 * Chargement du template du modal dans le modal d'exécution
                 * Par défaut, les boutons ACTIONS sont cachés
                 */
                $(modalName).find('#btnAction').hide();
                $(modalName).find('.modal-header').addClass('font-grey-salsa');
                $(modalName).find('.modal-title').html('<i class="fa fa-refresh"></i> Consultation ' + edit);

                /** -----------------------------------------------------------------------------------
                 * Init des zones
                 */
                var annee = data.AG.annee;
                var date = data.AG.date_ag;
                var notes = data.AG.notes;
                $(modalName + ' #ztAnneeAG').val(annee).addClass('edited');
                $(modalName + ' #ztDateAG').val(date);
                $(modalName + ' #ztNotes').val(notes);

                $(modalName).find('.form-control').each(function () {
                    $(this).prop('disabled', true);
                });
                $(modalName + ' textarea').each(function () {
                    $(this).prop('disabled', false);
                    $(this).prop('readonly', true);
                });

                $(modalName).modal('show');

            }
        });
    },

    manageActions: function () {

        /** ---------------------------------------------------------------------------------------
         * Ajout d'une PERSONNE PHYSIQUE
         */
        $('#btnAdd').click(function () {
            ClassAG.add();
        });
    },
}