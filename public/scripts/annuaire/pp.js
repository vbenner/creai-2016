/** -----------------------------------------------------------------------------------------------
 *
 * @author : Vincent BENNER
 * @copyright : Page Up 2016
 * @version : 1.0.0
 *
 * @history : 1.0.0 - Release
 * @description : Gestion des PERSONNES PHYSIQUES
 *
 */

/** -----------------------------------------------------------------------------------------------
 * Données de base
 */
var tableName = 'table';
var table;
var route = 'pp'
var edit = ' d\'une Personne Physique / Usager';

var old_bd = 0;
var old_boot = 0;

/** -----------------------------------------------------------------------------------------------
 * JQuery start
 */
$(function () {

    ClassPP.init();
    ClassPP.initDataTable();
    ClassPP.manageActions();

});

/** -----------------------------------------------------------------------------------------------
 * @class : PERSONNE PHYSIQUE
 */
var ClassPP = {

    refreshEdited: function (modalName) {
        $(modalName).find('.form-md-floating-label').children('.form-control').each(function () {
            if ($(this).val().length > 0) {
                $(this).addClass('edited');
            }
        });
    },

    doReadOnly: function (modalName) {
        $(modalName).find('input[type=text], textarea').each(function () {
            $(this).prop('readonly', true);
        });
        $(modalName).find('select').each(function () {
            $(this).prop('disabled', true);
        });
        $(modalName).find('input[type=checkbox]').each(function () {
            $(this).prop('disabled', true);
        });
        $(modalName).find('.remove-telephone').hide();
        $(modalName).find('.remove-mail').hide();
    },

    editGeneric : function(type, route, modalName) {
        $('.edit-' + type).editable({
            emptytext : 'notes',
            validate: function (value) {

                if (value != null) {
                    $('.editable-submit').hide();
                    $('.editable-cancel').hide();

                    $.ajax({
                        url: '/annuaire/'+route+'/' + $(this).attr('data-id'),
                        type: 'PUT',
                        data: {
                            _token : $(modalName + ' #token').val(),
                            pValue: value
                        },
                        dataType: "json",
                        async: false
                    }).complete(function (code_html, statut) {
                    });

                }
            }
        });
    },

    init : function () {
    },

    /* --------------------------------------------------------------------------------------------
     * Init DataTable
     */
    initDataTable : function () {

        table = $('#' + tableName).removeAttr('width').DataTable({
            "dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
            iDisplayLength:50,
            ajax : "/datatable/" + route,
            serverSide : false,
            autoWidth: false,
            columnDefs: [
                {className: "dt-body-left", "targets": [0, 1, 2]},
                {className: "dt-body-right", "targets": [3]}
            ],
            columns : [
                null, null, null,
                {orderable: true, width: 140},
            ],
            lengthMenu : [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "Tous"]
            ],

            //fixedColumns: true,
            sPagingType     : "bootstrap_full_numbers",
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
            },
            initComplete : function (settingssettings) {

                /** On change les largeurs par défaut des contrôles */
                var tableWrapper = $('#' + tableName + '_wrapper');
                var tableLength = $('#' + tableName + '_length');
                tableLength.css('width', '400px');
                tableWrapper.find('.dataTables_length select').
                addClass("form-control input-inline").
                select2();
                tableWrapper.find('.dataTables_filter input').addClass("form-control input-inline");

                /** Ajout du tooltip */
                $('[data-toggle="tooltip"]').tooltip({
                    'delay': { show: 500, hide: 0 }
                });

            },

            rowCallback : function( row, data, index ) {

                var colAction   = 3;

                $('td:eq(' + colAction + ')', row).find('.dropdown-toggle').dropdown();

                /** --------------------------------------------------------------------------------
                 * On checke la colonne ACTION - lien A
                 */
                $('td:eq(' + colAction + ')', row).children('a').bind('click', function () {
                    if ($(this).hasClass('visu')) {
                        ClassPP.view($(this).attr('data-id'));
                    }
                });

                /** --------------------------------------------------------------------------------
                 * On checke la colonne ACTION
                 */
                $('td:eq(' + colAction + ')', row).find('.dropdown-menu > li > a').bind('click', function () {
                    if($(this).hasClass('modif')) {
                        ClassPP.update($(this).attr('data-id'));
                    }
                    if ($(this).hasClass('activite')) {
                        ClassPP.updateActivite($(this).attr('data-id'));
                    }
                    if ($(this).hasClass('telephone')) {
                        ClassPP.phoneList('#dialogExecLev9', 'pp', $(this).attr('data-id'));
                    }
                    if ($(this).hasClass('mail')) {
                        ClassPP.mailList('#dialogExecLev9', 'pp', $(this).attr('data-id'));
                    }
                });
            }
        });


        return true;
    },

    /** -------------------------------------------------------------------------------------------
     *
     * Modal d'ajout d'un enregistrement
     *
     */
    add: function () {

        /** ---------------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        var modalName = '#dialogExecLev0';
        var modal = $(modalName);
        modal.html($('#form').html());

        /** ---------------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        $(modal).find('#btnAction').addClass('green').html('<i class="fa fa-plus-circle"></i> Ajouter');
        $(modal).find('.modal-header').addClass('font-green');
        $(modal).find('.modal-title').html('<i class="fa fa-plus-circle"></i> Ajout ' + edit);
        $(modalName).find('.historique').hide();
        $(modal).modal({
            backdrop: 'static',
            keyboard: false,
        });

        /** ---------------------------------------------------------------------------------------
         * Init des LISTEs
         */
        ClassPP.initListes(modalName);

        /** ---------------------------------------------------------------------------------------
         * Attributs des listes par défaut
         */
        $(modalName + ' #zlReceptionCF').select2('enable', false);
        $(modalName + ' #zlReceptionBI').select2('enable', false);
        $(modalName + ' #zlRoleAG').select2('enable', false);
        $(modalName + ' #zlRoleCA').select2('enable', false);
        $(modalName + ' #zlRoleBURO').select2('enable', false);

        /** ---------------------------------------------------------------------------------------
         * Sauvegarde
         */
        ClassPP.manageModalButtons(modalName, 0);
        ClassPP.manageModalCC(modalName);

        /** ---------------------------------------------------------------------------------------
         * Sauvegarde
         */
        $(modalName + ' #btnAction').unbind().on('click', function () {

            /** -----------------------------------------------------------------------------------
             * Vérification des ZONES Mandatory
             */
            var oblig = true;
            $(modalName + ' .required').each(function () {
                if (!$(this).val().trim()) {
                    oblig = false;
                }
            });
            if (!oblig) {
                toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                return;
            }


            // ------------------------------------------------------------------------------------
            // Données de BASE
            // ------------------------------------------------------------------------------------
            var civilite = $(modalName + ' #zlCivilite').val();
            var nom = $(modalName + ' #ztNom').val().trim().toUpperCase();
            var prenom = $(modalName + ' #ztPrenom').val().trim().toUpperCase();
            var usager = $(modalName + ' #ccUSAGER').is(":checked") ? 1 : 0;
            var etablissement = $(modalName + ' #zlEtablissement').val();
            var typeusager = $(modalName + ' #zlTypeUsager').val();
            var code = $(modalName + ' #ztCodeInterne').val().trim().toUpperCase();
            var adr1 = $(modalName + ' #ztAdr1').val().trim().toUpperCase();
            var adr2 = $(modalName + ' #ztAdr2').val().trim().toUpperCase();
            var adr3 = $(modalName + ' #ztAdr3').val().trim().toUpperCase();
            var cp = $(modalName + ' #ztCP').val();
            var ville = $(modalName + ' #ztVille').val().trim().toUpperCase();
            var pays = $(modalName + ' #zlPays').val();
            var actif = $(modalName + ' #ccACTIF').is(":checked") ? 1 : 0;
            var visible = $(modalName + ' #ccVISIBLE').is(":checked") ? 1 : 0;
            var acces = $(modalName + ' #ccACCES').is(":checked") ? 1 : 0;

            // ------------------------------------------------------------------------------------
            // Données COMPLEMENTAIRES
            // ------------------------------------------------------------------------------------
            var voeux = $(modalName + ' #ccVOEUX').is(":checked") ? 1 : 0;
            var nl = $(modalName + ' #ccNL').is(":checked") ? 1 : 0;
            var bi = $(modalName + ' #ccBI').is(":checked") ? 1 : 0;
            var bi_recep = (bi == 1 ? $(modalName + ' #zlReceptionBI').val() : null);
            var cf = $(modalName + ' #ccCF').is(":checked") ? 1 : 0;
            var cf_recep = (cf == 1 ? $(modalName + ' #zlReceptionCF').val() : null);
            var ag = $(modalName + ' #ccAG').is(":checked") ? 1 : 0;
            var ag_role = (ag == 1 ? $(modalName + ' #zlRoleAG').val() : null);
            var bu = $(modalName + ' #ccBURO').is(":checked") ? 1 : 0;
            var bu_role = (bu == 1 ? $(modalName + ' #zlRoleBURO').val() : null);
            var ca = $(modalName + ' #ccCA').is(":checked") ? 1 : 0;
            var ca_role = (ca == 1 ? $(modalName + ' #zlRoleCA').val() : null);
            var cotis = $(modalName + ' #ccCOTIS').is(":checked") ? 1 : 0;

            if ((cf == 1 && cf_recep == '') ||
                (bi == 1 && bi_recep == '') ||
                (ag == 1 && ag_role == '') ||
                (bu == 1 && bu_role == '') ||
                (ca == 1 && ca_role == '') ||
                ((usager == 1 && etablissement == '') ||
                (usager == 1 && typeusager == ''))

            ) {
                toastr.warning('Vérifier les données complémentaires.', "Attention");
                return;
            }

            if (usager == 0) {
                etablissement = '';
                typeusager = '';
            }

            $(this).off().removeClass('green').addClass('grey-cascade').html(
                '<i class="fa fa-spin fa-spinner"></i> Patientez...'
            );

            // ------------------------------------------------------------------------------------
            // Le reste
            // ------------------------------------------------------------------------------------
            var notes = $($(modalName + ' #ztNotes').val()).text();
            var tels = [];
            $.each($('#dialogExecLev0' + ' #tableTel tbody tr td:first-child'), function (index, row) {
                var tel = $(this).attr('data-tel');
                var type = $(this).attr('data-type-tel');
                var item = [tel, type];
                tels.push(item);
            });
            var mails = [];
            $.each($('#dialogExecLev0' + ' #tableMail tbody tr td:first-child'), function (index, row) {
                var mail = $(this).attr('data-mail');
                var type = $(this).attr('data-type-mail');
                var item = [mail, type];
                mails.push(item);
            });

            $.ajax({
                url: '/annuaire/' + route,
                type: 'POST',
                data: {
                    _token : $(modalName + ' #token').val(),
                    pCivilite :  civilite,
                    pNom :  nom,
                    pPrenom : prenom,
                    pCode : code,
                    pAdr1 : adr1,
                    pAdr2 : adr2,
                    pAdr3 : adr3,
                    pCP : cp,
                    pVille : ville,
                    pPays : pays,
                    pVisible : visible,
                    pAcces : acces,
                    pActif : actif,
                    pUsager : usager,
                    pEtablissement : etablissement,
                    pVoeux : voeux,
                    pNL : nl,

                    pBI : bi,
                    pBI_Recep : bi_recep,
                    pCF : cf,
                    pCF_Recep : cf_recep,
                    pAG : ag,
                    pAG_Role : ag_role,
                    pBU : bu,
                    pBU_Role : bu_role,
                    pCA : ca,
                    pCA_Role : ca_role,
                    pTels : tels,
                    pMails : mails,
                    pCOTIS : cotis,
                    pNotes : notes

                },
                dataType: 'json',
                success: function(data){
                    result = true;
                    $(modal).modal('hide');
                    toastr.success('Saisie enregistrée', "Information");
                    table.ajax.reload();
                }
            });

        });

    },

    initGeneric : function (modal, route, zone, value) {

        $.ajax({
            url: '/list/' + route,
            type: 'GET',
            dataType: 'json',
            async:false,
            success: function(data){
                var select = '';
                if(data.LISTE.length > 0){
                    var zl = $(modal + ' ' + zone);
                    zl.empty();
                    if (data.LISTE.length > 1) {
                        zl.append('<option value=""></option>');
                    }
                    $.each(data.LISTE, function(index, item){
                        zl.append('<option value="'+item.id+'" ' + (item.id == value ? ' selected="selected" ' : '') + '>'+item.libelle+'</option>');
                    });
                    zl.select2({
                        width: '100%',
                        allowClear:(data.LISTE.length > 1 ? true : false),
                        placeholder: ' '
                    });
                }

            }
        });
    },

    initListes : function (modal) {

        /** ---------------------------------------------------------------------------------------
         * Listes de bases
         */
        ClassPP.initGeneric(modal, 'civilite', '#zlCivilite');
        ClassPP.initGeneric(modal, 'pays', '#zlPays');

        /** ---------------------------------------------------------------------------------------
         * Liste additionnelles avec EVENTS
         */
        ClassPP.initGeneric(modal, 'reception', '#zlReceptionCF');
        ClassPP.initGeneric(modal, 'reception', '#zlReceptionBI');
        ClassPP.initGeneric(modal, 'roleag', '#zlRoleAG');
        ClassPP.initGeneric(modal, 'rolebureau', '#zlRoleBURO');
        ClassPP.initGeneric(modal, 'roleca', '#zlRoleCA');
        ClassPP.initGeneric(modal, 'typetel', '#portletTel #zlTypeSaisie');
        ClassPP.initGeneric(modal, 'typemail', '#portletMail #zlTypeSaisie');

        ClassPP.initGeneric(modal, 'etablissements', '#zlEtablissement');
        ClassPP.initGeneric(modal, 'roleusager', '#zlTypeUsager');

    },

    updateMail : function (id) {

        /** -----------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        var modalName = '#dialogExecLev0';
        var modal = $(modalName);
        modal.html($('#formMail').html());

        /** -----------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        $.ajax({
            url: '/datatable/pp/mails/' + id,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {

                var pp_id = undefined
                $.each(data.MAIL, function (index, item) {
                    pp_id = item.PP_ID;
                    $(modalName + ' #tableMail tbody').append(
                        '<tr>' +
                        '<td data-pk="' + item.OWN_ID + '" ' +
                        '    data-type-tel="' + item.TYPE_ID + '"' +
                        '    data-pp="' + item.PP_ID + '"' +
                        '    data-mail="' + item.mail + '" ' +
                        '>' + item.mail +'</td>' +
                        '<td>' + item.libelle +'</td>' +
                        '<td class="text-right">' +
                        '<a href="#" class="editable edit-mail" ' +
                        '   data-type="text" ' +
                        '   data-id="' +item.OWN_ID+'" ' +
                        '   data-title="Saisir les notes" ' +
                        '   data-value="'+ item.notes +'" ' +
                        '   data-placement="right" ' +
                        '   data-mode="inline"' +
                        '   style="display: inline;" ></a>'+
                        '<td class="text-right">' +
                        '<a class="remove-mail btn btn-xs red"' +
                        ' data-placement="left" data-toggle="confirmation" data-original-title="" title="" ' +
                        '><i class="fa fa-remove"></i></a>' +
                        '</td>' +
                        '</tr>'
                    );
                });
                ClassPP.initGeneric(modalName, 'typemail', '#zlTypeMail');
                ClassPP.manageModalButtons(modalName, 1, pp_id);



                /** -------------------------------------------------------------------------------
                 * Custom des boutons / défaut
                 *
                 * @type {string}
                 */
                $.fn.editableform.buttons =
                    '<button class="btn btn-xs green editable-submit" type="submit"><i class="fa fa-check"></i></button>'+
                    '<button class="btn btn-xs default editable-cancel" type="button"><i class="fa fa-times"></i></button>'
                ;
                $('.edit-mail ').editable({
                    emptytext : 'notes',
                    validate: function (value) {

                        if (value != null) {
                            $('.editable-submit').hide();
                            $('.editable-cancel').hide();

                            $.ajax({
                                url: '/annuaire/mail/' + $(this).attr('data-id'),
                                type: 'PUT',
                                data: {
                                    _token : $(modalName + ' #token').val(),
                                    pValue: value
                                },
                                dataType: "json",
                                async: false
                            }).complete(function (code_html, statut) {
                            });

                        }
                    }
                });


                /**
                 $(modal).find('#btnActionTel').addClass('red').html(
                 '<i class="fa fa-phone"></i> Valider'
                 );
                 */
                $(modal).find('.modal-header').addClass('font-green-seagreen ');
                $(modal).find('.modal-title').html('<i class="fa fa-send"></i> Gestion des Mails');
                $(modal).modal('show');

            }
        });
    },

    updateActivite : function (id) {

        /** -----------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        var modalName = '#dialogExecLev0';
        var modal = $(modalName);
        modal.html($('#formActivite').html());

        $(modal).find('.modal-header').addClass('font-green-seagreen ');
        $(modal).find('.modal-title').html('<i class="fa fa-phone"></i> Gestion des Activités');
        $(modal).modal('show');
    },

    update: function (id) {

        /** -----------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        var modalName = '#dialogExecLev0';
        var modal = $(modalName);
        modal.html($('#form').html());

        /** -----------------------------------------------------------------------------------
         * On récupère les infos de la civilite
         */
        $.ajax({
            url: '/annuaire/' + route + '/' + id,
            type: 'GET',
            dataType: 'json',
            async : false,
            success: function(data){

                /** -----------------------------------------------------------------------------------
                 * Chargement du template du modal dans le modal d'exécution
                 */
                $(modal).find('#btnAction').addClass('yellow-gold').html('<i class="fa fa-plus-circle"></i> Modifier');
                $(modal).find('.modal-header').addClass('font-yellow-casablanca');
                $(modal).find('.modal-title').html('<i class="fa fa-refresh"></i> Modification ' + edit);

                $(modalName).find('.historique').show().html(
                    'Modifié le ' + data.UPDATED_DATE + ' par ' + data.UPDATED_BY
                );

                $(modal).modal('show');

                /** -----------------------------------------------------------------------------------
                 * Init des zones PERSONNES PHYSIQUES
                 */
                var civilite = data.PP.civilite_id;
                var nom = data.PP.nom;
                var prenom = data.PP.prenom;
                var code = data.PP.id_interne;
                var adr1 = data.PP.adr1;
                var adr2 = data.PP.adr2;
                var adr3 = data.PP.adr3;
                var cp = data.PP.cp;
                var ville = data.PP.ville;
                var pays = data.PP.pays_id;

                var usager = data.PP.usager;
                var etablissement = data.PP.etablissement_id;
                var typeusager = data.PP.type_role_usager_id;
                var actif = data.PP.active;
                var visible = data.PP.visible;
                var acces = data.PP.acces;

                var voeux = data.PP.voeux;
                var nl = data.PP.nl;

                var bi = data.PP.bi;
                var bi_recep = data.PP.bi_reception_id;
                var cf = data.PP.cf;
                var cf_recep = data.PP.cf_reception_id;

                var ag = data.PP.ag;
                var ag_role = data.PP.role_ag_id;
                var bu = data.PP.buro;
                var bu_role = data.PP.role_buro_id;
                var ca = data.PP.ca;
                var ca_role = data.PP.role_ca_id;
                var cotis = data.PP.cotis;
                var notes = data.PP.notes;

                $(modalName + ' #ztNom').val(nom);//.addClass(nom != '' ? 'edited' : '');
                $(modalName + ' #ztNom').focus();
                $(modalName + ' #ztPrenom').val(prenom);
                $(modalName + ' #ztCodeInterne').val(code);
                $(modalName + ' #ztAdr1').val(adr1);
                $(modalName + ' #ztAdr2').val(adr2);
                $(modalName + ' #ztAdr3').val(adr3);
                $(modalName + ' #ztCP').val(cp);
                $(modalName + ' #ztVille').val(ville);
                $(modalName + ' #ztNotes').val(notes);

                ClassPP.initGeneric (modalName, 'civilite', '#zlCivilite', civilite);
                ClassPP.initGeneric (modalName, 'pays', '#zlPays', pays);

                if (usager == 1) {
                    $(modalName + ' #btnCopieData').removeClass('display-hide');
                }
                /** -------------------------------------------------------------------------------
                 * Données complémentaires
                 */
                $(modalName + ' #ccACTIF').prop('checked', (actif == 1 ? true : false));
                $(modalName + ' #ccVISIBLE').prop('checked', (visible == 1 ? true : false));
                $(modalName + ' #ccACCES').prop('checked', (acces == 1 ? true : false));

                $(modalName + ' #ccCOTIS').prop('checked', (cotis == 1 ? true : false));
                $(modalName + ' #ccVOEUX').prop('checked', (voeux == 1 ? true : false));
                $(modalName + ' #ccNL').prop('checked', (nl == 1 ? true : false));
                $(modalName + ' #ccBI').prop('checked', (bi == 1 ? true : false));
                $(modalName + ' #ccCF').prop('checked', (cf == 1 ? true : false));
                $(modalName + ' #ccAG').prop('checked', (ag == 1 ? true : false));
                $(modalName + ' #ccBURO').prop('checked', (bu == 1 ? true : false));
                $(modalName + ' #ccCA').prop('checked', (ca == 1 ? true : false));
                $(modalName + ' #ccUSAGER').prop('checked', (usager == 1 ? true : false));

                ClassPP.initGeneric(modalName, 'reception', '#zlReceptionCF', cf_recep);
                ClassPP.initGeneric(modalName, 'reception', '#zlReceptionBI', bi_recep);
                ClassPP.initGeneric(modalName, 'roleag', '#zlRoleAG', ag_role);
                ClassPP.initGeneric(modalName, 'rolebureau', '#zlRoleBURO', bu_role);
                ClassPP.initGeneric(modalName, 'roleca', '#zlRoleCA', ca_role);
                ClassPP.initGeneric(modalName, 'typetel', '#zlTypeTel');
                ClassPP.initGeneric(modalName, 'typemail', '#zlTypeMail');
                ClassPP.initGeneric(modalName, 'etablissements', '#zlEtablissement', etablissement);
                ClassPP.initGeneric(modalName, 'roleusager', '#zlTypeUsager', typeusager);

                /** -----------------------------------------------------------------------------------
                 * Init des zones TELEPHONES
                 */
                ClassPP.displayTels(modalName, data.TEL);
                ClassPP.displayMails(modalName, data.MAIL);
                $('#portletTel .portlet-body').find('.row:first').show();
                $('#portletMail .portlet-body').find('.row:first').show();

                /** ---------------------------------------------------------------------------------------
                 * Evènements
                 */
                ClassPP.manageModalButtons(modalName, 0, id);
                ClassPP.manageModalCC(modalName);

                /** ---------------------------------------------------------------------------------------
                 * Attributs des listes par défaut
                 */
                if (bi == 0) {
                    $(modalName + ' #zlReceptionBI').select2('enable', false);
                }
                if (cf == 0) {
                    $(modalName + ' #zlReceptionCF').select2('enable', false);
                }
                if (ag == 0) {
                    $(modalName + ' #zlRoleAG').select2('enable', false);
                }
                if (ca == 0) {
                    $(modalName + ' #zlRoleCA').select2('enable', false);
                }
                if (bu == 0) {
                    $(modalName + ' #zlRoleBURO').select2('enable', false);
                }
                if (bu == 0) {
                    $(modalName + ' #zlRoleBURO').select2('enable', false);
                }
                if (usager == 0) {
                } else {
                    $(modalName + ' #zlEtablissement').select2('enable');
                    $(modalName + ' #zlTypeUsager').select2('enable');
                }

                /** -----------------------------------------------------------------------------------
                 * Bind de la modification
                 */
                ClassPP.refreshEdited(modalName);
                $(modalName + ' #btnAction').unbind().on('click', function () {

                    /** -----------------------------------------------------------------------------------
                     * Vérification des ZONES Mandatory
                     */
                    var oblig = true;
                    $(modalName + ' .required').each(function () {
                        if (!$(this).val().trim()) {
                            oblig = false;
                        }
                    });
                    if (!oblig) {
                        toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                        return;
                    }

                    // ------------------------------------------------------------------------------------
                    // Données de BASE
                    // ------------------------------------------------------------------------------------
                    civilite = $(modalName + ' #zlCivilite').val();
                    nom = $(modalName + ' #ztNom').val().trim().toUpperCase();
                    prenom = $(modalName + ' #ztPrenom').val().trim().toUpperCase();
                    code = $(modalName + ' #ztCodeInterne').val().trim().toUpperCase();
                    adr1 = $(modalName + ' #ztAdr1').val().trim().toUpperCase();
                    adr2 = $(modalName + ' #ztAdr2').val().trim().toUpperCase();
                    adr3 = $(modalName + ' #ztAdr3').val().trim().toUpperCase();
                    cp = $(modalName + ' #ztCP').val();
                    ville = $(modalName + ' #ztVille').val().trim().toUpperCase();
                    pays = $(modalName + ' #zlPays').val();

                    usager = $(modalName + ' #ccUSAGER').is(":checked") ? 1 : 0;
                    etablissement = (usager == 1 ? $(modalName + ' #zlEtablissement').val() : null);
                    typeusager = (usager == 1 ? $(modalName + ' #zlTypeUsager').val() : null);
                    actif = $(modalName + ' #ccACTIF').is(":checked") ? 1 : 0;
                    visible = $(modalName + ' #ccVISIBLE').is(":checked") ? 1 : 0;
                    acces = $(modalName + ' #ccACCES').is(":checked") ? 1 : 0;

                    // ------------------------------------------------------------------------------------
                    // Données COMPLEMENTAIRES
                    // ------------------------------------------------------------------------------------
                    cotis = $(modalName + ' #ccCOTIS').is(":checked") ? 1 : 0;
                    voeux = $(modalName + ' #ccVOEUX').is(":checked") ? 1 : 0;
                    nl = $(modalName + ' #ccNL').is(":checked") ? 1 : 0;
                    bi = $(modalName + ' #ccBI').is(":checked") ? 1 : 0;
                    bi_recep = (bi == 1 ? $(modalName + ' #zlReceptionBI').val() : null);
                    cf = $(modalName + ' #ccCF').is(":checked") ? 1 : 0;
                    cf_recep = (cf == 1 ? $(modalName + ' #zlReceptionCF').val() : null);
                    ag = $(modalName + ' #ccAG').is(":checked") ? 1 : 0;
                    ag_role = (ag == 1 ? $(modalName + ' #zlRoleAG').val() : null);
                    bu = $(modalName + ' #ccBURO').is(":checked") ? 1 : 0;
                    bu_role = (bu == 1 ? $(modalName + ' #zlRoleBURO').val() : null);
                    ca = $(modalName + ' #ccCA').is(":checked") ? 1 : 0;
                    ca_role = (ca == 1 ? $(modalName + ' #zlRoleCA').val() : null);

                    if ((cf == 1 && cf_recep == '') ||
                        (bi == 1 && bi_recep == '') ||
                        (ag == 1 && ag_role == '') ||
                        (bu == 1 && bu_role == '') ||
                        (ca == 1 && ca_role == '') ||
                        (usager == 1 && etablissement == '')
                    ) {
                        toastr.warning('Vérifier les données complémentaires.', "Attention");
                        return;
                    }

                    $(this).off().removeClass('green').addClass('grey-cascade').html(
                        '<i class="fa fa-spin fa-spinner"></i> Patientez...'
                    );

                    // ------------------------------------------------------------------------------------
                    // Le reste
                    // ------------------------------------------------------------------------------------
                    notes = $($(modalName + ' #ztNotes').val()).text();

                    // ------------------------------------------------------------------------------------
                    // Les nouveaux téléphones uniquement
                    // ------------------------------------------------------------------------------------
                    var tels = [];
                    $.each($('#dialogExecLev0' + ' #tableTel tbody tr td:first-child'), function (index, row) {
                        if ($(this).attr('data-pk') == undefined) {
                            var tel = $(this).attr('data-tel');
                            var type = $(this).attr('data-type-tel');
                            var item = [tel, type];
                            tels.push(item);
                        }
                    });
                    var mails = [];
                    $.each($('#dialogExecLev0' + ' #tableMail tbody tr td:first-child'), function (index, row) {
                        if ($(this).attr('data-pk') == undefined) {
                            var mail = $(this).attr('data-mail');
                            var type = $(this).attr('data-type-mail');
                            var item = [mail, type];
                            mails.push(item)
                        };
                    });

                    $.ajax({
                        url: '/annuaire/' + route + '/' + id,
                        type: 'PUT',
                        data: {
                            _token : $(modalName + ' #token').val(),
                            pCivilite :  civilite,
                            pNom :  nom,
                            pPrenom : prenom,
                            pCode : code,
                            pAdr1 : adr1,
                            pAdr2 : adr2,
                            pAdr3 : adr3,
                            pCP : cp,
                            pVille : ville,
                            pPays : pays,

                            pUsager : usager,
                            pEtablissement : etablissement,
                            pTypeUsager : typeusager,
                            pActif : actif,
                            pVisible : visible,
                            pAcces : acces,

                            pVoeux : voeux,
                            pNL : nl,

                            pBI : bi,
                            pBI_Recep : bi_recep,
                            pCF : cf,
                            pCF_Recep : cf_recep,
                            pAG : ag,
                            pAG_Role : ag_role,
                            pBU : bu,
                            pBU_Role : bu_role,
                            pCA : ca,
                            pCA_Role : ca_role,
                            pTels : tels,
                            pMails : mails,
                            pCOTIS : cotis,
                            pNotes : notes
                        },
                        dataType: 'json',
                        success: function(data){
                            if (data == -1) {
                                $(modal).modal('hide');
                                toastr.error('Modification non effectuée', "Attention");
                            } else {
                                $(modal).modal('hide');
                                toastr.info('Modification enregistrée', "Information");
                                table.ajax.reload();
                            }
                        }
                    });
                });
            }
        });
    },

    view: function (id) {

        /** -----------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        var modalName = '#dialogExecLev0';
        var modal = $(modalName);
        modal.html($('#form').html());

        /** -----------------------------------------------------------------------------------
         * On récupère les infos de la civilite
         */
        $.ajax({
            url: '/annuaire/' + route + '/' + id,
            type: 'GET',
            dataType: 'json',
            async : false,
            success: function(data){

                /** -------------------------------------------------------------------------------
                 * Chargement du template du modal dans le modal d'exécution
                 * Par défaut, les boutons ACTIONS sont cachés
                 */
                $(modalName).find('#btnAction').hide();
                $(modalName).find('#btnAddTel').hide();
                $(modalName).find('#btnAddMail').hide();
                $(modalName).find('.modal-header').addClass('font-grey-salsa');
                $(modalName).find('.modal-title').html('<i class="fa fa-refresh"></i> Consultation ' + edit);

                $(modalName).find('.historique').show().html(
                    'Modifié le ' + data.UPDATED_DATE + ' par ' + data.UPDATED_BY
                );

                $(modal).modal('show');

                /** -----------------------------------------------------------------------------------
                 * Init des zones PERSONNES PHYSIQUES
                 */
                var civilite = data.PP.civilite_id;
                var nom = data.PP.nom;
                var prenom = data.PP.prenom;
                var code = data.PP.id_interne;
                var adr1 = data.PP.adr1;
                var adr2 = data.PP.adr2;
                var adr3 = data.PP.adr3;
                var cp = data.PP.cp;
                var ville = data.PP.ville;
                var pays = data.PP.pays_id;

                var usager = data.PP.usager;
                var etablissement = data.PP.etablissement_id;
                var typeusager = data.PP.type_role_usager_id;
                var actif = data.PP.active;
                var visible = data.PP.visible;
                var acces = data.PP.acces;

                var voeux = data.PP.voeux;
                var nl = data.PP.nl;

                var bi = data.PP.bi;
                var bi_recep = data.PP.bi_reception_id;
                var cf = data.PP.cf;
                var cf_recep = data.PP.cf_reception_id;

                var ag = data.PP.ag;
                var ag_role = data.PP.role_ag_id;
                var bu = data.PP.buro;
                var bu_role = data.PP.role_buro_id;
                var ca = data.PP.ca;
                var ca_role = data.PP.role_ca_id;
                var cotis = data.PP.cotis;
                var notes = data.PP.notes;

                var cotisations = data.COTISATIONS;

                $(modalName + ' #ztNom').val(nom);//.addClass(nom != '' ? 'edited' : '');
                $(modalName + ' #ztNom').focus();
                $(modalName + ' #ztPrenom').val(prenom);
                $(modalName + ' #ztCodeInterne').val(code);
                $(modalName + ' #ztAdr1').val(adr1);
                $(modalName + ' #ztAdr2').val(adr2);
                $(modalName + ' #ztAdr3').val(adr3);
                $(modalName + ' #ztCP').val(cp);
                $(modalName + ' #ztVille').val(ville);
                $(modalName + ' #ztNotes').val(notes);

                ClassPP.initGeneric (modalName, 'civilite', '#zlCivilite', civilite);
                ClassPP.initGeneric (modalName, 'pays', '#zlPays', pays);

                /** -------------------------------------------------------------------------------
                 * Données complémentaires
                 */
                $(modalName + ' #ccACTIF').prop('checked', (actif == 1 ? true : false));
                $(modalName + ' #ccVISIBLE').prop('checked', (visible == 1 ? true : false));
                $(modalName + ' #ccACCES').prop('checked', (acces == 1 ? true : false));

                $(modalName + ' #ccCOTIS').prop('checked', (cotis == 1 ? true : false));
                $(modalName + ' #ccVOEUX').prop('checked', (voeux == 1 ? true : false));
                $(modalName + ' #ccNL').prop('checked', (nl == 1 ? true : false));
                $(modalName + ' #ccBI').prop('checked', (bi == 1 ? true : false));
                $(modalName + ' #ccCF').prop('checked', (cf == 1 ? true : false));
                $(modalName + ' #ccAG').prop('checked', (ag == 1 ? true : false));
                $(modalName + ' #ccBURO').prop('checked', (bu == 1 ? true : false));
                $(modalName + ' #ccCA').prop('checked', (ca == 1 ? true : false));
                $(modalName + ' #ccUSAGER').prop('checked', (usager == 1 ? true : false));

                ClassPP.initGeneric(modalName, 'reception', '#zlReceptionCF', cf_recep);
                ClassPP.initGeneric(modalName, 'reception', '#zlReceptionBI', bi_recep);
                ClassPP.initGeneric(modalName, 'roleag', '#zlRoleAG', ag_role);
                ClassPP.initGeneric(modalName, 'rolebureau', '#zlRoleBURO', bu_role);
                ClassPP.initGeneric(modalName, 'roleca', '#zlRoleCA', ca_role);
                ClassPP.initGeneric(modalName, 'typetel', '#zlTypeTel');
                ClassPP.initGeneric(modalName, 'typemail', '#zlTypeMail');
                ClassPP.initGeneric(modalName, 'etablissements', '#zlEtablissement', etablissement);
                ClassPP.initGeneric(modalName, 'roleusager', '#zlTypeUsager', typeusager);

                /** -------------------------------------------------------------------------------
                 * TRAITEMENT DES COTISATIONS
                 */
                $.each(cotisations, function (index, item) {

                    var hasLignes = item.REGLEMENT.count;
                    var tr;
                    var trStart = '<tr>' + '<td>' + (hasLignes>0 ? item.ANNEE : '<span class="red">' + item.ANNEE + '</span>') + '</td>';
                    $.each(item.REGLEMENT, function (indexr, reglement) {

                        console.log(reglement);

                        tr += trStart + '<td>' + reglement.TYPE_COTISATION+'</td>'+
                            '<td style="text-align:right">' + reglement.MONTANT +'</td>'+
                            '<td>' + reglement.DATE +'</td>'+
                            '<td>' + reglement.TYPE_REGLEMENT +'</td>'+
                            '<td>' + reglement.REFERENCE +'</td></tr>';
                    });
                    if (hasLignes == 0) {
                        tr = trStart + '<td></td>'+
                            '<td></td>'+
                            '<td></td>'+
                            '<td></td>'+
                            '<td></td></tr>';
                    }
                    $(modalName + ' #tableCotisation').append(
                        tr
                    );
                });

                /** -------------------------------------------------------------------------------
                 * Init des zones TELEPHONES + MAILS
                 */
                $.each(data.TEL, function (index, item) {
                    $(modalName + ' #tableTel tbody').append(
                        '<tr>' +
                        '<td data-pk="' + item.OWN_ID + '" ' +
                        '    data-type-tel="' + item.TYPE_ID + '"' +
                        '    data-pp="' + item.PP_ID + '"' +
                        '>' + item.numero +'</td>' +
                        '<td>' + item.libelle +'</td>' +
                        '<td class="text-right">' +
                        '<a class="remove-telephone btn btn-xs red" ' +
                        ' data-placement="left" data-toggle="confirmation" data-original-title="" title="" ' +
                        '><i class="fa fa-remove"></i></a>' +
                        //'<a class="modif btn btn-xs blue"><i class="fa fa-edit"></i></a>' +
                        '</td>' +
                        '</tr>'
                    );
                });
                $.each(data.MAIL, function (index, item) {
                    $(modalName + ' #tableMail tbody').append(
                        '<tr>' +
                        '<tr>' +
                        '<td data-pk="' + item.OWN_ID + '" ' +
                        '    data-type-mail="' + item.TYPE_ID + '"' +
                        '    data-pp="' + item.PP_ID + '"' +
                        '>' + item.mail +'</td>' +
                        '<td>' + item.libelle +'</td>' +
                        '<td class="text-right">' +
                        '<a class="remove-mail btn btn-xs red"' +
                        ' data-placement="left" data-toggle="confirmation" data-original-title="" title="" ' +
                        '><i class="fa fa-remove"></i></a>' +
                        //'<a class="modif btn btn-xs blue"><i class="fa fa-edit"></i></a>' +
                        '</td>' +
                        '</tr>'
                    );
                });
                $('#portletTel .portlet-body').find('.row:first').hide();
                $('#portletMail .portlet-body').find('.row:first').hide();

                /** -------------------------------------------------------------------------------
                 * Modification de l'aspect final du formulaires. Les zones de saisie prennent la
                 * classe edited. Les zones deviennent readonly et les boutons d'action sont
                 * cachés (au pire, comme il n'y a pas d'action, il ne se passerait rien...
                 */
                ClassPP.refreshEdited(modalName);
                ClassPP.doReadOnly(modalName);
            }
        });
    },

    manageActions: function () {

        /** ---------------------------------------------------------------------------------------
         * Anti CLOSE
         */
        $('#dialogExecLev0').on('keypress', 'input', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });

        /** ---------------------------------------------------------------------------------------
         * Ajout d'une PERSONNE PHYSIQUE
         */
        $('#btnAdd').click(function () {
            ClassPP.add();
        });

    },

    manageModalCC : function (modal) {
        $(modal + ' #ccCF').on('click', function () {
            if ($(modal + ' #ccCF').is(':checked')) {
                $(modal + ' #zlReceptionCF').select2('enable');//.enable(true);
            } else {
                $(modal + ' #zlReceptionCF').select2('enable', false);//.enable(true);
            }
        });

        $(modal + ' #ccBI').on('click', function () {
            if ($(modal + ' #ccBI').is(':checked')) {
                $(modal + ' #zlReceptionBI').select2('enable');//.enable(true);
                $(modal + ' #ccNL').prop('checked', true);//.enable(true);

            } else {
                $(modal + ' #zlReceptionBI').select2('enable', false);//.enable(true);
            }
        });

        $(modal + ' #ccAG').on('click', function () {
            if ($(modal + ' #ccAG').is(':checked')) {
                $(modal + ' #zlRoleAG').select2('enable');//.enable(true);
            } else {
                $(modal + ' #zlRoleAG').select2('enable', false);//.enable(true);
            }
        });

        $(modal + ' #ccCA').on('click', function () {
            if ($(modal + ' #ccCA').is(':checked')) {
                $(modal + ' #zlRoleCA').select2('enable');//.enable(true);
            } else {
                $(modal + ' #zlRoleCA').select2('enable', false);//.enable(true);
            }
        });

        $(modal + ' #ccBURO').on('click', function () {
            if ($(modal + ' #ccBURO').is(':checked')) {
                $(modal + ' #zlRoleBURO').select2('enable');//.enable(true);
            } else {
                $(modal + ' #zlRoleBURO').select2('enable', false);//.enable(true);
            }
        });

        $(modal + ' #ccUSAGER').on('click', function () {
            if ($(modal + ' #ccUSAGER').is(':checked')) {
                $(modal + ' #zlEtablissement').select2('enable');//.enable(true);
                $(modal + ' #zlTypeUsager').select2('enable');//.enable(true);
                $(modal + ' #btnCopieData').removeClass("display-hide");
            } else {
                $(modal + ' #zlEtablissement').select2('enable', false);//.enable(true);
                $(modal + ' #zlTypeUsager').select2('enable', false);//.enable(true);
                $(modal + ' #btnCopieData').addClass("display-hide");
            }
        });

        $(modal + ' #btnCopieData').unbind().on('click', function () {
            if ($('#zlEtablissement').val() != '') {
                $.ajax({
                    url: '/annuaire/et/' + $('#zlEtablissement').val(),
                    type: 'GET',
                    data: {
                    },
                    dataType: "json",
                    async: false,
                    success: function (data) {

                        $(modal + ' #ztAdr1').val(data.ET.adr1);
                        $(modal + ' #ztAdr2').val(data.ET.adr2);
                        $(modal + ' #ztAdr3').val(data.ET.adr3);
                        $(modal + ' #ztCP').val(data.ET.cp);
                        $(modal + ' #ztVille').val(data.ET.ville);
                        $(modal + ' #zlPays').val(pays_id).trigger('change');

                        ClassPP.refreshEdited(modalName);

                    }
                }).complete(function (code_html, statut) {
                });
            }
        });
    },

    /** -------------------------------------------------------------------------------------------
     * Boutons d'AJOUT et de SUPPRESSION des TELEPHONES et des MAILS
     */

    phoneList: function (modalName, type, id) {

        /** ---------------------------------------------------------------------------------------
         * Les téléphones et les mails sont toujours dans une dialog99
         */
        var modal = $(modalName);
        modal.html($('#formTel').html());

        $.ajax({
            url: '/datatable/' + type + '/telephones/' + id,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {

                $.each(data.TEL, function (index, item) {
                    var td1 = $('<td/>').html(
                        item.NUM + (item.prefere == 1 ?
                            ' <i class="fa fa-star prefere font-yellow" style="cursor:pointer"> </i>' :
                            ' <i class="fa fa-star-o prefere" style="cursor:pointer"> </i>')
                    )
                        .attr('data-pk', item.OWN_ID)
                        .attr('data-type', type)
                        .attr('data-type-telephone', item.TYPE_ID)
                        .attr('data-id', item.ID)
                        .attr('data-telephone', item.NUM);
                    var td2 = $('<td/>').html(
                        item.libelle
                    );
                    var td3 = $('<td/>')
                        .addClass('text-right')
                        .html(
                            $('<a/>')
                                .attr('href', '#')
                                .addClass('editable')
                                .addClass('edit-telephone')
                                .attr('data-type', 'text')
                                .attr('data-id', item.OWN_ID)
                                .attr('data-title', 'Saisir les notes')
                                .attr('data-value', item.notes)
                                .attr('data-placement', 'right')
                                .attr('data-mode', 'inline')
                                .css('display', 'inline')
                        );
                    var td4 = $('<td/>')
                        .addClass('text-right')
                        .html(
                            $('<a/>')
                                .addClass('remove-telephone')
                                .addClass('btn')
                                .addClass('btn-xs')
                                .addClass('red')
                                .html(
                                    $('<i/>')
                                        .addClass('fa')
                                        .addClass('fa-remove')
                                )
                        );
                    $(modalName + ' #tableTel tbody').append(
                        $('<tr/>')
                            .append(
                                td1
                            ).append(
                            td2
                        ).append(
                            td3
                        ).append(
                            td4
                        )
                    );
                });
                ClassPP.initGeneric(modalName, 'typetel', '#zlTypeSaisie');
                ClassPP.manageModalButtons(modalName, 1, type, id);


                /** -------------------------------------------------------------------------------
                 * Custom des boutons / défautzlTypeTel
                 *
                 * @type {string}
                 */
                $.fn.editableform.buttons =
                    '<button class="btn btn-xs green editable-submit" type="submit"><i class="fa fa-check"></i></button>' +
                    '<button class="btn btn-xs default editable-cancel" type="button"><i class="fa fa-times"></i></button>'
                ;
                ClassPP.updateEditableTels(modalName);

                $(modalName).find('.modal-header').addClass('font-green-seagreen ');
                $(modalName).find('.modal-title').html('<i class="fa fa-phone"></i> Gestion des Téléphones / Fax');
                $(modalName).modal('show');

            }
        });

        ClassPP.updateRemoveButton(modalName, 'telephone', type);
        ClassPP.updatePrefereButton(modalName, 'telephone', type);
    },

    mailList: function (modalName, type, id) {

        /** ---------------------------------------------------------------------------------------
         * Les téléphones et les mails sont toujours dans une dialog99
         */
        var modal = $(modalName);
        modal.html($('#formMail').html());

        $.ajax({
            url: '/datatable/' + type + '/mails/' + id,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {

                $.each(data.MAIL, function (index, item) {
                    var td1 = $('<td/>').html(
                        item.mail + (item.prefere == 1 ?
                            ' <i class="fa fa-star prefere font-yellow" style="cursor:pointer"> </i>' :
                            ' <i class="fa fa-star-o prefere" style="cursor:pointer"> </i>')
                    )
                        .attr('data-pk', item.OWN_ID)
                        .attr('data-type', type)
                        .attr('data-type-mail', item.TYPE_ID)
                        .attr('data-id', item.ID)
                        .attr('data-mail', item.mail);
                    var td2 = $('<td/>').html(
                        item.libelle
                    );
                    var td3 = $('<td/>')
                        .addClass('text-right')
                        .html(
                            $('<a/>')
                                .attr('href', '#')
                                .addClass('editable')
                                .addClass('edit-mail')
                                .attr('data-type', 'text')
                                .attr('data-id', item.OWN_ID)
                                .attr('data-title', 'Saisir les notes')
                                .attr('data-value', item.notes)
                                .attr('data-placement', 'right')
                                .attr('data-mode', 'inline')
                                .css('display', 'inline')
                        );
                    var td4 = $('<td/>')
                        .addClass('text-right')
                        .html(
                            $('<a/>')
                                .addClass('remove-mail')
                                .addClass('btn')
                                .addClass('btn-xs')
                                .addClass('red')
                                .html(
                                    $('<i/>')
                                        .addClass('fa')
                                        .addClass('fa-remove')
                                )
                        );
                    $(modalName + ' #tableMail tbody').append(
                        $('<tr/>')
                            .append(
                                td1
                            ).append(
                            td2
                        ).append(
                            td3
                        ).append(
                            td4
                        )
                    );
                });
                ClassPP.initGeneric(modalName, 'typemail', '#zlTypeSaisie');
                ClassPP.manageModalButtons(modalName, 1, type, id);


                /** -------------------------------------------------------------------------------
                 * Custom des boutons / défautzlTypeTel
                 *
                 * @type {string}
                 */
                $.fn.editableform.buttons =
                    '<button class="btn btn-xs green editable-submit" type="submit"><i class="fa fa-check"></i></button>' +
                    '<button class="btn btn-xs default editable-cancel" type="button"><i class="fa fa-times"></i></button>'
                ;
                ClassPP.updateEditableMails();

                $(modalName).find('.modal-header').addClass('font-green-seagreen ');
                $(modalName).find('.modal-title').html('<i class="fa fa-send"></i> Gestion des Mails');
                $(modalName).modal('show');

            }
        });

        ClassPP.updateRemoveButton(modalName, 'mail');
        ClassPP.updatePrefereButton(modalName, 'mail', type);
    },

    manageModalButtons: function (modalName, add, type, id) {

        var addTel = function () {

            /** -----------------------------------------------------------------------------------
             * Vérification des ZONES Mandatory
             */
            var saisie = $(modalName + ' #portletTel #ztSaisie').val().trim().toUpperCase();
            var typesaisie = $(modalName + ' #portletTel #zlTypeSaisie').select2('val');
            var typesaisietxt = $(modalName + ' #portletTel #zlTypeSaisie').select2('data');
            saisie.replace(' ', '');
            typesaisietxt = typesaisietxt[0].text;
            if (saisie == '' || typesaisie == '') {
                toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                return;
            }

            /** -----------------------------------------------------------------------------------
             * On vérifie la non existence dans la table
             */
            var stop = false;
            $(modalName + ' #tableTel tbody tr td:first-child').each(function () {
                if ($(this).attr('data-telephone') == saisie) {
                    stop = true;
                }
            });
            if (stop) {
                return false;
            }

            /** ---------------------------------------------------------------------------------------
             * Anti-rebond
             */
            $(modalName + ' #btnAddTel').off();

            /** -----------------------------------------------------------------------------------
             * On ajoute simplement dans la liste
             * Attention, si on est en mode ADD, on a 4 colonnes, sinon, on en n'a que 3
             */
            var pk = undefined;
            if (id != undefined) {
                $.ajax({
                    url: '/annuaire/telephone',
                    type: 'POST',
                    data: {
                        _token: $(modalName + ' #token').val(),
                        pNum: saisie,
                        pTypeTel: typesaisie,
                        pType: type,
                        pID: id
                    },
                    dataType: "json",
                    async: false
                }).success(function (data) {
                    pk = data.ID;
                });
            }


            var td1 = $('<td/>')
                .attr('data-id', id)
                .attr('data-type', type)
                .attr('data-telephone', saisie)
                .attr('data-type-telephone', typesaisie)
                .attr('data-prefere', 0)
                .html(saisie + ' ' +
                    $('<i/>')
                        .addClass('fa')
                        .addClass('fa-star-o')
                        .addClass('prefere')
                        .css('cursor', 'pointer')
                        .wrap("<div/>")
                        .parent().html()
                );
            if (pk != undefined) {
                td1.attr('data-pk', pk);
            }
            var td2 = $('<td/>').html(typesaisietxt);
            var td3 = $('<td/>');
            if (add) {
                td3.addClass('text-right').append(
                    $('<a/>').attr('href', '#')
                        .addClass('editable edit-telephone')
                        .attr('data-type', 'text')
                        .attr('data-id', pk)
                        .attr('data-title', 'Saisir les notes')
                        .attr('data-placement', 'right')
                        .attr('data-mode', 'inline')
                )
            }
            var td4 = $('<td/>')
                .addClass('text-right')
                .html(
                    $('<a/>').addClass('remove-telephone btn btn-xs red')
                        .attr('data-placement', 'left')
                        .attr('data-toggle', 'confirmation')
                        .html(
                            $('<i/>').addClass('fa fa-remove')
                        )
                );
            $(modalName + ' #tableTel tbody').append(
                $('<tr/>')
                    .append(td1)
                    .append(td2)
                    .append((add ? td3 : ''))
                    .append(td4)
            );

            $(modalName + ' #portletTel #ztSaisie').val('');
            $(modalName + ' #portletTel #zlTypeSaisie').select2('val', 0);
            ClassPP.editGeneric('telephone', 'telephone', modalName);
            $(modalName + ' #btnAddTel').unbind().on('click', addTel);

            /** -----------------------------------------------------------------------------------
             * Un petit rajout à cause du DOM
             */
            ClassPP.updateRemoveButton(modalName, 'telephone');
            ClassPP.updatePrefereButton(modalName, 'telephone', type);

        };

        var addMail = function () {

            /** -----------------------------------------------------------------------------------
             * Vérification des ZONES Mandatory
             */
            var saisie = $(modalName + ' #portletMail #ztSaisie').val().trim();
            var typesaisie = $(modalName + ' #portletMail #zlTypeSaisie').select2('val');
            var typesaisietxt = $(modalName + ' #portletMail #zlTypeSaisie').select2('data');
            if (!ClassPP.validMail(saisie)) {
                toastr.warning('Cette adresse mail n\'est pas valide.', "Attention");
                return;
            }

            typesaisietxt = typesaisietxt[0].text;
            if (saisie == '' || typesaisie == '') {
                toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                return;
            }

            /** -----------------------------------------------------------------------------------
             * On vérifie la non existence dans la table
             */
            var stop = false;
            $(modalName + ' #tableMail tbody tr td:first-child').each(function () {
                if ($(this).attr('data-mail') == saisie) {
                    stop = true;
                }
            });
            if (stop) {
                return false;
            }

            /** ---------------------------------------------------------------------------------------
             * Anti-rebond
             */
            $(modalName + ' #btnAddMail').off();

            /** -----------------------------------------------------------------------------------
             * On ajoute simplement dans la liste
             * Attention, si on est en mode ADD, on a 4 colonnes, sinon, on en n'a que 3
             */
            var pk = undefined;
            if (id != undefined) {
                $.ajax({
                    url: '/annuaire/mail',
                    type: 'POST',
                    data: {
                        _token: $(modalName + ' #token').val(),
                        pMail: saisie,
                        pTypeMail: typesaisie,
                        pType: type,
                        pID: id
                    },
                    dataType: "json",
                    async: false
                }).success(function (data) {
                    pk = data.ID;
                });
            }

            var td1 = $('<td/>')
                .attr('data-id', id)
                .attr('data-type', type)
                .attr('data-mail', saisie)
                .attr('data-type-mail', typesaisie)
                .attr('data-prefere', 0)
                .html(saisie + ' ' +
                    $('<i/>')
                        .addClass('fa')
                        .addClass('fa-star-o')
                        .addClass('prefere')
                        .css('cursor', 'pointer')
                        .wrap('<div/>')
                        .parent().html()
                );
            if (pk != undefined) {
                td1.attr('data-pk', pk);
            }
            var td2 = $('<td/>').html(typesaisietxt);
            var td3 = $('<td/>');
            if (add) {
                td3.addClass('text-right').append(
                    $('<a/>').attr('href', '#')
                        .addClass('editable edit-mail')
                        .attr('data-type', 'text')
                        .attr('data-id', pk)
                        .attr('data-title', 'Saisir les notes')
                        .attr('data-placement', 'right')
                        .attr('data-mode', 'inline')
                )
            }
            var td4 = $('<td/>')
                .addClass('text-right')
                .html(
                    $('<a/>').addClass('remove-mail btn btn-xs red')
                        .attr('data-placement', 'left')
                        .attr('data-toggle', 'confirmation')
                        .html(
                            $('<i/>').addClass('fa fa-remove')
                        )
                );
            $(modalName + ' #tableMail tbody').append(
                $('<tr/>')
                    .append(td1)
                    .append(td2)
                    .append((add ? td3 : ''))
                    .append(td4)
            );

            $(modalName + ' #portletMail #ztSaisie').val('');
            $(modalName + ' #portletMail #zlTypeSaisie').select2('val', 0);
            ClassPP.editGeneric('mail', 'mails', modalName);
            $(modalName + ' #btnAddMail').unbind().on('click', addMail);

            /** -----------------------------------------------------------------------------------
             * Un petit rajout à cause du DOM
             */
            ClassPP.updateRemoveButton(modalName, 'mail');
            ClassPP.updatePrefereButton(modalName, 'mail', type);

        };

        var addModalite = function() {

            var id_mod;
            var type_id = $(modalName + ' #zlTypeEtabl').val();
            if (id == '') {
                toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                return false;
            }

            var txt = $(modalName + ' #zlTypeEtabl').select2('data')[0].text;
            var isSMS = 0;
            if ($(modalName + ' #zlSecteur').val() != '') {
                isSMS = $(modalName + ' #zlSecteur').find('option:selected').attr('data-ms');
            }
            else {
                isSMS = 0;
            }


            /** -----------------------------------------------------------------------------------
             * On va désactiver les zones SECTEUR + SECTEUR MS + HANDICAP + TRANCHE AGE
             */
            $(modalName + ' #zlSecteur').prop('disabled', true);
            $(modalName + ' #zlSecteurSMS').prop('disabled', true);
            $(modalName + ' #ccHANDICAP').prop('disabled', true);
            $(modalName + ' #zlTypeAge').prop('disabled', true);

            /** -----------------------------------------------------------------------------------
             * Si le portlet data-id (avec id = zlTypeEtabl) dans
             */
            var trouve = false;
            $(modalName + " #divDetail div[data-type-id='" + type_id + "']").each(function() {
                trouve = true;
            });
            if (!trouve) {

                /** -------------------------------------------------------------------------------
                 * Lors de la création d'un PORTLET, en mode ADD, on l'ajoute directement
                 * dans la base ! A ce moment là, on devra changer
                 */
                if (add == 1) {
                    $.ajax({
                        url: '/annuaire/etablissement_modalites',
                        type: 'POST',
                        data: {
                            _token: $(modalName + ' #token').val(),
                            pID_ET: id,
                            pTypeID : type_id
                        },
                        dataType: "json",
                        async: false
                    }).success(function (data) {
                        id_mod = data;
                    });
                } else {
                    id_mod = 0;
                }

                /** -------------------------------------------------------------------------------
                 * Attention, on convertit le ADD en VIEW
                 */
                var portlet = ClassPP.createPortlet(id_mod, txt, (add ? 0 : 1), type_id);
                $(modalName + ' #divDetail').append(
                    $('<div/>', {
                        'class':'col-md-12'
                    }).append(
                        portlet));

                $(modalName + ' #divDetail .del-portlet').confirmation({
                    btnOkLabel: 'OUI',
                    btnOkClass: 'red-mint btn-xs',
                    btnCancelLabel: 'NON',
                    placement: 'left',
                    title: 'Supression ?',
                    singleton: true,
                    popout: true,
                    onConfirm: function () {
                        $(this).parent().parent().parent().remove();

                        /** -----------------------------------------------------------------------
                         * Si le détail est tout vide
                         */
                        if($('#divDetail').html().trim() == '<div class="col-md-12"></div>') {
                            $(modalName + ' #zlSecteur').prop('disabled', false);
                            $(modalName + ' #zlSecteur').trigger('change');
                            $(modalName + ' #zlTypeAge').prop('disabled', false);
                            $(modalName + ' #ccHANDICAP').prop('disabled', false);
                        }
                    }
                });

                /** -------------------------------------------------------------------------------
                 * Pour terminer, on initialise le contenu des zones
                 */
                var cible = modalName + ' #divDetail div[data-id="' + id_mod + '"] ';
                ClassPP.initSpecif(cible, '#zlModaliteAccueil', '', isSMS, 'modalites');
                ClassPP.initGeneric(cible, 'typepublic', '#zlTypePublic');
                $(cible + ' #zlDetailTypePublic').select2().prop('disabled', true);

                /** -------------------------------------------------------------------------------
                 * On bind le CHANGE
                 */
                ClassET.bindZL_portlet(modalName, cible, id_mod);

            }
        };

        var addOffre = function() {

            /** -----------------------------------------------------------------------------------
             * Pour ajouter une offre, on doit ajouter ANNEE + NOMBRE + DATE SAISIE
             * + (éventuellement) NOTES
             */
            var annee = $(modalName + ' #ztAnnee').val();
            var nb = $(modalName + ' #ztNb').val();
            var date = $(modalName + ' #ztDate').val();
            var notes = $('<p>' + $(modalName + ' #ztNotes').val() + '</p>').text();

            if (annee == '' || nb == '' || date == '') {
                toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                return false;
            }

            /** -------------------------------------------------------------------------------
             * Lors de la création d'un PORTLET, en mode ADD, on l'ajoute directement
             * dans la base ! A ce moment là, on devra changer
             */
            if (add == 1) {
                $.ajax({
                    url: '/annuaire/offre',
                    type: 'POST',
                    data: {
                        _token: $(modalName + ' #token').val(),
                        pAnnee: annee,
                        pNb : nb,
                        pDate : date,
                        pNotes : notes,
                        pType : type,
                        pId : id,
                    },
                    dataType: "json",
                    async: false
                }).success(function (data) {
                    var td1 = $('<td/>').html(
                        annee)
                        .attr('data-pk', data.ID);
                    var td2 = $('<td/>')
                        .addClass('text-right')
                        .html(
                            nb
                        );
                    var td3 = $('<td/>')
                        .addClass('text-right')
                        .html(
                            date
                        );
                    var td4 = $('<td/>')
                        .addClass('text-right')
                        .html(
                            notes
                        );
                    var td5 = $('<td/>')
                        .addClass('text-right')
                        .html(
                            $('<a/>')
                                .addClass('remove-offre')
                                .addClass('btn')
                                .addClass('btn-xs')
                                .addClass('red')
                                .html(
                                    $('<i/>')
                                        .addClass('fa')
                                        .addClass('fa-remove')
                                )
                        );
                    $(modalName + ' #tableOffre tbody').append(
                        $('<tr/>')
                            .append(
                                td1
                            ).append(
                            td2
                        ).append(
                            td3
                        ).append(
                            td4
                        ).append(
                            td5
                        )
                    );
                    ClassPP.updateRemoveButton(modalName, 'offre');
                });
            }

            $(modalName + ' #divDetail .del-portlet').confirmation({
                btnOkLabel: 'OUI',
                btnOkClass: 'red-mint btn-xs',
                btnCancelLabel: 'NON',
                placement: 'left',
                title: 'Supression ?',
                singleton: true,
                popout: true,
                onConfirm: function () {
                    $(this).parent().parent().parent().remove();

                    /** -----------------------------------------------------------------------
                     * Si le détail est tout vide
                     */
                    if($('#divDetail').html().trim() == '<div class="col-md-12"></div>') {
                        $(modalName + ' #zlSecteur').prop('disabled', false);
                        $(modalName + ' #zlSecteur').trigger('change');
                        $(modalName + ' #zlTypeAge').prop('disabled', false);
                        $(modalName + ' #ccHANDICAP').prop('disabled', false);
                    }
                }
            });
        };

        /** ---------------------------------------------------------------------------------------
         * Si on est en mode AJOUT, la variable oo_id est renseignée, donc on rajoute directement
         * les infos dans la base
         */
        $(modalName + ' #btnAddTel').unbind().on('click', addTel);
        $(modalName + ' #btnAddMail').unbind().on('click', addMail);
        $(modalName + ' #btnAddModalite').unbind().on('click', addModalite);
        $(modalName + ' #btnAddOffre').unbind().on('click', addOffre);
    },

    updateEditableTels: function (modalName) {
        $('.edit-telephone').editable({
            emptytext: 'notes',
            validate: function (value) {

                if (value != null) {
                    $('.editable-submit').hide();
                    $('.editable-cancel').hide();

                    $.ajax({
                        url: '/annuaire/telephone/' + $(this).attr('data-id'),
                        type: 'PUT',
                        data: {
                            _token: $(modalName + ' #token').val(),
                            pValue: value
                        },
                        dataType: "json",
                        async: false
                    }).complete(function (code_html, statut) {
                    });

                }
            }
        });
    },

    updateEditableMails: function () {
        $('.edit-mail').editable({
            emptytext: 'notes',
            validate: function (value) {

                if (value != null) {
                    $('.editable-submit').hide();
                    $('.editable-cancel').hide();

                    $.ajax({
                        url: '/annuaire/mail/' + $(this).attr('data-id'),
                        type: 'PUT',
                        data: {
                            _token: $(modalName + ' #token').val(),
                            pValue: value
                        },
                        dataType: "json",
                        async: false
                    }).complete(function (code_html, statut) {
                    });

                }
            }
        });
    },

    updateRemoveButton: function (modalName, type) {

        $(modalName + ' .remove-' + type).confirmation({
            btnOkLabel: 'OUI',
            btnOkClass: 'red-mint btn-xs',
            btnCancelLabel: 'NON',
            placement: 'left',
            title: 'Supression ?',
            singleton: true,
            popout: true,
            onConfirm: function () {
                var pk = $(this).closest('tr').children('td:first').attr('data-pk');
                if (pk != undefined) {
                    $.ajax({
                        url: '/annuaire/' + type + '/' + pk,
                        type: 'DELETE',
                        dataType: 'json',
                        data: {
                            _token: $(modalName + ' #token').val(),
                        },
                        async: false
                    }).complete(function (code_html, statut) {
                    });
                } else {

                }
                $(this).closest('tr').remove();
            }
        });
    },

    updatePrefereButton: function (modalName, route, type) {

        $(modalName + ' .prefere').unbind().on('click', function () {

            var td = $(this).parent();
            var table = td.parent().parent();
            var type = td.attr('data-type');
            var thisid = td.attr('data-pk');

            /** -----------------------------------------------------------------------------------
             * On récupère la liste des TR pour faire le ménage. Ici, le ménage est purement
             * graphique
             */
            $.each(table.find('tr'), function (index, tr) {
                $(tr).find('i.fa').removeClass('fa-star').removeClass('font-yellow').addClass('fa-star-o');
                $(tr).find('td:eq(0)').attr('data-prefere', 0);
            });

            /** -----------------------------------------------------------------------------------
             * Et celui-ci devient preferable avec un aspect
             */
            td.find('i.fa').removeClass('fa-star-o').addClass('fa-star').addClass('font-yellow');
            td.attr('data-prefere', 1);
            if (thisid != undefined) {
                $.ajax({
                    url: '/annuaire/' + route + '/' + thisid + '/edit',
                    type: 'GET',
                    data: {
                        _token: $(modalName + ' #token').val(),
                        pId: thisid,
                        pPrefere: 1,
                        pType: type
                    },
                    dataType: "json",
                    async: false
                }).complete(function (code_html, statut) {
                });
            }
        });

    },

    refreshEdited: function (modalName) {
        $(modalName).find('.form-md-floating-label').children('.form-control').each(function () {
            if ($(this).val().length > 0) {
                $(this).addClass('edited');
            }
        });
    },

    displayTels: function (modalName, tels) {
        $.each(tels, function (index, item) {
            $(modalName + ' #tableTel tbody').append(
                '<tr>' +
                '<td data-pk="' + item.OWN_ID + '" ' +
                '    data-type-telephone="' + item.TYPE_ID + '"' +
                '    data-id="' + item.ID + '"' +
                '    data-type="' + 'oo' + '"' +
                '    data-prefere="' + item.prefere + '"' +
                '>' + item.numero +
                (item.prefere == 1
                        ? ' <i class="fa fa-star prefere font-yellow" style="cursor:pointer"> </i>'
                        : ' <i class="fa fa-star-o prefere" style="cursor:pointer"> </i>'
                ) +
                '</td>' +
                '<td>' + item.libelle + '</td>' +
                '<td class="text-right">' +
                '<a class="remove-telephone btn btn-xs red" ' +
                ' data-placement="left" data-toggle="confirmation" data-original-title="" title="" ' +
                '><i class="fa fa-remove"></i></a>' +
                //'<a class="modif btn btn-xs blue"><i class="fa fa-edit"></i></a>' +
                '</td>' +
                '</tr>'
            );
        });
    },

    displayMails: function (modalName, mails) {
        $.each(mails, function (index, item) {
            $(modalName + ' #tableMail tbody').append(
                '<tr>' +
                '<tr>' +
                '<td data-pk="' + item.OWN_ID + '" ' +
                '    data-type-mail="' + item.TYPE_ID + '"' +
                '    data-id="' + item.ID + '"' +
                '    data-type="' + 'oo' + '"' +
                '>' + item.mail +
                (item.prefere == 1
                        ? ' <i class="fa fa-star prefere font-yellow" style="cursor:pointer"> </i>'
                        : ' <i class="fa fa-star-o prefere" style="cursor:pointer"> </i>'
                ) +
                '</td>' +
                '<td>' + item.libelle + '</td>' +
                '<td class="text-right">' +
                '<a class="remove-mail btn btn-xs red"' +
                ' data-placement="left" data-toggle="confirmation" data-original-title="" title="" ' +
                '><i class="fa fa-remove"></i></a>' +
                //'<a class="modif btn btn-xs blue"><i class="fa fa-edit"></i></a>' +
                '</td>' +
                '</tr>'
            );
        });
    },

    validMail: function (mail) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(mail);
    },

}