/** -----------------------------------------------------------------------------------------------
 *
 * @author : Vincent BENNER
 * @copyright : Page Up 2016
 * @version : 1.0.0
 *
 * @history : 1.0.0 - Release
 * @description : - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *              : Gestion des ORGANISMES
 *              : Gestion des POLES
 *              : Gestion des ETABLISSEMENTS
 *              : Gestion des ANTENNES
 *              : - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *              : Gestion des ADRESSES D'ACCUEIL
 *              : Gestion des PERSONNELS
 *
 */

/** -----------------------------------------------------------------------------------------------
 * Données de base
 */
var tableNameOrga = 'tableOrga';
var tableNamePole = 'tablePole';
var tableNameEtabl = 'tableEtabl';
var tableNameAdr = 'tableAdr';
var tableNameAntenne = 'tableAntenne';
var tableNamePersonnel = 'tablePersonnel';
var tableFiltre;
var tableNameFiltre = 'tableFiltre';
var tableOrga;
var tablePole;
var tableEtabl;
var tableAdr;
var tableAntenne;
var tablePersonnel;
var routeFiltreOO = 'filtreoo'
var routeOrga = 'oo';
var routePole = 'po';
var routeEtabl = 'et';
var routeAdr = 'aa';
var routeAntenne = 'an'
var routePersonnel = 'pe';
var edit = ' d\'un Organisme';
var editPole = ' d\'un Pole';
var editEtabl = ' d\'un Établissement / Service';
var editAntenne = ' d\'une Antenne';
var editAdr = ' d\'une Adresse d\'accueil';
var editPersonnel = ' d\'un Personnel';

var ariane = [];
var mode;
const MODE_DATATABLES = 1;
const MODE_FORM = 2;

const MODE_AJOUT = 1;
const MODE_MODIF = 2;
const MODE_VISU = 3;


/** -----------------------------------------------------------------------------------------------
 * JQuery start
 */
$(function () {

    /** -------------------------------------------------------------------------------------------
     * Problème de Focus dans les listes
     * https://github.com/select2/select2/issues/1436
     */
    $.fn.modal.Constructor.prototype.enforceFocus = function () {
    };

    /** -------------------------------------------------------------------------------------------
     *
     */
    ClassOO.init();
    ClassOO.initDataTable();
    ClassOO.manageActions();
    ClassOO.manageFilters();

});

/** -----------------------------------------------------------------------------------------------
 * @class : GENERIC
 */
var ClassGENERIC = {

    buildAriane : function (type, id) {
        $.ajax({
            url: '/activite/filter/ariane',
            type: 'POST',
            data: {
                pType: type,
                pId: id,
            },
            dataType: "json",
            async: false
        }).success(function (data) {
            ariane = [];
            $.each(data, function (id, item) {
                ariane.push(item);
            });
        });
    },

    verifPresenceMail : function(modalName, bi, cf) {
        var retour = true;
        var checkMail = 0;
        if (bi == 1) {
            checkMail = $(modalName + " #zlReceptionBI").select2().find(":selected").data("check");
        }
        if (cf == 1) {
            checkMail = checkMail || $(modalName + " #zlReceptionCF").select2().find(":selected").data("check");
        }
        if (checkMail == 1) {
            var rowCount = $(modalName + '#tableMail tr').length;
            if (rowCount == 0) {
                retour = false;
            }
        }
        return retour;
    },

    displayTels: function (modalName, tels) {
        $.each(tels, function (index, item) {
            $(modalName + ' #tableTel tbody').append(
                '<tr>' +
                '<td data-pk="' + item.OWN_ID + '" ' +
                '    data-type-telephone="' + item.TYPE_ID + '"' +
                '    data-id="' + item.ID + '"' +
                '    data-type="' + 'oo' + '"' +
                '    data-prefere="' + item.prefere + '"' +
                '>' + item.numero +
                (item.prefere == 1
                        ? ' <i class="fa fa-star prefere font-yellow" style="cursor:pointer"> </i>'
                        : ' <i class="fa fa-star-o prefere" style="cursor:pointer"> </i>'
                ) +
                '</td>' +
                '<td>' + item.libelle + '</td>' +
                '<td class="text-right">' +
                '<a class="remove-telephone btn btn-xs red" ' +
                ' data-placement="left" data-toggle="confirmation" data-original-title="" title="" ' +
                '><i class="fa fa-remove"></i></a>' +
                //'<a class="modif btn btn-xs blue"><i class="fa fa-edit"></i></a>' +
                '</td>' +
                '</tr>'
            );
        });
    },

    displayMails: function (modalName, mails) {
        $.each(mails, function (index, item) {
            $(modalName + ' #tableMail tbody').append(
                '<tr>' +
                '<tr>' +
                '<td data-pk="' + item.OWN_ID + '" ' +
                '    data-type-mail="' + item.TYPE_ID + '"' +
                '    data-id="' + item.ID + '"' +
                '    data-type="' + 'oo' + '"' +
                '>' + item.mail +
                (item.prefere == 1
                        ? ' <i class="fa fa-star prefere font-yellow" style="cursor:pointer"> </i>'
                        : ' <i class="fa fa-star-o prefere" style="cursor:pointer"> </i>'
                ) +
                '</td>' +
                '<td>' + item.libelle + '</td>' +
                '<td class="text-right">' +
                '<a class="remove-mail btn btn-xs red"' +
                ' data-placement="left" data-toggle="confirmation" data-original-title="" title="" ' +
                '><i class="fa fa-remove"></i></a>' +
                //'<a class="modif btn btn-xs blue"><i class="fa fa-edit"></i></a>' +
                '</td>' +
                '</tr>'
            );
        });
    },

    updateRemoveButton: function (modalName, type) {

        //console.log('modalName' + modalName + ' - type = ' + type);
        //return;

        $(modalName + ' .remove-' + type).confirmation({
            btnOkLabel: 'OUI',
            btnOkClass: 'red-mint btn-xs',
            btnCancelLabel: 'NON',
            placement: 'left',
            title: 'Suppression ?',
            singleton: true,
            popout: true,
            onConfirm: function () {
                var pk = $(this).closest('tr').children('td:first').attr('data-pk');
                if (pk != undefined) {
                    $.ajax({
                        url: '/annuaire/' + type + '/' + pk,
                        type: 'DELETE',
                        dataType: 'json',
                        data: {
                            _token: $(modalName + ' #token').val(),
                        },
                        async: false
                    }).complete(function (code_html, statut) {
                    });
                } else {

                }
                $(this).closest('tr').remove();
            }
        });
    },

    updatePrefereButton: function (modalName, route, type) {

        $(modalName + ' .prefere').unbind().on('click', function () {

            var td = $(this).parent();
            var table = td.parent().parent();
            var type = td.attr('data-type');
            var thisid = td.attr('data-pk');

            /** -----------------------------------------------------------------------------------
             * On récupère la liste des TR pour faire le ménage. Ici, le ménage est purement
             * graphique
             */
            $.each(table.find('tr'), function (index, tr) {
                $(tr).find('i.fa').removeClass('fa-star').removeClass('font-yellow').addClass('fa-star-o');
                $(tr).find('td:eq(0)').attr('data-prefere', 0);
            });

            /** -----------------------------------------------------------------------------------
             * Et celui-ci devient preferable avec un aspect
             */
            td.find('i.fa').removeClass('fa-star-o').addClass('fa-star').addClass('font-yellow');
            td.attr('data-prefere', 1);
            if (thisid != undefined) {
                $.ajax({
                    url: '/annuaire/' + route + '/' + thisid + '/edit',
                    type: 'GET',
                    data: {
                        _token: $(modalName + ' #token').val(),
                        pId: thisid,
                        pPrefere: 1,
                        pType: type
                    },
                    dataType: "json",
                    async: false
                }).complete(function (code_html, statut) {
                });
            }
        });

    },

    manageModalButtons: function (modalName, add, type, id) {

        var addTel = function () {

            /** -----------------------------------------------------------------------------------
             * Vérification des ZONES Mandatory
             */
            var saisie = $(modalName + ' #portletTel #ztSaisie').val().trim().toUpperCase();
            var typesaisie = $(modalName + ' #portletTel #zlTypeSaisie').select2('val');
            var typesaisietxt = $(modalName + ' #portletTel #zlTypeSaisie').select2('data');
            saisie.replace(' ', '');
            typesaisietxt = typesaisietxt[0].text;
            if (saisie == '' || typesaisie == '') {
                toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                return;
            }

            /** -----------------------------------------------------------------------------------
             * On vérifie la non existence dans la table
             */
            var stop = false;
            $(modalName + ' #tableTel tbody tr td:first-child').each(function () {
                if ($(this).attr('data-telephone') == saisie) {
                    stop = true;
                }
            });
            if (stop) {
                return false;
            }

            /** ---------------------------------------------------------------------------------------
             * Anti-rebond
             */
            $(modalName + ' #btnAddTel').off();

            /** -----------------------------------------------------------------------------------
             * On ajoute simplement dans la liste
             * Attention, si on est en mode ADD, on a 4 colonnes, sinon, on en n'a que 3
             */
            var pk = undefined;
            if (id != undefined) {
                $.ajax({
                    url: '/annuaire/telephone',
                    type: 'POST',
                    data: {
                        _token: $(modalName + ' #token').val(),
                        pNum: saisie,
                        pTypeTel: typesaisie,
                        pType: type,
                        pID: id
                    },
                    dataType: "json",
                    async: false
                }).success(function (data) {
                    pk = data.ID;
                });
            }


            var td1 = $('<td/>')
                .attr('data-id', id)
                .attr('data-type', type)
                .attr('data-telephone', saisie)
                .attr('data-type-telephone', typesaisie)
                .attr('data-prefere', 0)
                .html(saisie + ' ' +
                    $('<i/>')
                        .addClass('fa')
                        .addClass('fa-star-o')
                        .addClass('prefere')
                        .css('cursor', 'pointer')
                        .wrap("<div/>")
                        .parent().html()
                );
            if (pk != undefined) {
                td1.attr('data-pk', pk);
            }
            var td2 = $('<td/>').html(typesaisietxt);
            var td3 = $('<td/>');
            if (add) {
                td3.addClass('text-right').append(
                    $('<a/>').attr('href', '#')
                        .addClass('editable edit-telephone')
                        .attr('data-type', 'text')
                        .attr('data-id', pk)
                        .attr('data-title', 'Saisir les notes')
                        .attr('data-placement', 'right')
                        .attr('data-mode', 'inline')
                )
            }
            var td4 = $('<td/>')
                .addClass('text-right')
                .html(
                    $('<a/>').addClass('remove-telephone btn btn-xs red')
                        .attr('data-placement', 'left')
                        .attr('data-toggle', 'confirmation')
                        .html(
                            $('<i/>').addClass('fa fa-remove')
                        )
                );
            $(modalName + ' #tableTel tbody').append(
                $('<tr/>')
                    .append(td1)
                    .append(td2)
                    .append((add ? td3 : ''))
                    .append(td4)
            );

            $(modalName + ' #portletTel #ztSaisie').val('');
            $(modalName + ' #portletTel #zlTypeSaisie').select2('val', 0);
            ClassGENERIC.editGeneric('telephone', 'telephone', modalName);
            $(modalName + ' #btnAddTel').unbind().on('click', addTel);

            /** -----------------------------------------------------------------------------------
             * Un petit rajout à cause du DOM
             */
            ClassGENERIC.updateRemoveButton(modalName, 'telephone');
            ClassGENERIC.updatePrefereButton(modalName, 'telephone', type);

        };

        var addMail = function () {

            /** -----------------------------------------------------------------------------------
             * Vérification des ZONES Mandatory
             */
            var saisie = $(modalName + ' #portletMail #ztSaisie').val().trim();
            var typesaisie = $(modalName + ' #portletMail #zlTypeSaisie').select2('val');
            var typesaisietxt = $(modalName + ' #portletMail #zlTypeSaisie').select2('data');
            if (!ClassGENERIC.validMail(saisie)) {
                toastr.warning('Cette adresse mail n\'est pas valide.', "Attention");
                return;
            }

            typesaisietxt = typesaisietxt[0].text;
            if (saisie == '' || typesaisie == '') {
                toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                return;
            }

            /** -----------------------------------------------------------------------------------
             * On vérifie la non existence dans la table
             */
            var stop = false;
            $(modalName + ' #tableMail tbody tr td:first-child').each(function () {
                if ($(this).attr('data-mail') == saisie) {
                    stop = true;
                }
            });
            if (stop) {
                return false;
            }

            /** ---------------------------------------------------------------------------------------
             * Anti-rebond
             */
            $(modalName + ' #btnAddMail').off();

            /** -----------------------------------------------------------------------------------
             * On ajoute simplement dans la liste
             * Attention, si on est en mode ADD, on a 4 colonnes, sinon, on en n'a que 3
             */
            var pk = undefined;
            if (id != undefined) {
                $.ajax({
                    url: '/annuaire/mail',
                    type: 'POST',
                    data: {
                        _token: $(modalName + ' #token').val(),
                        pMail: saisie,
                        pTypeMail: typesaisie,
                        pType: type,
                        pID: id
                    },
                    dataType: "json",
                    async: false
                }).success(function (data) {
                    pk = data.ID;
                });
            }

            var td1 = $('<td/>')
                .attr('data-id', id)
                .attr('data-type', type)
                .attr('data-mail', saisie)
                .attr('data-type-mail', typesaisie)
                .attr('data-prefere', 0)
                .html(saisie + ' ' +
                    $('<i/>')
                        .addClass('fa')
                        .addClass('fa-star-o')
                        .addClass('prefere')
                        .css('cursor', 'pointer')
                        .wrap('<div/>')
                        .parent().html()
                );
            if (pk != undefined) {
                td1.attr('data-pk', pk);
            }
            var td2 = $('<td/>').html(typesaisietxt);
            var td3 = $('<td/>');
            if (add) {
                td3.addClass('text-right').append(
                    $('<a/>').attr('href', '#')
                        .addClass('editable edit-mail')
                        .attr('data-type', 'text')
                        .attr('data-id', pk)
                        .attr('data-title', 'Saisir les notes')
                        .attr('data-placement', 'right')
                        .attr('data-mode', 'inline')
                )
            }
            var td4 = $('<td/>')
                .addClass('text-right')
                .html(
                    $('<a/>').addClass('remove-mail btn btn-xs red')
                        .attr('data-placement', 'left')
                        .attr('data-toggle', 'confirmation')
                        .html(
                            $('<i/>').addClass('fa fa-remove')
                        )
                );
            $(modalName + ' #tableMail tbody').append(
                $('<tr/>')
                    .append(td1)
                    .append(td2)
                    .append((add ? td3 : ''))
                    .append(td4)
            );

            $(modalName + ' #portletMail #ztSaisie').val('');
            $(modalName + ' #portletMail #zlTypeSaisie').select2('val', 0);
            ClassGENERIC.editGeneric('mail', 'mail', modalName);
            $(modalName + ' #btnAddMail').unbind().on('click', addMail);

            /** -----------------------------------------------------------------------------------
             * Un petit rajout à cause du DOM
             */
            ClassGENERIC.updateRemoveButton(modalName, 'mail');
            ClassGENERIC.updatePrefereButton(modalName, 'mail', type);

        };

        var addModalite = function() {

            var id_mod;
            var type_id = $(modalName + ' #zlTypeEtabl').val();
            if (id == '') {
                toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                return false;
            }

            var txt = $(modalName + ' #zlTypeEtabl').select2('data')[0].text;
            var isSMS = 0;
            if ($(modalName + ' #zlSecteur').val() != '') {
                isSMS = $(modalName + ' #zlSecteur').find('option:selected').attr('data-ms');
            }
            else {
                isSMS = 0;
            }


            /** -----------------------------------------------------------------------------------
             * On va désactiver les zones SECTEUR + SECTEUR MS + HANDICAP + TRANCHE AGE
             */
            $(modalName + ' #zlSecteur').prop('readonly', true);
            $(modalName + ' #zlSecteurSMS').prop('readonly', true);
            $(modalName + ' #ccHANDICAP').prop('readonly', true);
            $(modalName + ' #zlTypeAge').prop('readonly', true);

            /** -----------------------------------------------------------------------------------
             * Si le portlet data-id (avec id = zlTypeEtabl) dans
             */
            var trouve = false;
            $(modalName + " #divDetail div[data-type-id='" + type_id + "']").each(function() {
                trouve = true;
            });
            if (!trouve) {

                /** -------------------------------------------------------------------------------
                 * Lors de la création d'un PORTLET, en mode ADD, on l'ajoute directement
                 * dans la base ! A ce moment là, on devra changer
                 */
                if (add == 1) {
                    $.ajax({
                        url: '/annuaire/etablissement_modalites',
                        type: 'POST',
                        data: {
                            _token: $(modalName + ' #token').val(),
                            pID_ET: id,
                            pTypeID : type_id
                        },
                        dataType: "json",
                        async: false
                    }).success(function (data) {
                        id_mod = data;
                    });
                } else {
                    id_mod = 0;
                }

                /** -------------------------------------------------------------------------------
                 * Attention, on convertit le ADD en VIEW
                 */
                var portlet = ClassGENERIC.createPortlet(id_mod, txt, (add ? 0 : 1), type_id);
                $(modalName + ' #divDetail').append(
                    $('<div/>', {
                        'class':'col-md-12'
                    }).append(
                        portlet));

                $(modalName + ' #divDetail .del-portlet').confirmation({
                    btnOkLabel: 'OUI',
                    btnOkClass: 'red-mint btn-xs',
                    btnCancelLabel: 'NON',
                    placement: 'left',
                    title: 'Suppression ?',
                    singleton: true,
                    popout: true,
                    onConfirm: function () {
                        $(this).parent().parent().parent().remove();

                        /** -----------------------------------------------------------------------
                         * Si le détail est tout vide
                         */
                        if($('#divDetail').html().trim() == '<div class="col-md-12"></div>') {
                            $(modalName + ' #zlSecteur').prop('readonly', false);
                            $(modalName + ' #zlSecteur').trigger('change');
                            $(modalName + ' #zlTypeAge').prop('readonly', false);
                            $(modalName + ' #ccHANDICAP').prop('readonly', false);
                        }
                    }
                });

                /** -------------------------------------------------------------------------------
                 * Pour terminer, on initialise le contenu des zones
                 */
                var cible = modalName + ' #divDetail div[data-id="' + id_mod + '"] ';
                ClassGENERIC.initSpecif(cible, '#zlModaliteAccueil', '', isSMS, 'modalites');
                ClassGENERIC.initGeneric(cible, 'typepublic', '#zlTypePublic');
                $(cible + ' #zlDetailTypePublic').select2().prop('readonly', true);

                /** -------------------------------------------------------------------------------
                 * On bind le CHANGE
                 */
                ClassET.bindZL_portlet(modalName, cible, id_mod);

            }
        };

        var addOffre = function() {

            /** -----------------------------------------------------------------------------------
             * Pour ajouter une offre, on doit ajouter ANNEE + NOMBRE + DATE SAISIE
             * + (éventuellement) NOTES
             */
            var annee = $(modalName + ' #ztAnnee').val();
            var nb = $(modalName + ' #ztNb').val();
            var date = $(modalName + ' #ztDate').val();
            var notes = $('<p>' + $(modalName + ' #ztNotes').val() + '</p>').text();

            if (annee == '' || nb == '' || date == '') {
                toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                return false;
            }

            /** -------------------------------------------------------------------------------
             * Lors de la création d'un PORTLET, en mode ADD, on l'ajoute directement
             * dans la base ! A ce moment là, on devra changer
             */
            if (add == 1) {
                $.ajax({
                    url: '/annuaire/offre',
                    type: 'POST',
                    data: {
                        _token: $(modalName + ' #token').val(),
                        pAnnee: annee,
                        pNb : nb,
                        pDate : date,
                        pNotes : notes,
                        pType : type,
                        pId : id,
                    },
                    dataType: "json",
                    async: false
                }).success(function (data) {
                    var td1 = $('<td/>').html(
                        annee)
                        .attr('data-pk', data.ID);
                    var td2 = $('<td/>')
                        .addClass('text-right')
                        .html(
                            nb
                        );
                    var td3 = $('<td/>')
                        .addClass('text-right')
                        .html(
                            date
                        );
                    var td4 = $('<td/>')
                        .addClass('text-right')
                        .html(
                            notes
                        );
                    var td5 = $('<td/>')
                        .addClass('text-right')
                        .html(
                            $('<a/>')
                                .addClass('remove-offre')
                                .addClass('btn')
                                .addClass('btn-xs')
                                .addClass('red')
                                .html(
                                    $('<i/>')
                                        .addClass('fa')
                                        .addClass('fa-remove')
                                )
                        );
                    $(modalName + ' #tableOffre tbody').append(
                        $('<tr/>')
                            .append(
                                td1
                            ).append(
                            td2
                        ).append(
                            td3
                        ).append(
                            td4
                        ).append(
                            td5
                        )
                    );
                    ClassGENERIC.updateRemoveButton(modalName, 'offre');
                });
            }

            $(modalName + ' #divDetail .del-portlet').confirmation({
                btnOkLabel: 'OUI',
                btnOkClass: 'red-mint btn-xs',
                btnCancelLabel: 'NON',
                placement: 'left',
                title: 'Suppression ?',
                singleton: true,
                popout: true,
                onConfirm: function () {
                    $(this).parent().parent().parent().remove();

                    /** -----------------------------------------------------------------------
                     * Si le détail est tout vide
                     */
                    if($('#divDetail').html().trim() == '<div class="col-md-12"></div>') {
                        $(modalName + ' #zlSecteur').prop('readonly', false);
                        $(modalName + ' #zlSecteur').trigger('change');
                        $(modalName + ' #zlTypeAge').prop('readonly', false);
                        $(modalName + ' #ccHANDICAP').prop('readonly', false);
                    }
                }
            });
        };

        /** ---------------------------------------------------------------------------------------
         * Si on est en mode AJOUT, la variable oo_id est renseignée, donc on rajoute directement
         * les infos dans la base
         */
        $(modalName + ' #btnAddTel').unbind().on('click', addTel);
        $(modalName + ' #btnAddMail').unbind().on('click', addMail);
        $(modalName + ' #btnAddModalite').unbind().on('click', addModalite);
        $(modalName + ' #btnAddOffre').unbind().on('click', addOffre);
    },

    manageModalCC : function (modalName) {

        console.log('OKOK');
        $(modalName + ' #ccNL').on('click', function () {
            console.log("ccNL");
        });
        $(modalName + ' #ccCF').on('click', function () {
            console.log(modalName + ' ccCF');

            if ($(modalName + ' #ccCF').is(':checked')) {
                $(modalName + ' #zlReceptionCF').select2('enable');//.enable(true);
            } else {
                $(modalName + ' #zlReceptionCF').select2('enable', false);//.enable(true);
            }
        });

        $(modalName + ' #ccBI').on('click', function () {
            if ($(modalName + ' #ccBI').is(':checked')) {
                $(modalName + ' #zlReceptionBI').select2('enable');//.enable(true);

                $(modalName + ' #ccNL').prop('checked', true);

            } else {
                $(modalName + ' #zlReceptionBI').select2('enable', false);//.enable(true);
            }
        });
    },

    updateEditableTels: function (modalName) {
        $('.edit-telephone').editable({
            emptytext: 'notes',
            validate: function (value) {

                if (value != null) {
                    $('.editable-submit').hide();
                    $('.editable-cancel').hide();

                    $.ajax({
                        url: '/annuaire/telephone/' + $(this).attr('data-id'),
                        type: 'PUT',
                        data: {
                            _token: $(modalName + ' #token').val(),
                            pValue: value
                        },
                        dataType: "json",
                        async: false
                    }).complete(function (code_html, statut) {
                    });

                }
            }
        });
    },

    updateEditableMails: function (modalName) {
        $('.edit-mail').editable({
            emptytext: 'notes',
            validate: function (value) {

                if (value != null) {
                    $('.editable-submit').hide();
                    $('.editable-cancel').hide();

                    $.ajax({
                        url: '/annuaire/mail/' + $(this).attr('data-id'),
                        type: 'PUT',
                        data: {
                            _token: $(modalName + ' #token').val(),
                            pValue: value
                        },
                        dataType: "json",
                        async: false
                    }).complete(function (code_html, statut) {
                    });

                }
            }
        });
    },

    offreList: function (modalName, type, id) {

        /** ---------------------------------------------------------------------------------------
         * Les offres d'emploi sont toujours dans une dialog99
         */
        var modal = $(modalName);
        modal.html($('#formOffre').html());

        $.ajax({
            url: '/datatable/' + type + '/offres/' + id,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {

                $.each(data.OFFRE, function (index, item) {
                    var td1 = $('<td/>').html(
                        item.annee)
                        .attr('data-pk', item.id);
                    var td2 = $('<td/>')
                        .addClass('text-right')
                        .html(
                            item.nb
                        );
                    var td3 = $('<td/>')
                        .addClass('text-right')
                        .html(
                            item.date_offre
                        );
                    var td4 = $('<td/>')
                        .addClass('text-right')
                        .html(
                            item.notes
                        );
                    var td5 = $('<td/>')
                        .addClass('text-right')
                        .html(
                            $('<a/>')
                                .addClass('remove-offre')
                                .addClass('btn')
                                .addClass('btn-xs')
                                .addClass('red')
                                .html(
                                    $('<i/>')
                                        .addClass('fa')
                                        .addClass('fa-remove')
                                )
                        );
                    $(modalName + ' #tableOffre tbody').append(
                        $('<tr/>')
                            .append(
                                td1
                            ).append(
                            td2
                        ).append(
                            td3
                        ).append(
                            td4
                        ).append(
                            td5
                        )
                    );
                });
                ClassGENERIC.manageModalButtons(modalName, 1, type, id);

                $(modalName + ' .date-picker').datepicker({
                    autoclose: true,
                    calendarWeeks: true,
                    format: 'dd/mm/yyyy',
                    language: 'fr',
                    todayBtn: true,
                    todayHighlight: true,

                });

                /** -------------------------------------------------------------------------------
                 * Custom des boutons / défautzlTypeTel
                 *
                 * @type {string}
                 */
                $.fn.editableform.buttons =
                    '<button class="btn btn-xs green editable-submit" type="submit"><i class="fa fa-check"></i></button>' +
                    '<button class="btn btn-xs default editable-cancel" type="button"><i class="fa fa-times"></i></button>'
                ;
                $(modalName).find('.modal-header').addClass('font-green-seagreen ');
                $(modalName).find('.modal-title').html('<i class="fa fa-sticky-note-o"></i> Gestion des Offres d\'Emploi');
                $(modalName).modal('show');
                $(modalName).on('hidden.bs.modal', function () {
                    if (type == 'oo'){
                        tableOrga.ajax.reload();
                    }
                });

            }
        });

        ClassGENERIC.updateRemoveButton(modalName, 'offre', type);
    },

    phoneList: function (modalName, type, id) {

        /** ---------------------------------------------------------------------------------------
         * Les téléphones et les mails sont toujours dans une dialog99
         */
        var modal = $(modalName);
        modal.html($('#formTel').html());

        $.ajax({
            url: '/datatable/' + type + '/telephones/' + id,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {

                $.each(data.TEL, function (index, item) {
                    var td1 = $('<td/>').html(
                        item.NUM + (item.prefere == 1 ?
                            ' <i class="fa fa-star prefere font-yellow" style="cursor:pointer"> </i>' :
                            ' <i class="fa fa-star-o prefere" style="cursor:pointer"> </i>')
                    )
                        .attr('data-pk', item.OWN_ID)
                        .attr('data-type', type)
                        .attr('data-type-telephone', item.TYPE_ID)
                        .attr('data-id', item.ID)
                        .attr('data-telephone', item.NUM);
                    var td2 = $('<td/>').html(
                        item.libelle
                    );
                    var td3 = $('<td/>')
                        .addClass('text-right')
                        .html(
                            $('<a/>')
                                .attr('href', '#')
                                .addClass('editable')
                                .addClass('edit-telephone')
                                .attr('data-type', 'text')
                                .attr('data-id', item.OWN_ID)
                                .attr('data-title', 'Saisir les notes')
                                .attr('data-value', item.notes)
                                .attr('data-placement', 'right')
                                .attr('data-mode', 'inline')
                                .css('display', 'inline')
                        );
                    var td4 = $('<td/>')
                        .addClass('text-right')
                        .html(
                            $('<a/>')
                                .addClass('remove-telephone')
                                .addClass('btn')
                                .addClass('btn-xs')
                                .addClass('red')
                                .html(
                                    $('<i/>')
                                        .addClass('fa')
                                        .addClass('fa-remove')
                                )
                        );
                    $(modalName + ' #tableTel tbody').append(
                        $('<tr/>')
                            .append(
                                td1
                            ).append(
                            td2
                        ).append(
                            td3
                        ).append(
                            td4
                        )
                    );
                });
                ClassGENERIC.initGeneric(modalName, 'typetel', '#zlTypeSaisie');
                ClassGENERIC.manageModalButtons(modalName, 1, type, id);


                /** -------------------------------------------------------------------------------
                 * Custom des boutons / défautzlTypeTel
                 *
                 * @type {string}
                 */
                $.fn.editableform.buttons =
                    '<button class="btn btn-xs green editable-submit" type="submit"><i class="fa fa-check"></i></button>' +
                    '<button class="btn btn-xs default editable-cancel" type="button"><i class="fa fa-times"></i></button>'
                ;
                ClassGENERIC.updateEditableTels(modalName);

                $(modalName).find('.modal-header').addClass('font-green-seagreen ');
                $(modalName).find('.modal-title').html('<i class="fa fa-phone"></i> Gestion des Téléphones / Fax');
                $(modalName).modal('show');

            }
        });

        ClassGENERIC.updateRemoveButton(modalName, 'telephone', type);
        ClassGENERIC.updatePrefereButton(modalName, 'telephone', type);
    },

    mailList: function (modalName, type, id) {

        /** ---------------------------------------------------------------------------------------
         * Les téléphones et les mails sont toujours dans une dialog99
         */
        var modal = $(modalName);
        modal.html($('#formMail').html());

        $.ajax({
            url: '/datatable/' + type + '/mails/' + id,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {

                $.each(data.MAIL, function (index, item) {
                    var td1 = $('<td/>').html(
                        item.mail + (item.prefere == 1 ?
                            ' <i class="fa fa-star prefere font-yellow" style="cursor:pointer"> </i>' :
                            ' <i class="fa fa-star-o prefere" style="cursor:pointer"> </i>')
                    )
                        .attr('data-pk', item.OWN_ID)
                        .attr('data-type', type)
                        .attr('data-type-mail', item.TYPE_ID)
                        .attr('data-id', item.ID)
                        .attr('data-mail', item.mail);
                    var td2 = $('<td/>').html(
                        item.libelle
                    );
                    var td3 = $('<td/>')
                        .addClass('text-right')
                        .html(
                            $('<a/>')
                                .attr('href', '#')
                                .addClass('editable')
                                .addClass('edit-mail')
                                .attr('data-type', 'text')
                                .attr('data-id', item.OWN_ID)
                                .attr('data-title', 'Saisir les notes')
                                .attr('data-value', item.notes)
                                .attr('data-placement', 'right')
                                .attr('data-mode', 'inline')
                                .css('display', 'inline')
                        );
                    var td4 = $('<td/>')
                        .addClass('text-right')
                        .html(
                            $('<a/>')
                                .addClass('remove-mail')
                                .addClass('btn')
                                .addClass('btn-xs')
                                .addClass('red')
                                .html(
                                    $('<i/>')
                                        .addClass('fa')
                                        .addClass('fa-remove')
                                )
                        );
                    $(modalName + ' #tableMail tbody').append(
                        $('<tr/>')
                            .append(
                                td1
                            ).append(
                            td2
                        ).append(
                            td3
                        ).append(
                            td4
                        )
                    );
                });
                ClassGENERIC.initGeneric(modalName, 'typemail', '#zlTypeSaisie');
                ClassGENERIC.manageModalButtons(modalName, 1, type, id);


                /** -------------------------------------------------------------------------------
                 * Custom des boutons / défautzlTypeTel
                 *
                 * @type {string}
                 */
                $.fn.editableform.buttons =
                    '<button class="btn btn-xs green editable-submit" type="submit"><i class="fa fa-check"></i></button>' +
                    '<button class="btn btn-xs default editable-cancel" type="button"><i class="fa fa-times"></i></button>'
                ;
                ClassGENERIC.updateEditableMails(modalName);

                $(modalName).find('.modal-header').addClass('font-green-seagreen ');
                $(modalName).find('.modal-title').html('<i class="fa fa-send"></i> Gestion des Mails');
                $(modalName).modal('show');

            }
        });

        ClassGENERIC.updateRemoveButton(modalName, 'mail');
        ClassGENERIC.updatePrefereButton(modalName, 'mail', type);
    },

    createPortlet: function (id, txt, visu, type_id) {

        /** -------------------------------------------------------------------------------
         * Création du PORTLET
         */
        var portlet = $('<div/>', {
            'data-id': id,
            'data-type-id': type_id,
            'class': 'portlet box blue-hoki'
        });

        /** -------------------------------------------------------------------------------
         * Création du titre (avec caption et actions
         */
        var title = $('<div/>', {
            'class': 'portlet-title'
        });

        title.append($('<div/>', {
            'class': 'caption',
            'style': 'font-size:14px;'
        }).append(
            txt
        ));
        if (visu != 1) {
            title.append($('<div/>', {
                'class': 'actions'
            }).append(
                $('<a/>', {
                    'class': 'btn btn-default btn-sm del-portlet',
                    'data-id': id
                }).append(
                    $('<i/>', {
                        'class': 'fa fa-trash'
                    })).append('SUPPRIMER')
            ));
        }


        /** -------------------------------------------------------------------------------
         * Maintenant, on crée le BODY
         */
        var body = $('<div/>', {
            'class': 'portlet-body'
        });

        /** -------------------------------------------------------------------------------
         * On crée les 3 listes et le input
         */
        var modalites = $('<div/>', {
            'class': 'col-md-8'
        }).append(
            '<div class="form-group form-md-line-input">' +
            '<select id="zlModaliteAccueil" class="form-control">' +
            '</select>' +
            '<label for="zlModaliteAccueil">Modalités d\'Accueil' +
            '</label>' +
            '</div>'
        );

        var typepublic = $('<div/>', {
            'class': 'col-md-5'
        }).append(
            '<div class="form-group form-md-line-input">' +
            '<select id="zlTypePublic" class="form-control">' +
            '</select>' +
            '<label for="zlTypePublic">Type Public' +
            '</label>' +
            '</div>'
        );

        var detailtypepublic = $('<div/>', {
            'class': 'col-md-5'
        }).append(
            '<div class="form-group form-md-line-input">' +
            '<select id="zlDetailTypePublic" class="form-control">' +
            '</select>' +
            '<label for="zlDetailTypePublic">Détail Type Public' +
            '</label>' +
            '</div>'
        );

        var places = $('<div/>', {
            'class': 'col-md-2'
        }).append(
            '<div class="form-group form-md-line-input form-md-floating-label has-success">' +
            '<input id="ztNbPlaces" class="form-control" type="number">' +
            '<label for="ztNbPlaces">Nb Places</label>' +
            '</div>'
        );

        var btn = $('<div/>', {
            'class': 'col-md-2'
        }).append(
            '<br/>' +
            '<button id="btnPlusDetailModalite" data-id="0" class="btn btn-success" type="button">' +
            '<i class="fa fa-plus-circle"></i>' +
            '</button>' +
            '<label for="btnPlusDetailModalite"></label>'
        );

        /** -------------------------------------------------------------------------------
         * On place le tout dans la première ligne et dans une deuxième ligne
         */
        if (visu != 1) {
            body.append($('<div/>', {
                    'class': 'row'
                }).append(modalites).append(places).append(btn)
            );
            body.append($('<div/>', {
                    'class': 'row'
                }).append(typepublic).append(detailtypepublic)
            );
        }

        /** -------------------------------------------------------------------------------
         * Dans une deuxième ligne, on met un TABLE
         */
        var table = '<div class="col-md-12">' +
            '<table table-id="' + id + '" class="table table-striped table-bordered table-hover order-column" data-toggle="table">' +
            '<thead><tr>' +
            '<th class="text-center"> Modalités Accueil</th>' +
            '<th class="text-center"> Type Public </th>' +
            '<th class="text-center"> Détail Public </th>' +
            '<th class="text-center"> Nb Places </th>' +
            (visu != 1 ? '<th class="text-center"> Actions </th>' : '') +
            '</tr>' +
            '</thead>' +
            '<tbody> </tbody>' +
            '</table>' +
            '</div>';
        body.append($('<div/>', {
                'class': 'row'
            }).append(table)
        );

        /** -------------------------------------------------------------------------------
         * On termine le PORTLET et on l'ajoute au DETAIL
         */
        portlet.append(title).append(body);

        return portlet;
    },

    refreshEdited: function (modalName) {
        $(modalName).find('.form-md-floating-label').children('.form-control').each(function () {
            if ($(this).val().length > 0) {
                $(this).addClass('edited');
            }
        });
    },

    doReadOnly: function (modalName) {
        $(modalName).find('input[type=text], textarea').each(function () {
            $(this).prop('readonly', true);
        });
        $(modalName).find('select').each(function () {
            $(this).prop('disabled', true);
        });
        $(modalName).find('input[type=checkbox]').each(function () {
            $(this).prop('disabled', true);
        });
        $(modalName).find('.remove-telephone').hide();
        $(modalName).find('.remove-mail').hide();
    },

    arianeToTxt: function () {
        var txt = '';
        ariane.forEach(function (item) {
            txt += '<i class="fa fa-caret-right font-red"></i> <span class="bold">' + item + '</bold>&nbsp;';
        });
        return txt;
    },

    /** -------------------------------------------------------------------------------------------
     * Retourne le nouveau level (dialog0 -> dialog1)
     * @param level
     * @returns {*}
     */
    newLevel: function (level) {
        if (level == '') {
            return '#dialogExecLev0';
        } else {
            var tmp = level;
            var dialog = tmp.substr(0, tmp.length - 1);
            var num = tmp.substr(tmp.length - 1, 1);
            return dialog + (parseInt(num) + 1);
        }
    },

    validMail: function (mail) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(mail);
    },

    editGeneric: function (type, route, modalName) {
        $('.edit-' + type).editable({
            emptytext: 'notes',
            validate: function (value) {

                if (value != null) {
                    $('.editable-submit').hide();
                    $('.editable-cancel').hide();

                    $.ajax({
                        url: '/annuaire/' + route + '/' + $(this).attr('data-id'),
                        type: 'PUT',
                        data: {
                            _token: $(modalName + ' #token').val(),
                            pValue: value
                        },
                        dataType: "json",
                        async: false
                    }).complete(function (code_html, statut) {
                    });

                }
            }
        });
    },

    initModaliteFiltre: function (zone) {

        /** ---------------------------------------------------------------------------------------
         * Dans tous les cas, on récupère la liste des ITEMS
         */
        $.ajax({
            url: '/list/' + 'modaliteaccueil',
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {
                if (data.LISTE.length > 0) {
                    var zl = $(zone);
                    zl.empty();
                    $.each(data.LISTE, function (index, item) {
                        zl.append('<option ' + ' value="' + item.libelle + '" ' + '>' + item.libelle + '</option>');
                    });
                    zl.select2({
                        width: '100%',
                        allowClear: (data.LISTE.length > 1 ? true : false),
                        placeholder: ' ',
                        language: 'fr'
                    });
                }
            }
        });
    },

    initSpecif: function (modalName, zone, value, filter, type) {

        var route = '';
        switch (type) {
            case 'modalitesEtablissement' :
                route = 'modaliteaccueilEtablissement';
                break;
            case 'modalites' :
                route = 'modaliteaccueil';
                break;
        }

        /** ---------------------------------------------------------------------------------------
         * Dans tous les cas, on récupère la liste des ITEMS
         */
        $.ajax({
            url: '/list/' + route + '/' + filter,
            type: 'GET',
            dataType: 'json',
            data: {
                //pFilter : filter
            },
            async: false,
            success: function (data) {
                if (data.LISTE.length > 0) {
                    var zl = $(modalName + ' ' + zone);
                    zl.empty();
                    if (data.LISTE.length > 1) {
                        zl.append('<option value=""></option>');
                    }
                    $.each(data.LISTE, function (index, item) {
                        zl.append('<option ' + ' value="' + item.id + '" ' + (item.id == value ? ' selected="selected" ' : '') + '>' + item.libelle + '</option>');
                    });
                    zl.select2({
                        width: '100%',
                        allowClear: (data.LISTE.length > 1 ? true : false),
                        placeholder: ' ',
                        language: 'fr'
                    });
                }
            }
        });
    },

    initGeneric: function (modalName, route, zone, value, filter, organisme, hasEmptyLine) {

        /** ---------------------------------------------------------------------------------------
         *
         */
        if (typeof hasEmptyLine === "undefined" || hasEmptyLine === null) {
            hasEmptyLine = 1;
        }

        var routeFiltre
        if (organisme != undefined) {
            routeFiltre = (filter === undefined ? '' : '/' + organisme);
        } else {
            routeFiltre = (filter === undefined ? '' : '/' + filter);
        }

        /** ---------------------------------------------------------------------------------------
         * On rajoute une SPECIFICITE au niveau des zones suivantes
         */
        var specif = false;
        if (zone == '#zlSecteur' ||
            zone == '#zlSecteurSMS' ||
            zone == '#zlTypePublic' ||
            zone == '#zlType' ||
            zone == '#zlTypeAge'
        ) {
            specif = true;
        }

        var specifTypeOrga = false;
        if (zone == '#zlType'
        ) {
            specifTypeOrga = true;
        }

        var specifCheckMail = false;
        if (zone == '#zlReceptionBI' ||
            zone == '#zlReceptionCF'
        ) {
            specifCheckMail = true;
        }

        var specifParrain = false;
        if (zone == '#zlDetailType'
        ) {
            specifParrain = true;
        }

        /** ---------------------------------------------------------------------------------------
         * Dans tous les cas, on récupère la liste des ITEMS
         */
        $.ajax({
            url: '/list/' + route + routeFiltre,
            type: 'GET',
            dataType: 'json',
            data: {
                //pFilter : filter
            },
            async: false,
            success: function (data) {

                var select = '';
                var zl = $(modalName + ' ' + zone);
                zl.empty();
                if (data.LISTE.length > 0) {
                    if (data.LISTE.length > 1 || zone == '#zlPole') {
                        if (hasEmptyLine == 1) {
                            zl.append('<option value=""' + '></option>');
                        }
                    }
                    $.each(data.LISTE, function (index, item) {
                        zl.append('<option ' +
                            (item.datadefaut == 1 ?  'selected="selected"' : '') +
                            (specifTypeOrga ? 'data-force="' + item.forceog + '"': '') +
                            (specifCheckMail ? 'data-check="' + item.check + '"': '') +
                            (specifParrain ? 'data-parrain="' + item.parrain + '"': '') +
                            (specif ? ' data-ms="' + item.ms + '" data-handicap="' + item.handicap + '" ' : '') +
                            ' value="' + item.id + '" ' + (item.id == value ? ' selected="selected" ' : '') + '>' + item.libelle + '</option>');
                    });
                    zl.select2({
                        width: '100%',
                        allowClear: (data.LISTE.length > 1 || zone == '#zlPole' ? true : false),
                        placeholder: ' ',
                        language: 'fr'
                    });
                } else {
                }
            }
        });

        /** ---------------------------------------------------------------------------------------
         * ATTENTION, en cas de filtre, on peut éventuellement avoir un SOUS-TYPE
         */
        if (filter != '') {

            switch (zone) {
                //case '#zlDetailTypePublic':
                //    if ($(modalName + ' #zlDetailTypePublic option').size() != 0) {
                //        $(modalName + ' #zlDetailTypePublic').parent().parent().removeClass('display-none');
                //    } else {
                //        $(modalName + ' #zlDetailTypePublic').parent().parent().addClass('display-none');
                //    }
                //    break;
                case '#zlDetailType':
                    if ($(modalName + ' #zlDetailType option').size() != 0) {
                        $(modalName + ' #zlDetailType').parent().parent().removeClass('display-none');
                    } else {
                        $(modalName + ' #zlDetailType').parent().parent().addClass('display-none');
                    }
                    break;
            }
        }
    },

    switchStatut: function (element, id, statut, field, route) {
        var newStatut = 1 - statut;
        if (newStatut == 0) {
            element.removeClass('green-seagreen').addClass('red-sunglo');
        } else {
            element.removeClass('red-sunglo').addClass('green-seagreen');
        }
        element.attr('data-statut', newStatut);

        $.ajax({
            url: '/annuaire/' + route + '/switch/' + id + '/edit',
            type: 'GET',
            data: {
                pValue: newStatut,
                pType: field
            },
            dataType: 'json',
            success: function (data) {

            }
        });

    },
};

/** -----------------------------------------------------------------------------------------------
 * @class : ORGANISME
 * Version simplifée
 */
var ClassOO = {

    init: function () {
    },

    getNL: function () {

    },

    manageFilters: function () {

        /** ---------------------------------------------------------------------------------------
         * InitZL
         * modalName, route, zone, value, filter, organisme
         */
        ClassGENERIC.initGeneric('', 'typeetablissement', '#zlFiltreTypeET', '', '', '', 0);
        ClassGENERIC.initGeneric('', 'typesecteur', '#zlFiltreTypeSecteur', '', '', '', 0);
        ClassGENERIC.initGeneric('', 'typesecteurms', '#zlFiltreTypeSecteurSMS', '', '', '', 0);
        ClassGENERIC.initGeneric('', 'typeage', '#zlFiltreTrancheAge', '', '', '', 0);
        ClassGENERIC.initGeneric('', 'typepublic', '#zlFiltreTypePublic', '', '', '', 0);
        ClassGENERIC.initGeneric('', 'detailtypepublic', '#zlFiltreDetailTypePublic', '', '', '', 0);
        ClassGENERIC.initGeneric('', 'fonction', '#zlFiltreFonction', '', '', '', 0);

        /** ---------------------------------------------------------------------------------------
         * Attention, pour les modalités d'accueil, on va se fier au libellé et non à l'ID
         */
        ClassGENERIC.initModaliteFiltre('#zlFiltreTypeModalite');
        ClassGENERIC.initGeneric('', 'typeorganisme', '#zlFiltreTypeOrganisme', '', '', '', 0);

        $('.make-switch').bootstrapSwitch();
<<<<<<< HEAD

        /** ---------------------------------------------------------------------------------------
         * Filtre NL ?
         */
        $('#btnFiltreNL_oui').on('click', function () {
            $('#btnFiltreNL_non').removeClass('btn-danger');
            $('#btnFiltreNL_na').removeClass('btn-default');
            $(this).addClass('btn-success');
            $('#btnFiltreNL').attr('data-nl', 1);
        });
        $('#btnFiltreNL_non').on('click', function () {
            $('#btnFiltreNL_oui').removeClass('btn-success');
            $('#btnFiltreNL_na').removeClass('btn-default');
            $(this).addClass('btn-danger');
            $('#btnFiltreNL').attr('data-nl', 0);
        });
        $('#btnFiltreNL_na').on('click', function () {
            $('#btnFiltreNL_non').removeClass('btn-danger');
            $('#btnFiltreNL_oui').removeClass('btn-success');
            $(this).addClass('btn-default');
            $('#btnFiltreNL').attr('data-nl', -1);
        });
=======
>>>>>>> debug8

        /** ---------------------------------------------------------------------------------------
         * Si il n'y a rien à filtrer, on ne fait rien
         */
        $('#btnFilterOn').on('click', function () {

            var filtreNom = $('#ztFiltreNom').val().trim();
            var filtreAdresse = $('#ztFiltreAdresse').val().trim();
            var filtreCP = $('#ztFiltreCP').val().trim();
            var filtreVille = $('#ztFiltreVille').val().trim();
            var filtreFiness = $('#ztFiltreFiness').val().trim();
            var filtreCode = $('#ztFiltreCode').val().trim();
            var filtreService = $('#ztFiltreService').val().trim();
<<<<<<< HEAD

            var filtreSigle = $('#ztFiltreSigle').val().trim();
            var filtreNbPlaces = $('#ztFiltreNbPlaces').val().trim();

=======
>>>>>>> debug8
            var filtreTypeEtablissement = $('#zlFiltreTypeET').val();
            var filtreTypeSecteur = $('#zlFiltreTypeSecteur').val();
            var filtreTypeSecteurSMS = $('#zlFiltreTypeSecteurSMS').val();
            var filtreTrancheAge = $('#zlFiltreTrancheAge').val();
            var filtreTypePublic = $('#zlFiltreTypePublic').val();
            var filtreDetailTypePublic = $('#zlFiltreDetailTypePublic').val();
            var filtreTypeModalite = $('#zlFiltreTypeModalite').val();
            var filtreTypeOrganisme = $('#zlFiltreTypeOrganisme').val();
            var filtreFonction = $('#zlFiltreFonction').val();
            var ccOO, ccPO, ccET, ccAN, ccAA, ccPE;

            ccOO = $('#ccFiltreOO').is(":checked") ? 1 : 0;
            ccPO = $('#ccFiltrePO').is(":checked") ? 1 : 0;
            ccET = $('#ccFiltreET').is(":checked") ? 1 : 0;
            ccAN = $('#ccFiltreAN').is(":checked") ? 1 : 0;
            ccAA = $('#ccFiltreAA').is(":checked") ? 1 : 0;
            ccPE = $('#ccFiltrePC').is(":checked") ? 1 : 0;

            if (filtreNom == '' && filtreAdresse == '' &&
                filtreCP == '' && filtreVille == '' &&
                filtreFiness == '' && filtreCode == '' &&
<<<<<<< HEAD
                filtreSigle == '' && filtreNbPlaces == '' &&
=======
>>>>>>> debug8
                filtreService == '' && filtreTypeEtablissement == null &&
                filtreTypeSecteur == null && filtreTypeSecteurSMS == null &&
                filtreTrancheAge == null && filtreTypePublic == null &&
                filtreDetailTypePublic == null && filtreTypeModalite == null &&
                filtreTypeOrganisme == null && filtreFonction == null &&
                filtreFonction == null
            ) {
                toastr.warning('Aucun critère de filtre n\'est renseigné.', "Attention");
                return false;
            }

            if (ccOO + ccPO + ccET + ccAN + ccAA + ccPE == 0) {
                toastr.warning('Indiquez dans quelles fiches vous voulez effectuer la recherche !', "Attention");
                return false;
            }

            if (tableFiltre === undefined) {
                tableFiltre = $('#' + tableNameFiltre).removeAttr('width').DataTable({
                    "dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
                    iDisplayLength:50,
                    ajax: {
                        type : 'POST',
                        url : "/datatable/" + routeFiltreOO,
                        data: function (d) {
                            d.pOO = $('#ccFiltreOO').is(":checked") ? 1 : 0;
                            d.pPO = $('#ccFiltrePO').is(":checked") ? 1 : 0;
                            d.pET = $('#ccFiltreET').is(":checked") ? 1 : 0;
                            d.pAN = $('#ccFiltreAN').is(":checked") ? 1 : 0;
                            d.pAA = $('#ccFiltreAA').is(":checked") ? 1 : 0;
                            d.pPE = $('#ccFiltrePE').is(":checked") ? 1 : 0;
                            d.pNom = $('#ztFiltreNom').val().trim();
                            d.pAdresse = $('#ztFiltreAdresse').val().trim();
                            d.pCP = $('#ztFiltreCP').val().trim();
                            d.pVille = $('#ztFiltreVille').val().trim();
                            d.pFiness = $('#ztFiltreFiness').val().trim();
                            d.pCode = $('#ztFiltreCode').val().trim();
<<<<<<< HEAD
                            d.pSigle = $('#ztFiltreSigle').val().trim();
                            d.pNbPlaces = $('#ztFiltreNbPlaces').val().trim();
=======
>>>>>>> debug8
                            d.pService = $('#ztFiltreService').val().trim();
                            d.pTypeEtablissement = $('#zlFiltreTypeET').val();
                            d.pTypeSecteur = $('#zlFiltreTypeSecteur').val();
                            d.pTypeSecteurSMS = $('#zlFiltreTypeSecteurSMS').val();
                            d.pTrancheAge = $('#zlFiltreTrancheAge').val();
                            d.pTypePublic = $('#zlFiltreTypePublic').val();
                            d.pDetailTypePublic = $('#zlFiltreDetailTypePublic').val();
                            d.pFiltreModalite = $('#zlFiltreTypeModalite').val();
                            d.pFiltreTypeOrganisme = $('#zlFiltreTypeOrganisme').val();
                            d.pFiltreFonction = $('#zlFiltreFonction').val();
<<<<<<< HEAD
                            d.pNL = $('#btnFiltreNL').attr('data-nl');
=======
>>>>>>> debug8
                        },
                    },
                    serverSide: false,
                    autoWidth: false,
                    columnDefs: [
                        {className: "dt-body-left", "targets": [0, 1, 2, 3, 4]},
                        {className: "dt-body-right", "targets": [5]},
                    ],
                    columns: [
                        {orderable: true, width: 100}, null, null, null, null,
                        {orderable: true, width: 140},
                    ],
                    lengthMenu: [
                        [10, 25, 50, 100, -1],
                        [10, 25, 50, 100, "Tous"]
                    ],

                    //fixedColumns: true,
                    sPagingType: "bootstrap_full_numbers",
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
                    },
                    initComplete: function (settingssettings) {
                        /** On change les largeurs par défaut des contrôles */
                        var tableWrapper = $('#' + tableNameOrga + '_wrapper');
                        var tableLength = $('#' + tableNameOrga + '_length');
                        //tableLength.css('width', '400px');
                        tableWrapper.find('.dataTables_length select').addClass("form-control input-inline").select2();
                        tableWrapper.find('.dataTables_filter input').addClass("form-control input-inline");
                    },
                    rowCallback: function (row, data, index) {

                        var colAction = 5;

                        /** -------------------------------------------------------------------------------
                         * Légère réduction de la taille des caractères
                         */
                        $('td:eq(1),td:eq(2),td:eq(3),td:eq(4)', row).addClass("txtSmall");

                        /** --------------------------------------------------------------------------------
                         * On checke la colonne ACTION - lien A
                         */
                        $('td:eq(' + colAction + ')', row).children('a').bind('click', function () {
<<<<<<< HEAD

                            ClassGENERIC.buildAriane($(this).attr('data-type'), $(this).attr('data-id'));
                            switch ($(this).attr('data-type')) {
                                case 'oo':
                                    if ($(this).hasClass('visu')) {
                                        ClassOO.view($(this).attr('data-id'));
                                    }
                                    if ($(this).hasClass('update')) {
                                        ClassOO.update($(this).attr('data-id'));
                                    }
                                    break;
                                case 'po':
                                    if ($(this).hasClass('visu')) {
                                        ClassPO.view('', $(this).attr('data-id'))
                                    };
                                    if ($(this).hasClass('update')) {
                                        ClassPO.update('', $(this).attr('data-id'))
                                    };
=======
                            switch ($(this).attr('data-type')) {
                                case 'oo':
                                    ClassOO.view($(this).attr('data-id'));
                                    break;
                                case 'po':
                                    ClassPO.view('', $(this).attr('data-id'));
>>>>>>> debug8
                                    break;
                                case 'et':
                                    ClassET.view('', $(this).attr('data-id'), '', '');
                                    break;
                                case 'an':
                                    ClassAN.view('', $(this).attr('data-id'), '', '');
                                    break;
                                case 'aa':
                                    ClassAA.view('', $(this).attr('data-id'), '', '');
                                    break;
                                case 'pe':
                                    ClassPERSONNEL.view('', $(this).attr('data-id'), '', '');
                                    break;
                            }
<<<<<<< HEAD
=======

                            if ($(this).hasClass('visu')) {
                                return;
                                ClassOO.view($(this).attr('data-id'));
                            }

                            if ($(this).hasClass('modif')) {
                                ClassOO.update($(this).attr('data-id'));
                            }

>>>>>>> debug8
                        });
                    }
                });
            }
            else {
                tableFiltre.ajax.reload();
            }

            $('#tableFiltre').removeClass('display-hide');
        });

        /** ---------------------------------------------------------------------------------------
         *
         */
        $('#btnFilterOff').on('click', function () {
            if (tableFiltre != undefined) {
                tableFiltre.destroy(false);
            }
            //tableFiltre = undefined;
        });

    },

    /** -------------------------------------------------------------------------------------------
     * Init DataTables
     */
    initDataTable: function () {

        tableOrga = $('#' + tableNameOrga).removeAttr('width').DataTable({
            "dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
            iDisplayLength:50,
            ajax: "/datatable/" + routeOrga,
            serverSide: false,
            autoWidth: false,
            columnDefs: [
                {className: "dt-body-left", "targets": [0, 1, 2, 3]},
                {className: "dt-body-right", "targets": [4]},
            ],
            columns: [
                {orderable: true, width: 100}, null, null, null,
                {orderable: true, width: 140},
            ],
            lengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "Tous"]
            ],

            //fixedColumns: true,
            sPagingType: "bootstrap_full_numbers",
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
            },
            initComplete: function (settingssettings) {
                /** On change les largeurs par défaut des contrôles */
                var tableWrapper = $('#' + tableNameOrga + '_wrapper');
                var tableLength = $('#' + tableNameOrga + '_length');
                //tableLength.css('width', '400px');
                tableWrapper.find('.dataTables_length select').addClass("form-control input-inline").select2();
                tableWrapper.find('.dataTables_filter input').addClass("form-control input-inline");
            },
            rowCallback: function (row, data, index) {

                var colAction = 4;

                /** -------------------------------------------------------------------------------
                 * Légère réduction de la taille des caractères
                 */
                $('td:eq(1),td:eq(2)', row).addClass("txtSmall");
                $('td:eq(' + colAction + ')', row).find('.dropdown-toggle').dropdown();

                /** --------------------------------------------------------------------------------
                 * On checke la colonne ACTION - lien A
                 */
                $('td:eq(' + colAction + ')', row).children('a').bind('click', function () {
                    if ($(this).hasClass('visu')) {
                        ClassOO.view($(this).attr('data-id'));
                    }
                });

                /** --------------------------------------------------------------------------------
                 * On checke la colonne ACTION - menu DROPDOWN
                 */
                $('td:eq(' + colAction + ')', row).find('a').bind('click', function () {

                    /** ---------------------------------------------------------------------------
                     * Contruction du fil d'ariane des titres
                     */
<<<<<<< HEAD
                    ariane = [];

=======
                        //console.log($(this).closest('tr').find('td:eq(1)').html());
>>>>>>> debug8
                    var lib = $(this).closest('tr').find('td:eq(1)').find('span').attr('data-nom');
                    if ($(this).hasClass('logo')) {
                        ClassOO.manageLogo($(this).attr('data-id'));
                    }
                    if ($(this).hasClass('pole')) {
                        mode = MODE_DATATABLES;
                        ariane.push(lib);
                        ClassPO.managePoles('', $(this).attr('data-id'));
                    }
                    if ($(this).hasClass('etabl')) {
                        mode = MODE_DATATABLES;
                        ariane.push(lib);
                        ClassET.manageEtabls('', $(this).attr('data-id'));
                    }
                    if ($(this).hasClass('personnel')) {
                        mode = MODE_DATATABLES;
                        ariane.push(lib);
                        ClassPERSONNEL.managePersonnel('', $(this).attr('data-id'));
                    }
                    if ($(this).hasClass('modif')) {
                        ClassOO.update($(this).attr('data-id'));
                    }
                    if ($(this).hasClass('telephone')) {
                        ClassGENERIC.phoneList('#dialogExecLev9', 'oo', $(this).attr('data-id'));
                    }
                    if ($(this).hasClass('mail')) {
                        ClassGENERIC.mailList('#dialogExecLev9', 'oo', $(this).attr('data-id'));
                    }
                    if ($(this).hasClass('offres')) {
                        ClassGENERIC.offreList('#dialogExecLev9', 'oo', $(this).attr('data-id'));
                    }
                });
            }
        });
    },

    /** -------------------------------------------------------------------------------------------
     * Modal d'ajout d'un enregistrement
     */
    add: function () {

        mode = MODE_FORM;
        waitingDialog.show('Préparation du Formulaire', {dialogSize: 'large', progressType: 'info'});
        //$('#dialogWait').modal('show');
        //return;

        /** ---------------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        var modalName = ClassGENERIC.newLevel('');
        var modal = $(modalName);
        modal.html($('#formOrganisme').html());

        /** ---------------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        $(modalName).find('#btnAction').addClass('green').html('<i class="fa fa-plus-circle"></i> Ajouter');
        $(modalName).find('.modal-header').addClass('font-green');
        $(modalName).find('.modal-title').html('<i class="fa fa-plus-circle"></i> Ajout ' + edit);
        $(modalName).find('.historique').hide();

        /** ---------------------------------------------------------------------------------------
         * Init des LISTEs
         */
        ClassOO.initListes(modalName);
        $(modalName + ' #zlReceptionCF').select2('enable', false);
        $(modalName + ' #zlReceptionBI').select2('enable', false);
        $('#portletTel .portlet-body').find('.row:first').show();
        $('#portletMail .portlet-body').find('.row:first').show();

        /** ---------------------------------------------------------------------------------------
         * Si on a un DETAIL, on l'affiche
         */
        $(modalName + ' #zlType').on('change', function () {
            if ($(modalName + ' #zlType').val() != '') {
                ClassGENERIC.initGeneric(modalName, 'detailtypeorganisme/' + $(modalName + ' #zlType').val(), '#zlDetailType', '');

                /** -------------------------------------------------------------------------------
                 * On a un forceOG ?
                 */
                var tmp = $(modalName + " #zlType").select2('data')[0];
                var force = tmp.element.dataset.force;
                if (force == 1) {
                    $(modalName + ' #ccGestionnaire').prop('checked', true);
                }

            } else {
                $(modalName + ' #zlDetailType').empty();
                $(modalName + ' #zlDetailType').parent().parent().addClass('display-none');
            }
        });

        $(modalName + ' #zlDetailType').on('change', function (e) {
            var tmp = $(modalName + " #zlDetailType").select2('data')[0];
            var parrain = tmp.element.dataset.parrain;
            if (parrain == 1) {
                $(modalName + ' #ztNomParrain').parent().show();
            } else {
                $(modalName + ' #ztNomParrain').parent().hide();
            }
        });

        /** ---------------------------------------------------------------------------------------
         * Pour le premier LEVEL, pas de SHOW DIALOG
         */
        //$(modalName).modal('show');

        /** ---------------------------------------------------------------------------------------
         * Sauvegarde
         */
        ClassGENERIC.manageModalButtons(modalName, 0, 'oo', undefined);
        ClassGENERIC.manageModalCC(modalName);

        /** ---------------------------------------------------------------------------------------
         * Sauvegarde
         */
        $(":input").inputmask();
        $(modalName + ' #btnAction').unbind().on('click', function () {

            /** -----------------------------------------------------------------------------------
             * Vérification des ZONES Mandatory
             */
            var oblig = true;
            $(modalName + ' .required').each(function () {
                if (!$(this).val().trim()) {
                    oblig = false;
                }
            });
            if (!oblig) {
                toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                return;
            }

            // ------------------------------------------------------------------------------------
            // Données de BASE
            // ------------------------------------------------------------------------------------
            var nom = $(modalName + ' #ztNom').val().trim().toUpperCase();
            var sigle = $(modalName + ' #ztSigle').val().trim().toUpperCase();
            var code = $(modalName + ' #ztCodeInterne').val().trim();
            var gestionnaire = $(modalName + ' #ccGestionnaire').is(":checked") ? 1 : 0;
            var typeo = $(modalName + ' #zlType').val();
            var detailo = $(modalName + ' #zlDetailType').val();
            var parrain = $(modalName + ' #ztNomParrain').val().toUpperCase();
            var statuto = $(modalName + ' #zlStatut').val();
            var adr1 = $(modalName + ' #ztAdr1').val().trim().toUpperCase();
            var adr2 = $(modalName + ' #ztAdr2').val().trim().toUpperCase();
            var adr3 = $(modalName + ' #ztAdr3').val().trim().toUpperCase();
            var cp = $(modalName + ' #ztCP').val();
            var ville = $(modalName + ' #ztVille').val().trim().toUpperCase();
            var pays = $(modalName + ' #zlPays').val();

            var web = $(modalName + ' #ztWEB').val();
            var notes = $('<p>' + $(modalName + ' #ztNotes').val() + '</p>').text();

            var finess = $(modalName + ' #ztFINESS').val().trim().toUpperCase();
            var siret = $(modalName + ' #ztSIRET').val().trim().toUpperCase();
            var iban = $(modalName + ' #ztIBAN').val().trim().toUpperCase();
            var bic = $(modalName + ' #ztBIC').val().trim().toUpperCase();

            // ------------------------------------------------------------------------------------
            // Données COMPLEMENTAIRES
            // ------------------------------------------------------------------------------------
            var nl = $(modalName + ' #ccNL').is(":checked") ? 1 : 0;
            var bi = $(modalName + ' #ccBI').is(":checked") ? 1 : 0;
            var bi_recep = (bi == 1 ? $(modalName + ' #zlReceptionBI').val() : null);
            var cf = $(modalName + ' #ccCF').is(":checked") ? 1 : 0;
            var cf_recep = (cf == 1 ? $(modalName + ' #zlReceptionCF').val() : null);
            var cotis = $(modalName + ' #ccCOTIS').is(":checked") ? 1 : 0;

            var actif = $(modalName + ' #ccACTIF').is(":checked") ? 1 : 0;
            var acces = $(modalName + ' #ccACCES').is(":checked") ? 1 : 0;
            var visible = $(modalName + ' #ccVISIBLE').is(":checked") ? 1 : 0;

            /** -----------------------------------------------------------------------------------
             * Un dernier petit contrôle
             */
            if (typeo == '' || statuto == '') {
                toastr.warning('Certaines données de base sont incomplètes.', "Attention");
                return;
            }
            if ((bi == 1 && bi_recep == '') ||
                (cf == 1 && cf_recep == '')) {
                toastr.warning('Vérifier les données complémentaires.', "Attention");
                return;
            }

            /** -----------------------------------------------------------------------------------
             * On va rajouter un contrôle de vérification des MAILS si l'une des zones
             * doit vérifier les mails
             */
            if (!ClassGENERIC.verifPresenceMail(modalName, bi, cf)) {
                toastr.warning('Vous devez renseigner au moins un mail.', "Attention");
                return;
            }

            if (!$(modalName + ' #ztNomParrain').parent().is(":visible")) {
                parrain = '';
            }

            $(this).off().removeClass('green').addClass('grey-cascade').html(
                '<i class="fa fa-spin fa-spinner"></i> Patientez...'
            );

            // ------------------------------------------------------------------------------------
            // Le reste (
            // ------------------------------------------------------------------------------------
            var tels = [];
            $.each($(modalName + ' #tableTel tbody tr td:first-child'), function (index, row) {
                var telephone = $(this).attr('data-telephone');
                var type = $(this).attr('data-type-telephone');
                var prefere = $(this).attr('data-prefere');
                var item = [telephone, prefere, type];
                tels.push(item);
            });
            var mails = [];
            $.each($(modalName + ' #tableMail tbody tr td:first-child'), function (index, row) {
                var mail = $(this).attr('data-mail');
                var type = $(this).attr('data-type-mail');
                var prefere = $(this).attr('data-prefere');
                var item = [mail, prefere, type];
                mails.push(item);
            });

            $.ajax({
                url: '/annuaire/' + routeOrga,
                type: 'POST',
                data: {
                    _token: $(modalName + ' #token').val(),
                    pNom: nom,
                    pSigle: sigle,
                    pCode: code,
                    pGestionnaire: gestionnaire,
                    pTypeo: typeo,
                    pDetailo: detailo,
                    pParrain: parrain,
                    pStatuto: statuto,
                    pAdr1: adr1,
                    pAdr2: adr2,
                    pAdr3: adr3,
                    pCP: cp,
                    pVille: ville,
                    pPays: pays,
                    pWeb: web,
                    pFiness: finess,
                    pSiret: siret,
                    pIban: iban,
                    pBic: bic,
                    pNl: nl,
                    pBI: bi,
                    pBI_recep: bi_recep,
                    pCF: cf,
                    pCF_recep: cf_recep,
                    pCotis: cotis,
                    pNotes: notes,

                    pTels: tels,
                    pMails: mails,

                    pActif: actif,
                    pAcces: acces,
                    pVisible: visible,
                },
                dataType: 'json',
                async:false,
                success: function (data) {
                    result = true;
                    $(modalName).modal('hide');
                    toastr.success('Saisie enregistrée', "Information");
                    tableOrga.ajax.reload();
                }
            });

        });
        $(modalName).on('hidden.bs.modal', function () {
            mode = MODE_DATATABLES;
        });

        //console.log($(modalName).css('z-index'));
        //$('input[type=checkbox]').css('z-index', '10050')
        waitingDialog.hide();
        $(modalName).modal('show');


    },

    initListes: function (modalName) {

        /** ---------------------------------------------------------------------------------------
         * Listes de bases
         */
        ClassGENERIC.initGeneric(modalName, 'typeorganisme', '#zlType', '', '#zlDetailType');
        ClassGENERIC.initGeneric(modalName, 'typestatutorganisme', '#zlStatut');

        ClassGENERIC.initGeneric(modalName, 'pays', '#zlPays');
        ClassGENERIC.initGeneric(modalName, 'reception', '#zlReceptionCF');
        ClassGENERIC.initGeneric(modalName, 'reception', '#zlReceptionBI');

        ClassGENERIC.initGeneric(modalName, 'typetel', '#portletTel #zlTypeSaisie');
        ClassGENERIC.initGeneric(modalName, 'typemail', '#portletMail #zlTypeSaisie');

    },

    manageLogo: function (idOO) {

        /** ---------------------------------------------------------------------------------------
         * Gestion des LOGO
         */
        var modalName = '#dialogExecLev9';
        mode = MODE_FORM;
        var modal = $(modalName);
        $(modalName).find('.modal-dialog').removeClass('modal-full').addClass('modal-full-lg');
        modal.html($('#formShowLogo').html());

        $(modalName + ' #ztLogo').fileinput('destroy');

        /** ---------------------------------------------------------------------------------------
         * Affichage des LOGOS
         */
        $(modalName).find('.modal-header').addClass('font-green-seagreen ');
        $(modalName).find('.modal-title').html('<i class="fa fa-file-picture-o"></i> Gestion du Logo');

        /** ---------------------------------------------------------------------------------------
         * On récupère les infos
         */
        $.ajax({
            url: '/annuaire/' + routeOrga + '/' + idOO,
            type: 'GET',
            dataType: 'json',
            async: true,
            success: function (data) {
                if (data.LOGO != '') {
                    $(modalName + ' #blocLogo').removeClass('display-none');
                    $(modalName + ' #picLogo').attr('src', 'data:image/gif;base64,' + data.OO.logo);
                }
            }
        });

        /** ---------------------------------------------------------------------------------------
         * INIT
         */
        $(modalName + ' #ztLogo').fileinput({
            language: 'fr',
            allowedFileExtensions: ['jpg', 'png'],
            fileActionSettings: {
                showZoom: false,
            },
            maxFileCount: 1,
            dropZoneEnabled: false,
            uploadUrl: '/annuaire/' + routeOrga + '/logo',
            uploadExtraData: {
                pId: idOO
            }
        });

        /** ---------------------------------------------------------------------------------------
         * BINDS
         */
        $(modalName + ' #blocLogo .fileinput-remove').on('click', function () {
            $.ajax({
                url: '/annuaire/' + routeOrga + '/logo/del',
                type: 'POST',
                data: {
                    pId: idOO
                },
                dataType: 'json',
                success: function (data) {
                    $(modalName + ' #blocLogo').addClass('display-none');
                    $(modalName + ' #picLogo').removeAttr('src');
                }
            });
        })

        $(modalName + ' #ztLogo').on('fileuploaded', function (event, data, previewId, index) {
            $(modalName + ' #blocLogo').removeClass('display-none');
            $(modalName + ' #picLogo').attr('src', 'data:image/png;base64,' + data.response.data);

            $(modalName + ' #ztLogo').fileinput('destroy').fileinput({
                language: 'fr',
                allowedFileExtensions: ['jpg', 'png'],
                fileActionSettings: {
                    showZoom: false,
                },
                maxFileCount: 1,
                dropZoneEnabled: false,
                uploadUrl: '/annuaire/' + routeOrga + '/logo',
                uploadExtraData: {
                    pId: idOO
                }
            });
            $(modalName + ' #formLogo').find('.file-preview .fileinput-remove').trigger('click');
            $(modalName + ' .file-caption').removeClass('file-caption-disabled');

        });
        $(modalName).on('hidden.bs.modal', function () {
            mode = MODE_DATATABLES;
        });

        $(modalName).modal('show');

    },

    update: function (idOO) {

        waitingDialog.show('Préparation du Formulaire', {dialogSize: 'large', progressType: 'info'});

        /** ---------------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        var modalName = ClassGENERIC.newLevel('');
        var modal = $(modalName);
        modal.html($('#formOrganisme').html());
        mode = MODE_FORM;

        /** ---------------------------------------------------------------------------------------
         * On récupère les infos de la civilite
         */
        $.ajax({
            url: '/annuaire/' + routeOrga + '/' + idOO,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {

                /** -------------------------------------------------------------------------------
                 * Chargement du template du modal dans le modal d'exécution
                 */
                $(modalName).find('#btnAction').addClass('yellow-gold').html('<i class="fa fa-plus-circle"></i> Modifier');
                $(modalName).find('.modal-header').addClass('font-yellow-casablanca');
                $(modalName).find('.modal-title').html('<i class="fa fa-refresh"></i> Modification ' + edit);

                $(modalName).find('.historique').show().html(
                    'Modifié le ' + data.UPDATED_DATE + ' par ' + data.UPDATED_BY
                );

                /** -------------------------------------------------------------------------------
                 * Y'a-t-il un LOGO ?
                 */
                if (data.OO.logo != '') {
                    $(modalName + ' #blocLogo').removeClass('display-none');
                    $(modalName + ' #picLogo').attr('src', 'data:image/png;base64,' + data.OO.logo);
                }

                /** -------------------------------------------------------------------------------
                 * Init des zones ORGANISME
                 */
                var nom = data.OO.nom;
                var sigle = data.OO.sigle;
                var code = data.OO.id_interne;
                var gestionnaire = data.OO.isGEST;
                var typeo = data.OO.type_organisme_id;
                var detailo = data.OO.detail_type_organisme_id;
                var parrain = data.OO.parrain;
                var statuto = data.OO.type_statut_id;
                var adr1 = data.OO.adr1;
                var adr2 = data.OO.adr2;
                var adr3 = data.OO.adr3;
                var cp = data.OO.cp;
                var ville = data.OO.ville;
                var pays = data.OO.pays_id;

                var web = data.OO.web;
                var notes = data.OO.notes;

                var finess = data.OO.finess;
                var siret = data.OO.siret;
                var iban = data.OO.iban;
                var bic = data.OO.bic;

                // ------------------------------------------------------------------------------------
                // Données COMPLEMENTAIRES
                // ------------------------------------------------------------------------------------
                var nl = data.OO.nl;
                var bi = data.OO.bi;
                var bi_recep = data.OO.bi_reception_id;
                var cf = data.OO.cf;
                var cf_recep = data.OO.cf_reception_id;
                var cotis = data.OO.cotis;

                var actif = data.OO.active;//$(modalName + ' #ccACTIF').is(":checked") ? 1 : 0;
                var acces = data.OO.acces;//$(modalName + ' #ccACCES').is(":checked") ? 1 : 0;
                var visible = data.OO.visible;//$(modalName + ' #ccVISIBLE').is(":checked") ? 1 : 0;


                $(modalName + ' #ztNom').val(nom);
                $(modalName + ' #ztSigle').val(sigle);
                $(modalName + ' #ztCodeInterne').val(code);
                $(modalName + ' #ztAdr1').val(adr1);
                $(modalName + ' #ztAdr2').val(adr2);
                $(modalName + ' #ztAdr3').val(adr3);
                $(modalName + ' #ztCP').val(cp);
                $(modalName + ' #ztVille').val(ville);
                $(modalName + ' #ztNotes').val(notes);
                $(modalName + ' #ztWEB').val(web);
                $(modalName + ' #ztNomParrain').val(parrain);
                if (parrain != '') {
                    $(modalName + ' #ztNomParrain').parent().show();
                }
                $(modalName + ' #ztFINESS').val(finess);
                $(modalName + ' #ztSIRET').val(siret);
                $(modalName + ' #ztIBAN').val(iban);
                $(modalName + ' #ztBIC').val(bic);

                ClassGENERIC.initGeneric(modalName, 'typeorganisme', '#zlType', typeo);
                ClassGENERIC.initGeneric(modalName, 'typestatutorganisme', '#zlStatut', statuto);
                ClassGENERIC.initGeneric(modalName, 'pays', '#zlPays', pays);

                /** ---------------------------------------------------------------------------------------
                 */
                //$(modalName).modal('show');

                /** -------------------------------------------------------------------------------
                 * Modification en cascade TYPE -> DETAIL
                 */
                if (typeo != null) {
                    ClassGENERIC.initGeneric(modalName, 'detailtypeorganisme', '#zlDetailType', detailo, typeo);
                }
                $('#portletTel .portlet-body').find('.row:first').show();
                $('#portletMail .portlet-body').find('.row:first').show();

                /** ---------------------------------------------------------------------------------------
                 * Si on a un DETAIL, on l'affiche
                 */
                $(modalName + ' #zlType').on('change', function () {
                    if ($(modalName + ' #zlType').val() != '') {
                        ClassGENERIC.initGeneric(modalName, 'detailtypeorganisme/' + $(modalName + ' #zlType').val(), '#zlDetailType', '');
                    } else {
                        $(modalName + ' #zlDetailType').empty();
                        $(modalName + ' #zlDetailType').parent().parent().addClass('display-none');
                    }
                });
                /** -------------------------------------------------------------------------------
                 * Données complémentaires
                 */

                $(modalName + ' #ccGestionnaire').prop('checked', (gestionnaire == 1 ? true : false));
                $(modalName + ' #ccNL').prop('checked', (nl == 1 ? true : false));
                $(modalName + ' #ccBI').prop('checked', (bi == 1 ? true : false));
                $(modalName + ' #ccCF').prop('checked', (cf == 1 ? true : false));
                $(modalName + ' #ccCOTIS').prop('checked', (cotis == 1 ? true : false));

                $(modalName + ' #ccACTIF').prop('checked', (actif == 1 ? true : false));
                $(modalName + ' #ccACCES').prop('checked', (acces == 1 ? true : false));
                $(modalName + ' #ccVISIBLE').prop('checked', (visible == 1 ? true : false));

                ClassGENERIC.initGeneric(modalName, 'reception', '#zlReceptionCF', cf_recep);
                ClassGENERIC.initGeneric(modalName, 'reception', '#zlReceptionBI', bi_recep);
                ClassGENERIC.initGeneric(modalName, 'typetel', '#portletTel #zlTypeSaisie');
                ClassGENERIC.initGeneric(modalName, 'typemail', '#portletMail #zlTypeSaisie');

                /** -----------------------------------------------------------------------------------
                 * Init des zones TELEPHONES + MAILS
                 */
                ClassGENERIC.displayTels(modalName, data.TEL);
                ClassGENERIC.displayMails(modalName, data.MAIL);

                ClassGENERIC.refreshEdited(modalName);

                /** ---------------------------------------------------------------------------------------
                 * Evènements
                 */
                ClassGENERIC.manageModalButtons(modalName, 0, 'oo', idOO);
                ClassGENERIC.updateRemoveButton(modalName, 'telephone');
                ClassGENERIC.updateRemoveButton(modalName, 'mail');
                ClassGENERIC.updatePrefereButton(modalName, 'telephone', 'oo');
                ClassGENERIC.updatePrefereButton(modalName, 'mail', 'oo');

                ClassGENERIC.manageModalCC(modalName);

                /** ---------------------------------------------------------------------------------------
                 * Attributs des listes par défaut
                 */
                if (bi == 0) {
                    $(modalName + ' #zlReceptionBI').select2('enable', false);
                }
                if (cf == 0) {
                    $(modalName + ' #zlReceptionCF').select2('enable', false);
                }

                /** -----------------------------------------------------------------------------------
                 * Bind de la modification
                 */
                $(":input").inputmask();
                $(modalName + ' #btnAction').unbind().on('click', function () {

                    /** -----------------------------------------------------------------------------------
                     * Vérification des ZONES Mandatory
                     */
                    var oblig = true;
                    $(modalName + ' .required').each(function () {
                        if (!$(this).val().trim()) {
                            oblig = false;
                        }
                    });
                    if (!oblig) {
                        toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                        return;
                    }

                    // ------------------------------------------------------------------------------------
                    // Données de BASE
                    // ------------------------------------------------------------------------------------
                    nom = $(modalName + ' #ztNom').val().trim().toUpperCase();
                    sigle = $(modalName + ' #ztSigle').val().trim().toUpperCase();
                    code = $(modalName + ' #ztCodeInterne').val().trim();
                    gestionnaire = $(modalName + ' #ccGestionnaire').is(":checked") ? 1 : 0;
                    typeo = $(modalName + ' #zlType').val();
                    detailo = $(modalName + ' #zlDetailType').val();
                    parrain = $(modalName + ' #ztNomParrain').val().toUpperCase();
                    statuto = $(modalName + ' #zlStatut').val();
                    adr1 = $(modalName + ' #ztAdr1').val().trim().toUpperCase();
                    adr2 = $(modalName + ' #ztAdr2').val().trim().toUpperCase();
                    adr3 = $(modalName + ' #ztAdr3').val().trim().toUpperCase();
                    cp = $(modalName + ' #ztCP').val();
                    ville = $(modalName + ' #ztVille').val().trim().toUpperCase();
                    pays = $(modalName + ' #zlPays').val();

                    web = $(modalName + ' #ztWEB').val();
                    notes = $('<p>' + $(modalName + ' #ztNotes').val() + '</p>').text();

                    finess = $(modalName + ' #ztFINESS').val().trim().toUpperCase();
                    siret = $(modalName + ' #ztSIRET').val().trim().toUpperCase();
                    iban = $(modalName + ' #ztIBAN').val().trim().toUpperCase();
                    bic = $(modalName + ' #ztBIC').val().trim().toUpperCase();

                    // ------------------------------------------------------------------------------------
                    // Données COMPLEMENTAIRES
                    // ------------------------------------------------------------------------------------
                    nl = $(modalName + ' #ccNL').is(":checked") ? 1 : 0;
                    bi = $(modalName + ' #ccBI').is(":checked") ? 1 : 0;
                    bi_recep = (bi == 1 ? $(modalName + ' #zlReceptionBI').val() : null);
                    cf = $(modalName + ' #ccCF').is(":checked") ? 1 : 0;
                    cf_recep = (cf == 1 ? $(modalName + ' #zlReceptionCF').val() : null);
                    cotis = $(modalName + ' #ccCOTIS').is(":checked") ? 1 : 0;

                    actif = $(modalName + ' #ccACTIF').is(":checked") ? 1 : 0;
                    acces = $(modalName + ' #ccACCES').is(":checked") ? 1 : 0;
                    visible = $(modalName + ' #ccVISIBLE').is(":checked") ? 1 : 0;

                    /** -----------------------------------------------------------------------------------
                     * On va rajouter un contrôle de vérification des MAILS si l'une des zones
                     * doit vérifier les mails
                     */
                    if (!ClassGENERIC.verifPresenceMail(modalName, bi, cf)) {
                        toastr.warning('Vous devez renseigner au moins un mail.', "Attention");
                        return;
                    }

                    if (!$(modalName + ' #ztNomParrain').parent().is(":visible")) {
                        parrain = '';
                    }

                    /** -----------------------------------------------------------------------------------
                     * Un dernier petit contrôle
                     */
                    if (typeo == '' || statuto == '') {
                        toastr.warning('Certaines données de base sont incomplètes.', "Attention");
                        return;
                    }
                    if ((bi == 1 && bi_recep == '') ||
                        (cf == 1 && cf_recep == '')) {
                        toastr.warning('Vérifier les données complémentaires.', "Attention");
                        return;
                    }

                    $(this).off().removeClass('green').addClass('grey-cascade').html(
                        '<i class="fa fa-spin fa-spinner"></i> Patientez...'
                    );

                    // ------------------------------------------------------------------------------------
                    // Les nouveaux téléphones uniquement
                    // ------------------------------------------------------------------------------------
                    var tels = [];
                    $.each($(modalName + ' #tableTel tbody tr td:first-child'), function (index, row) {
                        if ($(this).attr('data-pk') == undefined) {
                            var telephone = $(this).attr('data-telephone');
                            var prefere = $(this).attr('data-prefere');
                            var type = $(this).attr('data-type-telephone');
                            var item = [telephone, prefere, type];
                            tels.push(item);
                        }
                    });
                    var mails = [];
                    $.each($(modalName + ' #tableMail tbody tr td:first-child'), function (index, row) {
                        if ($(this).attr('data-pk') == undefined) {
                            var mail = $(this).attr('data-mail');
                            var prefere = $(this).attr('data-prefere');
                            var type = $(this).attr('data-type-mail');
                            var item = [mail, prefere, type];
                            mails.push(item)
                        }
                        ;
                    });

                    $.ajax({
                        url: '/annuaire/' + routeOrga + '/' + idOO,
                        type: 'PUT',
                        data: {
                            _token: $(modalName + ' #token').val(),
                            pNom: nom,
                            pSigle: sigle,
                            pCode: code,
                            pGestionnaire: gestionnaire,
                            pTypeo: typeo,
                            pDetailo: detailo,
                            pParrain: parrain,
                            pStatuto: statuto,
                            pAdr1: adr1,
                            pAdr2: adr2,
                            pAdr3: adr3,
                            pCP: cp,
                            pVille: ville,
                            pPays: pays,
                            pWeb: web,
                            pFiness: finess,
                            pSiret: siret,
                            pIban: iban,
                            pBic: bic,
                            pNl: nl,
                            pBI: bi,
                            pBI_recep: bi_recep,
                            pCF: cf,
                            pCF_recep: cf_recep,
                            pCotis: cotis,
                            pNotes: notes,

                            pActif: actif,
                            pAcces: acces,
                            pVisible: visible,

                        },
                        dataType: 'json',
                        success: function (data) {
                            if (data == -1) {
                                $(modalName).modal('hide');
                                toastr.error('Modification non effectuée', "Attention");
                            } else {
                                $(modalName).modal('hide');
                                toastr.info('Modification enregistrée', "Information");
                                tableOrga.ajax.reload();
                            }
                        }
                    });
                });
            }
        });

        $(modalName).on('hidden.bs.modal', function () {
            mode = MODE_DATATABLES;
        });
        waitingDialog.hide();
        $(modalName).modal('show');

    },

    view: function (idOO) {

        waitingDialog.show('Préparation du Formulaire', {dialogSize: 'large', progressType: 'info'});

        /** ---------------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        var modalName = '#dialogExecLev0';
        var modal = $(modalName);
        modal.html($('#formOrganisme').html());
        mode = MODE_FORM;

        /** ---------------------------------------------------------------------------------------
         * On récupère les infos de la civilite
         */
        $.ajax({
            url: '/annuaire/' + routeOrga + '/' + idOO,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {

                /** -------------------------------------------------------------------------------
                 * Chargement du template du modal dans le modal d'exécution
                 * Par défaut, les boutons ACTIONS sont cachés
                 */
                $(modalName).find('#btnAction').hide();
                $(modalName).find('#btnAddTel').hide();
                $(modalName).find('#btnAddMail').hide();
                $(modalName).find('.modal-header').addClass('font-grey-salsa');
                $(modalName).find('.modal-title').html('<i class="fa fa-refresh"></i> Consultation ' + edit);

                $(modalName).find('.historique').show().html(
                    'Modifié le ' + data.UPDATED_DATE + ' par ' + data.UPDATED_BY
                );

                /** -------------------------------------------------------------------------------
                 * Y'a-t-il un LOGO ?
                 */
                if (data.OO.logo != '') {
                    $(modalName + ' #blocLogo').removeClass('display-none');
                    $(modalName + ' #picLogo').attr('src', 'data:image/png;base64,' + data.OO.logo);
                }

                /** -------------------------------------------------------------------------------
                 * Init des zones ORGANISME
                 */
                var nom = data.OO.nom;
                var sigle = data.OO.sigle;
                var code = data.OO.id_interne;
                var gestionnaire = data.OO.isGEST;
                var typeo = data.OO.type_organisme_id;
                var detailo = data.OO.detail_type_organisme_id;
                var parrain = data.OO.parrain;

                var statuto = data.OO.type_statut_id;
                var adr1 = data.OO.adr1;
                var adr2 = data.OO.adr2;
                var adr3 = data.OO.adr3;
                var cp = data.OO.cp;
                var ville = data.OO.ville;
                var pays = data.OO.pays_id;

                var web = data.OO.web;
                var notes = data.OO.notes;

                var finess = data.OO.finess;
                var siret = data.OO.siret;
                var iban = data.OO.iban;
                var bic = data.OO.bic;

                // ------------------------------------------------------------------------------------
                // Données COMPLEMENTAIRES
                // ------------------------------------------------------------------------------------
                var nl = data.OO.nl;
                var bi = data.OO.bi;
                var bi_recep = data.OO.bi_reception_id;
                var cf = data.OO.cf;
                var cf_recep = data.OO.cf_reception_id;
                var cotis = data.OO.cotis;

                var actif = data.OO.active;//$(modalName + ' #ccACTIF').is(":checked") ? 1 : 0;
                var acces = data.OO.acces;//$(modalName + ' #ccACCES').is(":checked") ? 1 : 0;
                var visible = data.OO.visible;//$(modalName + ' #ccVISIBLE').is(":checked") ? 1 : 0;


                $(modalName + ' #ztNom').val(nom);
                $(modalName + ' #ztSigle').val(sigle);
                $(modalName + ' #ztCodeInterne').val(code);
                $(modalName + ' #ztAdr1').val(adr1);
                $(modalName + ' #ztAdr2').val(adr2);
                $(modalName + ' #ztAdr3').val(adr3);
                $(modalName + ' #ztCP').val(cp);
                $(modalName + ' #ztVille').val(ville);
                $(modalName + ' #ztNotes').val(notes);
                $(modalName + ' #ztWEB').val(web);

                $(modalName + ' #ztFINESS').val(finess);
                $(modalName + ' #ztSIRET').val(siret);
                $(modalName + ' #ztIBAN').val(iban);
                $(modalName + ' #ztBIC').val(bic);

                ClassGENERIC.initGeneric(modalName, 'typeorganisme', '#zlType', typeo);
                ClassGENERIC.initGeneric(modalName, 'typestatutorganisme', '#zlStatut', statuto);
                ClassGENERIC.initGeneric(modalName, 'pays', '#zlPays', pays);

                $(modalName + ' #ztNomParrain').val(parrain);
                if (parrain != ''){
                    $(modalName + ' #ztNomParrain').parent().show();
                }

                /** -------------------------------------------------------------------------------
                 * Modification en cascade TYPE -> DETAIL
                 */
                if (typeo != null) {
                    ClassGENERIC.initGeneric(modalName, 'detailtypeorganisme', '#zlDetailType', detailo, typeo);
                }

                /** -------------------------------------------------------------------------------
                 * Données complémentaires
                 */

                $(modalName + ' #ccGestionnaire').prop('checked', (gestionnaire == 1 ? true : false));
                $(modalName + ' #ccNL').prop('checked', (nl == 1 ? true : false));
                $(modalName + ' #ccBI').prop('checked', (bi == 1 ? true : false));
                $(modalName + ' #ccCF').prop('checked', (cf == 1 ? true : false));
                $(modalName + ' #ccCOTIS').prop('checked', (cotis == 1 ? true : false));

                $(modalName + ' #ccACTIF').prop('checked', (actif == 1 ? true : false));
                $(modalName + ' #ccACCES').prop('checked', (acces == 1 ? true : false));
                $(modalName + ' #ccVISIBLE').prop('checked', (visible == 1 ? true : false));

                ClassGENERIC.initGeneric(modalName, 'reception', '#zlReceptionCF', cf_recep);
                ClassGENERIC.initGeneric(modalName, 'reception', '#zlReceptionBI', bi_recep);
                $('#portletTel .portlet-body').find('.row:first').hide();
                $('#portletMail .portlet-body').find('.row:first').hide();

                /** -----------------------------------------------------------------------------------
                 * Init des zones TELEPHONES + MAILS
                 */
                $.each(data.TEL, function (index, item) {
                    $(modalName + ' #tableTel tbody').append(
                        '<tr>' +
                        '<td data-pk="' + item.OWN_ID + '" ' +
                        '    data-type-telephone="' + item.TYPE_ID + '"' +
                        '    data-id="' + item.OO_ID + '"' +
                        '>' + item.numero +
                        (item.prefere == 1
                                ? ' <i class="fa fa-star prefere font-yellow" style="cursor:pointer"> </i>'
                                : ' <i class="fa fa-star-o prefere" style="cursor:pointer"> </i>'
                        ) +
                        '</td>' +
                        '<td>' + item.libelle + '</td>' +
                        '<td>' + item.notes + '</td>' +
                        '</tr>'
                    );
                });
                $.each(data.MAIL, function (index, item) {
                    $(modalName + ' #tableMail tbody').append(
                        '<tr>' +
                        '<tr>' +
                        '<td data-pk="' + item.OWN_ID + '" ' +
                        '    data-type-mail="' + item.TYPE_ID + '"' +
                        '    data-id="' + item.OO_ID + '"' +
                        '>' + item.mail +
                        (item.prefere == 1
                                ? ' <i class="fa fa-star prefere font-yellow" style="cursor:pointer"> </i>'
                                : ' <i class="fa fa-star-o prefere" style="cursor:pointer"> </i>'
                        ) +
                        '</td>' +
                        '<td>' + item.libelle + '</td>' +
                        '<td class="text-right">' +
                        '<a class="remove-mail btn btn-xs red"' +
                        ' data-placement="left" data-toggle="confirmation" data-original-title="" title="" ' +
                        '><i class="fa fa-remove"></i></a>' +
                        //'<a class="modif btn btn-xs blue"><i class="fa fa-edit"></i></a>' +
                        '</td>' +
                        '</tr>'
                    );
                });

                /** -------------------------------------------------------------------------------
                 * Modification de l'aspect final du formulaires. Les zones de saisie prennent la
                 * classe edited. Les zones deviennent readonly et les boutons d'action sont
                 * cachés (au pire, comme il n'y a pas d'action, il ne se passerait rien...
                 */
                ClassGENERIC.refreshEdited(modalName);
<<<<<<< HEAD
                $(modalName).find('.form-control').each(function () {
                    //console.log($(this).nodeName());
                    $(this).prop('readonly', true);
                });
                $(modalName + ' input').each(function () {
                    $(this).prop('readonly', true);
                });
                $(modalName).find('.remove-telephone').hide();
                $(modalName).find('.remove-mail').hide();
=======
                ClassGENERIC.doReadOnly(modalName);
>>>>>>> debug8

            }
        });

        $(modalName).on('hidden.bs.modal', function () {
            mode = MODE_DATATABLES;
        })

        waitingDialog.hide();
        $(modalName).modal('show');
    },

    manageActions: function () {

        /** ---------------------------------------------------------------------------------------
         * Ajout d'une PERSONNE PHYSIQUE
         */
        $('#btnAdd').click(function () {
            ClassOO.add();
        });
    },

    /** -------------------------------------------------------------------------------------------
     * Ajout d'une LIGNE dans un TABLE
     */
    manageModalButtons: function (modalName, add, type, idOO) {

        var addTel = function () {

            /** -----------------------------------------------------------------------------------
             * Vérification des ZONES Mandatory
             */
            var num = $(modalName + ' #ztNumTel').val().trim().toUpperCase();
            var typetel = $(modalName + ' #zlTypeTel').select2('val');
            var typeteltxt = $(modalName + ' #zlTypeTel').select2('data');
            num.replace(' ', '');
            typeteltxt = typeteltxt[0].text;
            if (num == '' || typetel == '') {
                toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                return;
            }

            /** -----------------------------------------------------------------------------------
             * On vérifie la non existence dans la table
             */
            var stop = false;
            $(modalName + ' #tableTel tbody tr td:first-child').each(function () {
                if ($(this).attr('data-telephone') == num) {
                    stop = true;
                }
            });
            if (stop) {
                return false;
            }

            /** ---------------------------------------------------------------------------------------
             * Anti-rebond
             */
            $(modalName + ' #btnAddTel').off();

            /** -----------------------------------------------------------------------------------
             * On ajoute simplement dans la liste
             * Attention, si on est en mode ADD, on a 4 colonnes, sinon, on en n'a que 3
             */
            var pk = undefined;
            if (idOO != undefined) {
                $.ajax({
                    url: '/annuaire/telephone',
                    type: 'POST',
                    data: {
                        _token: $(modalName + ' #token').val(),
                        pNum: num,
                        pTypeTel: typetel,
                        pType: type,
                        pID: idOO
                    },
                    dataType: "json",
                    async: false
                }).success(function (data) {
                    pk = data.ID;
                });
            }

            $(modalName + ' #tableTel tbody').append(
                '<tr>' +
                '<td ' +
                '   data-id="' + idOO + '" ' +
                '   data-telephone="' + num + '" ' +
                (pk != undefined ? ' data-pk="' + pk + '" ' : '') +
                '   data-type-telephone="' + typetel + '"' +
                '>' + num + '</td>' +
                '<td>' + typeteltxt + '</td>' +
                (add == 1 ?
                    '<td class="text-right">' +
                    '<a href="#" ' +
                    '   class="editable edit-telephone" data-type="text"  data-id="' + pk + '" ' +
                    '   data-title="Saisir les notes" data-value=""  data-placement="right" data-mode="inline"></a>' +
                    '</td>' : '') +
                '<td class="text-right">' +
                '<a class="remove-telephone btn btn-xs red"' +
                ' data-placement="left" data-toggle="confirmation" data-original-title="" title="" ' +
                '><i class="fa fa-remove"></i></a>' +
                '</td>' +
                '</tr>'
            );

            $(modalName + ' #ztNumTel').val('');
            $(modalName + ' #zlTypeTel').select2('val', 0);
            ClassGENERIC.editGeneric('telephone', 'telephone', modalName);
            $(modalName + ' #btnAddTel').on('click', addTel);

            /** -----------------------------------------------------------------------------------
             * Un petit rajout à cause du DOM
             */
            $(modalName + ' .remove-telephone').confirmation({
                btnOkLabel: 'OUI',
                btnOkClass: 'red-mint btn-xs',
                btnCancelLabel: 'NON',
                placement: 'left',
                title: 'Suppression ?',
                singleton: true,
                popout: true,
                onConfirm: function () {
                    var pk = $(this).closest('tr').children('td:first').attr('data-pk');
                    if (pk != undefined) {
                        $.ajax({
                            url: '/annuaire/telephone/' + pk,
                            type: 'DELETE',
                            dataType: 'json',
                            data: {
                                _token: $(modalName + ' #token').val(),
                            },
                            async: false
                        }).complete(function (code_html, statut) {
                        });
                    } else {

                    }
                    $(this).closest('tr').remove();
                }
            });

        };
        var addMail = function () {

            /** -----------------------------------------------------------------------------------
             * Vérification des ZONES Mandatory
             */
            var mail = $(modalName + ' #ztAdrMail').val().trim();
            var typemail = $(modalName + ' #zlTypeMail').select2('val');
            var typemailtxt = $(modalName + ' #zlTypeMail').select2('data');

            if (!ClassGENERIC.validMail(mail)) {
                toastr.warning('Cette adresse mail n\'est pas valide.', "Attention");
                return;
            }

            typemailtxt = typemailtxt[0].text;
            if (mail == '' || typemail == '') {
                toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                return;
            }

            /** -----------------------------------------------------------------------------------
             * On vérifie la non existence dans la table
             */
            var stop = false;
            $(modalName + ' #tableMail tbody tr td:first-child').each(function () {
                if ($(this).attr('data-mail') == mail) {
                    stop = true;
                }
            });
            if (stop) {
                return false;
            }

            /** ---------------------------------------------------------------------------------------
             * Anti-rebond
             */
            $(modalName + ' #btnAddMail').off();

            /** -----------------------------------------------------------------------------------
             * On ajoute simplement dans la liste
             * Attention, si on est en mode ADD, on a 4 colonnes, sinon, on en n'a que 3
             */
            var pk = undefined;
            if (idOO != undefined) {
                $.ajax({
                    url: '/annuaire/mail',
                    type: 'POST',
                    data: {
                        _token: $(modalName + ' #token').val(),
                        pMail: mail,
                        pTypeMail: typemail,
                        pType: type,
                        pID: idOO
                    },
                    dataType: "json",
                    async: false
                }).success(function (data) {
                    pk = data.ID;
                });
            }

            $(modalName + ' #tableMail tbody').append(
                '<tr>' +
                '<td ' +
                '   data-id="' + idOO + '" ' +
                '   data-mail = "' + mail + '"' +
                (pk != undefined ? ' data-pk="' + pk + '" ' : '') +
                '   data-type-mail="' + typemail + '"' +
                '>' + mail + '</td>' +
                '<td>' + typemailtxt + '</td>' +
                (add == 1 ?
                    '<td class="text-right">' +
                    '<a href="#" ' +
                    '   class="editable edit-mail" data-type="text"  data-id="' + pk + '" ' +
                    '   data-title="Saisir les notes" data-value=""  data-placement="right" data-mode="inline"></a>' +
                    '</td>' : '') +
                '<td class="text-right">' +
                '<a class="remove-mail btn btn-xs red"' +
                ' data-placement="left" data-toggle="confirmation" data-original-title="" title="" ' +
                '><i class="fa fa-remove"></i></a>' +
                '</td>' +
                '</tr>'
            );
            $(modalName + ' #ztAdrMail').val('');
            $(modalName + ' #zlTypeMail').select2('val', 0);
            ClassGENERIC.editGeneric('mail', 'mails', modalName);
            $(modalName + ' #btnAddMail').on('click', addMail);

            /** -----------------------------------------------------------------------------------
             * Un petit rajout à cause du DOM
             */
            $(modalName + ' .remove-mail').confirmation({
                btnOkLabel: 'OUI',
                btnOkClass: 'red-mint btn-xs',
                btnCancelLabel: 'NON',
                placement: 'left',
                title: 'Suppression ?',
                singleton: true,
                popout: true,
                onConfirm: function () {
                    var pk = $(this).closest('tr').children('td:first').attr('data-pk');
                    if (pk != undefined) {
                        $.ajax({
                            url: '/annuaire/mail/' + pk,
                            type: 'DELETE',
                            dataType: 'json',
                            data: {
                                _token: $(modalName + ' #token').val(),
                            },
                            async: false
                        }).complete(function (code_html, statut) {
                        });
                    } else {

                    }
                    $(this).closest('tr').remove();
                }
            });

        };

        /** ---------------------------------------------------------------------------------------
         * Si on est en mode AJOUT, la variable oo_id est renseignée, donc on rajoute directement
         * les infos dans la base
         */
        $(modalName + ' #btnAddTel').on('click', addTel);
        $(modalName + ' #btnAddMail').on('click', addMail);

    },

};

/** -----------------------------------------------------------------------------------------------
 * @class : POLES
 * Version simplifiée
 */
var ClassPO = {

    /** -------------------------------------------------------------------------------------------
     * GESTION DES POLES
     *
     * @param level
     * @param idOO
     */
    managePoles: function (level, idOO) {

        /** ---------------------------------------------------------------------------------------
         * Gestion du LEVEL
         */
        var modalName = ClassGENERIC.newLevel(level);
        var modal = $(modalName);

        /** ---------------------------------------------------------------------------------------
         * Changement du bon formulaire
         */
        modal.html($('#formListPole').html());
        $(modalName).find('.modal-dialog').removeClass('modal-full').addClass('modal-full-lg');

        /** ---------------------------------------------------------------------------------------
         * Ecriture du libellé parent
         */
        var titre = ClassGENERIC.arianeToTxt();
        $(modalName + ' .modal-header .col-md-12').empty().append(
            titre
        );

        ClassPO.initDataTable(modalName, idOO);

        /** ---------------------------------------------------------------------------------------
         * Affichage des PÔLES d'un ORGANISME
         */
        $(modalName).unbind().on('click', '#btnAddPole', function () {
            ClassPO.add(modalName, idOO);
        });

        $(modalName).on('hidden.bs.modal', function (e) {
            if (mode == MODE_DATATABLES) {
                ariane.pop();
            }
            tablePole.ajax.reload();
        });

        $(modalName).modal('show');

    },

    /** -------------------------------------------------------------------------------------------
     * On récupère les PÔLES d'un ORGANISME (d'où le paramètre idOO)
     */
    initDataTable: function (modalName, idOO) {

        tablePole = $(modalName + ' #' + tableNamePole).removeAttr('width').DataTable({
            "dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
            iDisplayLength:50,
            ajax: "/datatable/" + routePole + '/' + idOO,
            serverSide: false,
            autoWidth: false,
            columnDefs: [
                {className: "dt-body-left", "targets": [0, 1, 2, 3]},
                {className: "dt-body-right", "targets": [4]}
            ],
            columns: [
                {orderable: true, width: 100}, null, null, null,
                {orderable: true, width: 110},
            ],
            lengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "Tous"]
            ],

            //fixedColumns: true,
            sPagingType: "bootstrap_full_numbers",
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
            },
            initComplete: function (settingssettings) {
                /** On change les largeurs par défaut des contrôles */
                var tableWrapper = $(modalName + ' #' + tableNamePole + '_wrapper');
                var tableLength = $(modalName + ' #' + tableNamePole + '_length');
                tableLength.css('width', '400px');
                tableWrapper.find('.dataTables_length select').addClass("form-control input-inline").select2();
                tableWrapper.find('.dataTables_filter input').addClass("form-control input-inline");
            },
            rowCallback: function (row, data, index) {

                var colStatut = 99;
                var colAction = 4;

                $('td:eq(1),td:eq(2)', row).addClass("txtSmall");
                $('td:eq(' + colAction + ')', row).find('.dropdown-toggle').dropdown();

                /** --------------------------------------------------------------------------------
                 * On checke la colonne ACTION - lien A
                 */
                $('td:eq(' + colAction + ')', row).children('a').bind('click', function () {
                    if ($(this).hasClass('visu')) {
                        ClassPO.view(modalName, $(this).attr('data-id'));
                    }
                });
                $('td:eq(' + colAction + ')', row).find('a').bind('click', function () {

                    var lib = $(this).closest('tr').find('td:eq(1)').find('span').attr('data-nom');
                    if ($(this).hasClass('personnel')) {
                        mode = MODE_DATATABLES;
                        ariane.push(lib);
                        ClassPERSONNEL.managePersonnel(modalName, null, $(this).attr('data-id'), null, null);
                    }
                    if ($(this).hasClass('etabl')) {
                        mode = MODE_DATATABLES;
                        ariane.push(lib);
                        ClassET.manageEtabls(modalName, null, $(this).attr('data-id'));
                    }
                    if ($(this).hasClass('modif')) {
                        ClassPO.update(modalName, $(this).attr('data-id'));
                    }
                    if ($(this).hasClass('telephone')) {
                        ClassGENERIC.phoneList('#dialogExecLev9', 'po', $(this).attr('data-id'));
                    }
                    if ($(this).hasClass('mail')) {
                        ClassGENERIC.mailList('#dialogExecLev9', 'po', $(this).attr('data-id'));
                    }
                    if ($(this).hasClass('offres')) {
                        ClassGENERIC.offreList('#dialogExecLev9', 'po', $(this).attr('data-id'));
                    }
                });
            }
        });


        return true;
    },

    add: function (level, idOO) {

        waitingDialog.show('Préparation du Formulaire', {dialogSize: 'large', progressType: 'info'});
        var modalName = ClassGENERIC.newLevel(level);
        var modal = $(modalName);
        modal.html($('#formPole').html());
        mode = MODE_FORM;

        /** ---------------------------------------------------------------------------------------
         * Fil d'Ariane
         *
         * Attention, il y a 2 header dans les FORMS (hors ORGANISME)
         * donc il faut bien distinguer celui avec le fil d'ariane et celui avec le CAPTION !
         */
        var titre = ClassGENERIC.arianeToTxt();
        $(modalName + ' #prec').empty().append(
            titre
        );
<<<<<<< HEAD
        $(modalName).find('.complement-footer').hide();
=======
>>>>>>> debug8

        /** ---------------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        $(modalName).find('#btnAction').addClass('green').html('<i class="fa fa-plus-circle"></i> Ajouter');
        $(modalName).find('.modal-header.caption').addClass('font-green');
        $(modalName).find('.modal-title').html('<i class="fa fa-plus-circle"></i> Ajout ' + editPole);
        $(modalName).find('.historique').hide();

        /** ---------------------------------------------------------------------------------------
         * Init des LISTEs
         */
        ClassPO.initListes(modalName);
        $(modalName + ' #zlReceptionCF').select2('enable', false);
        $(modalName + ' #zlReceptionBI').select2('enable', false);
        $('#portletTel .portlet-body').find('.row:first').show();
        $('#portletMail .portlet-body').find('.row:first').show();
        $(modalName + ' #portletET').addClass("display-none");

        /** ---------------------------------------------------------------------------------------
         *
         */
        ClassGENERIC.manageModalButtons(modalName, 0, 'po', undefined);
        ClassGENERIC.manageModalCC(modalName);

        /** ---------------------------------------------------------------------------------------
         * Bouton de recopie de l'adresse
         */
        $(modalName + ' #btnCopieDataOO').unbind().on('click', function () {
            ClassPO.recopieDataOO(idOO, modalName);
            ClassGENERIC.refreshEdited(modalName);
        });

        /** ---------------------------------------------------------------------------------------
         * Sauvegarde
         */
        $(":input").inputmask();
        $(modalName + ' #btnAction').unbind().on('click', function () {

            /** -----------------------------------------------------------------------------------
             * Vérification des ZONES Mandatory
             */
            var oblig = true;
            $(modalName + ' .required').each(function () {
                if (!$(this).val().trim()) {
                    oblig = false;
                }
            });
            if (!oblig) {
                toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                return;
            }

            // ------------------------------------------------------------------------------------
            // Données de BASE
            // ------------------------------------------------------------------------------------
            var nom = $(modalName + ' #ztNom').val().trim().toUpperCase();
            var sigle = $(modalName + ' #ztSigle').val().trim().toUpperCase();
            var typep = $(modalName + ' #zlTypePole').val();
            var publiq = $(modalName + ' #zlTypePublic').val();
            var detail = $(modalName + ' #zlDetailTypePublic').val();
            var nbplaces = $(modalName + ' #ztNbPlaces').val();
            var age = $(modalName + ' #ztAge').val();
            var adr1 = $(modalName + ' #ztAdr1').val().trim().toUpperCase();
            var adr2 = $(modalName + ' #ztAdr2').val().trim().toUpperCase();
            var adr3 = $(modalName + ' #ztAdr3').val().trim().toUpperCase();
            var cp = $(modalName + ' #ztCP').val();
            var ville = $(modalName + ' #ztVille').val().trim().toUpperCase();
            var pays = $(modalName + ' #zlPays').val();

            var web = $(modalName + ' #ztWEB').val();
            var notes = $('<p>' + $(modalName + ' #ztNotes').val() + '</p>').text();

            // ------------------------------------------------------------------------------------
            // Données COMPLEMENTAIRES
            // ------------------------------------------------------------------------------------
            var nl = $(modalName + ' #ccNL').is(":checked") ? 1 : 0;
            var bi = $(modalName + ' #ccBI').is(":checked") ? 1 : 0;
            var bi_recep = (bi == 1 ? $(modalName + ' #zlReceptionBI').val() : null);
            var cf = $(modalName + ' #ccCF').is(":checked") ? 1 : 0;
            var cf_recep = (cf == 1 ? $(modalName + ' #zlReceptionCF').val() : null);
            var contrib = $(modalName + ' #ccCONT').is(":checked") ? 1 : 0;

            var active = $(modalName + ' #ccACTIF').is(":checked") ? 1 : 0;
            var acces = $(modalName + ' #ccACCES').is(":checked") ? 1 : 0;
            var visible = $(modalName + ' #ccVISIBLE').is(":checked") ? 1 : 0;

            /** -----------------------------------------------------------------------------------
             * Un dernier petit contrôle
             */
            if (typep == '' || publiq == '') {
                toastr.warning('Certaines données de base sont incomplètes.', "Attention");
                return;
            }
            if ((bi == 1 && bi_recep == '') ||
                (cf == 1 && cf_recep == '')) {
                toastr.warning('Vérifier les données complémentaires.', "Attention");
                return;
            }

            /** -----------------------------------------------------------------------------------
             * On va rajouter un contrôle de vérification des MAILS si l'une des zones
             * doit vérifier les mails
             */
            if (!ClassGENERIC.verifPresenceMail(modalName, bi, cf)) {
                toastr.warning('Vous devez renseigner au moins un mail.', "Attention");
                return;
            }

            $(this).off().removeClass('green').addClass('grey-cascade').html(
                '<i class="fa fa-spin fa-spinner"></i> Patientez...'
            );

            // ------------------------------------------------------------------------------------
            // Le reste (
            // ------------------------------------------------------------------------------------
            var tels = [];
            $.each($(modalName + ' #tableTel tbody tr td:first-child'), function (index, row) {
                var telephone = $(this).attr('data-telephone');
                var type = $(this).attr('data-type-telephone');
                var prefere = $(this).attr('data-prefere');
                var item = [telephone, prefere, type];
                tels.push(item);
            });
            var mails = [];
            $.each($(modalName + ' #tableMail tbody tr td:first-child'), function (index, row) {
                var mail = $(this).attr('data-mail');
                var type = $(this).attr('data-type-mail');
                var prefere = $(this).attr('data-prefere');
                var item = [mail, prefere, type];
                mails.push(item);
            });

            $.ajax({
                url: '/annuaire/' + routePole,
                type: 'POST',
                data: {
                    _token: $(modalName + ' #token').val(),
                    pOO: idOO,
                    pNom: nom,
                    pSigle: sigle,
                    pTypeP: typep,
                    pPublic: publiq,
                    pDetail: detail,
                    pNbPlaces: nbplaces,
                    pAge: age,
                    pAdr1: adr1,
                    pAdr2: adr2,
                    pAdr3: adr3,
                    pCP: cp,
                    pVille: ville,
                    pPays: pays,
                    pWeb: web,
                    pNotes: notes,
                    pNl: nl,
                    pBI: bi,
                    pBI_recep: bi_recep,
                    pCF: cf,
                    pCF_recep: cf_recep,
                    pContrib: contrib,
                    pTels: tels,
                    pMails: mails,
                    pActif: active,
                    pAcces: acces,
                    pVisible: visible,
                },
                dataType: 'json',
                success: function (data) {
                    result = true;
                    $(modalName).modal('hide');
                    toastr.success('Saisie enregistrée', "Information");
                    tablePole.ajax.reload();
                }
            });

        });

        $(modalName).on('hidden.bs.modal', function () {
            mode = MODE_DATATABLES;
        })
        waitingDialog.hide();
        $(modalName).modal('show');

    },

    update: function (level, idPO) {

        waitingDialog.show('Préparation du Formulaire', {dialogSize: 'large', progressType: 'info'});
        var modalName = ClassGENERIC.newLevel(level);
        var modal = $(modalName);
        modal.html($('#formPole').html());
        mode = MODE_FORM;

        /** ---------------------------------------------------------------------------------------
         * Fil d'Ariane
         *
         * Attention, il y a 2 header dans les FORMS (hors ORGANISME)
         * donc il faut bien distinguer celui avec le fil d'ariane et celui avec le CAPTION !
         */
        var titre = ClassGENERIC.arianeToTxt();
        $(modalName + ' #prec').empty().append(
            titre
        );

        /** ---------------------------------------------------------------------------------------
         * On récupère les infos de la civilite
         */
        $.ajax({
            url: '/annuaire/' + routePole + '/' + idPO,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {

                /** -------------------------------------------------------------------------------
                 * Chargement du template du modal dans le modal d'exécution
                 */
                $(modalName).find('#btnAction').addClass('yellow-gold').html('<i class="fa fa-plus-circle"></i> Modifier');
                $(modalName).find('.modal-header.caption').addClass('font-yellow-casablanca');
                $(modalName).find('.modal-title').html('<i class="fa fa-refresh"></i> Modification ' + editPole);

                $(modalName).find('.historique').show().html(
                    'Modifié le ' + data.UPDATED_DATE + ' par ' + data.UPDATED_BY
                );

                // ------------------------------------------------------------------------------------
                // Nouvelles données ET
                // ------------------------------------------------------------------------------------
                var setVisible = false;
                $(modalName + ' #zlTypePublic').empty();
                $.each(data.TYPE_PUBLIC, function (index, item) {
                    $(modalName + ' #zlTypePublic').append(
                        '<option value="' + index + '" selected>'+ item + '</option>'
                    );
                    setVisible = true;
                });
                $(modalName + ' #zlTypePublic').select2({
                    width:'100%'
                }).trigger('change');


                $(modalName + ' #zlTrancheAge').empty();
                $.each(data.TYPE_AGE, function (index, item) {
                    $(modalName + ' #zlTrancheAge').append(
                        '<option value="' + index + '" selected>'+ item + '</option>'
                    );
                    setVisible = true;
                });
                $(modalName + ' #zlTrancheAge').select2({
                    width:'100%'
                }).trigger('change');

                $(modalName + ' #znNbPlacesET').val(data.NB_PLACES);
                if (setVisible) {
                    $(modalName + ' #portletET').removeClass("display-none");
                }

                /** -------------------------------------------------------------------------------
                 * Init des zones ORGANISME
                 */

                var nom = data.PO.nom;
                var idOO = data.PO.organisme_id;
                var sigle = data.PO.sigle;
                var typep = data.PO.type_pole_id;
                var publiq = data.PO.type_public_id;
                var detail = data.PO.detail_type_public_id;
                var nbplaces = data.PO.nb_places;
                var age = data.PO.age;
                var adr1 = data.PO.adr1;
                var adr2 = data.PO.adr2;
                var adr3 = data.PO.adr3;
                var cp = data.PO.cp;
                var ville = data.PO.ville;
                var pays = data.PO.pays_id;

                var web = data.PO.web;
                var notes = data.PO.notes;

                // ------------------------------------------------------------------------------------
                // Données COMPLEMENTAIRES
                // ------------------------------------------------------------------------------------
                var nl = data.PO.nl;
                var bi = data.PO.bi;
                var bi_recep = data.PO.bi_reception_id;
                var cf = data.PO.cf;
                var cf_recep = data.PO.cf_reception_id;
                var contrib = data.PO.isCONTRIBUANT;

                var actif = data.PO.active;
                var acces = data.PO.acces;
                var visible = data.PO.visible;

                $(modalName + ' #ztNom').val(nom);
                $(modalName + ' #ztNom').focus();
                $(modalName + ' #ztSigle').val(sigle);
                $(modalName + ' #ztNbPlaces').val(nbplaces);
                $(modalName + ' #ztAge').val(age);

                $(modalName + ' #ztAdr1').val(adr1);
                $(modalName + ' #ztAdr2').val(adr2);
                $(modalName + ' #ztAdr3').val(adr3);
                $(modalName + ' #ztCP').val(cp);
                $(modalName + ' #ztVille').val(ville);
                $(modalName + ' #ztNotes').val(notes);
                $(modalName + ' #ztWEB').val(web);

                ClassGENERIC.initGeneric(modalName, 'typepole', '#zlTypePole', typep);
                ClassGENERIC.initGeneric(modalName, 'typepublic', '#zlTypePublic', publiq);
                ClassGENERIC.initGeneric(modalName, 'pays', '#zlPays', pays);
                if (publiq != '') {
                    ClassGENERIC.initGeneric(modalName, 'detailtypepublic', '#zlDetailTypePublic', detail, publiq);
                }
                /** -------------------------------------------------------------------------------
                 * Données complémentaires
                 */
                $(modalName + ' #ccNL').prop('checked', (nl == 1 ? true : false));
                $(modalName + ' #ccBI').prop('checked', (bi == 1 ? true : false));
                $(modalName + ' #ccCF').prop('checked', (cf == 1 ? true : false));
                $(modalName + ' #ccCONT').prop('checked', (contrib == 1 ? true : false));

                $(modalName + ' #ccACTIF').prop('checked', (actif == 1 ? true : false));
                $(modalName + ' #ccACCES').prop('checked', (acces == 1 ? true : false));
                $(modalName + ' #ccVISIBLE').prop('checked', (visible == 1 ? true : false));

                ClassGENERIC.initGeneric(modalName, 'reception', '#zlReceptionCF', cf_recep);
                ClassGENERIC.initGeneric(modalName, 'reception', '#zlReceptionBI', bi_recep);
                ClassGENERIC.initGeneric(modalName, 'typetel', '#portletTel #zlTypeSaisie');
                ClassGENERIC.initGeneric(modalName, 'typemail', '#portletMail #zlTypeSaisie');
                $('#portletTel .portlet-body').find('.row:first').show();
                $('#portletMail .portlet-body').find('.row:first').show();

                /** ---------------------------------------------------------------------------------------
                 * Bouton de recopie de l'adresse
                 */
                $(modalName + ' #btnCopieDataOO').unbind().on('click', function () {
                    ClassPO.recopieDataOO(idOO, modalName);
                    ClassGENERIC.refreshEdited(modalName);
                });

                /** -----------------------------------------------------------------------------------
                 * Init des zones TELEPHONES + MAILS
                 */
                ClassGENERIC.displayTels(modalName, data.TEL);
                ClassGENERIC.displayMails(modalName, data.MAIL);

                ClassGENERIC.refreshEdited(modalName);

                /** ---------------------------------------------------------------------------------------
                 * Evènements
                 */
                ClassGENERIC.manageModalButtons(modalName, 0, 'po', idPO);
                ClassGENERIC.updateRemoveButton(modalName, 'telephone', 'po');
                ClassGENERIC.updateRemoveButton(modalName, 'mail', 'po');
                ClassGENERIC.updatePrefereButton(modalName, 'telephone', 'po');
                ClassGENERIC.updatePrefereButton(modalName, 'mail', 'po');

                ClassGENERIC.manageModalCC(modalName);

                /** ---------------------------------------------------------------------------------------
                 * Attributs des listes par défaut
                 */
                if (bi == 0) {
                    $(modalName + ' #zlReceptionBI').select2('enable', false);
                }
                if (cf == 0) {
                    $(modalName + ' #zlReceptionCF').select2('enable', false);
                }

                /** -----------------------------------------------------------------------------------
                 * Bind de la modification
                 */
                $(":input").inputmask();
                $(modalName + ' #btnAction').unbind().on('click', function () {

                    /** -----------------------------------------------------------------------------------
                     * Vérification des ZONES Mandatory
                     */
                    var oblig = true;
                    $(modalName + ' .required').each(function () {
                        if (!$(this).val().trim()) {
                            oblig = false;
                        }
                    });
                    if (!oblig) {
                        toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                        return;
                    }

                    // ----------------------------------------------------------------------------
                    // Données de BASE
                    // ----------------------------------------------------------------------------
                    nom = $(modalName + ' #ztNom').val().trim().toUpperCase();
                    sigle = $(modalName + ' #ztSigle').val().trim();
                    typep = $(modalName + ' #zlTypePole').val();
                    publiq = $(modalName + ' #zlTypePublic').val();
                    detail = $(modalName + ' #zlDetailTypePublic').val();
                    nbplaces = $(modalName + ' #ztNbPlaces').val();
                    age = $(modalName + ' #ztAge').val();
                    adr1 = $(modalName + ' #ztAdr1').val().trim().toUpperCase();
                    adr2 = $(modalName + ' #ztAdr2').val().trim().toUpperCase();
                    adr3 = $(modalName + ' #ztAdr3').val().trim().toUpperCase();
                    cp = $(modalName + ' #ztCP').val();
                    ville = $(modalName + ' #ztVille').val().trim().toUpperCase();
                    pays = $(modalName + ' #zlPays').val();

                    web = $(modalName + ' #ztWEB').val();

                    // ----------------------------------------------------------------------------
                    // Données COMPLEMENTAIRES
                    // ----------------------------------------------------------------------------
                    nl = $(modalName + ' #ccNL').is(":checked") ? 1 : 0;
                    bi = $(modalName + ' #ccBI').is(":checked") ? 1 : 0;
                    bi_recep = (bi == 1 ? $(modalName + ' #zlReceptionBI').val() : null);
                    cf = $(modalName + ' #ccCF').is(":checked") ? 1 : 0;
                    cf_recep = (cf == 1 ? $(modalName + ' #zlReceptionCF').val() : null);
                    contrib = $(modalName + ' #ccCONT').is(":checked") ? 1 : 0;
                    notes = $('<p>' + $(modalName + ' #ztNotes').val() + '</p>').text();

                    actif = $(modalName + ' #ccACTIF').is(":checked") ? 1 : 0;
                    acces = $(modalName + ' #ccACCES').is(":checked") ? 1 : 0;
                    visible = $(modalName + ' #ccVISIBLE').is(":checked") ? 1 : 0;

                    /** -----------------------------------------------------------------------------------
                     * Un dernier petit contrôle
                     */
                    if (typep == '' || publiq == '') {
                        toastr.warning('Certaines données de base sont incomplètes.', "Attention");
                        return;
                    }
                    if ((bi == 1 && bi_recep == '') ||
                        (cf == 1 && cf_recep == '')) {
                        toastr.warning('Vérifier les données complémentaires.', "Attention");
                        return;
                    }

                    /** -----------------------------------------------------------------------------------
                     * On va rajouter un contrôle de vérification des MAILS si l'une des zones
                     * doit vérifier les mails
                     */
                    if (!ClassGENERIC.verifPresenceMail(modalName, bi, cf)) {
                        toastr.warning('Vous devez renseigner au moins un mail.', "Attention");
                        return;
                    }

                    $(this).off().removeClass('green').addClass('grey-cascade').html(
                        '<i class="fa fa-spin fa-spinner"></i> Patientez...'
                    );

                    // ------------------------------------------------------------------------------------
                    // Les nouveaux téléphones uniquement
                    // ------------------------------------------------------------------------------------
                    var tels = [];
                    $.each($(modalName + ' #tableTel tbody tr td:first-child'), function (index, row) {
                        if ($(this).attr('data-pk') == undefined) {
                            var telephone = $(this).attr('data-telephone');
                            var prefere = $(this).attr('data-prefere');
                            var type = $(this).attr('data-type-telephone');
                            var item = [telephone, prefere, type];
                            tels.push(item);
                        }
                    });
                    var mails = [];
                    $.each($(modalName + ' #tableMail tbody tr td:first-child'), function (index, row) {
                        if ($(this).attr('data-pk') == undefined) {
                            var mail = $(this).attr('data-mail');
                            var prefere = $(this).attr('data-prefere');
                            var type = $(this).attr('data-type-mail');
                            var item = [mail, prefere, type];
                            mails.push(item)
                        }
                        ;
                    });

                    $.ajax({
                        url: '/annuaire/' + routePole + '/' + idPO,
                        type: 'PUT',
                        data: {
                            _token: $(modalName + ' #token').val(),
                            pID: idPO,
                            pNom: nom,
                            pSigle: sigle,
                            pTypeP: typep,
                            pPublic: publiq,
                            pDetail: detail,
                            pNbPlaces: nbplaces,
                            pAge: age,
                            pAdr1: adr1,
                            pAdr2: adr2,
                            pAdr3: adr3,
                            pCP: cp,
                            pVille: ville,
                            pPays: pays,
                            pWeb: web,
                            pNotes: notes,
                            pNl: nl,
                            pBI: bi,
                            pBI_recep: bi_recep,
                            pCF: cf,
                            pCF_recep: cf_recep,
                            pContrib: contrib,

                            pActif: actif,
                            pAcces: acces,
                            pVisible: visible

                        },
                        dataType: 'json',
                        success: function (data) {
                            if (data == -1) {
                                $(modalName).modal('hide');
                                toastr.error('Modification non effectuée', "Attention");
                            } else {
                                $(modalName).modal('hide');
                                toastr.info('Modification enregistrée', "Information");
                                tablePole.ajax.reload();
                            }
                        }
                    });
                });
            }
        });

        $(modalName).on('hidden.bs.modal', function () {
            mode = MODE_DATATABLES;
        })
        waitingDialog.hide();
        $(modalName).modal('show');
    },

    view: function (level, idPO) {

        waitingDialog.show('Préparation du Formulaire', {dialogSize: 'large', progressType: 'info'});
        var modalName = ClassGENERIC.newLevel(level);
        var modal = $(modalName);
        modal.html($('#formPole').html());
        mode = MODE_FORM;

        /** ---------------------------------------------------------------------------------------
         * Fil d'Ariane
         *
         * Attention, il y a 2 header dans les FORMS (hors ORGANISME)
         * donc il faut bien distinguer celui avec le fil d'ariane et celui avec le CAPTION !
         */
        var titre = ClassGENERIC.arianeToTxt();
        $(modalName + ' #prec').empty().append(
            titre
        );

        /** ---------------------------------------------------------------------------------------
         * On récupère les infos de la civilite
         */
        $.ajax({
            url: '/annuaire/' + routePole + '/' + idPO,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {

                /** -------------------------------------------------------------------------------
                 * Chargement du template du modal dans le modal d'exécution
                 * Par défaut, les boutons ACTIONS sont cachés
                 */
                $(modalName).find('#btnAction').hide();
                $(modalName).find('#btnCopieDataOO').hide();
                $(modalName).find('#btnAddTel').hide();
                $(modalName).find('#btnAddMail').hide();
                $(modalName).find('.modal-header').addClass('font-grey-salsa');
                $(modalName).find('.modal-title').html('<i class="fa fa-refresh"></i> Consultation ' + editPole);

                $(modalName).find('.historique').show().html(
                    'Modifié le ' + data.UPDATED_DATE + ' par ' + data.UPDATED_BY
                );

                /** -------------------------------------------------------------------------------
                 * Init des zones ORGANISME
                 */
                var nom = data.PO.nom;
                var idOO = data.PO.organisme_id;
                var sigle = data.PO.sigle;
                var typep = data.PO.type_pole_id;
                var publiq = data.PO.type_public_id;
                var detail = data.PO.detail_type_public_id;
                var nbplaces = data.PO.nb_places;
                var age = data.PO.age;
                var adr1 = data.PO.adr1;
                var adr2 = data.PO.adr2;
                var adr3 = data.PO.adr3;
                var cp = data.PO.cp;
                var ville = data.PO.ville;
                var pays = data.PO.pays_id;

                var web = data.PO.web;
                var notes = data.PO.notes;

                // ------------------------------------------------------------------------------------
                // Données COMPLEMENTAIRES
                // ------------------------------------------------------------------------------------
                var nl = data.PO.nl;
                var bi = data.PO.bi;
                var bi_recep = data.PO.bi_reception_id;
                var cf = data.PO.cf;
                var cf_recep = data.PO.cf_reception_id;
                var contrib = data.PO.isCONTRIBUANT;

                var actif = data.PO.active;
                var acces = data.PO.acces;
                var visible = data.PO.visible;

                // ------------------------------------------------------------------------------------
                // Nouvelles données ET
                // ------------------------------------------------------------------------------------
                var setVisible = false;
                $(modalName + ' #zlTypePublic').empty();
                $.each(data.TYPE_PUBLIC, function (index, item) {
                    $(modalName + ' #zlTypePublic').append(
                        '<option value="' + index + '" selected>'+ item + '</option>'
                    );
                    setVisible = true;
                });
                $(modalName + ' #zlTypePublic').select2({
                    width:'100%'
                }).trigger('change');


                $(modalName + ' #zlTrancheAge').empty();
                $.each(data.TYPE_AGE, function (index, item) {
                    $(modalName + ' #zlTrancheAge').append(
                        '<option value="' + index + '" selected>'+ item + '</option>'
                    );
                    setVisible = true;
                });
                $(modalName + ' #zlTrancheAge').select2({
                    width:'100%'
                }).trigger('change');

                $(modalName + ' #znNbPlacesET').val(data.NB_PLACES);
                if (setVisible) {
                    $(modalName + ' #portletET').removeClass("display-none");
                }

                /** -------------------------------------------------------------------------------
                 *
                 */
                $(modalName + ' #ztNom').val(nom);
                $(modalName + ' #ztNom').focus();
                $(modalName + ' #ztSigle').val(sigle);
                $(modalName + ' #ztNbPlaces').val(nbplaces);
                $(modalName + ' #ztAge').val(age);

                $(modalName + ' #ztAdr1').val(adr1);
                $(modalName + ' #ztAdr2').val(adr2);
                $(modalName + ' #ztAdr3').val(adr3);
                $(modalName + ' #ztCP').val(cp);
                $(modalName + ' #ztVille').val(ville);
                $(modalName + ' #ztNotes').val(notes);
                $(modalName + ' #ztWEB').val(web);

                ClassGENERIC.initGeneric(modalName, 'typepole', '#zlTypePole', typep);
                //ClassGENERIC.initGeneric(modalName, 'typepublic', '#zlTypePublic', publiq);
                ClassGENERIC.initGeneric(modalName, 'pays', '#zlPays', pays);
                if (publiq != '') {
                    ClassGENERIC.initGeneric(modalName, 'detailtypepublic', '#zlDetailTypePublic', detail, publiq);
                }
                /** -------------------------------------------------------------------------------
                 * Données complémentaires
                 */
                $(modalName + ' #ccNL').prop('checked', (nl == 1 ? true : false));
                $(modalName + ' #ccBI').prop('checked', (bi == 1 ? true : false));
                $(modalName + ' #ccCF').prop('checked', (cf == 1 ? true : false));
                $(modalName + ' #ccCONT').prop('checked', (contrib == 1 ? true : false));

                $(modalName + ' #ccACTIF').prop('checked', (actif == 1 ? true : false));
                $(modalName + ' #ccACCES').prop('checked', (acces == 1 ? true : false));
                $(modalName + ' #ccVISIBLE').prop('checked', (visible == 1 ? true : false));

                ClassGENERIC.initGeneric(modalName, 'reception', '#zlReceptionCF', cf_recep);
                ClassGENERIC.initGeneric(modalName, 'reception', '#zlReceptionBI', bi_recep);
                $('#portletTel .portlet-body').find('.row:first').hide();
                $('#portletMail .portlet-body').find('.row:first').hide();

                /** -----------------------------------------------------------------------------------
                 * Init des zones TELEPHONES
                 */
                $.each(data.TEL, function (index, item) {
                    $(modalName + ' #tableTel tbody').append(
                        '<tr>' +
                        '<td data-pk="' + item.OWN_ID + '" ' +
                        '    data-type-telephone="' + item.TYPE_ID + '"' +
                        '    data-id="' + item.OO_ID + '"' +
                        '>' + item.numero +
                        (item.prefere == 1
                                ? ' <i class="fa fa-star prefere font-yellow" style="cursor:pointer"> </i>'
                                : ' <i class="fa fa-star-o prefere" style="cursor:pointer"> </i>'
                        ) +
                        '</td>' +
                        '<td>' + item.libelle + '</td>' +
                        '<td class="text-right">' +
                        '<a class="remove-telephone btn btn-xs red" ' +
                        ' data-placement="left" data-toggle="confirmation" data-original-title="" title="" ' +
                        '><i class="fa fa-remove"></i></a>' +
                        //'<a class="modif btn btn-xs blue"><i class="fa fa-edit"></i></a>' +
                        '</td>' +
                        '</tr>'
                    );
                });

                /** -----------------------------------------------------------------------------------
                 * Init des zones MAILS
                 */
                $.each(data.MAIL, function (index, item) {
                    $(modalName + ' #tableMail tbody').append(
                        '<tr>' +
                        '<tr>' +
                        '<td data-pk="' + item.OWN_ID + '" ' +
                        '    data-type-mail="' + item.TYPE_ID + '"' +
                        '    data-id="' + item.OO_ID + '"' +
                        '>' + item.mail +
                        (item.prefere == 1
                                ? ' <i class="fa fa-star prefere font-yellow" style="cursor:pointer"> </i>'
                                : ' <i class="fa fa-star-o prefere" style="cursor:pointer"> </i>'
                        ) +
                        '</td>' +
                        '<td>' + item.libelle + '</td>' +
                        '<td class="text-right">' +
                        '<a class="remove-mail btn btn-xs red"' +
                        ' data-placement="left" data-toggle="confirmation" data-original-title="" title="" ' +
                        '><i class="fa fa-remove"></i></a>' +
                        //'<a class="modif btn btn-xs blue"><i class="fa fa-edit"></i></a>' +
                        '</td>' +
                        '</tr>'
                    );
                });

                /** -------------------------------------------------------------------------------
                 * Modification de l'aspect final du formulaires. Les zones de saisie prennent la
                 * classe edited. Les zones deviennent readonly et les boutons d'action sont
                 * cachés (au pire, comme il n'y a pas d'action, il ne se passerait rien...
                 */
                ClassGENERIC.refreshEdited(modalName);
<<<<<<< HEAD
                $(modalName).find('.form-control').each(function () {
                    $(this).prop('readonly', true);
                });
                $(modalName + ' input').each(function () {
                    $(this).prop('readonly', true);
                });
                $(modalName).find('.remove-telephone').hide();
                $(modalName).find('.remove-mail').hide();

=======
                ClassGENERIC.doReadOnly(modalName);
>>>>>>> debug8
            }
        });

        $(modalName).on('hidden.bs.modal', function () {
            mode = MODE_DATATABLES;
        })

        waitingDialog.hide();
        $(modalName).modal('show');

    },

    recopieDataOO: function (idOO, modalName) {

        /** ---------------------------------------------------------------------------------------
         * On récupère les données à recopier (adresse + BI + ...
         */
        $.ajax({
            url: '/datacopy/' + routeOrga + '/' + idOO,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {
                $(modalName + ' #ztAdr1').val(data.ADR1);
                $(modalName + ' #ztAdr2').val(data.ADR2);
                $(modalName + ' #ztAdr3').val(data.ADR3);
                $(modalName + ' #ztCP').val(data.CP);
                $(modalName + ' #ztVille').val(data.VILLE);
                $(modalName + ' #zlPays').val(data.PAYS).trigger("change");

                /*
                 $(modalName + ' #ccNL').prop('checked', (data.NL == 1 ? true : false));
                 $(modalName + ' #ccBI').prop('checked', (data.BI == 1 ? true : false));
                 $(modalName + ' #ccCF').prop('checked', (data.CF == 1 ? true : false));
                 $(modalName + ' #ccCONT').prop('checked', (data.CONTRIB == 1 ? true : false));
                 $(modalName + ' #zlReceptionBI').val(data.RECEP_BI).trigger("change");
                 $(modalName + ' #zlReceptionCF').val(data.RECEP_CF).trigger("change");

                 if (data.BI == 1) {
                 $(modalName + ' #zlReceptionBI').select2('enable');
                 }
                 if (data.CF == 1) {
                 $(modalName + ' #zlReceptionCF').select2('enable');
                 }
                 */
            }
        });

    },

    initListes: function (modalName) {

        /** ---------------------------------------------------------------------------------------
         * Listes de bases
         */
        ClassGENERIC.initGeneric(modalName, 'typepole', '#zlTypePole');
        ClassGENERIC.initGeneric(modalName, 'typepublic', '#zlTypePublic');

        ClassGENERIC.initGeneric(modalName, 'pays', '#zlPays', '');
        ClassGENERIC.initGeneric(modalName, 'reception', '#zlReceptionCF');
        ClassGENERIC.initGeneric(modalName, 'reception', '#zlReceptionBI');

        ClassGENERIC.initGeneric(modalName, 'typetel', '#portletTel #zlTypeSaisie');
        ClassGENERIC.initGeneric(modalName, 'typemail', '#portletMail #zlTypeSaisie');
    },

    erermanageModalCC: function (modalName) {
        $(modalName + ' #ccCF').on('click', function () {
            if ($(modalName + ' #ccCF').is(':checked')) {
                $(modalName + ' #zlReceptionCF').select2('enable');//.enable(true);
            } else {
                $(modalName + ' #zlReceptionCF').select2('enable', false);//.enable(true);
            }
        });

        $(modalName + ' #ccBI').on('click', function () {
            if ($(modalName + ' #ccBI').is(':checked')) {
                $(modalName + ' #zlReceptionBI').select2('enable');//.enable(true);
            } else {
                $(modalName + ' #zlReceptionBI').select2('enable', false);//.enable(true);
            }
        });
    },

};

/** -----------------------------------------------------------------------------------------------
 * @class : ETABLISSEMENTS
 * Version simplifiée
 *
 * Attention, l'établissement peut être rattaché à un POLE ou à un ORGANISME
 */
var ClassET = {


    /** -------------------------------------------------------------------------------------------
     * Les ETABLISSEMENTS peuvent être rattachés soit à un POLE, soit directement à un
     * ORGANISME
     *
     * @param level
     * @param idPO
     * @param idOO
     */
    manageEtabls: function (level, idOO, idPO) {

        /** ---------------------------------------------------------------------------------------
         * Gestion des ETABLISSEMENTS
         * La liste se trouve au niveau N, l'écran de saisie au niveau N+1
         */
        var modalName = ClassGENERIC.newLevel(level);
        var modal = $(modalName);

        /** ---------------------------------------------------------------------------------------
         * Chargement du bon formulaire
         */
        modal.html($('#formListEtabl').html());
        $(modalName).find('.modal-dialog').removeClass('modal-full').addClass('modal-full-lg');

        /** ---------------------------------------------------------------------------------------
         * Ecriture du libellé parent
         */
        var titre = ClassGENERIC.arianeToTxt();
        $(modalName + ' .modal-header .col-md-12').empty().append(
            titre
        );

        ClassET.initDataTable(modalName, idOO, idPO);

        /** ---------------------------------------------------------------------------------------
         * Affichage des PÔLES d'un ORGANISME
         */
        $(modalName).unbind().on('click', '#btnAddEtabl', function () {
            ClassET.add(modalName, idOO, idPO);
        });

        $(modalName).on('hidden.bs.modal', function (e) {
            if (mode == MODE_DATATABLES) {
                ariane.pop();
            }
            if (idOO != null) {
                tableOrga.ajax.reload();
            } else {
                tablePole.ajax.reload();
            }
        });

        $(modalName).modal('show');
    },

    /** -------------------------------------------------------------------------------------------
     *
     * @param secteur
     * @param secteursms
     * @param age
     */
    updateTypeEtablissement: function (modalName, secteur, secteursms, age, typeetablisssement) {

        /** -------------------------------------------------------------------------------
         * Comme on doit l'afficher, on va regarder par rapport au triple filtre
         */
        $.ajax({
            url: '/list/typeetablissement',
            type: 'GET',
            dataType: 'json',
            data: {
                pSecteur: secteur,
                pSecteurMS: secteursms,
                pAge: age,
            },
            async: false,
            success: function (data) {
                var select = '';
                var zl = $(modalName + ' #zlTypeEtabl');
                if (data.LISTE.length > 0) {
                    zl.empty();
                    if (data.LISTE.length > 1) {
                        zl.append('<option value=""></option>');
                    }
                    $.each(data.LISTE, function (index, item) {
                        zl.append('<option ' + ' value="' + item.id + '" ' + (item.id == typeetablisssement ? 'selected="selected"' : '') + '>' + item.libelle + '</option>');
                    });
                    zl.select2({
                        width: '100%',
                        allowClear: (data.LISTE.length > 1 ? true : false),
                        placeholder: ' '
                    });
                    zl.prop("disabled", false);
                    $(modalName + ' #btnAddModalite').prop('readonly', false);
                } else {
                    zl.empty().select2('enable', false);
                    $(modalName + ' #btnAddModalite').prop('readonly', true);
                }
            }
        });

    },

    bindZL_portlet: function (modalName, cible, id, add) {

        $(cible + ' #zlTypePublic').on('change', function () {
            if ($(this).val() != '') {
                ClassGENERIC.initGeneric(cible,
                    'detailtypepublic', '#zlDetailTypePublic', '', $(this).val());
                if ($(cible + ' #zlDetailTypePublic option').size() != 0) {
                    $(cible + ' #zlDetailTypePublic').prop('readonly', false);
                } else {
                    $(cible + ' #zlDetailTypePublic').prop('readonly', true);
                }
            } else {
                $(modalName + ' #divDetail div[data-id="' + id + '"] #zlDetailTypePublic')
                    .empty().select2().prop('readonly', true);
            }
        });

        /** ---------------------------------------------------------------------------------------
         * On bind le PLUS
         */
        $(cible + ' #btnPlusDetailModalite').on('click', function () {
            var modalites, typepublic, detailtypepublic, nb_places;
            var modalites_txt, typepublic_txt, detailtypepublic_txt;

            /** -----------------------------------------------------------------------------------
             *
             * @type {any}
             */
            modalites = $(cible + ' #zlModaliteAccueil').val();
            typepublic = $(cible + ' #zlTypePublic').val();
            detailtypepublic = $(cible + ' #zlDetailTypePublic').val();
            nb_places = parseInt($(cible + ' #ztNbPlaces').val() || 0);

            if (modalites == '' || typepublic == '' || $(cible + ' #ztNbPlaces').val() == '') {
                toastr.warning('Les champs obligatoires doivent être saisis.<br/>Pour un nombre de places à zéro, saisir 0 pour confirmer.', "Attention");
                return;
            }

            modalites_txt = $(cible + ' #zlModaliteAccueil').select2('data')[0].text;
            typepublic_txt = $(cible + ' #zlTypePublic').select2('data')[0].text;

            if (detailtypepublic == null || detailtypepublic == '') {
                detailtypepublic = 0;
                detailtypepublic_txt = '';
            } else {
                detailtypepublic_txt = $(cible + ' #zlDetailTypePublic').select2('data')[0].text;
            }

            if (detailtypepublic != null && detailtypepublic != '') {
                detailtypepublic_txt = $(cible + ' #zlDetailTypePublic').select2('data')[0].text;
            } else {
                detailtypepublic_txt = '';
            }

            /** ---------------------------------------------------------------------------
             * On ajoute une ligne si elle n'existe pas déjà !
             */
            var cpt = 0;
            var tr = $('<tr/>');
            var trouve = false;
            var oldVal = 0;
            $(cible + ' [table-id="' + id + '"] tbody tr').each(function () {
                cpt++;
                var cur_mod = $(this).find('td:eq(0)').attr('data-mod-id');
                var cur_pub = $(this).find('td:eq(1)').attr('data-pub-id');
                var cur_det = $(this).find('td:eq(2)').attr('data-det-id');
                var cur_nbp = $(this).find('td:eq(3)').html();

                if (cur_mod == modalites && cur_pub == typepublic && cur_det == detailtypepublic) {
                    //oldVal = $(this).find('td:eq(3)').html();
                    $(this).find('td:eq(3)').html(nb_places);
                    trouve = true;
                    return;
                } else {
                }
            });
            if (!trouve) {

                /** -----------------------------------------------------------------------------------
                 *  ATTENTION, en cas de add == 1, on ajoute directement dans la base de données !
                 */
                var rec = 0;
                if (add == 1) {
                    $.ajax({
                        url: '/annuaire/detail_modalite',
                        type: 'POST',
                        data: {
                            _token: $(modalName + ' #token').val(),
                            pID_Detail: id,
                            pID_Mod: modalites,
                            pID_Pub: typepublic,
                            pID_PubDet: detailtypepublic,
                            pNB: nb_places,
                        },
                        dataType: "json",
                        async: false
                    }).success(function (data) {
                        rec = data;
                    });
                }


                tr.append('<td data-mod-id="' + modalites + '">' + modalites_txt + '</td>');
                tr.append('<td data-pub-id="' + typepublic + '">' + typepublic_txt + '</td>');
                tr.append('<td data-det-id="' + detailtypepublic + '">' + detailtypepublic_txt + '</td>');
                tr.append('<td>' + nb_places + '</td>');

                tr.append('<td class="text-right">' +
                    '<a class="remove-mod btn btn-xs red"' +
                    ' data-placement="left" data-toggle="confirmation" data-original-title="" title="" ' +
                    (rec != 0 ? ' data-id="' + rec + '" ' : '') +
                    '><i class="fa fa-remove"></i></a>' +
                    '</td>');

                $(cible + ' [table-id="' + id + '"] tbody').append(tr);
            } else {
                if (add == 1) {
                    $.ajax({
                        /** -----------------------------------------------------------------------
                         * Si, on a déjà trouvé, on met à jour aussi le nombre de places dans la
                         * base de données
                         */
                    });
                }
            }

            //var nbcumul = parseInt($(modalName + ' #ztNbPlacesET').val() || 0);
            //nbcumul = (nbcumul - oldVal) + nb_places;
            //$(modalName + ' #ztNbPlacesET').val(nbcumul);
            //$(modalName + ' #ztNbPlacesET').addClass('edited');
            //if (nbcumul != 0) {
            //    $(modalName + ' #ztNbPlacesET').prop('readonly', true);
            //}
            $(cible + ' .remove-mod').confirmation({
                btnOkLabel: 'OUI',
                btnOkClass: 'red-mint btn-xs',
                btnCancelLabel: 'NON',
                placement: 'left',
                title: 'Suppression ?',
                singleton: true,
                popout: true,
                onConfirm: function () {
                    //var oldVal = $(this).closest('tr').find('td:eq(3)').html();
                    //var nbcumul = parseInt($(modalName + ' #ztNbPlacesET').val() || 0);
                    //nbcumul = (nbcumul - oldVal);
                    //$(modalName + ' #ztNbPlacesET').val(nbcumul);
                    //if ($(modalName + ' #ztNbPlacesET').val() == 0) {
                    //    $(modalName + ' #ztNbPlacesET').prop('readonly', false);
                    //}
                    $(this).closest('tr').remove();
                }
            });
        });
    },

    bindZL_base: function (modalName, idOO, idPO, noCascade) {

        var secteur = 0, secteursms = 0, handicap = 0, age = 0, etablissement = 0;

        /** ---------------------------------------------------------------------------------------
         * SECTEUR --> SECTEUR SMS
         *
         * SECTEUR --> déclenche TYPE ETABLISSEMENT
         */
        $(modalName + ' ' + '#zlSecteur').on('change', function (e) {

            console.log('CHANGE');
            /** -----------------------------------------------------------------------------------
             */
            var isSMS = 0;

            if ($(this).val() != '') {
                isSMS = $(this).find('option:selected').attr('data-ms');
                secteur = $(this).val();
            }
            else {
                isSMS = 0;
                secteur = 0;
                $(modalName + ' ' + '#zlSecteurSMS').val('').trigger('change');
                $(modalName + ' #ccHANDICAP').removeAttr('checked');
            }


            if (isSMS == 1) {
                $(modalName + ' #zlSecteurSMS').prop('readonly', false);
            } else {
                $(modalName + ' #zlSecteurSMS').select2('val', '');
                $(modalName + ' #zlSecteurSMS').prop('readonly', true);
            }
            if (noCascade == 0) {
                ClassET.updateTypeEtablissement(modalName, secteur, secteursms, age, 0);
            }
        });

        /** ---------------------------------------------------------------------------------------
         * SECTEUR SMS --> HANDICAP
         *
         * SECTEUR --> déclenche TYPE ETABLISSEMENT
         */
        $(modalName + ' ' + '#zlSecteurSMS').on('change', function (e) {

            if ($(this).val() != '') {
                secteursms = $(this).val();
            }
            else {
                secteursms = 0;
            }

            /** -----------------------------------------------------------------------------------
             */
            var isHANDI = $(this).find('option:selected').attr('data-handicap');
            if (isHANDI == 1) {
                $(modalName + ' .section-handicap').removeClass('display-none');
            } else {
                $(modalName + ' .section-handicap').addClass('display-none');
            }
            if (noCascade == 0) {
                ClassET.updateTypeEtablissement(modalName, secteur, secteursms, age, 0);
            }
        });

        /** ---------------------------------------------------------------------------------------
         * TRANCHE D'AGE --> déclenche TYPE ETABLISSEMENT
         */
        $(modalName + ' ' + '#zlTypeAge').on('change', function (e) {

            /** -----------------------------------------------------------------------------------
             */
            if ($(this).val() != '') {
                age = $(this).val();
            }
            else {
                age = 0;
            }
            if (noCascade == 0) {
                ClassET.updateTypeEtablissement(modalName, secteur, secteursms, age, 0);
            }

        });

    },

    bindZL_complement: function (modalName, idOO, idPO) {

        $(modalName + ' ' + '#zlTypePublic').on('change', function (e) {

            /** -----------------------------------------------------------------------------------
             * Tout changement de TypePublic déclenche un changement de Détail
             */
            $(modalName + ' ' + '#zlDetailTypePublic').parent().parent().addClass('display-none');
            if ($(this).val() != '') {
                ClassGENERIC.initGeneric(modalName, 'detailtypepublic', '#zlDetailTypePublic', '', $(this).val());
            } else {
                $(modalName + ' ' + '#zlDetailTypePublic').parent().parent().addClass('display-none');
            }
        });
    },

    initDataTable: function (modalName, idOO, idPO) {

        var subRoute;
        if (idPO != null) {
            subRoute = 'et/po/' + idPO;
        } else {
            subRoute = 'et/oo/' + idOO;
        }

        tableEtabl = $(modalName + ' #' + tableNameEtabl).removeAttr('width').DataTable({
            "dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
            iDisplayLength:50,
            destroy: true,
            ajax: "/datatable/" + subRoute,
            serverSide: false,
            autoWidth: false,
            columnDefs: [
                {className: "dt-body-left", "targets": [0, 1, 2, 3, 4, 5]},
                {className: "dt-body-right", "targets": [6]}
            ],
            columns: [
                {orderable: true, width: 100}, null, null, null, null, null,
                {orderable: true, width: 120},
            ],
            lengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "Tous"]
            ],

            //fixedColumns: true,
            sPagingType: "bootstrap_full_numbers",
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
            },
            initComplete: function (settingssettings) {
                /** On change les largeurs par défaut des contrôles */
                var tableWrapper = $('#' + tableNameEtabl + '_wrapper');
                var tableLength = $('#' + tableNameEtabl + '_length');
                tableLength.css('width', '400px');
                tableWrapper.find('.dataTables_length select').addClass("form-control input-inline").select2();
                tableWrapper.find('.dataTables_filter input').addClass("form-control input-inline");
            },
            rowCallback: function (row, data, index) {

                var colStatut = 99;
                var colAction = 6;

                $('td:eq(1),td:eq(2),td:eq(3)', row).addClass("txtSmall");
                $('td:eq(' + colAction + ')', row).find('.dropdown-toggle').dropdown();

                /** --------------------------------------------------------------------------------
                 * On checke la colonne ACTION - lien A
                 */
                $('td:eq(' + colAction + ')', row).children('a').bind('click', function () {
                    if ($(this).hasClass('visu')) {
                        console.log('ET = ' + $(this).attr('data-id'));
                        ClassET.view(modalName, $(this).attr('data-id'), idOO, idPO);
                    }
                });

                $('td:eq(' + colAction + ')', row).find('a').bind('click', function () {

                    var lib = $(this).closest('tr').find('td:eq(1)').find('span').attr('data-nom');

                    if ($(this).hasClass('personnel')) {
                        mode = MODE_DATATABLES;
                        ariane.push(lib);
                        ClassPERSONNEL.managePersonnel(modalName, null, null, $(this).attr('data-id'), null);
                    }
                    if ($(this).hasClass('adresse')) {
                        mode = MODE_DATATABLES;
                        ariane.push(lib);
                        ClassAA.manageAdresses(modalName, $(this).attr('data-id'), null);
                    }
                    if ($(this).hasClass('antenne')) {
                        mode = MODE_DATATABLES;
                        ariane.push(lib);
                        ClassAN.manageAntennes(modalName, $(this).attr('data-id'));
                    }
                    if ($(this).hasClass('modif')) {
                        ClassET.update(modalName, $(this).attr('data-id'), idOO, idPO);
                    }
                    if ($(this).hasClass('telephone')) {
                        ClassGENERIC.phoneList('#dialogExecLev9', 'et', $(this).attr('data-id'));
                    }
                    if ($(this).hasClass('mail')) {
                        ClassGENERIC.mailList('#dialogExecLev9', 'et', $(this).attr('data-id'));
                    }
                    if ($(this).hasClass('offres')) {
                        ClassGENERIC.offreList('#dialogExecLev9', 'et', $(this).attr('data-id'));
                    }
                });
            }
        });
        return true;
    },

    add: function (level, idOO, idPO) {

        /** ---------------------------------------------------------------------------------------
         * Préparation du Formulaire
         */
        waitingDialog.show('Préparation du Formulaire', {dialogSize: 'large', progressType: 'info'});

        /** ---------------------------------------------------------------------------------------
         * On est toujours 1 niveau de plus que le datatable
         */
        var modalName = ClassGENERIC.newLevel(level);
        var modal = $(modalName);
        modal.html($('#formEtablissement').html());
        mode = MODE_FORM;

        /** ---------------------------------------------------------------------------------------
         * Fil d'Ariane
         *
         * Attention, il y a 2 header dans les FORMS (hors ORGANISME)
         * donc il faut bien distinguer celui avec le fil d'ariane et celui avec le CAPTION !
         */
        var titre = ClassGENERIC.arianeToTxt();
        $(modalName + ' #prec').empty().append(
            titre
        );

        $(modalName).find('#btnAction').addClass('green').html('<i class="fa fa-plus-circle"></i> Ajouter');
        $(modalName).find('.modal-header.caption').addClass('font-green');
        $(modalName).find('.modal-title').html('<i class="fa fa-plus-circle"></i> Ajout ' + editEtabl);
        $(modalName).find('.historique').hide();

        /** ---------------------------------------------------------------------------------------
         * On prépare l'affichage d'une liste déroulante monitorée par le comportement de 3
         * variables
         */
        //objTypeEtablissement.modal = modalName;
        //objTypeEtablissement.zl = '#zlTypeEtabl';

        /** ---------------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        if (idPO != null) {
            $(modalName).find('#btnCopieData').html('RECOPIER POLE');
        } else {
            $(modalName).find('#btnCopieData').html('RECOPIER ORGANISME');
        }

        $(modalName + ' .date-picker').datepicker({
            autoclose: true,
            calendarWeeks: true,
            format: 'dd/mm/yyyy',
            language: 'fr',
            today: true
        });

        /** ---------------------------------------------------------------------------------------
         * Init des LISTEs
         */
        ClassET.initListes(modalName, idOO, idPO);
        $(modalName + ' #zlSecteurSMS').prop("disabled", true);
        $(modalName + ' #zlReceptionCF').prop('readonly', true);
        $(modalName + ' #zlReceptionBI').prop('readonly', true);
        $('#portletTel .portlet-body').find('.row:first').show();
        $('#portletMail .portlet-body').find('.row:first').show();

        /** ---------------------------------------------------------------------------------------
         * Préselection à FAUX
         */
        $(modalName + ' #btnAddModalite').prop('readonly', true);
        $(modalName + ' #zlTypeEtabl').select2({width: '100%'}).select2('enable', false);

        /** ---------------------------------------------------------------------------------------
         * 2 bindZL
         */
        ClassET.bindZL_base(modalName, idOO, idPO, 0);
        ClassET.bindZL_complement(modalName, idOO, idPO);
        ClassGENERIC.manageModalButtons(modalName, 1, 'et', undefined);
        ClassGENERIC.manageModalCC(modalName);

        /** ---------------------------------------------------------------------------------------
         * Bouton de recopie de l'adresse
         */
        $(modalName + ' #btnCopieData').unbind().on('click', function () {
            if (idPO != null) {
                ClassET.recopieDataPO(idPO, modalName);
            } else {
                ClassET.recopieDataOO(idOO, modalName);
            }
            ClassGENERIC.refreshEdited(modalName);
        });

        /** ---------------------------------------------------------------------------------------
         * Sauvegarde
         */
        $(":input").inputmask();
        $(modalName + ' #btnAction').unbind().on('click', function () {

            /** -----------------------------------------------------------------------------------
             * Vérification des ZONES Mandatory
             */
            var oblig = true;
            $(modalName + ' .required').each(function () {
                if (!$(this).val().trim()) {
                    oblig = false;
                }
            });
            if (!oblig) {
                toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                return;
            }

            // ------------------------------------------------------------------------------------
            // Données de base
            // ------------------------------------------------------------------------------------
            var nom = $(modalName + ' #ztNom').val().trim().toUpperCase();
            var sigle = $(modalName + ' #ztSigle').val().trim().toUpperCase();
            var adr1 = $(modalName + ' #ztAdr1').val().trim().toUpperCase();
            var adr2 = $(modalName + ' #ztAdr2').val().trim().toUpperCase();
            var adr3 = $(modalName + ' #ztAdr3').val().trim().toUpperCase();
            var cp = $(modalName + ' #ztCP').val();
            var ville = $(modalName + ' #ztVille').val().trim().toUpperCase();
            var pays = $(modalName + ' #zlPays').val();
            var nbplaces = $(modalName + ' #ztNbPlacesET').val();

            // ------------------------------------------------------------------------------------
            // Caractéristiques Établissement / Service
            // ------------------------------------------------------------------------------------
            var dispositif = $(modalName + ' #ccDISP').is(":checked") ? 1 : 0;
            var secteur = $(modalName + ' #zlSecteur').val();
            var secteursms = $(modalName + ' #zlSecteurSMS').val();
            var handicap = $(modalName + ' #ccHANDICAP').is(":checked") ? 1 : 0;
            var typeAge = $(modalName + ' #zlTypeAge').val();
            if ($(modalName + ' #ccHANDICAP').closest('.section-handicap').hasClass('display-none')) {
                handicap = 0;
            }
            var arrete = $(modalName + ' #ztArrete').val();
            var datearrete = $(modalName + ' #ztDateArrete').val();
            var age = $(modalName + ' #ztAge').val();
            var finess = $(modalName + ' #ztFINESS').val().trim().toUpperCase();
            var projet = $(modalName + ' #ccPROJ').is(":checked") ? 1 : 0;
            var opening = $(modalName + ' #ztDateOpen').val();

            /** -----------------------------------------------------------------------------------
             * Données Complémentaires
             */
            var nl = $(modalName + ' #ccNL').is(":checked") ? 1 : 0;
            var bi = $(modalName + ' #ccBI').is(":checked") ? 1 : 0;
            var bi_recep = (bi == 1 ? $(modalName + ' #zlReceptionBI').val() : null);
            var cf = $(modalName + ' #ccCF').is(":checked") ? 1 : 0;
            var cf_recep = (cf == 1 ? $(modalName + ' #zlReceptionCF').val() : null);
            var contrib = $(modalName + ' #ccCONT').is(":checked") ? 1 : 0;
            var code = $(modalName + ' #ztCodeInterne').val().trim().toUpperCase();

            /** -----------------------------------------------------------------------------------
             * Modalités
             */
            var modalites = [];
            $.each($(modalName + ' #divDetail .portlet'), function (index, row) {
                var portlet = $(this);
                var type = portlet.attr('data-type-id');

                var details = [];
                $.each($(portlet).find('.table tbody tr'), function (index, row) {
                    var modalite = $(this).find('td:eq(0)').attr('data-mod-id');
                    var typep = $(this).find('td:eq(1)').attr('data-pub-id');
                    var dtypep = $(this).find('td:eq(2)').attr('data-det-id');
                    var places = $(this).find('td:eq(3)').html();

                    details.push({
                        'modalite_id': modalite,
                        'type_public_id': typep,
                        'detail_type_public_id': dtypep,
                        'nb_places': places,
                    });
                });

                modalites.push({
                    'id': type,
                    'detail': details
                });
            });

            /** -----------------------------------------------------------------------------------
             * Le Reste
             */
            var pole = $(modalName + ' #zlPole').val();
            var web = $(modalName + ' #ztWEB').val();
            var notes = $('<p>' + $(modalName + ' #ztNotes').val() + '</p>').text();
            var notes_public = $('<p>' + $(modalName + ' #ztNoteAccueilPublic').val() + '</p>').text();

            var actif = $(modalName + ' #ccACTIF').is(":checked") ? 1 : 0;
            var visible = $(modalName + ' #ccVISIBLE').is(":checked") ? 1 : 0;
            var acces = $(modalName + ' #ccACCES').is(":checked") ? 1 : 0;

            /** -----------------------------------------------------------------------------------
             * Un dernier petit contrôle
             */
            if ((bi == 1 && bi_recep == '') ||
                (cf == 1 && cf_recep == '')) {
                toastr.warning('Vérifier les données complémentaires.', "Attention");
                return;
            }

            /** -----------------------------------------------------------------------------------
             * On va rajouter un contrôle de vérification des MAILS si l'une des zones
             * doit vérifier les mails
             */
            if (!ClassGENERIC.verifPresenceMail(modalName, bi, cf)) {
                toastr.warning('Vous devez renseigner au moins un mail.', "Attention");
                return;
            }

            /** -----------------------------------------------------------------------------------
             * Contrôle de cohérence
             */
            var isMS = false;
            if (secteur == '' || typeAge == '') {
                toastr.warning('Les champs obligatoires doivent être saisis !', "Attention");
                return;
            }
            if (!$(modalName + ' #zlSecteurSMS').prop('disabled')) {
                isMS = true;
                if (secteursms == '') {
                    toastr.warning('Le type de secteur SMS est obligatoire !', "Attention");
                    return;
                }
            }

            $(this).off().removeClass('green').addClass('grey-cascade').html(
                '<i class="fa fa-spin fa-spinner"></i> Patientez...'
            );

            // ------------------------------------------------------------------------------------
            // Le reste (
            // ------------------------------------------------------------------------------------
            var tels = [];
            $.each($(modalName + ' #tableTel tbody tr td:first-child'), function (index, row) {
                var telephone = $(this).attr('data-telephone');
                var type = $(this).attr('data-type-telephone');
                var prefere = $(this).attr('data-prefere');
                var item = [telephone, prefere, type];
                tels.push(item);
            });
            var mails = [];
            $.each($(modalName + ' #tableMail tbody tr td:first-child'), function (index, row) {
                var mail = $(this).attr('data-mail');
                var type = $(this).attr('data-type-mail');
                var prefere = $(this).attr('data-prefere');
                var item = [mail, prefere, type];
                mails.push(item);
            });

            $.ajax({
                url: '/annuaire/' + routeEtabl,
                type: 'POST',
                data: {

                    _token: $(modalName + ' #token').val(),

                    /** ---------------------------------------------------------------------------
                     * Données de base
                     */
                    pPO: idPO,
                    pOO: idOO,

                    pNom: nom,
                    pSigle: sigle,
                    pAdr1: adr1,
                    pAdr2: adr2,
                    pAdr3: adr3,
                    pCp: cp,
                    pVille: ville,
                    pPays: pays,

                    /** ---------------------------------------------------------------------------
                     * Caractéristiques Établissement / Service
                     */
                    pNbPlaces : nbplaces,
                    pDispositif: dispositif,
                    pSecteur: secteur,
                    pSecteurSms: secteursms,
                    pHandicap: handicap,
                    pTypeAge: typeAge,

                    pArrete: arrete,
                    pDateArrete: datearrete,
                    pAge: age,
                    pFiness: finess,
                    pProjet: projet,
                    pOpening: opening,

                    /** ---------------------------------------------------------------------------
                     * Données Complémentaires
                     */
                    pNL: nl,
                    pBI: bi,
                    pBI_recep: bi_recep,
                    pCF: cf,
                    pCF_recep: cf_recep,
                    pContrib: contrib,
                    pCode: code,

                    /** ---------------------------------------------------------------------------
                     * Le Reste
                     */
                    pWeb: web,
                    pNotes: notes,
                    pNotes_public: notes_public,

                    pActif: actif,
                    pVisible: visible,
                    pAcces: acces,

                    /** ---------------------------------------------------------------------------
                     * Tableaux
                     */
                    pModalites: modalites,
                    pTels: tels,
                    pMails: mails,
                },
                dataType: 'json',
                success: function (data) {
                    result = true;
                    $(modalName).modal('hide');
                    toastr.success('Saisie enregistrée', "Information");
                    tableEtabl.ajax.reload();
                }
            });

        });

        /** ---------------------------------------------------------------------------------------
         * Affichage
         */
        $(modalName).on('hidden.bs.modal', function () {
            mode = MODE_DATATABLES;
        });
        waitingDialog.hide();
        $(modalName).modal('show');

    },

    update: function (level, idET, idOO, idPO) {

        /** ---------------------------------------------------------------------------------------
         * On est toujours 1 niveau de plus que le datatable
         */
        waitingDialog.show('Préparation du Formulaire', {dialogSize: 'large', progressType: 'info'});

        var modalName = ClassGENERIC.newLevel(level);
        var modal = $(modalName);
        modal.html($('#formEtablissement').html());
        mode = MODE_FORM;

        /** ---------------------------------------------------------------------------------------
         * Fil d'Ariane
         *
         * Attention, il y a 2 header dans les FORMS (hors ORGANISME)
         * donc il faut bien distinguer celui avec le fil d'ariane et celui avec le CAPTION !
         */
        var titre = ClassGENERIC.arianeToTxt();
        $(modalName + ' #prec').empty().append(
            titre
        );

        /** ---------------------------------------------------------------------------------------
         * On récupère les infos de la civilite
         */
        $.ajax({
            url: '/annuaire/' + routeEtabl + '/' + idET,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {

                /** -------------------------------------------------------------------------------
                 * Chargement du template du modal dans le modal d'exécution
                 */
                $(modalName).find('#btnAction').addClass('yellow-gold').html('<i class="fa fa-plus-circle"></i> Modifier');
                $(modalName).find('.modal-header.caption').addClass('font-yellow-casablanca');
                $(modalName).find('.modal-title').html('<i class="fa fa-refresh"></i> Modification ' + editEtabl);

                $(modalName).find('.historique').show().html(
                    'Modifié le ' + data.UPDATED_DATE + ' par ' + data.UPDATED_BY
                );

                /** -------------------------------------------------------------------------------
                 * Chargement du template du modal dans le modal d'exécution
                 */
                if (idPO != null) {
                    $(modalName).find('#btnCopieData').html('RECOPIER POLE');
                } else {
                    $(modalName).find('#btnCopieData').html('RECOPIER ORGANISME');
                }

                /** -------------------------------------------------------------------------------
                 * LECTURE -> Données de base
                 */
                var nom = data.ET.nom;
                var sigle = data.ET.sigle;
                var adr1 = data.ET.adr1;
                var adr2 = data.ET.adr2;
                var adr3 = data.ET.adr3;
                var cp = data.ET.cp;
                var ville = data.ET.ville;
                var pays = data.ET.pays_id;

                /** -------------------------------------------------------------------------------
                 * Caractéristiques Établissement / Service
                 */
                var dispositif = data.ET.dispositif;
                var secteur = data.ET.type_secteur_id;
                var secteursms = data.ET.type_secteur_ms_id;
                var handicap = data.ET.handicap;
                var typeAge = data.ET.type_age_id;
                var nbplaces = data.ET.nbplaces;
                var arrete = data.ET.arrete_pref;
                var datearrete = data.ET.date_arrete;
                var age = data.ET.age;
                var finess = data.ET.finess;
                var projet = data.ET.projet;
                var opening = data.ET.date_ouverture;

                /** -------------------------------------------------------------------------------
                 * Données Complémentaires
                 */
                var nl = data.ET.nl;
                var bi = data.ET.bi;
                var bi_recep = data.ET.bi_reception_id;
                var cf = data.ET.cf;
                var cf_recep = data.ET.cf_reception_id
                var contrib = data.ET.isCONTRIBUANT;
                var code = data.ET.id_interne;

                /** -------------------------------------------------------------------------------
                 * Le Reste
                 */
                var pole = data.ET.po_id;
                var web = data.ET.web;
                var notes = data.ET.notes;
                var notespublic = data.ET.notesTypePublic;

                var actif = data.ET.active;
                var visible = data.ET.visible;
                var acces = data.ET.acces;

                /** -------------------------------------------------------------------------------
                 * AFFICHAGE -> Données de base
                 */
                $(modalName + ' #ztNom').val(nom);
                $(modalName + ' #ztSigle').val(sigle);
                $(modalName + ' #ztAdr1').val(adr1);
                $(modalName + ' #ztAdr2').val(adr2);
                $(modalName + ' #ztAdr3').val(adr3);
                $(modalName + ' #ztCP').val(cp);
                $(modalName + ' #ztVille').val(ville);
                ClassGENERIC.initGeneric(modalName, 'pays', '#zlPays', pays);

                /** -------------------------------------------------------------------------------
                 * AFFICHAGE -> Caractéristiques Établissement / Service
                 */
                $(modalName + ' #ccDISP').prop('checked', (dispositif == 1 ? true : false));
                ClassGENERIC.initGeneric(modalName, 'typesecteur', '#zlSecteur', secteur);
                ClassGENERIC.initGeneric(modalName, 'typesecteurms', '#zlSecteurSMS', secteursms);
                ClassGENERIC.initGeneric(modalName, 'typeage', '#zlTypeAge', typeAge);

                /** -------------------------------------------------------------------------------
                 * On aura besoin de ça pour créer le PORTLET
                 */
                var isSMS = 0;
                if ($(modalName + ' #zlSecteur').val() != '') {
                    isSMS = $(modalName + ' #zlSecteur').find('option:selected').attr('data-ms');
                }
                else {
                    isSMS = 0;
                }

                /** -------------------------------------------------------------------------------
                 * Attention, pour les ZL
                 */
                ClassET.bindZL_base(modalName, idOO, idPO, 1);
                $(modalName + ' #zlSecteur').trigger('change');
                $(modalName + ' #zlSecteurSMS').trigger('change');
                $(modalName + ' #ccHANDICAP').prop('checked', (handicap == 1 ? true : false));
                ClassET.updateTypeEtablissement(modalName, secteur, secteursms, typeAge, 0);


                $(modalName + ' #ztNbPlacesET').val(nbplaces);
                $(modalName + ' #ztArrete').val(arrete);
                $(modalName + ' #ztDateArrete').val(datearrete);
                $(modalName + ' #ztAge').val(age);
                $(modalName + ' #ztFINESS').val(finess);
                $(modalName + ' #ccPROJ').prop('checked', (projet == 1 ? true : false));
                $(modalName + ' #ztDateOpen').val(opening);
                $(modalName + ' #ztNoteAccueilPublic').val(notespublic);

                /** -------------------------------------------------------------------------------
                 * AFFICHAGE -> Données Complémentaires
                 */
                $(modalName + ' #ccNL').prop('checked', (nl == 1 ? true : false));
                $(modalName + ' #ccBI').prop('checked', (bi == 1 ? true : false));
                $(modalName + ' #ccCF').prop('checked', (cf == 1 ? true : false));
                $(modalName + ' #ccCONT').prop('checked', (contrib == 1 ? true : false));

                $(modalName + ' #ccACTIF').prop('checked', (actif == 1 ? true : false));
                $(modalName + ' #ccVISIBLE').prop('checked', (visible == 1 ? true : false));
                $(modalName + ' #ccACCES').prop('checked', (acces == 1 ? true : false));

                ClassGENERIC.initGeneric(modalName, 'reception', '#zlReceptionCF', cf_recep);
                ClassGENERIC.initGeneric(modalName, 'reception', '#zlReceptionBI', bi_recep);
                $('#portletTel .portlet-body').find('.row:first').show();
                $('#portletMail .portlet-body').find('.row:first').show();
                ClassGENERIC.initGeneric(modalName, 'typetel', '#portletTel #zlTypeSaisie');
                ClassGENERIC.initGeneric(modalName, 'typemail', '#portletMail #zlTypeSaisie');

                /** -------------------------------------------------------------------------------
                 * Le Reste
                 */
                $(modalName + ' #ztWEB').val(web);
                $(modalName + ' #ztNotes').val(notes);
                $(modalName + ' #ztCodeInterne').val(code);
                ClassGENERIC.initGeneric(modalName, 'poles', '#zlPole', pole, pole, idOO);

                /** -------------------------------------------------------------------------------
                 * Init des zones TELEPHONES
                 */
                /** -----------------------------------------------------------------------------------
                 * Init des zones TELEPHONES + MAILS
                 */
                ClassGENERIC.displayTels(modalName, data.TEL);
                ClassGENERIC.displayMails(modalName, data.MAIL);

                /** -------------------------------------------------------------------------------
                 * On va maintenant recréer les portlets
                 */
                var hasOnePortlet = false;
                $.each(data.MODALITES, function (index, item) {
                    hasOnePortlet = true;
                    var id = item.ID;
                    var type_id = item.ID_TYPE_ET;
                    var portlet = ClassGENERIC.createPortlet(id, item.TXT, 0, type_id);
                    var cible = modalName + ' #divDetail div[data-id="' + id + '"] ';
                    $(modalName + ' #divDetail').append(
                        $('<div/>', {
                            'class': 'col-md-12'
                        }).append(
                            portlet)
                    );

                    /** ---------------------------------------------------------------------------
                     * Et maintenant, on recomplète (si besoin) la table
                     */
                    $.each(item.DETAIL, function (index, detail) {

                        var tr = $('<tr/>');
                        tr.append('<td data-pk="' + detail.ID + '" class="txtSmall" data-mod-id="' + detail.ID_MOD + '">' + detail.TXT_MOD + '</td>');
                        tr.append('<td data-pub-id="' + detail.ID_PUB + '">' + detail.TXT_PUB + '</td>');
                        tr.append('<td data-det-id="' + detail.ID_DET + '">' + detail.TXT_DET + '</td>');
                        tr.append('<td>' + detail.NB + '</td>');

                        tr.append('<td class="text-right">' +
                            '<a class="remove-mod btn btn-xs red"' +
                            ' data-placement="left" data-toggle="confirmation" data-original-title="" title="" ' +
                            ' data-id="' + detail.ID + '" ' +
                            '><i class="fa fa-remove"></i></a>' +
                            '</td>');

                        $(cible + ' [table-id="' + id + '"] tbody').append(tr);
                        $(cible + ' .remove-mod').confirmation({
                            btnOkLabel: 'OUI',
                            btnOkClass: 'red-mint btn-xs',
                            btnCancelLabel: 'NON',
                            placement: 'left',
                            title: 'Suppression ?',
                            singleton: true,
                            popout: true,
                            onConfirm: function () {

                                var oldVal = $(this).closest('tr').find('td:eq(3)').html();
                                var nbcumul = parseInt($(modalName + ' #ztNbPlacesET').val() || 0);
                                nbcumul = (nbcumul - oldVal);
                                $(modalName + ' #ztNbPlacesET').val(nbcumul);
                                //if ($(modalName + ' #ztNbPlacesET').val() == 0) {
                                //    $(modalName + ' #ztNbPlacesET').prop('readonly', false);
                                //}

                                var pk = $(this).closest('tr').children('td:first').attr('data-pk');
                                $.ajax({
                                    url: '/annuaire/detail_modalite/' + pk,
                                    type: 'DELETE',
                                    dataType: 'json',
                                    data: {
                                        _token: $(modalName + ' #token').val(),
                                    },
                                    async: false
                                }).complete(function (code_html, statut) {
                                });
                                $(this).closest('tr').remove();
                            }
                        });


                    });

                    $(modalName + ' #divDetail .del-portlet').confirmation({
                        btnOkLabel: 'OUI',
                        btnOkClass: 'red-mint btn-xs',
                        btnCancelLabel: 'NON',
                        placement: 'left',
                        title: 'Suppression ?',
                        singleton: true,
                        popout: true,
                        onConfirm: function () {
                            var pk = $(this).attr('data-id');
                            $.ajax({
                                url: '/annuaire/etablissement_modalites/' + pk,
                                type: 'DELETE',
                                dataType: 'json',
                                data: {
                                    _token: $(modalName + ' #token').val(),
                                },
                                async: false
                            }).complete(function (code_html, statut) {
                            });
                            $(this).parent().parent().parent().remove();

                            /** -----------------------------------------------------------------------
                             * Si le détail est tout vide
                             */
                            if ($('#divDetail').html().trim() == '<div class="col-md-12"></div>') {
                                $(modalName + ' #zlSecteur').prop('readonly', false);
                                $(modalName + ' #zlSecteur').trigger('change');
                                $(modalName + ' #zlTypeAge').prop('readonly', false);
                                $(modalName + ' #ccHANDICAP').prop('readonly', false);
                            }
                        }
                    });

                    /** ---------------------------------------------------------------------------
                     * Pour terminer, on initialise le contenu des zones
                     */
                    ClassGENERIC.initSpecif(cible, '#zlModaliteAccueil', '', isSMS, 'modalites');
                    ClassGENERIC.initGeneric(cible, 'typepublic', '#zlTypePublic');
                    $(cible + ' #zlDetailTypePublic').select2({
                        width: '100%',
                    }).prop('readonly', true);

                    /** ---------------------------------------------------------------------------
                     * On bind le CHANGE
                     */
                    ClassET.bindZL_portlet(modalName, cible, id, 1);
                });

                if (hasOnePortlet) {
                    /** ---------------------------------------------------------------------------
                     * Les ZONES SECTEUR / SECTEUR SMS / HANDICAP et TRANCHE D'AGE
                     * deviennent disabled
                     */
                    $(modalName + ' #zlSecteur').prop('disabled', true)
                    $(modalName + ' #zlSecteurSMS').prop('readonly', true);
                    $(modalName + ' #ccHANDICAP').prop('readonly', true);
                    $(modalName + ' #zlTypeAge').prop('readonly', true);
                }
                /** -------------------------------------------------------------------------------
                 * On va recréer les PORTLETS
                 */

                /** -------------------------------------------------------------------------------
                 * Evènements
                 */
                ClassGENERIC.manageModalButtons(modalName, 1, 'et', idET);
                ClassGENERIC.updateRemoveButton(modalName, 'telephone', 'et');
                ClassGENERIC.updateRemoveButton(modalName, 'mail', 'et');
                ClassGENERIC.updatePrefereButton(modalName, 'telephone', 'et');
                ClassGENERIC.updatePrefereButton(modalName, 'mail', 'et');
                ClassGENERIC.manageModalCC(modalName);

                /** -------------------------------------------------------------------------------
                 * Attributs des listes par défaut
                 */
                if (bi == 0) {
                    $(modalName + ' #zlReceptionBI').select2('enable', false);
                }
                if (cf == 0) {
                    $(modalName + ' #zlReceptionCF').select2('enable', false);
                }

                /** -----------------------------------------------------------------------------------
                 * Le Reste
                 */
                ClassGENERIC.refreshEdited(modalName);

                /** -------------------------------------------------------------------------------
                 * Bind de la modification
                 */
                $(":input").inputmask();
                $(modalName + ' .date-picker').datepicker({
                    autoclose: true,
                    calendarWeeks: true,
                    format: 'dd/mm/yyyy',
                    language: 'fr',
                    today: true
                });
                /** ---------------------------------------------------------------------------------------
                 * Bouton de recopie de l'adresse
                 */
                $(modalName + ' #btnCopieData').unbind().on('click', function () {
                    if (idPO != null) {
                        ClassET.recopieDataPO(idPO, modalName);
                    } else {
                        ClassET.recopieDataOO(idOO, modalName);
                    }
                    ClassGENERIC.refreshEdited(modalName);
                });

                $(modalName + ' #btnAction').unbind().on('click', function () {


                    // ------------------------------------------------------------------------------------
                    // Données de base
                    // ------------------------------------------------------------------------------------
                    nom = $(modalName + ' #ztNom').val().trim().toUpperCase();
                    sigle = $(modalName + ' #ztSigle').val().trim().toUpperCase();
                    adr1 = $(modalName + ' #ztAdr1').val().trim().toUpperCase();
                    adr2 = $(modalName + ' #ztAdr2').val().trim().toUpperCase();
                    adr3 = $(modalName + ' #ztAdr3').val().trim().toUpperCase();
                    cp = $(modalName + ' #ztCP').val();
                    ville = $(modalName + ' #ztVille').val().trim().toUpperCase();
                    pays = $(modalName + ' #zlPays').val();

                    // ------------------------------------------------------------------------------------
                    // Caractéristiques Établissement / Service
                    // ------------------------------------------------------------------------------------
                    nbplaces = $(modalName + ' #ztNbPlacesET').val();
                    dispositif = $(modalName + ' #ccDISP').is(":checked") ? 1 : 0;
                    secteur = $(modalName + ' #zlSecteur').val();
                    secteursms = $(modalName + ' #zlSecteurSMS').val();
                    handicap = $(modalName + ' #ccHANDICAP').is(":checked") ? 1 : 0;
                    typeAge = $(modalName + ' #zlTypeAge').val();
                    if ($(modalName + ' #ccHANDICAP').closest('.section-handicap').hasClass('display-none')) {
                        handicap = 0;
                    }
                    arrete = $(modalName + ' #ztArrete').val();
                    datearrete = $(modalName + ' #ztDateArrete').val();
                    age = $(modalName + ' #ztAge').val();
                    finess = $(modalName + ' #ztFINESS').val().trim().toUpperCase();
                    projet = $(modalName + ' #ccPROJ').is(":checked") ? 1 : 0;
                    opening = $(modalName + ' #ztDateOpen').val();

                    /** -----------------------------------------------------------------------------------
                     * Données Complémentaires
                     */
                    nl = $(modalName + ' #ccNL').is(":checked") ? 1 : 0;
                    bi = $(modalName + ' #ccBI').is(":checked") ? 1 : 0;
                    bi_recep = (bi == 1 ? $(modalName + ' #zlReceptionBI').val() : null);
                    cf = $(modalName + ' #ccCF').is(":checked") ? 1 : 0;
                    cf_recep = (cf == 1 ? $(modalName + ' #zlReceptionCF').val() : null);
                    contrib = $(modalName + ' #ccCONT').is(":checked") ? 1 : 0;
                    code = $(modalName + ' #ztCodeInterne').val().trim().toUpperCase();

                    /** -----------------------------------------------------------------------------------
                     * Modalités --> on n'en passe pas dans les UPDATE !
                     */

                    /** -----------------------------------------------------------------------------------
                     * Le Reste
                     */
                    pole = $(modalName + ' #zlPole').val();
                    web = $(modalName + ' #ztWEB').val();
                    notes = $('<p>' + $(modalName + ' #ztNotes').val() + '</p>').text();
                    notespublic = $('<p>' + $(modalName + ' #ztNoteAccueilPublic').val() + '</p>').text();

                    actif = $(modalName + ' #ccACTIF').is(":checked") ? 1 : 0;
                    visible = $(modalName + ' #ccVISIBLE').is(":checked") ? 1 : 0;
                    acces = $(modalName + ' #ccACCES').is(":checked") ? 1 : 0;

                    /** -----------------------------------------------------------------------------------
                     * Un dernier petit contrôle
                     */
                    if ((bi == 1 && bi_recep == '') ||
                        (cf == 1 && cf_recep == '')) {
                        toastr.warning('Vérifier les données complémentaires.', "Attention");
                        return;
                    }

                    /** -----------------------------------------------------------------------------------
                     * On va rajouter un contrôle de vérification des MAILS si l'une des zones
                     * doit vérifier les mails
                     */
                    if (!ClassGENERIC.verifPresenceMail(modalName, bi, cf)) {
                        toastr.warning('Vous devez renseigner au moins un mail.', "Attention");
                        return;
                    }

                    /** -----------------------------------------------------------------------------------
                     * Contrôle de cohérence
                     */
                    var isMS = false;
                    if (secteur == '' || typeAge == '') {
                        toastr.warning('Les champs obligatoires doivent être saisis !', "Attention");
                        return;
                    }
                    if (!$(modalName + ' #zlSecteurSMS').prop('disabled')) {
                        isMS = true;
                        if (secteursms == '') {
                            toastr.warning('Le type de secteur SMS est obligatoire !', "Attention");
                            return;
                        }
                    }

                    $(this).off().removeClass('green').addClass('grey-cascade').html(
                        '<i class="fa fa-spin fa-spinner"></i> Patientez...'
                    );

                    // ----------------------------------------------------------------------------
                    // Les nouveaux téléphones uniquement
                    // ----------------------------------------------------------------------------
                    var tels = [];
                    $.each($(modalName + ' #tableTel tbody tr td:first-child'), function (index, row) {
                        if ($(this).attr('data-pk') == undefined) {
                            var telephone = $(this).attr('data-telephone');
                            var prefere = $(this).attr('data-prefere');
                            var type = $(this).attr('data-type-telephone');
                            var item = [telephone, prefere, type];
                            tels.push(item);
                        }
                    });
                    var mails = [];
                    $.each($(modalName + ' #tableMail tbody tr td:first-child'), function (index, row) {
                        if ($(this).attr('data-pk') == undefined) {
                            var mail = $(this).attr('data-mail');
                            var prefere = $(this).attr('data-prefere');
                            var type = $(this).attr('data-type-mail');
                            var item = [mail, prefere, type];
                            mails.push(item)
                        }
                        ;
                    });

                    $.ajax({
                        url: '/annuaire/' + routeEtabl + '/' + idET,
                        type: 'PUT',
                        data: {
                            _token: $(modalName + ' #token').val(),

                            /** ---------------------------------------------------------------------------
                             * Données de base
                             */
                            pPO: pole,

                            pNom: nom,
                            pSigle: sigle,
                            pAdr1: adr1,
                            pAdr2: adr2,
                            pAdr3: adr3,
                            pCp: cp,
                            pVille: ville,
                            pPays: pays,

                            /** ---------------------------------------------------------------------------
                             * Caractéristiques Établissement / Service
                             */
                            pNbPlaces : nbplaces,
                            pDispositif: dispositif,
                            pSecteur: secteur,
                            pSecteurSms: secteursms,
                            pHandicap: handicap,
                            pTypeAge: typeAge,

                            pArrete: arrete,
                            pDateArrete: datearrete,
                            pAge: age,
                            pFiness: finess,
                            pProjet: projet,
                            pOpening: opening,

                            /** ---------------------------------------------------------------------------
                             * Données Complémentaires
                             */
                            pNL: nl,
                            pBI: bi,
                            pBI_recep: bi_recep,
                            pCF: cf,
                            pCF_recep: cf_recep,
                            pContrib: contrib,
                            pCode: code,

                            /** ---------------------------------------------------------------------------
                             * Le Reste
                             */
                            pWeb: web,
                            pNotes: notes,
                            pNotes_public: notespublic,

                            pActif: actif,
                            pVisible: visible,
                            pAcces: acces,

                            /** ---------------------------------------------------------------------------
                             * Tableaux
                             */
                            pTels: tels,
                            pMails: mails,

                        },
                        dataType: 'json',
                        success: function (data) {
                            if (data == -1) {
                                $(modalName).modal('hide');
                                toastr.error('Modification non effectuée', "Attention");
                            } else {
                                $(modalName).modal('hide');
                                toastr.info('Modification enregistrée', "Information");
                                tableEtabl.ajax.reload();
                            }
                        }
                    });
                });
            }
        });

        $(modalName).on('hidden.bs.modal', function () {
            mode = MODE_DATATABLES;
        });
        waitingDialog.hide();
        $(modalName).modal('show');

    },

    view: function (level, idET, idOO, idPO) {

        waitingDialog.show('Préparation du Formulaire', {dialogSize: 'large', progressType: 'info'});

        var modalName = ClassGENERIC.newLevel(level);
        var modal = $(modalName);
        modal.html($('#formEtablissement').html());
        mode = MODE_FORM;

        /** ---------------------------------------------------------------------------------------
         * Fil d'Ariane
         *
         * Attention, il y a 2 header dans les FORMS (hors ORGANISME)
         * donc il faut bien distinguer celui avec le fil d'ariane et celui avec le CAPTION !
         */
        var titre = ClassGENERIC.arianeToTxt();
        $(modalName + ' #prec').empty().append(
            titre
        );

        /** ---------------------------------------------------------------------------------------
         * On récupère les infos de la civilite
         */
        $.ajax({
            url: '/annuaire/' + routeEtabl + '/' + idET,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {

                /** -------------------------------------------------------------------------------
                 * Chargement du template du modal dans le modal d'exécution
                 * Par défaut, les boutons ACTIONS sont cachés
                 */
                $(modalName).find('#btnAction').hide();
                $(modalName).find('#btnCopieData').hide();
                $(modalName).find('#btnAddTel').hide();
                $(modalName).find('#btnAddMail').hide();
                $(modalName).find('.modal-header').addClass('font-grey-salsa');
                $(modalName).find('.modal-title').html('<i class="fa fa-refresh"></i> Consultation ' + editEtabl);

                $(modalName).find('.historique').show().html(
                    'Modifié le ' + data.UPDATED_DATE + ' par ' + data.UPDATED_BY
                );

                /** -------------------------------------------------------------------------------
                 * LECTURE -> Données de base
                 */
                var nom = data.ET.nom;
                var sigle = data.ET.sigle;
                var adr1 = data.ET.adr1;
                var adr2 = data.ET.adr2;
                var adr3 = data.ET.adr3;
                var cp = data.ET.cp;
                var ville = data.ET.ville;
                var pays = data.ET.pays_id;

                /** -------------------------------------------------------------------------------
                 * Caractéristiques Établissement / Service
                 */
                var nbplaces = data.ET.nbplaces;
                var dispositif = data.ET.dispositif;
                var secteur = data.ET.type_secteur_id;
                var secteursms = data.ET.type_secteur_ms_id;
                var handicap = data.ET.handicap;
                var typeAge = data.ET.type_age_id;

                var arrete = data.ET.arrete_pref;
                var datearrete = data.ET.date_arrete;
                var age = data.ET.age;
                var finess = data.ET.finess;
                var projet = data.ET.projet;
                var opening = data.ET.date_ouverture;

                /** -------------------------------------------------------------------------------
                 * Données Complémentaires
                 */
                var nl = data.ET.nl;
                var bi = data.ET.bi;
                var bi_recep = data.ET.bi_reception_id;
                var cf = data.ET.cf;
                var cf_recep = data.ET.cf_reception_id
                var contrib = data.ET.isCONTRIBUANT;
                var code = data.ET.id_interne;

                /** -------------------------------------------------------------------------------
                 * Le Reste
                 */
                var pole = data.ET.po_id;
                var web = data.ET.web;
                var notes = data.ET.notes;
                var notespublic = data.ET.notesTypePublic;

                var actif = data.ET.active;
                var visible = data.ET.visible;
                var acces = data.ET.acces;

                /** -------------------------------------------------------------------------------
                 * AFFICHAGE -> Données de base
                 */
                $(modalName + ' #ztNom').val(nom);
                $(modalName + ' #ztSigle').val(sigle);
                $(modalName + ' #ztAdr1').val(adr1);
                $(modalName + ' #ztAdr2').val(adr2);
                $(modalName + ' #ztAdr3').val(adr3);
                $(modalName + ' #ztCP').val(cp);
                $(modalName + ' #ztVille').val(ville);
                $(modalName + ' #ztNbPlacesET').val(nbplaces);
                ClassGENERIC.initGeneric(modalName, 'pays', '#zlPays', pays);

                /** -------------------------------------------------------------------------------
                 * AFFICHAGE -> Caractéristiques Établissement / Service
                 */
                $(modalName + ' #ccDISP').prop('checked', (dispositif == 1 ? true : false));
                ClassGENERIC.initGeneric(modalName, 'typesecteur', '#zlSecteur', secteur);
                ClassGENERIC.initGeneric(modalName, 'typesecteurms', '#zlSecteurSMS', secteursms);
                ClassGENERIC.initGeneric(modalName, 'typeage', '#zlTypeAge', typeAge);

                /** -------------------------------------------------------------------------------
                 * On aura besoin de ça pour créer le PORTLET
                 */
                var isSMS = 0;
                if ($(modalName + ' #zlSecteur').val() != '') {
                    isSMS = $(modalName + ' #zlSecteur').find('option:selected').attr('data-ms');
                }
                else {
                    isSMS = 0;
                }

                /** -------------------------------------------------------------------------------
                 * Attention, pour les ZL
                 */
                ClassET.bindZL_base(modalName, idOO, idPO, 1);
                $(modalName + ' #zlSecteur').trigger('change');
                $(modalName + ' #zlSecteurSMS').trigger('change');
                $(modalName + ' #zlTypeEtabl').parent().hide();
                $(modalName + ' #ccHANDICAP').prop('checked', (handicap == 1 ? true : false));
                $(modalName + ' #btnAddModalite').hide();

                $(modalName + ' #ztArrete').val(arrete);
                $(modalName + ' #ztDateArrete').val(datearrete);
                $(modalName + ' #ztAge').val(age);
                $(modalName + ' #ztFINESS').val(finess);
                $(modalName + ' #ccPROJ').prop('checked', (projet == 1 ? true : false));
                $(modalName + ' #ztDateOpen').val(opening);
                $(modalName + ' #ztNoteAccueilPublic').val(notespublic);

                /** -------------------------------------------------------------------------------
                 * AFFICHAGE -> Données Complémentaires
                 */
                $(modalName + ' #ccNL').prop('checked', (nl == 1 ? true : false));
                $(modalName + ' #ccBI').prop('checked', (bi == 1 ? true : false));
                $(modalName + ' #ccCF').prop('checked', (cf == 1 ? true : false));
                $(modalName + ' #ccCONT').prop('checked', (contrib == 1 ? true : false));

                $(modalName + ' #ccACTIF').prop('checked', (actif == 1 ? true : false));
                $(modalName + ' #ccVISIBLE').prop('checked', (visible == 1 ? true : false));
                $(modalName + ' #ccACCES').prop('checked', (acces == 1 ? true : false));

                ClassGENERIC.initGeneric(modalName, 'reception', '#zlReceptionCF', cf_recep);
                ClassGENERIC.initGeneric(modalName, 'reception', '#zlReceptionBI', bi_recep);
                $('#portletTel .portlet-body').find('.row:first').hide();
                $('#portletMail .portlet-body').find('.row:first').hide();

                /** -------------------------------------------------------------------------------
                 * Le Reste
                 */
                $(modalName + ' #ztWEB').val(web);
                $(modalName + ' #ztNotes').val(notes);
                $(modalName + ' #ztCodeInterne').val(code);
                ClassGENERIC.initGeneric(modalName, 'poles', '#zlPole', pole, idOO);

                /** -------------------------------------------------------------------------------
                 * Init des zones TELEPHONES + MAILS
                 */
                ClassGENERIC.displayTels(modalName, data.TEL);
                ClassGENERIC.displayMails(modalName, data.MAIL);

                /** -------------------------------------------------------------------------------
                 * On va maintenant recréer les portlets
                 */
                $.each(data.MODALITES, function (index, item) {
                    var id = item.ID;
                    var type_id = item.ID_TYPE_ET;
                    var portlet = ClassGENERIC.createPortlet(id, item.TXT, 1, type_id);
                    var cible = modalName + ' #divDetail div[data-id="' + id + '"] ';
                    $(modalName + ' #divDetail').append(
                        $('<div/>', {
                            'class': 'col-md-12'
                        }).append(
                            portlet));

                    /** ---------------------------------------------------------------------------
                     * Et maintenant, on recomplète (si besoin) la table
                     */
                    $.each(item.DETAIL, function (index, detail) {

                        var tr = $('<tr/>');
                        tr.append('<td data-pk="' + detail.ID + '" class="txtSmall" data-mod-id="' + detail.ID_MOD + '">' + detail.TXT_MOD + '</td>');
                        tr.append('<td data-pub-id="' + detail.ID_PUB + '">' + detail.TXT_PUB + '</td>');
                        tr.append('<td data-det-id="' + detail.ID_DET + '">' + detail.TXT_DET + '</td>');
                        tr.append('<td>' + detail.NB + '</td>');

                        $(cible + ' [table-id="' + id + '"] tbody').append(tr);


                    });

                });


                /** -------------------------------------------------------------------------------
                 * Modification de l'aspect final du formulaires. Les zones de saisie prennent la
                 * classe edited. Les zones deviennent readonly et les boutons d'action sont
                 * cachés (au pire, comme il n'y a pas d'action, il ne se passerait rien...
                 */
                ClassGENERIC.refreshEdited(modalName);
<<<<<<< HEAD
                $(modalName).find('.form-control').each(function () {
                    $(this).prop('readonly', true);
                });
                $(modalName + ' input').each(function () {
                    $(this).prop('readonly', true);
                });
                $(modalName).find('.remove-telephone').hide();
                $(modalName).find('.remove-mail').hide();
=======
                ClassGENERIC.doReadOnly(modalName);
>>>>>>> debug8

            }
        });

        $(modalName).on('hidden.bs.modal', function () {
            mode = MODE_DATATABLES;
        })

        waitingDialog.hide();
        $(modalName).modal('show');

    },

    recopieDataOO: function (idOO, modalName) {

        /** ---------------------------------------------------------------------------------------
         * On récupère les données à recopier (adresse + BI + ...
         */
        $.ajax({
            url: '/datacopy/' + routeOrga + '/' + idOO,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {
                $(modalName + ' #ztAdr1').val(data.ADR1);
                $(modalName + ' #ztAdr2').val(data.ADR2);
                $(modalName + ' #ztAdr3').val(data.ADR3);
                $(modalName + ' #ztCP').val(data.CP);
                $(modalName + ' #ztVille').val(data.VILLE);
                $(modalName + ' #zlPays').val(data.PAYS).trigger("change");

                /*
                 $(modalName + ' #ccNL').prop('checked', (data.NL == 1 ? true : false));
                 $(modalName + ' #ccBI').prop('checked', (data.BI == 1 ? true : false));
                 $(modalName + ' #ccCF').prop('checked', (data.CF == 1 ? true : false));
                 $(modalName + ' #ccCONT').prop('checked', (data.CONTRIB == 1 ? true : false));

                 $(modalName + ' #zlReceptionBI').val(data.RECEP_BI).trigger("change");
                 $(modalName + ' #zlReceptionCF').val(data.RECEP_CF).trigger("change");

                 if (data.BI == 1) {
                 $(modalName + ' #zlReceptionBI').select2('enable');
                 }
                 if (data.CF == 1) {
                 $(modalName + ' #zlReceptionCF').select2('enable');
                 }
                 */
            }
        });

    },

    recopieDataPO: function (idPO, modalName) {

        /** ---------------------------------------------------------------------------------------
         * On récupère les données à recopier (adresse + BI + ...
         */
        $.ajax({
            url: '/datacopy/' + routePole + '/' + idPO,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {
                $(modalName + ' #ztAdr1').val(data.ADR1);
                $(modalName + ' #ztAdr2').val(data.ADR2);
                $(modalName + ' #ztAdr3').val(data.ADR3);
                $(modalName + ' #ztCP').val(data.CP);
                $(modalName + ' #ztVille').val(data.VILLE);
                $(modalName + ' #zlPays').val(data.PAYS).trigger("change");

                /*
                 $(modalName + ' #ccNL').prop('checked', (data.NL == 1 ? true : false));
                 $(modalName + ' #ccBI').prop('checked', (data.BI == 1 ? true : false));
                 $(modalName + ' #ccCF').prop('checked', (data.CF == 1 ? true : false));
                 $(modalName + ' #ccCONT').prop('checked', (data.CONTRIB == 1 ? true : false));

                 $(modalName + ' #zlReceptionBI').val(data.RECEP_BI).trigger("change");
                 $(modalName + ' #zlReceptionCF').val(data.RECEP_CF).trigger("change");

                 if (data.BI == 1) {
                 $(modalName + ' #zlReceptionBI').select2('enable');
                 }
                 if (data.CF == 1) {
                 $(modalName + ' #zlReceptionCF').select2('enable');
                 }
                 */
            }
        });

    },

    recopieDataET: function (idET, modalName) {

        /** ---------------------------------------------------------------------------------------
         * On récupère les données à recopier (adresse + BI + ...
         */
        $.ajax({
            url: '/datacopy/' + routeEtabl + '/' + idET,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {
                $(modalName + ' #ztAdr1').val(data.ADR1);
                $(modalName + ' #ztAdr2').val(data.ADR2);
                $(modalName + ' #ztAdr3').val(data.ADR3);
                $(modalName + ' #ztCP').val(data.CP);
                $(modalName + ' #ztVille').val(data.VILLE);
                $(modalName + ' #zlPays').val(data.PAYS).trigger("change");

                /*
                 $(modalName + ' #ccNL').prop('checked', (data.NL == 1 ? true : false));
                 $(modalName + ' #ccBI').prop('checked', (data.BI == 1 ? true : false));
                 $(modalName + ' #ccCF').prop('checked', (data.CF == 1 ? true : false));
                 $(modalName + ' #ccCONT').prop('checked', (data.CONTRIB == 1 ? true : false));

                 $(modalName + ' #zlReceptionBI').val(data.RECEP_BI).trigger("change");
                 $(modalName + ' #zlReceptionCF').val(data.RECEP_CF).trigger("change");

                 if (data.BI == 1) {
                 $(modalName + ' #zlReceptionBI').select2('enable');
                 }
                 if (data.CF == 1) {
                 $(modalName + ' #zlReceptionCF').select2('enable');
                 }
                 */
            }
        });

    },

    recopieDataAN: function (idAN, modalName) {

        /** ---------------------------------------------------------------------------------------
         * On récupère les données à recopier (adresse + BI + ...
         */
        $.ajax({
            url: '/datacopy/' + routeAntenne + '/' + idAN,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {
                $(modalName + ' #ztAdr1').val(data.ADR1);
                $(modalName + ' #ztAdr2').val(data.ADR2);
                $(modalName + ' #ztAdr3').val(data.ADR3);
                $(modalName + ' #ztCP').val(data.CP);
                $(modalName + ' #ztVille').val(data.VILLE);
                $(modalName + ' #zlPays').val(data.PAYS).trigger("change");

                $(modalName + ' #ccNL').prop('checked', (data.NL == 1 ? true : false));
                $(modalName + ' #ccBI').prop('checked', (data.BI == 1 ? true : false));
                $(modalName + ' #ccCF').prop('checked', (data.CF == 1 ? true : false));
                $(modalName + ' #ccCONT').prop('checked', (data.CONTRIB == 1 ? true : false));

                $(modalName + ' #zlReceptionBI').val(data.RECEP_BI).trigger("change");
                $(modalName + ' #zlReceptionCF').val(data.RECEP_CF).trigger("change");

                if (data.BI == 1) {
                    $(modalName + ' #zlReceptionBI').select2('enable');
                }
                if (data.CF == 1) {
                    $(modalName + ' #zlReceptionCF').select2('enable');
                }
            }
        });
    },

    initListes: function (modalName, idOO, idPO) {

        /** ---------------------------------------------------------------------------------------
         * zlSecteur + zlSecteurSMS + zlTypeAge
         * --> zlTypeEtabl
         */
        ClassGENERIC.initGeneric(modalName, 'typesecteur', '#zlSecteur');
        ClassGENERIC.initGeneric(modalName, 'typeage', '#zlTypeAge');
        ClassGENERIC.initGeneric(modalName, 'typesecteurms', '#zlSecteurSMS');

        /** ---------------------------------------------------------------------------------------
         * Listes de bases
         */
        ClassGENERIC.initGeneric(modalName, 'pays', '#zlPays');
        ClassGENERIC.initGeneric(modalName, 'reception', '#zlReceptionCF');
        ClassGENERIC.initGeneric(modalName, 'reception', '#zlReceptionBI');

        ClassGENERIC.initGeneric(modalName, 'typetel', '#portletTel #zlTypeSaisie');
        ClassGENERIC.initGeneric(modalName, 'typemail', '#portletMail #zlTypeSaisie');

        ClassGENERIC.initGeneric(modalName, 'poles', '#zlPole', idPO, idOO, idPO);

    },

    manageModalCC: function (modalName) {
        $(modalName + ' #ccCF').on('click', function () {
            if ($(modalName + ' #ccCF').is(':checked')) {
                $(modalName + ' #zlReceptionCF').select2('enable');
            } else {
                $(modalName + ' #zlReceptionCF').select2('enable', false);
            }
        });

        $(modalName + ' #ccBI').on('click', function () {
            if ($(modalName + ' #ccBI').is(':checked')) {
                $(modalName + ' #zlReceptionBI').select2('enable');
            } else {
                $(modalName + ' #zlReceptionBI').select2('enable', false);
            }
        });
    },

};

/** -----------------------------------------------------------------------------------------------
 * @class : ANTENNES
 * Version simplifiée
 */
var ClassAN = {

    manageAntennes: function (level, idET) {

        /** ---------------------------------------------------------------------------------------
         * Gestion des ANTENNES
         * La liste se trouve au niveau N, l'écran de saisie au niveau N+1
         */
        var modalName = ClassGENERIC.newLevel(level);
        var modal = $(modalName);
        modal.html($('#formListAntenne').html());
        $(modalName).find('.modal-dialog').removeClass('modal-full').addClass('modal-full-lg');

        /** ---------------------------------------------------------------------------------------
         * Ecriture du libellé parent
         */
        var titre = ClassGENERIC.arianeToTxt();
        $(modalName + ' .modal-header .col-md-12').empty().append(
            titre
        );

        ClassAN.initDataTable(modalName, idET);

        /** ---------------------------------------------------------------------------------------
         * On ajoute une ADRESSE D'ACCUEIL
         */
        $(modalName).unbind().on('click', '#btnAddAntenne', function () {
            ClassAN.add(modalName, idET);
        });

        $(modalName).on('hidden.bs.modal', function (e) {
            if (mode == MODE_DATATABLES) {
                ariane.pop();
            }
            tableEtabl.ajax.reload();
        });

        $(modalName).modal('show');
    },

    initDataTable: function (modalName, idET) {

        /* Antennes d'un établissemnts */
        subRoute = 'an/' + idET;

        tableAntenne = $(modalName + ' #' + tableNameAntenne).removeAttr('width').DataTable({
            "dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
            iDisplayLength:50,
            destroy: true,
            ajax: "/datatable/" + subRoute,
            serverSide: false,
            autoWidth: false,
            columnDefs: [
                {className: "dt-body-left", "targets": [0, 1, 2]},
                {className: "dt-body-right", "targets": [3]}
            ],
            columns: [
                null, null, null,
                {orderable: true, width: 120},
            ],
            lengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "Tous"]
            ],

            //fixedColumns: true,
            sPagingType: "bootstrap_full_numbers",
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
            },
            initComplete: function (settingssettings) {
                /** On change les largeurs par défaut des contrôles */
                var tableWrapper = $('#' + tableNameAntenne + '_wrapper');
                var tableLength = $('#' + tableNameAntenne + '_length');
                tableLength.css('width', '400px');
                tableWrapper.find('.dataTables_length select').addClass("form-control input-inline").select2();
                tableWrapper.find('.dataTables_filter input').addClass("form-control input-inline");
            },
            rowCallback: function (row, data, index) {

                var colStatut = 99;
                var colAction = 3;

                $('td:eq(0),td:eq(1)', row).addClass("txtSmall");
                $('td:eq(' + colAction + ')', row).find('.dropdown-toggle').dropdown();

                /** --------------------------------------------------------------------------------
                 * On checke la colonne ACTION - lien A
                 */
                $('td:eq(' + colAction + ')', row).children('a').bind('click', function () {
                    if ($(this).hasClass('visu')) {
                        ClassAN.view(modalName, $(this).attr('data-id'));
                    }
                });

                $('td:eq(' + colAction + ')', row).find('a').bind('click', function () {

                    var lib = $(this).closest('tr').find('td:first').find('span').attr('data-nom');
                    if ($(this).hasClass('personnel')) {
                        mode = MODE_DATATABLES;
                        ariane.push(lib);
                        ClassPERSONNEL.managePersonnel(modalName, null, null, null, $(this).attr('data-id'));
                    }
                    if ($(this).hasClass('adresse')) {
                        mode = MODE_DATATABLES;
                        ariane.push(lib);
                        ClassAA.manageAdresses(modalName, null, $(this).attr('data-id'));
                    }
                    if ($(this).hasClass('modif')) {
                        ClassAN.update(modalName, $(this).attr('data-id'));
                    }
                    if ($(this).hasClass('telephone')) {
                        ClassGENERIC.phoneList('#dialogExecLev9', 'an', $(this).attr('data-id'));
                    }
                    if ($(this).hasClass('mail')) {
                        ClassGENERIC.mailList('#dialogExecLev9', 'an', $(this).attr('data-id'));
                    }
                    if ($(this).hasClass('offres')) {
                        ClassGENERIC.offreList('#dialogExecLev9', 'an', $(this).attr('data-id'));
                    }
                });
            }
        });


        return true;
    },

    /** -------------------------------------------------------------------------------------------
     * Formulaire pour l'ajout d'une antenne
     *
     * @param level
     * @param idET
     */
    add: function (level, idET) {

        /** ---------------------------------------------------------------------------------------
         * Préparation du Formulaire
         */
        waitingDialog.show('Préparation du Formulaire', {dialogSize: 'large', progressType: 'info'});
        var modalName = ClassGENERIC.newLevel(level);
        var modal = $(modalName);
        modal.html($('#formAntenne').html());
        mode = MODE_FORM;

        /** ---------------------------------------------------------------------------------------
         * Fil d'Ariane
         *
         * Attention, il y a 2 header dans les FORMS (hors ORGANISME)
         * donc il faut bien distinguer celui avec le fil d'ariane et celui avec le CAPTION !
         */
        var titre = ClassGENERIC.arianeToTxt();
        $(modalName + ' #prec').empty().append(
            titre
        );

        $(modalName).find('#btnAction').addClass('green').html('<i class="fa fa-plus-circle"></i> Ajouter');
        $(modalName).find('.modal-header.caption').addClass('font-green');
        $(modalName).find('.modal-title').html('<i class="fa fa-plus-circle"></i> Ajout ' + editAntenne);
        $(modalName).find('.historique').hide();

        /** -----------------------------------------compo----------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        $(modalName).find('#btnCopieData').html('RECOPIER ETABLISSEMENT');

        $(modalName + ' .date-picker').datepicker({
            autoclose: true,
            calendarWeeks: true,
            format: 'dd/mm/yyyy',
            language: 'fr',
            today: true
        });

        /** ---------------------------------------------------------------------------------------
         * Init des LISTEs
         */
        ClassAN.initListes(modalName, idET);
        $('#portletTel .portlet-body').find('.row:first').show();
        $('#portletMail .portlet-body').find('.row:first').show();
        $(modalName + ' #zlModaliteAccueil').prop('readonly', false);

        /** ---------------------------------------------------------------------------------------
         *
         */
        ClassGENERIC.manageModalButtons(modalName, 0, 'an', undefined);

        /** ---------------------------------------------------------------------------------------
         * Bouton de recopie de l'adresse
         */
        $(modalName + ' #btnCopieData').unbind().on('click', function () {
            ClassAN.recopieDataET(idET, modalName);
            ClassGENERIC.refreshEdited(modalName);
        });

        /** ---------------------------------------------------------------------------------------
         * Sauvegarde
         */
        $(":input").inputmask();
        $(modalName + ' #btnAction').unbind().on('click', function () {

            /** -----------------------------------------------------------------------------------
             * Vérification des ZONES Mandatory
             */
            var oblig = true;
            $(modalName + ' .required').each(function () {
                if (!$(this).val().trim()) {
                    oblig = false;
                }
            });
            if (!oblig) {
                toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                return;
            }

            // ------------------------------------------------------------------------------------
            // Données de BASE
            // ------------------------------------------------------------------------------------
            var nom = $(modalName + ' #ztNom').val().trim().toUpperCase();

            var arrete = $(modalName + ' #ztArrete').val();
            var datearrete = $(modalName + ' #ztDateArrete').val();
            var dateouverture = $(modalName + ' #ztDateOpen').val();
            var finess = $(modalName + ' #ztFINESS').val();
            var modaliteAccueil = $(modalName + ' #zlModaliteAccueil').val();

            var adr1 = $(modalName + ' #ztAdr1').val().trim().toUpperCase();
            var adr2 = $(modalName + ' #ztAdr2').val().trim().toUpperCase();
            var adr3 = $(modalName + ' #ztAdr3').val().trim().toUpperCase();
            var cp = $(modalName + ' #ztCP').val();
            var ville = $(modalName + ' #ztVille').val().trim().toUpperCase();
            var pays = $(modalName + ' #zlPays').val();

            var notes = $('<p>' + $(modalName + ' #ztNotes').val() + '</p>').text();

            var contribuant = $(modalName + ' #ccCONT').is(":checked") ? 1 : 0;

            var actif = $(modalName + ' #ccACTIF').is(":checked") ? 1 : 0;
            var visible = $(modalName + ' #ccVISIBLE').is(":checked") ? 1 : 0;
            var acces = $(modalName + ' #ccACCES').is(":checked") ? 1 : 0;

            $(this).off().removeClass('green').addClass('grey-cascade').html(
                '<i class="fa fa-spin fa-spinner"></i> Patientez...'
            );

            // ------------------------------------------------------------------------------------
            // Le reste (
            // ------------------------------------------------------------------------------------
            var tels = [];
            $.each($(modalName + ' #tableTel tbody tr td:first-child'), function (index, row) {
                var telephone = $(this).attr('data-telephone');
                var prefere = $(this).attr('data-prefere');
                var type = $(this).attr('data-type-telephone');
                var item = [telephone, prefere, type];
                tels.push(item);
            });
            var mails = [];
            $.each($(modalName + ' #tableMail tbody tr td:first-child'), function (index, row) {
                var mail = $(this).attr('data-mail');
                var prefere = $(this).attr('data-prefere');
                var type = $(this).attr('data-type-mail');
                var item = [mail, prefere, type];
                mails.push(item);
            });

            $.ajax({
                url: '/annuaire/' + routeAntenne,
                type: 'POST',
                data: {
                    _token: $(modalName + ' #token').val(),

                    pET: idET,
                    pNom: nom,
                    pArrete: arrete,
                    pDateArrete: datearrete,
                    pFiness: finess,
                    pModaliteAccueil: modaliteAccueil,
                    pOpening: dateouverture,
                    pContrib: contribuant,
                    pAdr1: adr1,
                    pAdr2: adr2,
                    pAdr3: adr3,
                    pCP: cp,
                    pVille: ville,
                    pPays: pays,

                    pNotes: notes,
                    pTels: tels,
                    pMails: mails,

                    pActif: actif,
                    pVisible: visible,
                    pAcces: acces,

                },
                dataType: 'json',
                success: function (data) {
                    result = true;
                    $(modalName).modal('hide');
                    toastr.success('Saisie enregistrée', "Information");
                    tableAntenne.ajax.reload();
                }
            });

        });

        /** ---------------------------------------------------------------------------------------
         * Affichage
         */
        $(modalName).on('hidden.bs.modal', function () {
            mode = MODE_DATATABLES;
        })
        $(modalName).modal('show');
        waitingDialog.hide();

    },

    view: function (level, idAN) {

        waitingDialog.show('Préparation du Formulaire', {dialogSize: 'large', progressType: 'info'});
        var modalName = ClassGENERIC.newLevel(level);
        var modal = $(modalName);
        modal.html($('#formAntenne').html());
        mode = MODE_FORM;

        /** ---------------------------------------------------------------------------------------
         * Fil d'Ariane
         *
         * Attention, il y a 2 header dans les FORMS (hors ORGANISME)
         * donc il faut bien distinguer celui avec le fil d'ariane et celui avec le CAPTION !
         */
        var titre = ClassGENERIC.arianeToTxt();
        $(modalName + ' #prec').empty().append(
            titre
        );

        /** ---------------------------------------------------------------------------------------
         * On récupère les infos de l'adresse d'accueil
         */
        $.ajax({
            url: '/annuaire/' + routeAntenne + '/' + idAN,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {

                /** -------------------------------------------------------------------------------
                 * Chargement du template du modal dans le modal d'exécution
                 * Par défaut, les boutons ACTIONS sont cachés
                 */
                $(modalName).find('#btnCopieData').hide();
                $(modalName).find('#btnAction').hide();
                $(modalName).find('#btnAddTel').hide();
                $(modalName).find('#btnAddMail').hide();
                $(modalName).find('.modal-header').addClass('font-grey-salsa');
                $(modalName).find('.modal-title').html('<i class="fa fa-refresh"></i> Consultation ' + editAntenne);

                $(modalName).find('.historique').show().html(
                    'Modifié le ' + data.UPDATED_DATE + ' par ' + data.UPDATED_BY
                );

                /** -------------------------------------------------------------------------------
                 * Init des zones ANTENNE
                 */
                var nom = data.AN.nom;
                var arrete = data.AN.arrete_pref;
                var dateArrete = data.AN.date_arrete;
                var finess = data.AN.finess;
                var dateOpening = data.AN.date_ouverture;
                var adr1 = data.AN.adr1;
                var adr2 = data.AN.adr2;
                var adr3 = data.AN.adr3;
                var cp = data.AN.cp;
                var ville = data.AN.ville;
                var pays = data.AN.pays_id;

                var modalites = new Array();
                $.each(data.HEBERGEMENT, function (index, item) {
                    modalites.push(item.TYPE_ID);
                });

                var contribuant = data.AN.isCONTRIBUANT;

                var actif = data.AN.active;
                var visible = data.AN.visible;
                var acces = data.AN.acces;
                var notes = data.AN.notes;

                var idET = data.AN.etablissement_id;

                // --------------------------------------------------------------------------------
                // Données COMPLEMENTAIRES
                // --------------------------------------------------------------------------------
                $(modalName + ' #ztNom').val(nom);

                $(modalName + ' #ztArrete').val(arrete);
                $(modalName + ' #ztDateArrete').val(dateArrete);
                $(modalName + ' #ztFINESS').val(finess);
                $(modalName + ' #ztDateOpen').val(dateOpening);

                $(modalName + ' #ztAdr1').val(adr1);
                $(modalName + ' #ztAdr2').val(adr2);
                $(modalName + ' #ztAdr3').val(adr3);
                $(modalName + ' #ztCP').val(cp);
                $(modalName + ' #ztVille').val(ville);

                $(modalName + ' #ztNotes').val(notes);

                ClassGENERIC.initGeneric(modalName, 'pays', '#zlPays', pays);
                ClassGENERIC.initSpecif(modalName, '#zlModaliteAccueil', '', idET, 'modalitesEtablissement');

                if (modalites.length > 0) {
                    $(modalName + ' #zlModaliteAccueil').val(modalites).trigger('change');
                }
                $(modalName + ' #zlModaliteAccueil').prop('readonly', false);

                $(modalName + ' #ccACTIF').prop('checked', (actif == 1 ? true : false));
                $(modalName + ' #cVISIBLE').prop('checked', (visible == 1 ? true : false));
                $(modalName + ' #ccACCES').prop('checked', (acces == 1 ? true : false));
                $(modalName + ' #ccCONT').prop('checked', (contribuant == 1 ? true : false));

                $('#portletTel .portlet-body').find('.row:first').hide();
                $('#portletMail .portlet-body').find('.row:first').hide();

                /** -----------------------------------------------------------------------------------
                 * Init des zones TELEPHONES
                 */
                $.each(data.TEL, function (index, item) {
                    $(modalName + ' #tableTel tbody').append(
                        '<tr>' +
                        '<td data-pk="' + item.OWN_ID + '" ' +
                        '    data-type-telephone="' + item.TYPE_ID + '"' +
                        '    data-id="' + item.OO_ID + '"' +
                        '>' + item.numero +
                        (item.prefere == 1
                                ? ' <i class="fa fa-star prefere font-yellow" style="cursor:pointer"> </i>'
                                : ' <i class="fa fa-star-o prefere" style="cursor:pointer"> </i>'
                        ) +
                        '</td>' +
                        '<td>' + item.libelle + '</td>' +
                        '<td class="text-right">' +
                        '<a class="remove-telephone btn btn-xs red" ' +
                        ' data-placement="left" data-toggle="confirmation" data-original-title="" title="" ' +
                        '><i class="fa fa-remove"></i></a>' +
                        //'<a class="modif btn btn-xs blue"><i class="fa fa-edit"></i></a>' +
                        '</td>' +
                        '</tr>'
                    );
                });

                /** -----------------------------------------------------------------------------------
                 * Init des zones MAILS
                 */
                $.each(data.MAIL, function (index, item) {
                    $(modalName + ' #tableMail tbody').append(
                        '<tr>' +
                        '<tr>' +
                        '<td data-pk="' + item.OWN_ID + '" ' +
                        '    data-type-mail="' + item.TYPE_ID + '"' +
                        '    data-id="' + item.OO_ID + '"' +
                        '>' + item.mail +
                        (item.prefere == 1
                                ? ' <i class="fa fa-star prefere font-yellow" style="cursor:pointer"> </i>'
                                : ' <i class="fa fa-star-o prefere" style="cursor:pointer"> </i>'
                        ) +
                        '</td>' +
                        '<td>' + item.libelle + '</td>' +
                        '<td class="text-right">' +
                        '<a class="remove-mail btn btn-xs red"' +
                        ' data-placement="left" data-toggle="confirmation" data-original-title="" title="" ' +
                        '><i class="fa fa-remove"></i></a>' +
                        //'<a class="modif btn btn-xs blue"><i class="fa fa-edit"></i></a>' +
                        '</td>' +
                        '</tr>'
                    );
                });

<<<<<<< HEAD
                $(modalName).find('.form-control').each(function () {
                    $(this).prop('readonly', true);
                });
                $(modalName + ' input').each(function () {
                    $(this).prop('readonly', true);
                });
                $(modalName).find('.remove-telephone').hide();
                $(modalName).find('.remove-mail').hide();
=======
                ClassGENERIC.refreshEdited(modalName);
                ClassGENERIC.doReadOnly(modalName);
>>>>>>> debug8

            }
        });

        $(modalName).on('hidden.bs.modal', function () {
            mode = MODE_DATATABLES;
        });
        waitingDialog.hide();
        $(modalName).modal('show');

    },

    update: function (level, idAN) {

        waitingDialog.show('Préparation du Formulaire', {dialogSize: 'large', progressType: 'info'});
        var modalName = ClassGENERIC.newLevel(level);
        var modal = $(modalName);
        modal.html($('#formAntenne').html());
        mode = MODE_FORM;

        /** ---------------------------------------------------------------------------------------
         * Fil d'Ariane
         *
         * Attention, il y a 2 header dans les FORMS (hors ORGANISME)
         * donc il faut bien distinguer celui avec le fil d'ariane et celui avec le CAPTION !
         */
        var titre = ClassGENERIC.arianeToTxt();
        $(modalName + ' #prec').empty().append(
            titre
        );

        /** ---------------------------------------------------------------------------------------
         * On récupère les infos de l'adresse d'accueil
         */
        $.ajax({
            url: '/annuaire/' + routeAntenne + '/' + idAN,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {

                /** -------------------------------------------------------------------------------
                 * Chargement du template du modal dans le modal d'exécution
                 */
                $(modalName).find('#btnAction').addClass('yellow-gold').html('<i class="fa fa-plus-circle"></i> Modifier');
                $(modalName).find('.modal-header.caption').addClass('font-yellow-casablanca');
                $(modalName).find('.modal-title').html('<i class="fa fa-refresh"></i> Modification ' + editAntenne);

                $(modalName).find('.historique').show().html(
                    'Modifié le ' + data.UPDATED_DATE + ' par ' + data.UPDATED_BY
                );

                /** ---------------------------------------------------------------------------------------
                 * Chargement du template du modal dans le modal d'exécution
                 */
                $(modalName).find('#btnCopieData').html('RECOPIER ETABLISSEMENT');

                /** -------------------------------------------------------------------------------
                 * Init des zones ANTENNE
                 */
                var nom = data.AN.nom;
                var arrete = data.AN.arrete_pref;
                var dateArrete = data.AN.date_arrete;
                var finess = data.AN.finess;
                var dateOpening = data.AN.date_ouverture;
                var adr1 = data.AN.adr1;
                var adr2 = data.AN.adr2;
                var adr3 = data.AN.adr3;
                var cp = data.AN.cp;
                var ville = data.AN.ville;
                var pays = data.AN.pays_id;

                var modalites = new Array();
                $.each(data.HEBERGEMENT, function (index, item) {
                    modalites.push(item.TYPE_ID);
                });

                var contribuant = data.AN.isCONTRIBUANT;

                var actif = data.AN.active;
                var visible = data.AN.visible;
                var acces = data.AN.acces;
                var notes = data.AN.notes;

                var idET = data.AN.etablissement_id;

                // --------------------------------------------------------------------------------
                // Données COMPLEMENTAIRES
                // --------------------------------------------------------------------------------
                $(modalName + ' #ztNom').val(nom);

                $(modalName + ' #ztArrete').val(arrete);
                $(modalName + ' #ztDateArrete').val(dateArrete);
                $(modalName + ' #ztFINESS').val(finess);
                $(modalName + ' #ztDateOpen').val(dateOpening);

                $(modalName + ' #ztAdr1').val(adr1);
                $(modalName + ' #ztAdr2').val(adr2);
                $(modalName + ' #ztAdr3').val(adr3);
                $(modalName + ' #ztCP').val(cp);
                $(modalName + ' #ztVille').val(ville);

                $(modalName + ' #ztNotes').val(notes);

                ClassGENERIC.initGeneric(modalName, 'pays', '#zlPays', pays);
                ClassGENERIC.initSpecif(modalName, '#zlModaliteAccueil', '', idET, 'modalitesEtablissement');

                if (modalites.length > 0) {
                    $(modalName + ' #zlModaliteAccueil').val(modalites).trigger('change');
                }
                $(modalName + ' #zlModaliteAccueil').prop('readonly', false);

                $(modalName + ' #ccACTIF').prop('checked', (actif == 1 ? true : false));
                $(modalName + ' #cVISIBLE').prop('checked', (visible == 1 ? true : false));
                $(modalName + ' #ccACCES').prop('checked', (acces == 1 ? true : false));
                $(modalName + ' #ccCONT').prop('checked', (contribuant == 1 ? true : false));

                ClassGENERIC.initGeneric(modalName, 'typetel', '#portletTel #zlTypeSaisie');
                ClassGENERIC.initGeneric(modalName, 'typemail', '#portletMail #zlTypeSaisie');
                $('#portletTel .portlet-body').find('.row:first').show();
                $('#portletMail .portlet-body').find('.row:first').show();


                /** -------------------------------------------------------------------------------
                 * Init des zones TELEPHONES + MAILS
                 */
                ClassGENERIC.displayTels(modalName, data.TEL);
                ClassGENERIC.displayMails(modalName, data.MAIL);
                ClassGENERIC.refreshEdited(modalName);

                /** -------------------------------------------------------------------------------
                 * Evènements
                 */
                ClassGENERIC.manageModalButtons(modalName, 0, 'an', idAN);
                ClassGENERIC.updateRemoveButton(modalName, 'telephone', 'an');
                ClassGENERIC.updateRemoveButton(modalName, 'mail', 'an');
                ClassGENERIC.updatePrefereButton(modalName, 'telephone', 'an');
                ClassGENERIC.updatePrefereButton(modalName, 'mail', 'an');

                /** -------------------------------------------------------------------------------
                 * Bouton de recopie
                 */
                $(modalName + ' #btnCopieData').unbind().on('click', function () {
                    ClassAN.recopieDataET(idET, modalName);
                    ClassGENERIC.refreshEdited(modalName);
                });

                /** -------------------------------------------------------------------------------
                 * Bind de la modification
                 */
                $(":input").inputmask();
                $(modalName + ' #btnAction').unbind().on('click', function () {


                    // ----------------------------------------------------------------------------
                    // Données de BASE
                    // ----------------------------------------------------------------------------
                    // ------------------------------------------------------------------------------------
                    // Données de BASE
                    // ------------------------------------------------------------------------------------
                    nom = $(modalName + ' #ztNom').val().trim().toUpperCase();

                    arrete = $(modalName + ' #ztArrete').val();
                    dateArrete = $(modalName + ' #ztDateArrete').val();
                    dateOpening = $(modalName + ' #ztDateOpen').val();
                    finess = $(modalName + ' #ztFINESS').val();
                    modalites = $(modalName + ' #zlModaliteAccueil').val();

                    adr1 = $(modalName + ' #ztAdr1').val().trim().toUpperCase();
                    adr2 = $(modalName + ' #ztAdr2').val().trim().toUpperCase();
                    adr3 = $(modalName + ' #ztAdr3').val().trim().toUpperCase();
                    cp = $(modalName + ' #ztCP').val();
                    ville = $(modalName + ' #ztVille').val().trim().toUpperCase();
                    pays = $(modalName + ' #zlPays').val();

                    notes = $('<p>' + $(modalName + ' #ztNotes').val() + '</p>').text();

                    contribuant = $(modalName + ' #ccCONT').is(":checked") ? 1 : 0;

                    actif = $(modalName + ' #ccACTIF').is(":checked") ? 1 : 0;
                    visible = $(modalName + ' #ccVISIBLE').is(":checked") ? 1 : 0;
                    acces = $(modalName + ' #ccACCES').is(":checked") ? 1 : 0;

                    /** ---------------------------------------------------------------------------
                     * Vérification des ZONES Mandatory
                     */
                    var oblig = true;
                    $(modalName + ' .required').each(function () {
                        if (!$(this).val().trim()) {
                            oblig = false;
                        }
                    });
                    if (!oblig) {
                        toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                        return;
                    }

                    $(this).off().removeClass('green').addClass('grey-cascade').html(
                        '<i class="fa fa-spin fa-spinner"></i> Patientez...'
                    );

                    // ----------------------------------------------------------------------------
                    // Les nouveaux téléphones uniquement
                    // ----------------------------------------------------------------------------
                    var tels = [];
                    $.each($(modalName + ' #tableTel tbody tr td:first-child'), function (index, row) {
                        if ($(this).attr('data-pk') == undefined) {
                            var telephone = $(this).attr('data-telephone');
                            var prefere = $(this).attr('data-prefere');
                            var type = $(this).attr('data-type-telephone');
                            var item = [telephone, prefere, type];
                            tels.push(item);
                        }
                    });
                    var mails = [];
                    $.each($(modalName + ' #tableMail tbody tr td:first-child'), function (index, row) {
                        if ($(this).attr('data-pk') == undefined) {
                            var mail = $(this).attr('data-mail');
                            var prefere = $(this).attr('data-prefere');
                            var type = $(this).attr('data-type-mail');
                            var item = [mail, prefere, type];
                            mails.push(item)
                        }
                        ;
                    });

                    $.ajax({
                        url: '/annuaire/' + routeAntenne + '/' + idAN,
                        type: 'PUT',
                        data: {
                            _token: $(modalName + ' #token').val(),

                            pNom: nom,
                            pArrete: arrete,
                            pDateArrete: dateArrete,
                            pFiness: finess,
                            pModaliteAccueil: modalites,
                            pOpening: dateOpening,
                            pContrib: contribuant,
                            pAdr1: adr1,
                            pAdr2: adr2,
                            pAdr3: adr3,
                            pCP: cp,
                            pVille: ville,
                            pPays: pays,

                            pNotes: notes,
                            pTels: tels,
                            pMails: mails,

                            pActif: actif,
                            pVisible: visible,
                            pAcces: acces,

                        },
                        dataType: 'json',
                        success: function (data) {
                            if (data == -1) {
                                $(modalName).modal('hide');
                                toastr.error('Modification non effectuée', "Attention");
                            } else {
                                $(modalName).modal('hide');
                                toastr.info('Modification enregistrée', "Information");
                                tableAntenne.ajax.reload();
                            }
                        }
                    });
                });
            }
        });

        $(modalName).on('hidden.bs.modal', function () {
            mode = MODE_DATATABLES;
        });
        waitingDialog.hide();
        $(modalName).modal('show');

    },

    recopieDataET: function (idET, modalName) {

        /** ---------------------------------------------------------------------------------------
         * On récupère les données à recopier (adresse + BI + ...
         */
        $.ajax({
            url: '/datacopy/' + routeEtabl + '/' + idET,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {

                $(modalName + ' #ztArrete').val(data.ARRETE);
                $(modalName + ' #ztDateArrete').val(data.DATE_ARRETE);
                $(modalName + ' #ztFINESS').val(data.FINESS);
                $(modalName + ' #ztDateOpen').val(data.DATE_OUVERTURE);

                $(modalName + ' #zlModaliteAccueil').val(data.MODALITES).trigger('change');

                $(modalName + ' #ztAdr1').val(data.ADR1);
                $(modalName + ' #ztAdr2').val(data.ADR2);
                $(modalName + ' #ztAdr3').val(data.ADR3);
                $(modalName + ' #ztCP').val(data.CP);
                $(modalName + ' #ztVille').val(data.VILLE);
                $(modalName + ' #zlPays').val(data.PAYS).trigger("change");
            }
        });

    },

    recopieDataAN: function (idAN, modalName) {

        /** ---------------------------------------------------------------------------------------
         * On récupère les données à recopier (adresse + BI + ...
         */
        $.ajax({
            url: '/datacopy/' + routeAntenne + '/' + idAN,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {

                $(modalName + ' #ztAdr1').val(data.ADR1);
                $(modalName + ' #ztAdr2').val(data.ADR2);
                $(modalName + ' #ztAdr3').val(data.ADR3);
                $(modalName + ' #ztCP').val(data.CP);
                $(modalName + ' #ztVille').val(data.VILLE);
                $(modalName + ' #zlPays').val(data.PAYS).trigger("change");

            }
        });

    },

    initListes: function (modalName, idET, idAN) {

        /** ---------------------------------------------------------------------------------------
         * zlSecteur + zlSecteurSMS + zlTypeAge
         * --> zlTypeEtabl
         */

        /** ---------------------------------------------------------------------------------------
         * Listes de bases
         */
        ClassGENERIC.initGeneric(modalName, 'pays', '#zlPays');
        ClassGENERIC.initGeneric(modalName, 'typetel', '#portletTel #zlTypeSaisie');
        ClassGENERIC.initGeneric(modalName, 'typemail', '#portletMail #zlTypeSaisie');

        /** ---------------------------------------------------------------------------------------
         * On filtre aussi la liste des Modalités d'Accueil par rapport aux modalités de
         * l'établissement
         */

        ClassGENERIC.initSpecif(modalName, '#zlModaliteAccueil', '', idET, 'modalitesEtablissement');

    },

};

/** -----------------------------------------------------------------------------------------------
 * @class : ADRESSE D'ACCUEIL
 * Version simplifiée
 *
 * Attention, l'adresse peut être rattachée à un ETABLISSEMENT ou une ANTENNE
 */
var ClassAA = {

    manageAdresses: function (level, idET, idAN) {

        /** ---------------------------------------------------------------------------------------
         * Gestion des ADRESSES
         * La liste se trouve au niveau N, l'écran de saisie au niveau N+1
         */
        var modalName = ClassGENERIC.newLevel(level);
        var modal = $(modalName);
        modal.html($('#formListAdr').html());
        $(modalName).find('.modal-dialog').removeClass('modal-full').addClass('modal-full-lg');

        /** ---------------------------------------------------------------------------------------
         * Ecriture du libellé parent
         */
        var titre = ClassGENERIC.arianeToTxt();
        $(modalName + ' .modal-header .col-md-12').empty().append(
            titre
        );

        ClassAA.initDataTable(modalName, idET, idAN);

        /** ---------------------------------------------------------------------------------------
         * On ajoute une ADRESSE D'ACCUEIL
         */
        $(modalName).unbind().on('click', '#btnAddAdr', function () {
            ClassAA.add(modalName, idET, idAN);
        });

        $(modalName).on('hidden.bs.modal', function (e) {
            if (mode == MODE_DATATABLES) {
                ariane.pop();
            }
            if (idET != null) {
                tableEtabl.ajax.reload();
            } else {
                tableAntenne.ajax.reload();
            }
        });

        $(modalName).modal('show');
    },

    initDataTable: function (modalName, idET, idAN) {

        var subRoute;
        if (idET != null) {
            subRoute = 'aa/et/' + idET;
        } else {
            subRoute = 'aa/an/' + idAN;
        }

        tableAdr = $(modalName + ' #' + tableNameAdr).removeAttr('width').DataTable({
            "dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
            iDisplayLength:50,
            destroy: true,
            ajax: "/datatable/" + subRoute,
            serverSide: false,
            autoWidth: false,
            columnDefs: [
                {className: "dt-body-left", "targets": [0, 1, 2, 3]},
                {className: "dt-body-right", "targets": [4]}
            ],
            columns: [
                null, null, null, null,
                {orderable: true, width: 120},
            ],
            lengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "Tous"]
            ],

            //fixedColumns: true,
            sPagingType: "bootstrap_full_numbers",
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
            },
            initComplete: function (settingssettings) {
                /** On change les largeurs par défaut des contrôles */
                var tableWrapper = $('#' + tableNameAdr + '_wrapper');
                var tableLength = $('#' + tableNameAdr + '_length');
                tableLength.css('width', '400px');
                tableWrapper.find('.dataTables_length select').addClass("form-control input-inline").select2();
                tableWrapper.find('.dataTables_filter input').addClass("form-control input-inline");
            },
            rowCallback: function (row, data, index) {

                var colStatut = 99;
                var colAction = 4;

                $('td:eq(0),td:eq(1)', row).addClass("txtSmall");
                $('td:eq(' + colAction + ')', row).find('.dropdown-toggle').dropdown();

                /** --------------------------------------------------------------------------------
                 * On checke la colonne ACTION - lien A
                 */
                $('td:eq(' + colAction + ')', row).children('a').bind('click', function () {
                    if ($(this).hasClass('visu')) {
                        ClassAA.view(modalName, $(this).attr('data-id'));
                    }
                });

                $('td:eq(' + colAction + ')', row).find('a').bind('click', function () {

                    if ($(this).hasClass('modif')) {
                        ClassAA.update(modalName, $(this).attr('data-id'), idET, idAN);
                    }
                    if ($(this).hasClass('telephone')) {
                        ClassGENERIC.phoneList('#dialogExecLev9', 'aa', $(this).attr('data-id'));
                    }
                    if ($(this).hasClass('mail')) {
                        ClassGENERIC.mailList('#dialogExecLev9', 'aa', $(this).attr('data-id'));
                    }
                });

            }

        });


        return true;
    },

    /** -------------------------------------------------------------------------------------------
     * Formulaire pour l'ajoute d'une antenne
     *
     * @param level
     * @param idET
     * @param idAN
     */
    add: function (level, idET, idAN) {

        /** ---------------------------------------------------------------------------------------
         * Préparation du Formulaire
         */
        waitingDialog.show('Préparation du Formulaire', {dialogSize: 'large', progressType: 'info'});
        var modalName = ClassGENERIC.newLevel(level);
        var modal = $(modalName);
        modal.html($('#formAdr').html());
        mode = MODE_FORM;

        /** ---------------------------------------------------------------------------------------
         * Fil d'Ariane
         *
         * Attention, il y a 2 header dans les FORMS (hors ORGANISME)
         * donc il faut bien distinguer celui avec le fil d'ariane et celui avec le CAPTION !
         */
        var titre = ClassGENERIC.arianeToTxt();
        $(modalName + ' #prec').empty().append(
            titre
        );

        $(modalName).find('#btnAction').addClass('green').html('<i class="fa fa-plus-circle"></i> Ajouter');
        $(modalName).find('.modal-header.caption').addClass('font-green');
        $(modalName).find('.modal-title').html('<i class="fa fa-plus-circle"></i> Ajout ' + editAdr);

        $(modalName).find('.historique').hide();

        /** ---------------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        if (idET != null) {
            $(modalName).find('#btnCopieData').html('RECOPIER ETABLISSEMENT');
        } else {
            $(modalName).find('#btnCopieData').html('RECOPIER ANTENNE');
        }

        /** ---------------------------------------------------------------------------------------
         * Init des LISTEs
         */
        ClassAA.initListes(modalName, idET, idAN);
        $('#portletTel .portlet-body').find('.row:first').show();
        $('#portletMail .portlet-body').find('.row:first').show();

        /** ---------------------------------------------------------------------------------------
         *
         */
        ClassGENERIC.manageModalButtons(modalName, 0, 'aa', undefined);
        ClassAA.bindZL(modalName, idET, idAN);

        /** ---------------------------------------------------------------------------------------
         * Bouton de recopie de l'adresse
         */
        $(modalName + ' #btnCopieData').unbind().on('click', function () {
            if (idET != null) {
                ClassAA.recopieDataET(idET, modalName);
            } else {
                ClassAA.recopieDataAN(idAN, modalName);
            }
            ClassGENERIC.refreshEdited(modalName);
        });

        /** ---------------------------------------------------------------------------------------
         * Sauvegarde
         */
        $(":input").inputmask();
        $(modalName + ' #btnAction').unbind().on('click', function () {

            /** -----------------------------------------------------------------------------------
             * Vérification des ZONES Mandatory
             */
            var oblig = true;
            $(modalName + ' .required').each(function () {
                if (!$(this).val().trim()) {
                    oblig = false;
                }
            });
            if (!oblig) {
                toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                return;
            }

            // ------------------------------------------------------------------------------------
            // Données de BASE
            // ------------------------------------------------------------------------------------
            var nom = $(modalName + ' #ztNom').val().trim().toUpperCase();
            var typePublic = $(modalName + ' #zlTypePublic').val();
            var detailTypePublic = $(modalName + ' #zlDetailTypePublic').val();
            var nbplaces = $(modalName + ' #ztNbPlaces').val();
            var age = $(modalName + ' #ztAge').val();

            var adr1 = $(modalName + ' #ztAdr1').val().trim().toUpperCase();
            var adr2 = $(modalName + ' #ztAdr2').val().trim().toUpperCase();
            var adr3 = $(modalName + ' #ztAdr3').val().trim().toUpperCase();
            var cp = $(modalName + ' #ztCP').val();
            var ville = $(modalName + ' #ztVille').val().trim().toUpperCase();
            var pays = $(modalName + ' #zlPays').val();

            var notes = $('<p>' + $(modalName + ' #ztNotes').val() + '</p>').text();

            var actif = $(modalName + ' #ccACTIF').is(":checked") ? 1 : 0;
            //var visible = $(modalName + ' #ccVISIBLE').is(":checked") ? 1 : 0;
            //var acces = $(modalName + ' #ccACCES').is(":checked") ? 1 : 0;

            $(this).off().removeClass('green').addClass('grey-cascade').html(
                '<i class="fa fa-spin fa-spinner"></i> Patientez...'
            );

            // ------------------------------------------------------------------------------------
            // Le reste (
            // ------------------------------------------------------------------------------------
            var tels = [];
            $.each($(modalName + ' #tableTel tbody tr td:first-child'), function (index, row) {
                var telephone = $(this).attr('data-telephone');
                var type = $(this).attr('data-type-telephone');
                var prefere = $(this).attr('data-prefere');
                var item = [telephone, prefere, type];
                tels.push(item);
            });
            var mails = [];
            $.each($(modalName + ' #tableMail tbody tr td:first-child'), function (index, row) {
                var mail = $(this).attr('data-mail');
                var prefere = $(this).attr('data-prefere');
                var type = $(this).attr('data-type-mail');
                var item = [mail, prefere, type];
                mails.push(item);
            });

            $.ajax({
                url: '/annuaire/' + routeAdr,
                type: 'POST',
                data: {
                    _token: $(modalName + ' #token').val(),
                    pET: idET,
                    pAN: idAN,

                    pNom: nom,
                    pTypePublic: typePublic,
                    pDetailTypePublic: detailTypePublic,
                    pNbPlaces: nbplaces,
                    pAge: age,

                    pAdr1: adr1,
                    pAdr2: adr2,
                    pAdr3: adr3,
                    pCP: cp,
                    pVille: ville,
                    pPays: pays,

                    pNotes: notes,
                    pTels: tels,
                    pMails: mails,

                    pActif: actif,
                    //pVisible: visible,
                    //pAcces: acces,

                },
                dataType: 'json',
                success: function (data) {
                    result = true;
                    $(modalName).modal('hide');
                    toastr.success('Saisie enregistrée', "Information");
                    tableAdr.ajax.reload();
                }
            });

        });

        /** ---------------------------------------------------------------------------------------
         * Affichage
         */
        $(modalName).on('hidden.bs.modal', function () {
            mode = MODE_DATATABLES;
        });
        $(modalName).modal('show');
        waitingDialog.hide();

    },

    update: function (level, idAA, idET, idAN) {

        waitingDialog.show('Préparation du Formulaire', {dialogSize: 'large', progressType: 'info'});
        var modalName = ClassGENERIC.newLevel(level);
        var modal = $(modalName);
        modal.html($('#formAdr').html());
        mode = MODE_FORM;

        /** ---------------------------------------------------------------------------------------
         * Fil d'Ariane
         *
         * Attention, il y a 2 header dans les FORMS (hors ORGANISME)
         * donc il faut bien distinguer celui avec le fil d'ariane et celui avec le CAPTION !
         */
        var titre = ClassGENERIC.arianeToTxt();
        $(modalName + ' #prec').empty().append(
            titre
        );

        /** ---------------------------------------------------------------------------------------
         * On récupère les infos de l'adresse d'accueil
         */
        $.ajax({
            url: '/annuaire/' + routeAdr + '/' + idAA,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {

                /** -------------------------------------------------------------------------------
                 * Chargement du template du modal dans le modal d'exécution
                 */
                $(modalName).find('#btnAction').addClass('yellow-gold').html('<i class="fa fa-plus-circle"></i> Modifier');
                $(modalName).find('.modal-header.caption').addClass('font-yellow-casablanca');
                $(modalName).find('.modal-title').html('<i class="fa fa-refresh"></i> Modification ' + editAdr);

                $(modalName).find('.historique').show().html(
                    'Modifié le ' + data.UPDATED_DATE + ' par ' + data.UPDATED_BY
                );

                /** ---------------------------------------------------------------------------------------
                 * Chargement du template du modal dans le modal d'exécution
                 */
                if (idET != null) {
                    $(modalName).find('#btnCopieData').html('RECOPIER ETABLISSEMENT');
                } else {
                    $(modalName).find('#btnCopieData').html('RECOPIER ANTENNE');
                }

                /** -------------------------------------------------------------------------------
                 * Init des zones ADRESSE ACCUEIL
                 */
                var nom = data.AA.nom;
                var typePublic = data.AA.type_public_id;
                var detailTypePublic = data.AA.detail_type_public_id;

                var nbplaces = data.AA.nb_places;
                var age = data.AA.age;

                var adr1 = data.AA.adr1;
                var adr2 = data.AA.adr2;
                var adr3 = data.AA.adr3;
                var cp = data.AA.cp;
                var ville = data.AA.ville;
                var pays = data.AA.pays_id;

                var actif = data.AA.active;
                var notes = data.AA.notes;

                // --------------------------------------------------------------------------------
                // Données COMPLEMENTAIRES
                // --------------------------------------------------------------------------------
                $(modalName + ' #ztNom').val(nom);
                $(modalName + ' #ztNbPlaces').val(nbplaces);
                $(modalName + ' #ztAge').val(age);

                $(modalName + ' #ztAdr1').val(adr1);
                $(modalName + ' #ztAdr2').val(adr2);
                $(modalName + ' #ztAdr3').val(adr3);
                $(modalName + ' #ztCP').val(cp);
                $(modalName + ' #ztVille').val(ville);

                $(modalName + ' #ztNotes').val(notes);

                /** -------------------------------------------------------------------------------
                 * Et on BIND les événements
                 */
                ClassAA.bindZL(modalName, idET, idAN);

                /** -------------------------------------------------------------------------------
                 * Le type d'étalissement est géré par le watch, du coup, rien à faire
                 * de particulier ici.
                 */
                ClassGENERIC.initGeneric(modalName, 'typepublic', '#zlTypePublic', typePublic);
                $(modalName + ' #zlTypePublic').trigger('change');

                /** -------------------------------------------------------------------------------
                 * Le détail de type public est déjà setté par la zone ci-dessus, il suffit juste
                 * de mettre la VAL
                 */
                $(modalName + ' #zlDetailTypePublic').val(detailTypePublic).trigger('change');

                ClassGENERIC.initGeneric(modalName, 'pays', '#zlPays', pays);

                $(modalName + ' #ccACTIF').prop('checked', (actif == 1 ? true : false));

                ClassGENERIC.initGeneric(modalName, 'typetel', '#portletTel #zlTypeSaisie');
                ClassGENERIC.initGeneric(modalName, 'typemail', '#portletMail #zlTypeSaisie');
                $('#portletTel .portlet-body').find('.row:first').show();
                $('#portletMail .portlet-body').find('.row:first').show();

                /** -------------------------------------------------------------------------------
                 * Init des zones TELEPHONES
                 */
                ClassGENERIC.displayTels(modalName, data.TEL);
                ClassGENERIC.displayMails(modalName, data.MAIL);

                ClassGENERIC.refreshEdited(modalName);

                /** -------------------------------------------------------------------------------
                 * Evènements
                 */
                ClassGENERIC.manageModalButtons(modalName, 0, 'aa', idAA);
                ClassGENERIC.updateRemoveButton(modalName, 'telephone', 'aa');
                ClassGENERIC.updateRemoveButton(modalName, 'mail', 'aa');
                ClassGENERIC.updatePrefereButton(modalName, 'telephone', 'aa');
                ClassGENERIC.updatePrefereButton(modalName, 'mail', 'aa');

                /** -------------------------------------------------------------------------------
                 * Bind de la modification
                 */
                $(":input").inputmask();
                $(modalName + ' #btnAction').unbind().on('click', function () {


                    // ----------------------------------------------------------------------------
                    // Données de BASE
                    // ----------------------------------------------------------------------------
                    nom = $(modalName + ' #ztNom').val().trim().toUpperCase();
                    typePublic = $(modalName + ' #zlTypePublic').val();
                    detailTypePublic = $(modalName + ' #zlDetailTypePublic').val();

                    nbplaces = $(modalName + ' #ztNbPlaces').val();
                    age = $(modalName + ' #ztAge').val();

                    adr1 = $(modalName + ' #ztAdr1').val().trim().toUpperCase();
                    adr2 = $(modalName + ' #ztAdr2').val().trim().toUpperCase();
                    adr3 = $(modalName + ' #ztAdr3').val().trim().toUpperCase();
                    cp = $(modalName + ' #ztCP').val();
                    ville = $(modalName + ' #ztVille').val().trim().toUpperCase();
                    pays = $(modalName + ' #zlPays').val();

                    notes = $('<p>' + $(modalName + ' #ztNotes').val() + '</p>').text();

                    actif = $(modalName + ' #ccACTIF').is(":checked") ? 1 : 0;

                    /** ---------------------------------------------------------------------------
                     * Vérification des ZONES Mandatory
                     */
                    var oblig = true;
                    $(modalName + ' .required').each(function () {
                        if (!$(this).val().trim()) {
                            oblig = false;
                        }
                    });
                    if (!oblig) {
                        toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                        return;
                    }

                    $(this).off().removeClass('green').addClass('grey-cascade').html(
                        '<i class="fa fa-spin fa-spinner"></i> Patientez...'
                    );

                    // ----------------------------------------------------------------------------
                    // Les nouveaux téléphones uniquement
                    // ----------------------------------------------------------------------------
                    var tels = [];
                    $.each($(modalName + ' #tableTel tbody tr td:first-child'), function (index, row) {
                        if ($(this).attr('data-pk') == undefined) {
                            var telephone = $(this).attr('data-telephone');
                            var prefere = $(this).attr('data-prefere');
                            var type = $(this).attr('data-type-telephone');
                            var item = [telephone, prefere, type];
                            tels.push(item);
                        }
                    });
                    var mails = [];
                    $.each($(modalName + ' #tableMail tbody tr td:first-child'), function (index, row) {
                        if ($(this).attr('data-pk') == undefined) {
                            var mail = $(this).attr('data-mail');
                            var prefere = $(this).attr('data-prefere');
                            var type = $(this).attr('data-type-mail');
                            var item = [mail, prefere, type];
                            mails.push(item)
                        }
                        ;
                    });

                    $.ajax({
                        url: '/annuaire/' + routeAdr + '/' + idAA,
                        type: 'PUT',
                        data: {
                            _token: $(modalName + ' #token').val(),

                            pNom: nom,
                            pTypePublic: typePublic,
                            pDetailTypePublic: detailTypePublic,

                            pNbPlaces: nbplaces,
                            pAge: age,
                            pAdr1: adr1,
                            pAdr2: adr2,
                            pAdr3: adr3,
                            pCP: cp,
                            pVille: ville,
                            pPays: pays,
                            pNotes: notes,

                            pTels: tels,
                            pMails: mails,

                            pActif: actif,
                        },
                        dataType: 'json',
                        success: function (data) {
                            if (data == -1) {
                                $(modalName).modal('hide');
                                toastr.error('Modification non effectuée', "Attention");
                            } else {
                                $(modalName).modal('hide');
                                toastr.info('Modification enregistrée', "Information");
                                tableAdr.ajax.reload();
                            }
                        }
                    });
                });
            }
        });

        $(modalName).on('hidden.bs.modal', function () {
            mode = MODE_DATATABLES;
        });
        waitingDialog.hide();
        $(modalName).modal('show');

    },

    view: function (level, idAA) {

        waitingDialog.show('Préparation du Formulaire', {dialogSize: 'large', progressType: 'info'});
        var modalName = ClassGENERIC.newLevel(level);
        var modal = $(modalName);
        modal.html($('#formAdr').html());
        mode = MODE_FORM;

        /** ---------------------------------------------------------------------------------------
         * Fil d'Ariane
         *
         * Attention, il y a 2 header dans les FORMS (hors ORGANISME)
         * donc il faut bien distinguer celui avec le fil d'ariane et celui avec le CAPTION !
         */
        var titre = ClassGENERIC.arianeToTxt();
        $(modalName + ' #prec').empty().append(
            titre
        );

        /** ---------------------------------------------------------------------------------------
         * On récupère les infos de l'adresse d'accueil
         */
        $.ajax({
            url: '/annuaire/' + routeAdr + '/' + idAA,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {

                /** -------------------------------------------------------------------------------
                 * Chargement du template du modal dans le modal d'exécution
                 */
                $(modalName).find('#btnAction').hide();
                $(modalName).find('#btnCopieData').hide();
                $(modalName).find('#btnAddTel').hide();
                $(modalName).find('#btnAddMail').hide();
                $(modalName).find('.modal-header').addClass('font-grey-salsa');
                $(modalName).find('.modal-title').html('<i class="fa fa-refresh"></i> Consultation ' + editAntenne);

                $(modalName).find('.historique').show().html(
                    'Modifié le ' + data.UPDATED_DATE + ' par ' + data.UPDATED_BY
                );

                /** -------------------------------------------------------------------------------
                 * Init des zones ADRESSE ACCUEIL
                 */
                var nom = data.AA.nom;
                var typePublic = data.AA.type_public_id;
                var detailTypePublic = data.AA.detail_type_public_id;

                var nbplaces = data.AA.nb_places;
                var age = data.AA.age;

                var adr1 = data.AA.adr1;
                var adr2 = data.AA.adr2;
                var adr3 = data.AA.adr3;
                var cp = data.AA.cp;
                var ville = data.AA.ville;
                var pays = data.AA.pays_id;

                var actif = data.AA.active;
                var notes = data.AA.notes;

                // --------------------------------------------------------------------------------
                // Données COMPLEMENTAIRES
                // --------------------------------------------------------------------------------
                $(modalName + ' #ztNom').val(nom);
                $(modalName + ' #ztNbPlaces').val(nbplaces);
                $(modalName + ' #ztAge').val(age);

                $(modalName + ' #ztAdr1').val(adr1);
                $(modalName + ' #ztAdr2').val(adr2);
                $(modalName + ' #ztAdr3').val(adr3);
                $(modalName + ' #ztCP').val(cp);
                $(modalName + ' #ztVille').val(ville);

                $(modalName + ' #ztNotes').val(notes);

                /** -------------------------------------------------------------------------------
                 * Le type d'étalissement est géré par le watch, du coup, rien à faire
                 * de particulier ici.
                 */
                ClassGENERIC.initGeneric(modalName, 'typepublic', '#zlTypePublic', typePublic);
                $(modalName + ' #zlTypePublic').trigger('change');

                /** -------------------------------------------------------------------------------
                 * Le détail de type public est déjà setté par la zone ci-dessus, il suffit juste
                 * de mettre la VAL
                 */
                $(modalName + ' #zlDetailTypePublic').val(detailTypePublic).trigger('change');

                ClassGENERIC.initGeneric(modalName, 'pays', '#zlPays', pays);

                $(modalName + ' #ccACTIF').prop('checked', (actif == 1 ? true : false));

                $('#portletTel .portlet-body').find('.row:first').hide();
                $('#portletMail .portlet-body').find('.row:first').hide();

                /** -----------------------------------------------------------------------------------
                 * Init des zones TELEPHONES
                 */
                $.each(data.TEL, function (index, item) {
                    $(modalName + ' #tableTel tbody').append(
                        '<tr>' +
                        '<td data-pk="' + item.OWN_ID + '" ' +
                        '    data-type-telephone="' + item.TYPE_ID + '"' +
                        '    data-id="' + item.OO_ID + '"' +
                        '>' + item.numero +
                        (item.prefere == 1
                                ? ' <i class="fa fa-star prefere font-yellow" style="cursor:pointer"> </i>'
                                : ' <i class="fa fa-star-o prefere" style="cursor:pointer"> </i>'
                        ) +
                        '</td>' +
                        '<td>' + item.libelle + '</td>' +
                        '<td class="text-right">' +
                        '<a class="remove-telephone btn btn-xs red" ' +
                        ' data-placement="left" data-toggle="confirmation" data-original-title="" title="" ' +
                        '><i class="fa fa-remove"></i></a>' +
                        //'<a class="modif btn btn-xs blue"><i class="fa fa-edit"></i></a>' +
                        '</td>' +
                        '</tr>'
                    );
                });

                /** -----------------------------------------------------------------------------------
                 * Init des zones MAILS
                 */
                $.each(data.MAIL, function (index, item) {
                    $(modalName + ' #tableMail tbody').append(
                        '<tr>' +
                        '<tr>' +
                        '<td data-pk="' + item.OWN_ID + '" ' +
                        '    data-type-mail="' + item.TYPE_ID + '"' +
                        '    data-id="' + item.OO_ID + '"' +
                        '>' + item.mail +
                        (item.prefere == 1
                                ? ' <i class="fa fa-star prefere font-yellow" style="cursor:pointer"> </i>'
                                : ' <i class="fa fa-star-o prefere" style="cursor:pointer"> </i>'
                        ) +
                        '</td>' +
                        '<td>' + item.libelle + '</td>' +
                        '<td class="text-right">' +
                        '<a class="remove-mail btn btn-xs red"' +
                        ' data-placement="left" data-toggle="confirmation" data-original-title="" title="" ' +
                        '><i class="fa fa-remove"></i></a>' +
                        //'<a class="modif btn btn-xs blue"><i class="fa fa-edit"></i></a>' +
                        '</td>' +
                        '</tr>'
                    );
                });

                /** -------------------------------------------------------------------------------
                 * Modification de l'aspect final du formulaires. Les zones de saisie prennent la
                 * classe edited. Les zones deviennent readonly et les boutons d'action sont
                 * cachés (au pire, comme il n'y a pas d'action, il ne se passerait rien...
                 */
                ClassGENERIC.refreshEdited(modalName);
<<<<<<< HEAD
                $(modalName).find('.form-control').each(function () {
                    $(this).prop('readonly', true);
                });
                $(modalName + ' input').each(function () {
                    $(this).prop('readonly', true);
                });
                $(modalName).find('.remove-telephone').hide();
                $(modalName).find('.remove-mail').hide();
=======
                ClassGENERIC.doReadOnly(modalName);
>>>>>>> debug8

            }
        });

        $(modalName).on('hidden.bs.modal', function () {
            mode = MODE_DATATABLES;
        });
        waitingDialog.hide();
        $(modalName).modal('show');

    },

    recopieDataET: function (idET, modalName) {

        /** ---------------------------------------------------------------------------------------
         * On récupère les données à recopier (adresse + BI + ...
         */
        $.ajax({
            url: '/datacopy/' + routeEtabl + '/' + idET,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {

                $(modalName + ' #ztNbPlaces').val(data.NB_PLACES);
                $(modalName + ' #ztAge').val(data.AGE);

                $(modalName + ' #ztAdr1').val(data.ADR1);
                $(modalName + ' #ztAdr2').val(data.ADR2);
                $(modalName + ' #ztAdr3').val(data.ADR3);
                $(modalName + ' #ztCP').val(data.CP);
                $(modalName + ' #ztVille').val(data.VILLE);
                $(modalName + ' #zlPays').val(data.PAYS).trigger("change");

                /*
                 if (data.TYPE_PUBLIC != '') {
                 $(modalName + ' #zlTypePublic').val(data.TYPE_PUBLIC).trigger("change");
                 if (data.DETAIL_TYPE_PUBLIC != '') {
                 $(modalName + ' #zlDetailTypePublic').val(data.DETAIL_TYPE_PUBLIC).trigger("change");
                 }
                 }
                 */
            }
        });

    },

    recopieDataAN: function (idAN, modalName) {

        /** ---------------------------------------------------------------------------------------
         * On récupère les données à recopier (adresse + BI + ...
         */
        $.ajax({
            url: '/datacopy/' + routeAntenne + '/' + idAN,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {

                $(modalName + ' #ztAdr1').val(data.ADR1);
                $(modalName + ' #ztAdr2').val(data.ADR2);
                $(modalName + ' #ztAdr3').val(data.ADR3);
                $(modalName + ' #ztCP').val(data.CP);
                $(modalName + ' #ztVille').val(data.VILLE);
                $(modalName + ' #zlPays').val(data.PAYS).trigger("change");

                $(modalName + ' #ztNbPlaces').val(data.NB_PLACES);
                $(modalName + ' #ztAge').val(data.AGE);
                $(modalName + ' #zlTypePublic').val(data.TYPE_PUBLIC).trigger('change');
                $(modalName + ' #zlDetailTypePublic').val(data.DETAIL_TYPE_PUBLIC).trigger('change');

            }
        });

    },

    initListes: function (modalName, idET, idAN) {

        /** ---------------------------------------------------------------------------------------
         * zlSecteur + zlSecteurSMS + zlTypeAge
         * --> zlTypeEtabl
         */
        /** ---------------------------------------------------------------------------------------
         * Listes de bases
         */
        ClassGENERIC.initGeneric(modalName, 'typepublic', '#zlTypePublic');
        ClassGENERIC.initGeneric(modalName, 'pays', '#zlPays');

        ClassGENERIC.initGeneric(modalName, 'typetel', '#portletTel #zlTypeSaisie');
        ClassGENERIC.initGeneric(modalName, 'typemail', '#portletMail #zlTypeSaisie');

    },

    bindZL: function (modalName, idET, idAN) {

        $(modalName + ' ' + '#zlTypePublic').on('change', function (e) {

            if ($(this).val() != '') {
                ClassGENERIC.initGeneric(modalName, 'detailtypepublic', '#zlDetailTypePublic', '', $(this).val());
            } else {
                $(modalName + ' ' + '#zlDetailTypePublic').parent().parent().addClass('display-none');
            }

        });

    },

};

/** -----------------------------------------------------------------------------------------------
 * @class : PERSONNEL
 * Version simplifiée
 *
 * Attention, le PERSONNEL peut être rattaché à un ORGANISME
 * ou à un POLE ou à un ETABLISSEMENT ou à une ANTENNE
 */
var ClassPERSONNEL = {

    /** -------------------------------------------------------------------------------------------
     *
     * @param level
     * @param idOO
     * @param idPO
     * @param idET
     * @param idAN
     */
    managePersonnel: function (level, idOO, idPO, idET, idAN) {

        /** ---------------------------------------------------------------------------------------
         * Gestion des ETABLISSEMENTS
         * La liste se trouve au niveau N, l'écran de saisie au niveau N+1
         */
        var modalName = ClassGENERIC.newLevel(level);
        var modal = $(modalName);
        modal.html($('#formListPersonnel').html());
        $(modalName).find('.modal-dialog').removeClass('modal-full').addClass('modal-full-lg');

        /** ---------------------------------------------------------------------------------------
         * Ecriture du libellé parent
         */
        var titre = ClassGENERIC.arianeToTxt();
        $(modalName + ' .modal-header .col-md-12').empty().append(
            titre
        );

        ClassPERSONNEL.initDataTable(modalName, idOO, idPO, idET, idAN);

        /** ---------------------------------------------------------------------------------------
         * Affichage des PÔLES d'un ORGANISME
         */
        $(modalName).unbind().on('click', '#btnAddPersonnel', function () {
            ClassPERSONNEL.add(modalName, idOO, idPO, idET, idAN);
        });

        $(modalName).on('hidden.bs.modal', function (e) {
            ariane.pop();
            if (idOO != null) {
                tableOrga.ajax.reload();
            } else {
                if (idPO != null) {
                    tablePole.ajax.reload();
                } else {
                    if (idET != null) {
                        tableEtabl.ajax.reload();
                    } else {
                        tableAntenne.ajax.reload();
                    }
                }
            }
        });

        $(modalName).modal('show');
    },

    initDataTable: function (modalName, idOO, idPO, idET, idAN) {

        var subRoute;
        if (idOO != null) {
            subRoute = 'pe/oo/' + idOO;
        } else {
            if (idPO != null) {
                subRoute = 'pe/po/' + idPO;
            } else {
                if (idET != null) {
                    subRoute = 'pe/et/' + idET;
                } else {
                    subRoute = 'pe/an/' + idAN;
                }
            }
        }
        tablePersonnel = $(modalName + ' #' + tableNamePersonnel).removeAttr('width').DataTable({
            "dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
            iDisplayLength:50,
            destroy: true,
            ajax: "/datatable/" + subRoute,
            serverSide: false,
            autoWidth: false,
            columnDefs: [
                {className: "dt-body-left", "targets": [0, 1, 2, 3, 4]},
                {className: "dt-body-right", "targets": [5]}
            ],
            columns: [
                null, null, null, null, null,
                {orderable: true, width: 120},
            ],
            lengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "Tous"]
            ],

            //fixedColumns: true,
            sPagingType: "bootstrap_full_numbers",
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
            },
            initComplete: function (settingssettings) {
                /** On change les largeurs par défaut des contrôles */
                var tableWrapper = $('#' + tableNamePersonnel + '_wrapper');
                var tableLength = $('#' + tableNamePersonnel + '_length');
                tableLength.css('width', '400px');
                tableWrapper.find('.dataTables_length select').addClass("form-control input-inline").select2();
                tableWrapper.find('.dataTables_filter input').addClass("form-control input-inline");
            },
            rowCallback: function (row, data, index) {

                var colStatut = 99;
                var colAction = 5;

                $('td:eq(0),td:eq(1)', row).addClass("txtSmall");
                $('td:eq(' + colAction + ')', row).find('.dropdown-toggle').dropdown();


                /** --------------------------------------------------------------------------------
                 * On checke la colonne ACTION - lien A
                 */
                $('td:eq(' + colAction + ')', row).children('a').bind('click', function () {
                    if ($(this).hasClass('visu')) {
                        ClassPERSONNEL.view(modalName, $(this).attr('data-id'));
                    }
                });

                $('td:eq(' + colAction + ')', row).find('a').bind('click', function () {

                    /** ---------------------------------------------------------------------------
                     * Il n'y a pas de fil d'ariane sur le personnel !
                     */
                    //var lib = $(this).closest('tr').find('td:first').html();
                    //ariane.push(lib);

                    if ($(this).hasClass('modif')) {
                        ClassPERSONNEL.update(modalName, $(this).attr('data-id'), idOO, idPO, idET, idAN);
                    }
                    if ($(this).hasClass('telephone')) {
                        ClassGENERIC.phoneList('#dialogExecLev9', 'pe', $(this).attr('data-id'));
                    }
                    if ($(this).hasClass('mail')) {
                        ClassGENERIC.mailList('#dialogExecLev9', 'pe', $(this).attr('data-id'));
                    }
                });

            }

        });


        return true;
    },

    add: function (level, idOO, idPO, idET, idAN) {

        /** ---------------------------------------------------------------------------------------
         * Préparation du Formulaire
         */
        waitingDialog.show('Préparation du Formulaire', {dialogSize: 'large', progressType: 'info'});
        var modalName = ClassGENERIC.newLevel(level);
        var modal = $(modalName);
        modal.html($('#formPersonnel').html());
        mode = MODE_FORM;

        /** ---------------------------------------------------------------------------------------
         * Fil d'Ariane
         *
         * Attention, il y a 2 header dans les FORMS (hors ORGANISME)
         * donc il faut bien distinguer celui avec le fil d'ariane et celui avec le CAPTION !
         */
        var titre = ClassGENERIC.arianeToTxt();
        $(modalName + ' #prec').empty().append(
            titre
        );

        $(modalName).find('#btnAction').addClass('green').html('<i class="fa fa-plus-circle"></i> Ajouter');
        $(modalName).find('.modal-header.caption').addClass('font-green');
        $(modalName).find('.modal-title').html('<i class="fa fa-plus-circle"></i> Ajout ' + editPersonnel);
        $(modalName).find('.historique').hide();

        /** ---------------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        if (idOO != null) {
            $(modalName).find('#btnCopieData').html('RECOPIER ORGANISME');
        } else {
            if (idPO != null) {
                $(modalName).find('#btnCopieData').html('RECOPIER POLE');
            } else {
                if (idET != null) {
                    $(modalName).find('#btnCopieData').html('RECOPIER ETABLISSEMENT');
                } else {
                    $(modalName).find('#btnCopieData').html('RECOPIER ANTENNE');
                }
            }
        }

        $(modalName + ' .date-picker').datepicker({
            autoclose: true,
            calendarWeeks: true,
            format: 'dd/mm/yyyy',
            language: 'fr',
            today: true
        });

        /** ---------------------------------------------------------------------------------------
         * Init des LISTEs
         */
        ClassPERSONNEL.initListes(modalName, idOO, idPO, idET, idAN);
        $(modalName + ' #zlReceptionCF').select2('enable', false);
        $(modalName + ' #zlReceptionBI').select2('enable', false);
        $(modalName + ' #zlRoleAG').select2('enable', false);
        $(modalName + ' #zlRoleCA').select2('enable', false);
        $(modalName + ' #zlRoleBURO').select2('enable', false);
        $('#portletTel .portlet-body').find('.row:first').show();
        $('#portletMail .portlet-body').find('.row:first').show();

        /** ---------------------------------------------------------------------------------------
         *
         */
        ClassGENERIC.manageModalButtons(modalName, 0, 'pe', undefined);
        ClassPERSONNEL.manageModalCC(modalName);

        /** ---------------------------------------------------------------------------------------
         * Bouton de recopie de l'adresse
         */
        $(modalName + ' #btnCopieData').unbind().on('click', function () {
            if (idOO != null) {
                ClassET.recopieDataOO(idOO, modalName);
            } else {
                if (idPO != null) {
                    ClassET.recopieDataPO(idPO, modalName);
                } else {
                    if (idET != null) {
                        ClassET.recopieDataET(idET, modalName);
                    } else {
                        ClassAN.recopieDataAN(idAN, modalName)
                    }
                }
            }
            ClassGENERIC.refreshEdited(modalName);
        });

        /** ---------------------------------------------------------------------------------------
         * Sauvegarde
         */
        $(":input").inputmask();
        $(modalName + ' #btnAction').unbind().on('click', function () {

            /** -----------------------------------------------------------------------------------
             * Vérification des ZONES Mandatory
             */
            var oblig = true;
            $(modalName + ' .required').each(function () {
                if (!$(this).val().trim()) {
                    oblig = false;
                }
            });
            if (!oblig) {
                toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                return;
            }

            // ------------------------------------------------------------------------------------
            // Données de BASE
            // ------------------------------------------------------------------------------------
            var civilite = $(modalName + ' #zlCivilite').val();
            var nom = $(modalName + ' #ztNom').val().trim().toUpperCase();
            var prenom = $(modalName + ' #ztPrenom').val().trim().toUpperCase();
            var fonction = $(modalName + ' #zlFonction').val();
            var service = $(modalName + ' #ztService').val().trim().toUpperCase();
            var complementFonction = $(modalName + ' #ztComplementFonction').val().trim().toUpperCase();

            var adr1 = $(modalName + ' #ztAdr1').val().trim().toUpperCase();
            var adr2 = $(modalName + ' #ztAdr2').val().trim().toUpperCase();
            var adr3 = $(modalName + ' #ztAdr3').val().trim().toUpperCase();
            var cp = $(modalName + ' #ztCP').val();
            var ville = $(modalName + ' #ztVille').val().trim().toUpperCase();
            var pays = $(modalName + ' #zlPays').val();

            var notes = $('<p>' + $(modalName + ' #ztNotes').val() + '</p>').text();

            var actif = $(modalName + ' #ccACTIF').is(":checked") ? 1 : 0;
            var visible = $(modalName + ' #ccVISIBLE').is(":checked") ? 1 : 0;
            var acces = $(modalName + ' #ccACCES').is(":checked") ? 1 : 0;

            // ------------------------------------------------------------------------------------
            // Données COMPLEMENTAIRES
            // ------------------------------------------------------------------------------------
            var voeux = $(modalName + ' #ccVOEUX').is(":checked") ? 1 : 0;
            var nl = $(modalName + ' #ccNL').is(":checked") ? 1 : 0;
            var cotis = $(modalName + ' #ccCOTIS').is(":checked") ? 1 : 0;
            var bi = $(modalName + ' #ccBI').is(":checked") ? 1 : 0;
            var bi_recep = (bi == 1 ? $(modalName + ' #zlReceptionBI').val() : null);
            var cf = $(modalName + ' #ccCF').is(":checked") ? 1 : 0;
            var cf_recep = (cf == 1 ? $(modalName + ' #zlReceptionCF').val() : null);
            var ag = $(modalName + ' #ccAG').is(":checked") ? 1 : 0;
            var ag_role = (ag == 1 ? $(modalName + ' #zlRoleAG').val() : null);
            var bu = $(modalName + ' #ccBURO').is(":checked") ? 1 : 0;
            var bu_role = (bu == 1 ? $(modalName + ' #zlRoleBURO').val() : null);
            var ca = $(modalName + ' #ccCA').is(":checked") ? 1 : 0;
            var ca_role = (ca == 1 ? $(modalName + ' #zlRoleCA').val() : null);
            var ctc = $(modalName + ' #ccCTC').is(":checked") ? 1 : 0;
            var cc = $(modalName + ' #ccCC').is(":checked") ? 1 : 0;

            if ((cf == 1 && cf_recep == '') ||
                (bi == 1 && bi_recep == '') ||
                (ag == 1 && ag_role == '') ||
                (bu == 1 && bu_role == '') ||
                (ca == 1 && ca_role == '')) {
                toastr.warning('Vérifier les données complémentaires.', "Attention");
                return;
            }

            /** -----------------------------------------------------------------------------------
             * On va rajouter un contrôle de vérification des MAILS si l'une des zones
             * doit vérifier les mails
             */
            if (!ClassGENERIC.verifPresenceMail(modalName, bi, cf)) {
                toastr.warning('Vous devez renseigner au moins un mail.', "Attention");
                return;
            }

            $(this).off().removeClass('green').addClass('grey-cascade').html(
                '<i class="fa fa-spin fa-spinner"></i> Patientez...'
            );

            // ------------------------------------------------------------------------------------
            // Le reste (
            // ------------------------------------------------------------------------------------
            var tels = [];
            $.each($(modalName + ' #tableTel tbody tr td:first-child'), function (index, row) {
                var telephone = $(this).attr('data-telephone');
                var prefere = $(this).attr('data-prefere');
                var type = $(this).attr('data-type-telephone');
                var item = [telephone, prefere, type];
                tels.push(item);
            });
            var mails = [];
            $.each($(modalName + ' #tableMail tbody tr td:first-child'), function (index, row) {
                var mail = $(this).attr('data-mail');
                var prefere = $(this).attr('data-prefere');
                var type = $(this).attr('data-type-mail');
                var item = [mail, prefere, type];
                mails.push(item);
            });

            $.ajax({
                url: '/annuaire/' + routePersonnel,
                type: 'POST',
                data: {
                    _token: $(modalName + ' #token').val(),
                    pOO: idOO,
                    pPO: idPO,
                    pET: idET,
                    pAN: idAN,

                    pCivilite: civilite,
                    pNom: nom,
                    pPrenom: prenom,

                    pFonction: fonction,
                    pService: service,
                    pComplementFontion: complementFonction,

                    pAdr1: adr1,
                    pAdr2: adr2,
                    pAdr3: adr3,
                    pCP: cp,
                    pVille: ville,
                    pPays: pays,

                    pNotes: notes,

                    pVoeux: voeux,
                    pNL: nl,
                    pCOTIS: cotis,
                    pBI: bi,
                    pBI_Recep: bi_recep,

                    pCF: cf,
                    pCF_Recep: cf_recep,
                    pAG: ag,
                    pAG_Role: ag_role,

                    pBU: bu,
                    pBU_Role: bu_role,
                    pCA: ca,
                    pCA_Role: ca_role,

                    pCTC: ctc,
                    pCC: cc,

                    pTels: tels,
                    pMails: mails,

                    pActif: actif,
                    pVisible: visible,
                    pAcces: acces,

                },
                dataType: 'json',
                success: function (data) {
                    $(modalName).modal('hide');
                    toastr.success('Saisie enregistrée', "Information");
                    tablePersonnel.ajax.reload();
                }
            });

        });

        /** ---------------------------------------------------------------------------------------
         * Affichage
         */
        $(modalName).on('hidden.bs.modal', function () {
            mode = MODE_DATATABLES;
        })
        $(modalName).modal('show');
        waitingDialog.hide();

    },

    update: function (level, idPE, idOO, idPO, idET, idAN) {

        /** ---------------------------------------------------------------------------------------
         * On est toujours 1 niveau de plus que le datatable
         */
        waitingDialog.show('Préparation du Formulaire', {dialogSize: 'large', progressType: 'info'});
        var modalName = ClassGENERIC.newLevel(level);
        var modal = $(modalName);
        modal.html($('#formPersonnel').html());
        mode = MODE_FORM;

        /** ---------------------------------------------------------------------------------------
         * Fil d'Ariane
         *
         * Attention, il y a 2 header dans les FORMS (hors ORGANISME)
         * donc il faut bien distinguer celui avec le fil d'ariane et celui avec le CAPTION !
         */
        var titre = ClassGENERIC.arianeToTxt();
        $(modalName + ' #prec').empty().append(
            titre
        );

        /** ---------------------------------------------------------------------------------------
         * On récupère les infos de la civilite
         */
        $.ajax({
            url: '/annuaire/' + routePersonnel + '/' + idPE,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {

                /** -------------------------------------------------------------------------------
                 * Chargement du template du modal dans le modal d'exécution
                 */
                $(modalName).find('#btnAction').addClass('yellow-gold').html('<i class="fa fa-plus-circle"></i> Modifier');
                $(modalName).find('.modal-header.caption').addClass('font-yellow-casablanca');
                $(modalName).find('.modal-title').html('<i class="fa fa-refresh"></i> Modification ' + editPersonnel);

                $(modalName).find('.historique').show().html(
                    'Modifié le ' + data.UPDATED_DATE + ' par ' + data.UPDATED_BY
                );

                /** ---------------------------------------------------------------------------------------
                 * Chargement du template du modal dans le modal d'exécution
                 */
                if (idOO != null) {
                    $(modalName).find('#btnCopieData').html('RECOPIER ORGANISME');
                } else {
                    if (idPO != null) {
                        $(modalName).find('#btnCopieData').html('RECOPIER POLE');
                    } else {
                        if (idET != null) {
                            $(modalName).find('#btnCopieData').html('RECOPIER ETABLISSEMENT');
                        } else {
                            $(modalName).find('#btnCopieData').html('RECOPIER ANTENNE');
                        }
                    }
                }

                /** -------------------------------------------------------------------------------
                 * Init des zones PERSONNEL
                 */
                var civilite = data.PE.civilite_id;
                var nom = data.PE.nom;
                var prenom = data.PE.prenom;

                var fonction = data.PE.fonction_id;
                var complement_fonction = data.PE.notes_fonctions;
                var service = data.PE.service;

                var adr1 = data.PE.adr1;
                var adr2 = data.PE.adr2;
                var adr3 = data.PE.adr3;
                var cp = data.PE.cp;
                var ville = data.PE.ville;
                var pays = data.PE.pays_id;

                var actif = data.PE.active;
                var visible = data.PE.visible;
                var acces = data.PE.acces;

                var notes = data.PE.note;

                // --------------------------------------------------------------------------------
                // Données COMPLEMENTAIRES
                // --------------------------------------------------------------------------------
                var voeux = data.PE.voeux;
                var nl = data.PE.nl;
                var cotis = data.PE.cotis;
                var bi = data.PE.bi;
                var bi_recep = data.PE.bi_reception_id;
                var cf = data.PE.cf;
                var cf_recep = data.PE.cf_reception_id;
                var ag = data.PE.ag;
                var role_ag = data.PE.role_ag_id;
                var buro = data.PE.buro;
                var role_buro = data.PE.role_buro_id;
                var ca = data.PE.ca;
                var role_ca = data.PE.role_ca_id;
                var ctc = data.PE.ctc;
                var cc = data.PE.cc;

                $(modalName + ' #ztNom').val(nom);
                $(modalName + ' #ztPrenom').val(prenom);

                $(modalName + ' #ztAdr1').val(adr1);
                $(modalName + ' #ztAdr2').val(adr2);
                $(modalName + ' #ztAdr3').val(adr3);
                $(modalName + ' #ztCP').val(cp);
                $(modalName + ' #ztVille').val(ville);

                $(modalName + ' #ztNotes').val(notes);
                $(modalName + ' #ztComplementFonction').val(complement_fonction);
                $(modalName + ' #ztService').val(service);

                /** -------------------------------------------------------------------------------
                 * Listes déroulantes avec mises à jour en cascade
                 */
                ClassGENERIC.initGeneric(modalName, 'civilite', '#zlCivilite', civilite);
                ClassGENERIC.initGeneric(modalName, 'fonction', '#zlFonction', fonction);
                ClassGENERIC.initGeneric(modalName, 'pays', '#zlPays', pays);

                $(modalName + ' #ccVOEUX').prop('checked', (voeux == 1 ? true : false));
                $(modalName + ' #ccNL').prop('checked', (nl == 1 ? true : false));
                $(modalName + ' #ccCOTIS').prop('checked', (cotis == 1 ? true : false));
                $(modalName + ' #ccBI').prop('checked', (bi == 1 ? true : false));
                $(modalName + ' #ccCF').prop('checked', (cf == 1 ? true : false));
                $(modalName + ' #ccAG').prop('checked', (ag == 1 ? true : false));
                $(modalName + ' #ccBURO').prop('checked', (buro == 1 ? true : false));
                $(modalName + ' #ccCA').prop('checked', (ca == 1 ? true : false));
                $(modalName + ' #ccCTC').prop('checked', (ctc == 1 ? true : false));
                $(modalName + ' #ccCC').prop('checked', (cc == 1 ? true : false));

                $(modalName + ' #ccACTIF').prop('checked', (actif == 1 ? true : false));
                $(modalName + ' #ccVISIBLE').prop('checked', (visible == 1 ? true : false));
                $(modalName + ' #ccACCES').prop('checked', (acces == 1 ? true : false));

                ClassGENERIC.initGeneric(modalName, 'reception', '#zlReceptionCF', cf_recep);
                ClassGENERIC.initGeneric(modalName, 'reception', '#zlReceptionBI', bi_recep);
                ClassGENERIC.initGeneric(modalName, 'roleag', '#zlRoleAG', role_ag);
                ClassGENERIC.initGeneric(modalName, 'rolebureau', '#zlRoleBURO', role_buro);
                ClassGENERIC.initGeneric(modalName, 'roleca', '#zlRoleCA', role_ca);
                ClassGENERIC.initGeneric(modalName, 'typetel', '#portletTel #zlTypeSaisie');
                ClassGENERIC.initGeneric(modalName, 'typemail', '#portletMail #zlTypeSaisie');
                $('#portletTel .portlet-body').find('.row:first').show();
                $('#portletMail .portlet-body').find('.row:first').show();

                /** -----------------------------------------------------------------------------------
                 * Init des zones TELEPHONES + MAILS
                 */
                ClassGENERIC.displayTels(modalName, data.TEL);
                ClassGENERIC.displayMails(modalName, data.MAIL);

                ClassGENERIC.refreshEdited(modalName);

                /** ---------------------------------------------------------------------------------------
                 * Evènements
                 */
                ClassGENERIC.manageModalButtons(modalName, 0, 'po', idPO);
                ClassGENERIC.updateRemoveButton(modalName, 'telephone', 'po');
                ClassGENERIC.updateRemoveButton(modalName, 'mail', 'po');
                ClassGENERIC.updatePrefereButton(modalName, 'telephone', 'po');
                ClassGENERIC.updatePrefereButton(modalName, 'mail', 'po');

                ClassPERSONNEL.manageModalCC(modalName);

                /** -------------------------------------------------------------------------------
                 * Attributs des listes par défaut
                 */
                if (bi == 0) {
                    $(modalName + ' #zlReceptionBI').select2('enable', false);
                }
                if (cf == 0) {
                    $(modalName + ' #zlReceptionCF').select2('enable', false);
                }

                /** -------------------------------------------------------------------------------
                 * Bind de la modification
                 */
                $(":input").inputmask();
                $(modalName + ' #btnAction').unbind().on('click', function () {


                    // ------------------------------------------------------------------------------------
                    // Données de BASE
                    // ------------------------------------------------------------------------------------
                    civilite = $(modalName + ' #zlCivilite').val();
                    nom = $(modalName + ' #ztNom').val().trim().toUpperCase();
                    prenom = $(modalName + ' #ztPrenom').val().trim().toUpperCase();
                    fonction = $(modalName + ' #zlFonction').val();
                    service = $(modalName + ' #ztService').val().trim().toUpperCase();
                    complement_fonction = $(modalName + ' #ztComplementFonction').val().trim().toUpperCase();

                    adr1 = $(modalName + ' #ztAdr1').val().trim().toUpperCase();
                    adr2 = $(modalName + ' #ztAdr2').val().trim().toUpperCase();
                    adr3 = $(modalName + ' #ztAdr3').val().trim().toUpperCase();
                    cp = $(modalName + ' #ztCP').val();
                    ville = $(modalName + ' #ztVille').val().trim().toUpperCase();
                    pays = $(modalName + ' #zlPays').val();

                    notes = $('<p>' + $(modalName + ' #ztNotes').val() + '</p>').text();

                    actif = $(modalName + ' #ccACTIF').is(":checked") ? 1 : 0;
                    visible = $(modalName + ' #ccVISIBLE').is(":checked") ? 1 : 0;
                    acces = $(modalName + ' #ccACCES').is(":checked") ? 1 : 0;

                    // ------------------------------------------------------------------------------------
                    // Données COMPLEMENTAIRES
                    // ------------------------------------------------------------------------------------
                    voeux = $(modalName + ' #ccVOEUX').is(":checked") ? 1 : 0;
                    nl = $(modalName + ' #ccNL').is(":checked") ? 1 : 0;
                    cotis = $(modalName + ' #ccCOTIS').is(":checked") ? 1 : 0;
                    bi = $(modalName + ' #ccBI').is(":checked") ? 1 : 0;
                    bi_recep = (bi == 1 ? $(modalName + ' #zlReceptionBI').val() : null);
                    cf = $(modalName + ' #ccCF').is(":checked") ? 1 : 0;
                    cf_recep = (cf == 1 ? $(modalName + ' #zlReceptionCF').val() : null);
                    ag = $(modalName + ' #ccAG').is(":checked") ? 1 : 0;
                    role_ag = (ag == 1 ? $(modalName + ' #zlRoleAG').val() : null);
                    buro = $(modalName + ' #ccBURO').is(":checked") ? 1 : 0;
                    role_buro = (buro == 1 ? $(modalName + ' #zlRoleBURO').val() : null);
                    ca = $(modalName + ' #ccCA').is(":checked") ? 1 : 0;
                    role_ca = (ca == 1 ? $(modalName + ' #zlRoleCA').val() : null);
                    ctc = $(modalName + ' #ccCTC').is(":checked") ? 1 : 0;
                    cc = $(modalName + ' #ccCC').is(":checked") ? 1 : 0;

                    if ((cf == 1 && cf_recep == '') ||
                        (bi == 1 && bi_recep == '') ||
                        (ag == 1 && role_ag == '') ||
                        (buro == 1 && role_buro == '') ||
                        (ca == 1 && role_ca == '')) {
                        toastr.warning('Vérifier les données complémentaires.', "Attention");
                        return;
                    }

                    /** -----------------------------------------------------------------------------------
                     * On va rajouter un contrôle de vérification des MAILS si l'une des zones
                     * doit vérifier les mails
                     */
                    if (!ClassGENERIC.verifPresenceMail(modalName, bi, cf)) {
                        toastr.warning('Vous devez renseigner au moins un mail.', "Attention");
                        return;
                    }


                    $(this).off().removeClass('green').addClass('grey-cascade').html(
                        '<i class="fa fa-spin fa-spinner"></i> Patientez...'
                    );

                    // ----------------------------------------------------------------------------
                    // Les nouveaux téléphones uniquement
                    // ----------------------------------------------------------------------------
                    var tels = [];
                    $.each($(modalName + ' #tableTel tbody tr td:first-child'), function (index, row) {
                        if ($(this).attr('data-pk') == undefined) {
                            var telephone = $(this).attr('data-telephone');
                            var prefere = $(this).attr('data-prefere');
                            var type = $(this).attr('data-type-telephone');
                            var item = [telephone, prefere, type];
                            tels.push(item);
                        }
                    });
                    var mails = [];
                    $.each($(modalName + ' #tableMail tbody tr td:first-child'), function (index, row) {
                        if ($(this).attr('data-pk') == undefined) {
                            var mail = $(this).attr('data-mail');
                            var prefere = $(this).attr('data-prefere');
                            var type = $(this).attr('data-type-mail');
                            var item = [mail, prefere, type];
                            mails.push(item)
                        }
                        ;
                    });

                    $.ajax({
                        url: '/annuaire/' + routePersonnel + '/' + idPE,
                        type: 'PUT',
                        data: {
                            _token: $(modalName + ' #token').val(),

                            pCivilite: civilite,
                            pNom: nom,
                            pPrenom: prenom,

                            pFonction: fonction,
                            pService: service,
                            pComplementFontion: complement_fonction,

                            pAdr1: adr1,
                            pAdr2: adr2,
                            pAdr3: adr3,
                            pCP: cp,
                            pVille: ville,
                            pPays: pays,

                            pNotes: notes,

                            pVoeux: voeux,
                            pNL: nl,
                            pCOTIS: cotis,
                            pBI: bi,
                            pBI_Recep: bi_recep,

                            pCF: cf,
                            pCF_Recep: cf_recep,
                            pAG: ag,
                            pAG_Role: role_ag,

                            pBU: buro,
                            pBU_Role: role_buro,
                            pCA: ca,
                            pCA_Role: role_ca,

                            pCTC: ctc,
                            pCC: cc,

                            pTels: tels,
                            pMails: mails,

                            pActif: actif,
                            pVisible: visible,
                            pAcces: acces,
                        },
                        dataType: 'json',
                        success: function (data) {
                            if (data == -1) {
                                $(modalName).modal('hide');
                                toastr.error('Modification non effectuée', "Attention");
                            } else {
                                $(modalName).modal('hide');
                                toastr.info('Modification enregistrée', "Information");
                                tablePersonnel.ajax.reload();
                            }
                        }
                    });
                });
            }
        });

        $(modalName).on('hidden.bs.modal', function () {
            mode = MODE_DATATABLES;
        })
        waitingDialog.hide();
        $(modalName).modal('show');

    },

    view: function (level, idPE) {

        /** ---------------------------------------------------------------------------------------
         * On est toujours 1 niveau de plus que le datatable
         */
        waitingDialog.show('Préparation du Formulaire', {dialogSize: 'large', progressType: 'info'});
        var modalName = ClassGENERIC.newLevel(level);
        var modal = $(modalName);
        modal.html($('#formPersonnel').html());
        mode = MODE_FORM;

        /** ---------------------------------------------------------------------------------------
         * Fil d'Ariane
         *
         * Attention, il y a 2 header dans les FORMS (hors ORGANISME)
         * donc il faut bien distinguer celui avec le fil d'ariane et celui avec le CAPTION !
         */
        var titre = ClassGENERIC.arianeToTxt();
        $(modalName + ' #prec').empty().append(
            titre
        );

        /** ---------------------------------------------------------------------------------------
         * On récupère les infos de la civilite
         */
        $.ajax({
            url: '/annuaire/' + routePersonnel + '/' + idPE,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {

                /** -------------------------------------------------------------------------------
                 * Chargement du template du modal dans le modal d'exécution
                 */
                $(modalName).find('#btnAction').hide();
                $(modalName).find('#btnAddTel').hide();
                $(modalName).find('#btnAddMail').hide();
                $(modalName).find('#btnCopieData').hide();
                $(modalName).find('.modal-header').addClass('font-grey-salsa');
                $(modalName).find('.modal-title').html('<i class="fa fa-refresh"></i> Consultation ' + editPersonnel);

                $(modalName).find('.historique').show().html(
                    'Modifié le ' + data.UPDATED_DATE + ' par ' + data.UPDATED_BY
                );

                /** -------------------------------------------------------------------------------
                 * Init des zones PERSONNEL
                 */
                var civilite = data.PE.civilite_id;
                var nom = data.PE.nom;
                var prenom = data.PE.prenom;

                var fonction = data.PE.fonction_id;
                var complement_fonction = data.PE.notes_fonctions;
                var service = data.PE.service;

                var adr1 = data.PE.adr1;
                var adr2 = data.PE.adr2;
                var adr3 = data.PE.adr3;
                var cp = data.PE.cp;
                var ville = data.PE.ville;
                var pays = data.PE.pays_id;

                var actif = data.PE.active;
                var visible = data.PE.visible;
                var acces = data.PE.acces;

                var notes = data.PE.note;

                // --------------------------------------------------------------------------------
                // Données COMPLEMENTAIRES
                // --------------------------------------------------------------------------------
                var voeux = data.PE.voeux;
                var nl = data.PE.nl;
                var cotis = data.PE.cotis;
                var bi = data.PE.bi;
                var bi_recep = data.PE.bi_reception_id;
                var cf = data.PE.cf;
                var cf_recep = data.PE.cf_reception_id;
                var ag = data.PE.ag;
                var role_ag = data.PE.role_ag_id;
                var buro = data.PE.buro;
                var role_buro = data.PE.role_buro_id;
                var ca = data.PE.ca;
                var role_ca = data.PE.role_ca_id;
                var ctc = data.PE.ctc;
                var cc = data.PE.cc;

                $(modalName + ' #ztNom').val(nom);
                $(modalName + ' #ztPrenom').val(prenom);

                $(modalName + ' #ztAdr1').val(adr1);
                $(modalName + ' #ztAdr2').val(adr2);
                $(modalName + ' #ztAdr3').val(adr3);
                $(modalName + ' #ztCP').val(cp);
                $(modalName + ' #ztVille').val(ville);

                $(modalName + ' #ztNotes').val(notes);
                $(modalName + ' #ztComplementFonction').val(complement_fonction);
                $(modalName + ' #ztService').val(service);

                /** -------------------------------------------------------------------------------
                 * Listes déroulantes avec mises à jour en cascade
                 */
                ClassGENERIC.initGeneric(modalName, 'civilite', '#zlCivilite', civilite);
                ClassGENERIC.initGeneric(modalName, 'fonction', '#zlFonction', fonction);
                ClassGENERIC.initGeneric(modalName, 'pays', '#zlPays', pays);

                $(modalName + ' #ccVOEUX').prop('checked', (voeux == 1 ? true : false));
                $(modalName + ' #ccNL').prop('checked', (nl == 1 ? true : false));
                $(modalName + ' #ccCOTIS').prop('checked', (cotis == 1 ? true : false));
                $(modalName + ' #ccBI').prop('checked', (bi == 1 ? true : false));
                $(modalName + ' #ccCF').prop('checked', (cf == 1 ? true : false));
                $(modalName + ' #ccAG').prop('checked', (ag == 1 ? true : false));
                $(modalName + ' #ccBURO').prop('checked', (buro == 1 ? true : false));
                $(modalName + ' #ccCA').prop('checked', (ca == 1 ? true : false));
                $(modalName + ' #ccCTC').prop('checked', (ctc == 1 ? true : false));
                $(modalName + ' #ccCC').prop('checked', (cc == 1 ? true : false));

                $(modalName + ' #ccACTIF').prop('checked', (actif == 1 ? true : false));
                $(modalName + ' #ccVISIBLE').prop('checked', (visible == 1 ? true : false));
                $(modalName + ' #ccACCES').prop('checked', (acces == 1 ? true : false));

                ClassGENERIC.initGeneric(modalName, 'reception', '#zlReceptionCF', cf_recep);
                ClassGENERIC.initGeneric(modalName, 'reception', '#zlReceptionBI', bi_recep);
                ClassGENERIC.initGeneric(modalName, 'roleag', '#zlRoleAG', role_ag);
                ClassGENERIC.initGeneric(modalName, 'rolebureau', '#zlRoleBURO', role_buro);
                ClassGENERIC.initGeneric(modalName, 'roleca', '#zlRoleCA', role_ca);
                $('#portletTel .portlet-body').find('.row:first').hide();
                $('#portletMail .portlet-body').find('.row:first').hide();

                /** -------------------------------------------------------------------------------
                 * Init des zones TELEPHONES
                 */
                $.each(data.TEL, function (index, item) {
                    $(modalName + ' #tableTel tbody').append(
                        '<tr>' +
                        '<td data-pk="' + item.OWN_ID + '" ' +
                        '    data-type-telephone="' + item.TYPE_ID + '"' +
                        '    data-id="' + item.OO_ID + '"' +
                        '>' + item.numero +
                        (item.prefere == 1
                                ? ' <i class="fa fa-star prefere font-yellow" style="cursor:pointer"> </i>'
                                : ' <i class="fa fa-star-o prefere" style="cursor:pointer"> </i>'
                        ) +
                        '</td>' +
                        '<td>' + item.libelle + '</td>' +
                        '<td class="text-right">' +
                        '<a class="remove-telephone btn btn-xs red" ' +
                        ' data-placement="left" data-toggle="confirmation" data-original-title="" title="" ' +
                        '><i class="fa fa-remove"></i></a>' +
                        //'<a class="modif btn btn-xs blue"><i class="fa fa-edit"></i></a>' +
                        '</td>' +
                        '</tr>'
                    );
                });

                /** -------------------------------------------------------------------------------
                 * Init des zones MAILS
                 */
                $.each(data.MAIL, function (index, item) {
                    $(modalName + ' #tableMail tbody').append(
                        '<tr>' +
                        '<tr>' +
                        '<td data-pk="' + item.OWN_ID + '" ' +
                        '    data-type-mail="' + item.TYPE_ID + '"' +
                        '    data-id="' + item.OO_ID + '"' +
                        '>' + item.mail +
                        (item.prefere == 1
                                ? ' <i class="fa fa-star prefere font-yellow" style="cursor:pointer"> </i>'
                                : ' <i class="fa fa-star-o prefere" style="cursor:pointer"> </i>'
                        ) +
                        '</td>' +
                        '<td>' + item.libelle + '</td>' +
                        '<td class="text-right">' +
                        '<a class="remove-mail btn btn-xs red"' +
                        ' data-placement="left" data-toggle="confirmation" data-original-title="" title="" ' +
                        '><i class="fa fa-remove"></i></a>' +
                        //'<a class="modif btn btn-xs blue"><i class="fa fa-edit"></i></a>' +
                        '</td>' +
                        '</tr>'
                    );
                });

                /** -------------------------------------------------------------------------------
                 * Acivation .EDITED
                 */
                ClassGENERIC.refreshEdited(modalName);
<<<<<<< HEAD

                $(modalName).find('.form-control').each(function () {
                    $(this).prop('readonly', true);
                });
                $(modalName + ' input').each(function () {
                    $(this).prop('readonly', true);
                });
                $(modalName).find('.remove-telephone').hide();
                $(modalName).find('.remove-mail').hide();
=======
                ClassGENERIC.doReadOnly(modalName);
>>>>>>> debug8

            }
        });

        $(modalName).on('hidden.bs.modal', function () {
            mode = MODE_DATATABLES;
        })
        waitingDialog.hide();
        $(modalName).modal('show');

    },

    recopieDataOO: function (idOO, modalName) {

        /** ---------------------------------------------------------------------------------------
         * On récupère les données à recopier (adresse + BI + ...
         */
        $.ajax({
            url: '/datacopy/' + routeOrga + '/' + idOO,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {
                $(modalName + ' #ztAdr1').val(data.ADR1);
                $(modalName + ' #ztAdr2').val(data.ADR2);
                $(modalName + ' #ztAdr3').val(data.ADR3);
                $(modalName + ' #ztCP').val(data.CP);
                $(modalName + ' #ztVille').val(data.VILLE);
                $(modalName + ' #zlPays').val(data.PAYS).trigger("change");

                $(modalName + ' #ccNL').prop('checked', (data.NL == 1 ? true : false));
                $(modalName + ' #ccBI').prop('checked', (data.BI == 1 ? true : false));
                $(modalName + ' #ccCF').prop('checked', (data.CF == 1 ? true : false));
                $(modalName + ' #ccCONT').prop('checked', (data.CONTRIB == 1 ? true : false));

                $(modalName + ' #zlReceptionBI').val(data.RECEP_BI).trigger("change");
                $(modalName + ' #zlReceptionCF').val(data.RECEP_CF).trigger("change");

                if (data.BI == 1) {
                    $(modalName + ' #zlReceptionBI').select2('enable');
                }
                if (data.CF == 1) {
                    $(modalName + ' #zlReceptionCF').select2('enable');
                }

            }
        });

    },

    recopieDataPO: function (idPO, modalName) {

        /** ---------------------------------------------------------------------------------------
         * On récupère les données à recopier (adresse + BI + ...
         */
        $.ajax({
            url: '/datacopy/' + routePole + '/' + idPO,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {
                $(modalName + ' #ztAdr1').val(data.ADR1);
                $(modalName + ' #ztAdr2').val(data.ADR2);
                $(modalName + ' #ztAdr3').val(data.ADR3);
                $(modalName + ' #ztCP').val(data.CP);
                $(modalName + ' #ztVille').val(data.VILLE);
                $(modalName + ' #zlPays').val(data.PAYS).trigger("change");

                $(modalName + ' #ccNL').prop('checked', (data.NL == 1 ? true : false));
                $(modalName + ' #ccBI').prop('checked', (data.BI == 1 ? true : false));
                $(modalName + ' #ccCF').prop('checked', (data.CF == 1 ? true : false));
                $(modalName + ' #ccCONT').prop('checked', (data.CONTRIB == 1 ? true : false));

                $(modalName + ' #zlReceptionBI').val(data.RECEP_BI).trigger("change");
                $(modalName + ' #zlReceptionCF').val(data.RECEP_CF).trigger("change");

                if (data.BI == 1) {
                    $(modalName + ' #zlReceptionBI').select2('enable');
                }
                if (data.CF == 1) {
                    $(modalName + ' #zlReceptionCF').select2('enable');
                }

            }
        });

    },

    initListes: function (modalName, idOO, idPO) {

        /** ---------------------------------------------------------------------------------------
         * zlSecteur + zlSecteurSMS + zlTypeAge
         * --> zlTypeEtabl
         */
        /** ---------------------------------------------------------------------------------------
         * Listes de bases
         */
        ClassGENERIC.initGeneric(modalName, 'civilite', '#zlCivilite');
        ClassGENERIC.initGeneric(modalName, 'pays', '#zlPays');

        ClassGENERIC.initGeneric(modalName, 'fonction', '#zlFonction');

        ClassGENERIC.initGeneric(modalName, 'pays', '#zlPays');
        ClassGENERIC.initGeneric(modalName, 'reception', '#zlReceptionBI');
        ClassGENERIC.initGeneric(modalName, 'reception', '#zlReceptionCF');
        ClassGENERIC.initGeneric(modalName, 'roleag', '#zlRoleAG');
        ClassGENERIC.initGeneric(modalName, 'rolebureau', '#zlRoleBURO');
        ClassGENERIC.initGeneric(modalName, 'roleca', '#zlRoleCA');

        ClassGENERIC.initGeneric(modalName, 'typetel', '#portletTel #zlTypeSaisie');
        ClassGENERIC.initGeneric(modalName, 'typemail', '#portletMail #zlTypeSaisie');

    },

    manageModalCC: function (modalName) {

        $(modalName + ' #ccCF').on('click', function () {
            if ($(modalName + ' #ccCF').is(':checked')) {
                $(modalName + ' #zlReceptionCF').select2('enable');//.enable(true);
            } else {
                $(modalName + ' #zlReceptionCF').select2('enable', false);//.enable(true);
            }
        });

        $(modalName + ' #ccBI').on('click', function () {
            if ($(modalName + ' #ccBI').is(':checked')) {
                $(modalName + ' #zlReceptionBI').select2('enable');//.enable(true);
                $(modalName + ' #ccNL').prop('checked', true);//.enable(true);
            } else {
                $(modalName + ' #zlReceptionBI').select2('enable', false);//.enable(true);
                $(modalName + ' #ccNL').prop('checked', false);//.enable(true);
            }
        });

        $(modalName + ' #ccAG').on('click', function () {
            if ($(modalName + ' #ccAG').is(':checked')) {
                $(modalName + ' #zlRoleAG').select2('enable');//.enable(true);
            } else {
                $(modalName + ' #zlRoleAG').select2('enable', false);//.enable(true);
            }
        });

        $(modalName + ' #ccCA').on('click', function () {
            if ($(modalName + ' #ccCA').is(':checked')) {
                $(modalName + ' #zlRoleCA').select2('enable');//.enable(true);
            } else {
                $(modalName + ' #zlRoleCA').select2('enable', false);//.enable(true);
            }
        });

        $(modalName + ' #ccBURO').on('click', function () {
            if ($(modalName + ' #ccBURO').is(':checked')) {
                $(modalName + ' #zlRoleBURO').select2('enable');//.enable(true);
            } else {
                $(modalName + ' #zlRoleBURO').select2('enable', false);//.enable(true);
            }
        });
    },

};


var waitingDialog = waitingDialog || (function ($) {
        'use strict';

        // Creating modal dialog's DOM
        var $dialog = $(
            '<div class="modal fade" id="frmWait"" data-keyboard="false" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:none;">' +
            '<div class="modal-dialog modal-m">' +
            '<div class="modal-content" style="height: 105px;">' +
            '<div class="modal-header"><h3 style="margin:0;font-weight: bold"></h3></div>' +
            '<div class="modal-body" style="padding-top: 15px; padding-bottom: 20px;">' +
            '<div class="progress progress-striped active" style="height:19px;margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
            '</div>' +
            '</div></div></div>');

        return {
            /**
             * Opens our dialog
             * @param message Custom message
             * @param options Custom options:
             *                  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
             *                  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
             */
            show: function (message, options) {
                // Assigning defaults
                if (typeof options === 'undefined') {
                    options = {};
                }
                if (typeof message === 'undefined') {
                    message = 'Loading';
                }
                var settings = $.extend({
                    dialogSize: 'm',
                    progressType: '',
                    onHide: null // This callback runs after the dialog was hidden
                }, options);

                // Configuring dialog
                $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
                $dialog.find('.progress-bar').attr('class', 'progress-bar');
                if (settings.progressType) {
                    $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
                }
                $dialog.find('h3').text(message);
                // Adding callbacks
                if (typeof settings.onHide === 'function') {
                    $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
                        settings.onHide.call($dialog);
                    });
                }
                // Opening dialog
                $dialog.modal();
            },
            /**
             * Closes dialog
             */
            hide: function () {
                $dialog.modal('hide');
            }
        };

    })(jQuery);
