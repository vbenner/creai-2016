/** -----------------------------------------------------------------------------------------------
 *
 * @author : Vincent BENNER
 * @copyright : Page Up 2016
 * @version : 1.0.0
 *
 * @history : 1.0.0 - Release
 * @description : Gestion des TYPES D'ORGANISME
 *
 */

/** -----------------------------------------------------------------------------------------------
 *
 */
var tableNameType = 'tableType';
var tableNameDetail = 'tableDetail';
var tableType;
var tableDetail;
var routeType = 'typeorganisme'
var routeDetail = 'detailtypeorganisme';
var editType = ' d\'un Type d\'Organismes';
var editDetail = ' d\'un Detail de Type d\'Organisme';
var reload = false;

/** -----------------------------------------------------------------------------------------------
 * JQuery start
 */
$(function () {

    ClassTypeOrganisme.init();
    ClassTypeOrganisme.initDataTable();
    ClassTypeOrganisme.manageActions();

});

/** -----------------------------------------------------------------------------------------------
 * @class : TYPE D'ORGANISME
 */
var ClassTypeOrganisme = {

    /* --------------------------------------------------------------------------------------------
     * Init
     */
    init : function () {

    },

    initDataTable : function () {
        tableType = $('#' + tableNameType).DataTable({
            iDisplayLength:50,
            ajax : "/datatable/" + routeType,
            serverSide : false,
            autoWidth: false,
            columnDefs: [
                {className: "dt-body-left", "targets": [0, 1, 2, 3]},
                {className: "dt-body-right", "targets": [4, 5]},
            ],
            columns : [
                {orderable: true, width : null},
                {orderable: true, width : 100},
                {orderable: true, width : 100},
                {orderable: true, width : 100},
                {orderable: true, width : 80},
                {orderable: true, width : 120}
            ],
            sPagingType     : "bootstrap_full_numbers",
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
            },
            initComplete : function (settingssettings) {
                //return;
                /** On change les largeurs par défaut des contrôles */
                var tableWrapper = $('#' + tableNameType + '_wrapper');
                var tableLength = $('#' + tableNameType + '_length');
                tableLength.css('width', '400px');
                tableWrapper.find('.dataTables_length select').
                    addClass("form-control input-inline").
                    select2();
                tableWrapper.find('.dataTables_filter input').addClass("form-control input-inline");
            },
            rowCallback : function( row, data, index ) {

                var colActive   = 4;
                var colAction   = 5;

                /* --------------------------------------------------------------------------------
                 * On crée les Switch
                 */
                $('td:eq('+ colActive + ')', row).children('.make-switch').bootstrapSwitch();

                /* --------------------------------------------------------------------------------
                 * On checke la colonne ACTION
                 */
                $('td:eq(' + colAction + ')', row).children('a').bind('click', function () {
                    if ($(this).hasClass('modif')) {
                        ClassTypeOrganisme.update($(this).attr('data-id'));
                    }
                    if ($(this).hasClass('detail')) {
                        ClassTypeOrganisme.manageDetail($(this).attr('data-id'));
                    }
                });
            }

        });

        return true;
    },

    /* --------------------------------------------------------------------------------------------
     * On va faire comme pour les téléphonees en rajoutant un MODAL
     */
    manageDetail: function (idOrganisme) {

        /** ---------------------------------------------------------------------------------------
         * Gestion des DETAILS
         */
        reload = false;
        var modalName = '#dialogExec';
        var modal = $(modalName);
        modal.html($('#formListDetail').html());

        ClassDetail.initDataTable(idOrganisme);

        /** ---------------------------------------------------------------------------------------
         * Ajout du DETAIL
         */
        $(modal).unbind().on('click', '#btnAddDetail', function () {
            ClassDetail.add(idOrganisme);
        });

        /** ---------------------------------------------------------------------------------------
         * Modif du SWITCH OUI / NON
         */
        $('#tableDetail').on('switchChange.bootstrapSwitch', 'input[type="checkbox"]', function(event, state) {
            var id = event.target.id;
            var value = (state == true) ? 1 : 0;

            $.ajax({
                url: '/param/' + routeDetail + '/' + id + '/edit',
                type: 'GET',
                data: {
                    pValue: value,
                },
                dataType: 'json',
                success: function (data) {
                }
            });
        });

        /** ---------------------------------------------------------------------------------------
         *
         */
        $(modal).on('hidden.bs.modal', function (e) {
            if (reload) {
                tableType.ajax.reload();
            }
        });

        $(modal).modal('show');
    },


    /* --------------------------------------------------------------------------------------------
     * Modal d'ajout d'un enregistrement
     */
    add: function () {

        /** -----------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        var modalName = '#dialogExec';
        var modal = $(modalName);
        modal.html($('#formType').html());

        /** -----------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        $(modal).find('#btnAction').addClass('green').html('<i class="fa fa-plus-circle"></i> Ajouter');
        $(modal).find('.modal-header').addClass('font-green');
        $(modal).find('.modal-title').html('<i class="fa fa-plus-circle"></i> Ajout d\'un ' + editType);
        $(modal).modal('show');

        $(modalName + ' #btnAction').unbind().on('click', function () {

            /** -----------------------------------------------------------------------------------
             * Vérification des ZONES Mandatory
             */
            var oblig = true;
            $(modalName + ' .required').each(function () {
                if (!$(this).val().trim()) {
                    oblig = false;
                }
            });
            if (!oblig) {
                toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                return;
            }

            $(this).off().removeClass('green').addClass('grey-cascade').html(
                '<i class="fa fa-spin fa-spinner"></i> Patientez...'
            );

            var libelle = $(modalName + ' #ztLibelle').val().toUpperCase();
            var isOG = $(modalName + ' #ccOG').is(":checked") ? 1 : 0;
            var isPOLE = $(modalName + ' #ccPOLE').is(":checked") ? 1 : 0;
            var isGROUP = $(modalName + ' #ccGROUP').is(":checked") ? 1 : 0;
            var forceOG = $(modalName + ' #ccFORCE').is(":checked") ? 1 : 0;
            $.ajax({
                url: '/param/' + routeType,
                type: 'POST',
                data: {
                    _token : $(modalName + ' #token').val(),
                    pLibelle : libelle,
                    pOG : isOG,
                    pPOLE : isPOLE,
                    pGROUP : isGROUP,
                    pFORCE : forceOG,
                },
                dataType: 'json',
                success: function(data){
                    result = true;
                    $(modal).modal('hide');
                    toastr.success('Saisie enregistrée', "Information");
                    table.ajax.reload();
                }
            });

        });

    },

    update: function (id) {

        /** -----------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        var modalName = '#dialogExec';
        var modal = $(modalName);
        modal.html($('#formType').html());

        /** -----------------------------------------------------------------------------------
         * On récupère les infos du type de téléphone
         */
        $.ajax({
            url: '/param/' + routeType + '/' + id,
            type: 'GET',
            dataType: 'json',
            success: function(data){

                /** -----------------------------------------------------------------------------------
                 * Chargement du template du modal dans le modal d'exécution
                 */
                $(modal).find('#btnAction').addClass('yellow-gold').html('<i class="fa fa-plus-circle"></i> Modifier');
                $(modal).find('.modal-header').addClass('font-yellow-casablanca');
                $(modal).find('.modal-title').html('<i class="fa fa-refresh"></i> Modification ' + editType);
                $(modal).modal('show');

                /** -----------------------------------------------------------------------------------
                 * Init des zones
                 */
                var libelle = data.libelle;
                var isOG = data.isOG;
                var isPOLE = data.isPOLE;
                var isGROUP = data.isGROUP;
                var forceOG = data.forceOG;
                $(modalName + ' #ztLibelle').val(libelle).addClass('edited');
                $(modalName + ' #ccOG').prop('checked', (isOG == 1 ? true : false));
                $(modalName + ' #ccPOLE').prop('checked', (isPOLE == 1 ? true : false));
                $(modalName + ' #ccGROUP').prop('checked', (isGROUP == 1 ? true : false));
                $(modalName + ' #ccFORCE').prop('checked', (forceOG == 1 ? true : false));

                /** -----------------------------------------------------------------------------------
                 * Bind de la modification
                 */
                $(modalName + ' #btnAction').unbind().on('click', function () {

                    /** -----------------------------------------------------------------------------------
                     * Vérification des ZONES Mandatory
                     */
                    var oblig = true;
                    $(modalName + ' .required').each(function () {
                        if (!$(this).val().trim()) {
                            oblig = false;
                        }
                    });
                    if (!oblig) {
                        toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                        return;
                    }

                    $(this).off().removeClass('yellow-gold').addClass('grey-cascade').html(
                        '<i class="fa fa-spin fa-spinner"></i> Patientez...'
                    );

                    libelle = $(modalName + ' #ztLibelle').val().toUpperCase();
                    isOG = $(modalName + ' #ccOG').is(":checked") ? 1 : 0;
                    isPOLE = $(modalName + ' #ccPOLE').is(":checked") ? 1 : 0;
                    isGROUP = $(modalName + ' #ccGROUP').is(":checked") ? 1 : 0;
                    forceOG = $(modalName + ' #ccFORCE').is(":checked") ? 1 : 0;
                    $.ajax({
                        url: '/param/' + routeType + '/' + id,
                        type: 'PUT',
                        data: {
                            _token : $(modalName + ' #token').val(),
                            pLibelle : libelle,
                            pOG : isOG,
                            pPOLE : isPOLE,
                            pGROUP : isGROUP,
                            pFORCE : forceOG,
                        },
                        dataType: 'json',
                        success: function(data){
                            if (data == -1) {
                                $(modal).modal('hide');
                                toastr.error('Modification non effectuée', "Attention");
                            } else {
                                $(modal).modal('hide');
                                toastr.info('Modification enregistrée', "Information");
                                tableType.ajax.reload();
                            }
                        }
                    });
                });
            }
        });
    },

    manageActions: function () {

        /** ---------------------------------------------------------------------------------------
         * Anti CLOSE
         */
        $('#dialogExec').on('keypress', 'input', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });

        /** ---------------------------------------------------------------------------------------
         * Ajout d'un TYPE D'ORGANISME
         */
        $('#btnAdd').click(function () {
            ClassTypeOrganisme.add();
        });

        /** ---------------------------------------------------------------------------------------
         * Modif du SWITCH OUI / NON
         */
        $('#table').on('switchChange.bootstrapSwitch', 'input[type="checkbox"]', function(event, state) {
            var id = event.target.id;
            var value = (state == true) ? 1 : 0;

            $.ajax({
                url: '/param/' + routeType + '/' + id + '/edit',
                type: 'GET',
                data: {
                    pValue: value,
                },
                dataType: 'json',
                success: function (data) {
                }
            });
        });
    }
}

/** -----------------------------------------------------------------------------------------------
 * @class : DETAIL
 */
var ClassDetail = {

    /** -------------------------------------------------------------------------------------------
     * On récupère les PÔLES d'un ORGANISME (d'où le paramètre idOO)
     */
    initDataTable: function (idType) {

        tableDetail = $('#' + tableNameDetail).removeAttr('width').DataTable({
            iDisplayLength:50,
            ajax: "/datatable/" + routeDetail + '/' + idType,
            serverSide: false,
            autoWidth: false,
            columnDefs: [
                {className: "dt-body-left", "targets": [0, 1, 2]},
                {className: "dt-body-right", "targets": [3]}
            ],
            columns : [
                null, null, null,
                {orderable: true, width: 110},
            ],
            lengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "Tous"]
            ],

            //fixedColumns: true,
            sPagingType: "bootstrap_full_numbers",
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
            },
            initComplete: function (settingssettings) {
                /** On change les largeurs par défaut des contrôles */
                var tableWrapper = $('#' + tableNameDetail + '_wrapper');
                var tableLength = $('#' + tableNameDetail + '_length');
                tableLength.css('width', '400px');
                tableWrapper.find('.dataTables_length select').addClass("form-control input-inline").select2();
                tableWrapper.find('.dataTables_filter input').addClass("form-control input-inline");
            },
            rowCallback: function (row, data, index) {

                var colStatut = 1;
                var colAction = 3;

                /* --------------------------------------------------------------------------------
                 * On checke la colonne STATUT
                 */
                $('td:eq('+ colStatut + ')', row).children('.make-switch').bootstrapSwitch();

                $('td:eq(' + colAction + ')', row).children('a').bind('click', function () {
                    if ($(this).hasClass('modif')) {
                        ClassDetail.update($(this).attr('data-id'));
                    }
                });
            }

        });


        return true;
    },

    add: function (idType) {

        /** ---------------------------------------------------------------------------------------
         *
         */
        var modalName = '#dialogExecLvl1';
        var modal = $(modalName);
        modal.html($('#formDetail').html());

        /** ---------------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        $(modal).find('#btnAction').addClass('green').html('<i class="fa fa-plus-circle"></i> Ajouter');
        $(modal).find('.modal-header').addClass('font-green');
        $(modal).find('.modal-title').html('<i class="fa fa-plus-circle"></i> Ajout ' + editDetail);

        $(modal).modal('show');

        /** ---------------------------------------------------------------------------------------
         * Sauvegarde
         */
        $(modalName + ' #btnAction').unbind().on('click', function () {

            /** -----------------------------------------------------------------------------------
             * Vérification des ZONES Mandatory
             */
            var oblig = true;
            $(modalName + ' .required').each(function () {
                if (!$(this).val().trim()) {
                    oblig = false;
                }
            });

            if (!oblig) {
                toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                return;
            }

            // ------------------------------------------------------------------------------------
            // Données de BASE
            // ------------------------------------------------------------------------------------
            var libelle = $(modalName + ' #ztLibelle').val().trim().toUpperCase();
            var note = $(modalName + ' #ztNote').val().trim().toUpperCase();
            var parrain = $(modalName + ' #ccPARRAIN').is(":checked") ? 1 : 0;
            $(this).off().removeClass('green').addClass('grey-cascade').html(
                '<i class="fa fa-spin fa-spinner"></i> Patientez...'
            );
            reload = true;

            $.ajax({
                url: '/param/' + routeDetail,
                type: 'POST',
                data: {
                    _token: $(modalName + ' #token').val(),
                    pIdType:idType,
                    pLibelle: libelle,
                    pParrain:parrain,
                    pNote: note,
                },
                dataType: 'json',
                success: function (data) {
                    result = true;
                    $(modal).modal('hide');
                    toastr.success('Saisie enregistrée', "Information");
                    tableDetail.ajax.reload();
                }
            });

        });

    },

    update: function (idType) {

        /** ---------------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        var modalName = '#dialogExecLvl1';
        var modal = $(modalName);
        modal.html($('#formDetail').html());

        /** ---------------------------------------------------------------------------------------
         * On récupère les infos du DETAIL
         */
        $.ajax({
            url: '/param/' + routeDetail + '/' + idType,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {

                /** -------------------------------------------------------------------------------
                 * Chargement du template du modal dans le modal d'exécution
                 */
                $(modal).find('#btnAction').addClass('yellow-gold').html('<i class="fa fa-plus-circle"></i> Modifier');
                $(modal).find('.modal-header').addClass('font-yellow-casablanca');
                $(modal).find('.modal-title').html('<i class="fa fa-refresh"></i> Modification ' + editDetail);
                $(modal).modal('show');

                /** -------------------------------------------------------------------------------
                 */
                var libelle = data.libelle;
                var note = data.note;
                var parrain = data.hasParrain;
                var iddetail = data.id;
                $(modalName + ' #ztLibelle').val(libelle).addClass(libelle != '' ? 'edited' : '');
                $(modalName + ' #ztLibelle').focus();
                $(modalName + ' #ccPARRAIN').prop('checked', (parrain == 1 ? true : false));
                $(modalName + ' #ztNote').val(note);

                /** -----------------------------------------------------------------------------------
                 * Bind de la modification
                 */
                $(modalName + ' #btnAction').unbind().on('click', function () {

                    /** -----------------------------------------------------------------------------------
                     * Vérification des ZONES Mandatory
                     */
                    var oblig = true;
                    $(modalName + ' .required').each(function () {
                        if (!$(this).val().trim()) {
                            oblig = false;
                        }
                    });
                    if (!oblig) {
                        toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                        return;
                    }

                    // ----------------------------------------------------------------------------
                    // Données de BASE
                    // ----------------------------------------------------------------------------
                    libelle = $(modalName + ' #ztLibelle').val().trim().toUpperCase();
                    parrain = $(modalName + ' #ccPARRAIN').prop("checked") ? 1 : 0;
                    note = $(modalName + ' #ztNote').val().trim();

                    //$(this).off().removeClass('green').addClass('grey-cascade').html(
                    //    '<i class="fa fa-spin fa-spinner"></i> Patientez...'
                    //);

                    $.ajax({
                        url: '/param/' + routeDetail + '/' + iddetail,
                        type: 'PUT',
                        data: {
                            _token: $(modalName + ' #token').val(),
                            pLibelle: libelle,
                            pParrain:parrain,
                            pNote: note,
                            pType:idType
                        },
                        dataType: 'json',
                        success: function (data) {
                            if (data == -1) {
                                $(modal).modal('hide');
                                toastr.error('Modification non effectuée', "Attention");
                            } else {
                                $(modal).modal('hide');
                                toastr.info('Modification enregistrée', "Information");
                                tableDetail.ajax.reload();
                            }
                        }
                    });
                });
            }
        });

        /** ---------------------------------------------------------------------------------------
         * On met à jour les actions
         */

    },

};

