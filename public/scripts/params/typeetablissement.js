/** -----------------------------------------------------------------------------------------------
 *
 * @author : Vincent BENNER
 * @copyright : Page Up 2016
 * @version : 1.0.0
 *
 * @history : 1.0.0 - Release
 * @description : Gestion des TYPES D'ORGANISME
 *
 */

/** -----------------------------------------------------------------------------------------------
 *
 */
var tableName = 'table';
var table;
var route = 'typeetablissement'
var edit = ' Type d\'Établissement';

/** -----------------------------------------------------------------------------------------------
 * JQuery start
 */
$(function () {

    ClassTypeEtablissement.init();
    ClassTypeEtablissement.initDataTable();
    ClassTypeEtablissement.manageActions();

});

/** -----------------------------------------------------------------------------------------------
 * @class : TYPE D'ORGANISME
 */
var ClassTypeEtablissement = {

    /* --------------------------------------------------------------------------------------------
     * Init
     */
    init : function () {

    },

    initDataTable : function () {
        table = $('#' + tableName).DataTable({
            iDisplayLength:50,
            ajax : "/datatable/" + route,
            serverSide : false,
            autoWidth: false,
            columnDefs: [
                {className: "dt-body-left", "targets": [0, 1, 2, 3, 4]},
                {className: "dt-body-right", "targets": [5]},
            ],
            columns : [
                {orderable: true, width : 400, render : function(data, type, row) {
                    return '<small>'+data+'</small>'
                }},
                {orderable: true, width : 100, render : function(data, type, row) {
                    return '<small>'+data+'</small>'
                }},
                {orderable: true, width : 100, render : function(data, type, row) {
                    return '<small>'+data+'</small>'
                }},
                {orderable: true, width : 100, render : function(data, type, row) {
                    return '<small>'+data+'</small>'
                }},
                {orderable: true, width : 100
                }
            ],
            sPagingType     : "bootstrap_full_numbers",
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
            },
            initComplete : function (settingssettings) {
                //return;
                /** On change les largeurs par défaut des contrôles */
                var tableWrapper = $('#' + tableName + '_wrapper');
                var tableLength = $('#' + tableName + '_length');
                tableLength.css('width', '400px');
                tableWrapper.find('.dataTables_length select').
                    addClass("form-control input-inline").
                    select2();
                tableWrapper.find('.dataTables_filter input').addClass("form-control input-inline");
            },
            rowCallback : function( row, data, index ) {

                var colActive   = 4;
                var colAction   = 5;

                /* --------------------------------------------------------------------------------
                 * On crée les Switch
                 */
                $('td:eq('+ colActive + ')', row).children('.make-switch').bootstrapSwitch({
                });

                /* --------------------------------------------------------------------------------
                 * On checke la colonne ACTION
                 */
                $('td:eq(' + colAction + ')', row).children('a').bind('click', function () {
                    ClassTypeEtablissement.update($(this).attr('data-id'));
                });
            }

        });


        return true;
    },

    initListes : function (modal) {

        /** ---------------------------------------------------------------------------------------
         * Listes de bases
         */
        ClassTypeEtablissement.initGeneric(modal, 'typesecteur', '#zlTypeSecteur');
        ClassTypeEtablissement.initGeneric(modal, 'typesecteurms', '#zlTypeSecteurMS');
        ClassTypeEtablissement.initGeneric(modal, 'typeage', '#zlTypeAge');

        /** ---------------------------------------------------------------------------------------
         * Par défaut, on ne peut pas sélectionner de SECTEUR MS
         */
        $(modal + ' #zlTypeSecteurMS').prop("disabled", true);

        /** ---------------------------------------------------------------------------------------
         */
        $(modal + ' #zlTypeSecteur').on('change', function (e) {

            var ms = $(this).find('option:selected').attr('data-ms');
            console.log('MS = ' + ms);
            if (ms === undefined || ms == 0) {
                $(modal + ' #zlTypeSecteurMS').prop("disabled", true);
            } else {
                $(modal + ' #zlTypeSecteurMS').prop("disabled", false);
            }
        });
    },

    initGeneric : function (modal, route, zone, value) {

        $.ajax({
            url: '/list/' + route,
            type: 'GET',
            dataType: 'json',
            async:false,
            success: function(data){
                var select = '';
                if(data.LISTE.length > 0){
                    var zl = $(modal + ' ' + zone);
                    zl.empty();
                    if (data.LISTE.length > 1) {
                        zl.append('<option value=""></option>');
                    }
                    $.each(data.LISTE, function(index, item){
                        zl.append('<option data-ms="'+item.ms+'" value="'+item.id+'" ' + (item.id == value ? ' selected="selected" ' : '') + '>'+item.libelle+'</option>');
                    });
                    zl.select2({
                        width: '100%',
                        allowClear:(data.LISTE.length > 1 ? true : false),
                        placeholder: ' '
                    });
                }

            }
        });
    },


    /* --------------------------------------------------------------------------------------------
     * Modal d'ajout d'un enregistrement
     */
    add: function () {

        /** -----------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        var modalName = '#dialogExec';
        var modal = $(modalName);
        modal.html($('#form').html());

        /** -----------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        $(modal).find('#btnAction').addClass('green').html('<i class="fa fa-plus-circle"></i> Ajouter');
        $(modal).find('.modal-header').addClass('font-green');
        $(modal).find('.modal-title').html('<i class="fa fa-plus-circle"></i> Ajout d\'un ' + edit);
        $(modal).modal('show');

        /** ---------------------------------------------------------------------------------------
         * Init des LISTEs
         */
        ClassTypeEtablissement.initListes(modalName);

        $(modalName + ' #btnAction').unbind().on('click', function () {

            /** -----------------------------------------------------------------------------------
             * Vérification des ZONES Mandatory
             */
            var oblig = true;
            $(modalName + ' .required').each(function () {
                if (!$(this).val().trim()) {
                    oblig = false;
                }
            });
            if (!oblig) {
                toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                return;
            }

            /** -----------------------------------------------------------------------------------
             * Contrôle de cohérence
             */
            var isMS = false;
            if (secteur == '' || age == '') {
                toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                return;
            }
            if (!$(modalName + ' #zlTypeSecteurMS').prop('disabled')) {
                isMS = true;
                if (secteurms == '') {
                    toastr.warning('Le type de secteur MS est obligatoire !', "Attention");
                    return;
                }
            }

            $(this).off().removeClass('green').addClass('grey-cascade').html(
                '<i class="fa fa-spin fa-spinner"></i> Patientez...'
            );

            var libelle = $(modalName + ' #ztLibelle').val().toUpperCase();
            var sigle = $(modalName + ' #ztSigle').val().toUpperCase();
            var secteur = $(modalName + ' #zlTypeSecteur').val();
            var secteurms = $(modalName + ' #zlTypeSecteurMS').val();
            var age = $(modalName + ' #zlTypeAge').val();

            $.ajax({
                url: '/param/' + route,
                type: 'POST',
                data: {
                    _token : $(modalName + ' #token').val(),
                    pLibelle : libelle,
                    pSigle : sigle,
                    pSecteur : secteur,
                    pSecteurMS : (isMS ? secteurms : null),
                    pAge : age,
                },
                dataType: 'json',
                success: function(data){
                    result = true;
                    $(modal).modal('hide');
                    toastr.success('Saisie enregistrée', "Information");
                    table.ajax.reload();
                }
            });

        });

    },

    update: function (id) {

        /** -----------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        var modalName = '#dialogExec';
        var modal = $(modalName);
        modal.html($('#form').html());

        /** -----------------------------------------------------------------------------------
         * On récupère les infos du type de téléphone
         */
        $.ajax({
            url: '/param/' + route + '/' + id,
            type: 'GET',
            dataType: 'json',
            success: function(data){

                /** -----------------------------------------------------------------------------------
                 * Chargement du template du modal dans le modal d'exécution
                 */
                $(modal).find('#btnAction').addClass('yellow-gold').html('<i class="fa fa-plus-circle"></i> Modifier');
                $(modal).find('.modal-header').addClass('font-yellow-casablanca');
                $(modal).find('.modal-title').html('<i class="fa fa-refresh"></i> Modification ' + edit);
                $(modal).modal('show');

                /** -----------------------------------------------------------------------------------
                 * Init des zones
                 */
                var libelle = data.libelle;
                var sigle = data.sigle;

                var secteur = data.type_secteur_id;
                var secteurms = data.type_secteur_ms_id;
                var age = data.type_age_id;
                $(modalName + ' #ztLibelle').val(libelle).addClass('edited');
                $(modalName + ' #ztSigle').val(sigle).addClass('edited');

                ClassTypeEtablissement.initGeneric (modalName, 'typesecteur', '#zlTypeSecteur', secteur);
                ClassTypeEtablissement.initGeneric (modalName, 'typesecteurms', '#zlTypeSecteurMS', secteurms);
                ClassTypeEtablissement.initGeneric (modalName, 'typeage', '#zlTypeAge', age);

                /** -------------------------------------------------------------------------------
                 * Par défaut, on ne peut pas sélectionner de SECTEUR MS
                 */
                $(modalName + ' #zlTypeSecteurMS').prop("disabled", true);

                /** -------------------------------------------------------------------------------
                 */
                $(modalName + ' #zlTypeSecteur').on('change', function (e) {

                    var ms = $(this).find('option:selected').attr('data-ms');
                    if (ms === undefined || ms == 0) {
                        $(modalName + ' #zlTypeSecteurMS').prop("disabled", true);
                    } else {
                        $(modalName + ' #zlTypeSecteurMS').prop("disabled", false);
                    }
                });

                $(modalName + ' #zlTypeSecteur').trigger('change');

                /** -----------------------------------------------------------------------------------
                 * Bind de la modification
                 */
                $(modalName + ' #btnAction').unbind().on('click', function () {

                    /** -----------------------------------------------------------------------------------
                     * Vérification des ZONES Mandatory
                     */
                    var oblig = true;
                    $(modalName + ' .required').each(function () {
                        if (!$(this).val().trim()) {
                            oblig = false;
                        }
                    });
                    if (!oblig) {
                        toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                        return;
                    }

                    /** -----------------------------------------------------------------------------------
                     * Contrôle de cohérence
                     */
                    var isMS = false;
                    if (secteur == '' || age == '') {
                        toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                        return;
                    }
                    if (!$(modalName + ' #zlTypeSecteurMS').prop('disabled')) {
                        isMS = true;
                        if (secteurms == '') {
                            toastr.warning('Le type de secteur MS est obligatoire !', "Attention");
                            return;
                        }
                    }

                    $(this).off().removeClass('green').addClass('grey-cascade').html(
                        '<i class="fa fa-spin fa-spinner"></i> Patientez...'
                    );

                    var libelle = $(modalName + ' #ztLibelle').val().toUpperCase();
                    var sigle = $(modalName + ' #ztSigle').val().toUpperCase();
                    var secteur = $(modalName + ' #zlTypeSecteur').val();
                    var secteurms = $(modalName + ' #zlTypeSecteurMS').val();
                    var age = $(modalName + ' #zlTypeAge').val();

                    $.ajax({
                        url: '/param/' + route + '/' + id,
                        type: 'PUT',
                        data: {
                            _token : $(modalName + ' #token').val(),
                            pLibelle : libelle,
                            pSigle : sigle,
                            pSecteur : secteur,
                            pSecteurMS : (isMS ? secteurms : null),
                            pAge : age,
                        },
                        dataType: 'json',
                        success: function(data){
                            if (data == -1) {
                                $(modal).modal('hide');
                                toastr.error('Modification non effectuée', "Attention");
                            } else {
                                $(modal).modal('hide');
                                toastr.info('Modification enregistrée', "Information");
                                table.ajax.reload();
                            }
                        }
                    });
                });
            }
        });
    },

    manageActions: function () {

        /** ---------------------------------------------------------------------------------------
         * Anti CLOSE
         */
        $('#dialogExec').on('keypress', 'input', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });

        /** ---------------------------------------------------------------------------------------
         * Ajout d'un TYPE D'ORGANISME
         */
        $('#btnAdd').click(function () {
            ClassTypeEtablissement.add();
        });

        /** ---------------------------------------------------------------------------------------
         * Modif du SWITCH OUI / NON
         */
        $('#table').on('switchChange.bootstrapSwitch', 'input[type="checkbox"]', function(event, state) {
            var id = event.target.id;
            var value = (state == true) ? 1 : 0;

            $.ajax({
                url: '/param/' + route + '/' + id + '/edit',
                type: 'GET',
                data: {
                    pValue: value,
                },
                dataType: 'json',
                success: function (data) {
                }
            });
        });
    }
}