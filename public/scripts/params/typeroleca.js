/** -----------------------------------------------------------------------------------------------
 *
 * @author : Vincent BENNER
 * @copyright : Page Up 2016
 * @version : 1.0.0
 *
 * @history : 1.0.0 - Release
 * @description : Gestion des TYPES DE ROLES CA
 *
 */

/** -----------------------------------------------------------------------------------------------
 *
 */
var tableName = 'table';
var table;
var route = 'typeroleca'
var edit = ' d\'un Type de Rôle au CA';

/** -----------------------------------------------------------------------------------------------
 * JQuery start
 */
$(function () {

    ClassTypeRoleCA.init();
    ClassTypeRoleCA.initDataTable();
    ClassTypeRoleCA.manageActions();

});

/** -----------------------------------------------------------------------------------------------
 * @class : TYPE ROLE BUREAU
 */
var ClassTypeRoleCA = {

    /* --------------------------------------------------------------------------------------------
     * Init
     */
    init : function () {

    },

    initDataTable : function () {
        table = $('#' + tableName).DataTable({
            iDisplayLength:50,
            ajax : "/datatable/" + route,
            serverSide : false,
            columnDefs: [
                {className: "dt-body-left", "targets": [0]},
                {className: "dt-body-right", "targets": [1, 2]},
            ],
            autoWidth : false,
            columns : [
                {orderable: true, width : null},
                {orderable: true, width : 80},
                {orderable: true, width : 80}
            ],
            sPagingType     : "bootstrap_full_numbers",
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
            },
            initComplete : function (settingssettings) {
                //return;
                /** On change les largeurs par défaut des contrôles */
                var tableWrapper = $('#' + tableName + '_wrapper');
                var tableLength = $('#' + tableName + '_length');
                tableLength.css('width', '400px');
                tableWrapper.find('.dataTables_length select').
                    addClass("form-control input-inline").
                    select2();
                tableWrapper.find('.dataTables_filter input').addClass("form-control input-inline");
            },
            rowCallback : function( row, data, index ) {

                var colActive   = 1;
                var colAction   = 2;

                /* --------------------------------------------------------------------------------
                 * On crée les Switch
                 */
                $('td:eq('+ colActive + ')', row).children('.make-switch').bootstrapSwitch();

                /* --------------------------------------------------------------------------------
                 * On checke la colonne ACTION
                 */
                $('td:eq(' + colAction + ')', row).children('a').bind('click', function () {
                    ClassTypeRoleCA.update($(this).attr('data-id'));
                });
            }

        });


        return true;
    },

    /* --------------------------------------------------------------------------------------------
     * Modal d'ajout d'un enregistrement
     */
    add: function () {

        /** -----------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        var modalName = '#dialogExec';
        var modal = $(modalName);
        modal.html($('#form').html());

        /** -----------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        $(modal).find('#btnAction').addClass('green').html('<i class="fa fa-plus-circle"></i> Ajouter');
        $(modal).find('.modal-header').addClass('font-green');
        $(modal).find('.modal-title').html('<i class="fa fa-plus-circle"></i> Ajout ' + edit);
        $(modal).modal('show');

        $(modalName + ' #btnAction').unbind().on('click', function () {

            /** -----------------------------------------------------------------------------------
             * Vérification des ZONES Mandatory
             */
            var oblig = true;
            $(modalName + ' .required').each(function () {
                if (!$(this).val().trim()) {
                    oblig = false;
                }
            });
            if (!oblig) {
                toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                return;
            }

            $(this).off().removeClass('green').addClass('grey-cascade').html(
                '<i class="fa fa-spin fa-spinner"></i> Patientez...'
            );

            var libelle = $(modalName + ' #ztLibelle').val().toUpperCase();
            $.ajax({
                url: '/param/' + route,
                type: 'POST',
                data: {
                    _token : $(modalName + ' #token').val(),
                    pLibelle : libelle,
                },
                dataType: 'json',
                success: function(data){
                    result = true;
                    $(modal).modal('hide');
                    toastr.success('Saisie enregistrée', "Information");
                    table.ajax.reload();
                }
            });

        });

    },

    update: function (id) {

        /** -----------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        var modalName = '#dialogExec';
        var modal = $(modalName);
        modal.html($('#form').html());

        /** -----------------------------------------------------------------------------------
         * On récupère les infos du type de rôle bureau
         */
        $.ajax({
            url: '/param/' + route + '/' + id,
            type: 'GET',
            dataType: 'json',
            success: function(data){

                /** -----------------------------------------------------------------------------------
                 * Chargement du template du modal dans le modal d'exécution
                 */
                $(modal).find('#btnAction').addClass('yellow-gold').html('<i class="fa fa-plus-circle"></i> Modifier');
                $(modal).find('.modal-header').addClass('font-yellow-casablanca');
                $(modal).find('.modal-title').html('<i class="fa fa-refresh"></i> Modification ' + edit);
                $(modal).modal('show');

                /** -----------------------------------------------------------------------------------
                 * Init des zones
                 */
                var libelle = data.libelle;
                $(modalName + ' #ztLibelle').val(libelle).addClass('edited');

                /** -----------------------------------------------------------------------------------
                 * Bind de la modification
                 */
                $(modalName + ' #btnAction').unbind().on('click', function () {

                    /** -----------------------------------------------------------------------------------
                     * Vérification des ZONES Mandatory
                     */
                    var oblig = true;
                    $(modalName + ' .required').each(function () {
                        if (!$(this).val().trim()) {
                            oblig = false;
                        }
                    });
                    if (!oblig) {
                        toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                        return;
                    }

                    $(this).off().removeClass('yellow-gold').addClass('grey-cascade').html(
                        '<i class="fa fa-spin fa-spinner"></i> Patientez...'
                    );

                    libelle = $(modalName + ' #ztLibelle').val().toUpperCase();
                    $.ajax({
                        url: '/param/' + route + '/' + id,
                        type: 'PUT',
                        data: {
                            _token : $(modalName + ' #token').val(),
                            pLibelle : libelle,
                        },
                        dataType: 'json',
                        success: function(data){
                            if (data == -1) {
                                $(modal).modal('hide');
                                toastr.error('Modification non effectuée', "Attention");
                            } else {
                                $(modal).modal('hide');
                                toastr.info('Modification enregistrée', "Information");
                                table.ajax.reload();
                            }
                        }
                    });
                });
            }
        });
    },

    manageActions: function () {

        /** ---------------------------------------------------------------------------------------
         * Anti CLOSE
         */
        $('#dialogExec').on('keypress', 'input', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });

        /** ---------------------------------------------------------------------------------------
         * Ajout d'un TYPE DE ROLE BUREAU
         */
        $('#btnAdd').click(function () {
            ClassTypeRoleCA.add();
        });

        /** ---------------------------------------------------------------------------------------
         * Modif du SWITCH OUI / NON
         */
        $('#table').on('switchChange.bootstrapSwitch', 'input[type="checkbox"]', function(event, state) {
            var id = event.target.id;
            var value = (state == true) ? 1 : 0;

            $.ajax({
                url: '/param/' + route + '/' + id + '/edit',
                type: 'GET',
                data: {
                    pValue: value,
                },
                dataType: 'json',
                success: function (data) {
                }
            });
        });
    }
}