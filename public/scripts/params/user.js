/** -----------------------------------------------------------------------------------------------
 *
 * @author : Vincent BENNER
 * @copyright : Page Up 2016
 * @version : 1.0.0
 *
 * @history : 1.0.0 - Release
 * @description : Gestion des UTILISATEURS
 *
 */

/** -----------------------------------------------------------------------------------------------
 *
 */
var tableName = 'table';
var table;
var route = 'user'
var edit = ' d\'un Utilisateur';

/** -----------------------------------------------------------------------------------------------
 * JQuery start
 */
$(function () {

    /** -------------------------------------------------------------------------------------------
     *
     */
    $(document).on("click", ".update", function (e) {

        var _route = $(this).attr('data-type');
        var _field = $(this).attr('data-field');
        var _value = $(this).attr('data-statut');
        var _id = $(this).attr('data-id');
        var _user = $(this).attr('data-user');

        var _newVal = 1 - _value;

        if (_newVal == 0) {
            $(this).removeClass('green-seagreen').addClass('red-sunglo');
        } else {
            $(this).removeClass('red-sunglo').addClass('green-seagreen');
        }
        $(this).attr('data-statut', _newVal);

        $.ajax({
            url: '/param/users/grants/'+_route+'/' + _field + '/' + _id + '/' + _user + '/' + _newVal,
            type: 'PUT',
            data: {
                pValue: _value
            },
            dataType: "json",
            async: false
        }).complete(function (code_html, statut) {
        });
    });

    ClassUser.init();
    ClassUser.initDataTable();
    ClassUser.manageActions();

});

/** -----------------------------------------------------------------------------------------------
 * @class : USER
 */
var ClassUser = {

    /* --------------------------------------------------------------------------------------------
     * Init
     */
    init : function () {

    },

    /* --------------------------------------------------------------------------------------------
     * Init Datatables
     */
    initDataTable : function () {
        table = $('#' + tableName).DataTable({
            iDisplayLength:50,
            ajax : "/datatable/" + route,
            serverSide : false,
            autoWidth: false,
            columnDefs: [
                {className: "dt-body-left", "targets": [0, 1, 3]},
                {className: "dt-body-right", "targets": [2, 4]},
            ],
            columns : [
                {orderable: true, width : 200},
                {orderable: true, width : 200},
                {orderable: true, width : 100},
                {orderable: true, width : 100},
            ],
            lengthMenu : [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "Tous"]
            ],
            sPagingType     : "bootstrap_full_numbers",
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
            },
            initComplete : function (settingssettings) {
                //return;
                /** On change les largeurs par défaut des contrôles */
                var tableWrapper = $('#' + tableName + '_wrapper');
                var tableLength = $('#' + tableName + '_length');
                tableLength.css('width', '400px');
                tableWrapper.find('.dataTables_length select').
                    addClass("form-control input-inline").
                    select2();
                tableWrapper.find('.dataTables_filter input').addClass("form-control input-inline");
            },
            rowCallback : function( row, data, index ) {

                var colActive   = 2;
                var colAction   = 4;

                /* --------------------------------------------------------------------------------
                 * On crée les Switch
                 */
                $('td:eq('+ colActive + ')', row).children('.make-switch').bootstrapSwitch();

                /* --------------------------------------------------------------------------------
                 * On checke la colonne ACTION
                 */
                $('td:eq(' + colAction + ')', row).children('a').bind('click', function () {
                    if ($(this).hasClass('modif')) {
                        ClassUser.update($(this).attr('data-id'));
                    }
                });

                $('td:eq(' + colAction + ')', row).children('a').bind('click', function () {
                    if ($(this).hasClass('acces')) {
                        ClassUser.grants($(this).attr('data-id'));
                    }
                });
            }

        });


        return true;
    },

    grants: function (id) {

        /** ---------------------------------------------------------------------------------------
         * On va récupérer la liste des BLOCS -> MODULES -> SOUS-MODULES
         */
        var modalName = '#dialogExec';
        var modal = $(modalName);
        modal.html($('#formGrant').html());

        /** ---------------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        $(modal).find('.modal-header').addClass('font-green');
        $(modal).find('.modal-title').html('<i class="fa fa-gear"></i> Gestion des Droits');

        $.ajax({
            url: '/datatable/user/grants/' + id,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {

                /** -------------------------------------------------------------------------------
                 * On traite les BLOCS
                 */
                $.each(data.BLOC, function(index, item){

                    $('#tableBlocs').append('<tr>'+
                        '<td>' + (item.NAME == '' ? ' - ' : item.NAME) + '</td>' +
                        '<td>' +
                            '<a data-toggle="tooltip" title="ACTIF" ' +
                            'class="update pp-actif btn btn-xs ' + (item.GRANT == 1 ? 'green-seagreen' : 'red-sunglo') + '" ' +
                            'data-id="'+item.ID+'" data-statut="'+item.GRANT+'"' +
                            'data-user="'+id+'"' +
                            'data-type="bloc" data-field="grant" ' +
                            '>'+
                            '<i class="fa fa-check"></i>'+
                            '</a>'+
                        '</td>' +
                        '<td>' +
                            '<a data-toggle="tooltip" title="AJOUT" ' +
                            'class="update pp-actif btn btn-xs ' + (item.ADD == 1 ? 'green-seagreen' : 'red-sunglo') + '" ' +
                            'data-id="'+item.ID+'" data-statut="'+item.ADD+'"' +
                            'data-user="'+id+'"' +
                            'data-type="bloc" data-field="add" ' +
                            '>'+
                            '<i class="fa fa-check"></i>'+
                            '</a>'+
                        '</td>' +
                        '<td>' +
                            '<a data-toggle="tooltip" title="EDIT" ' +
                            'class="update pp-actif btn btn-xs ' + (item.EDIT == 1 ? 'green-seagreen' : 'red-sunglo') + '" ' +
                            'data-id="'+item.ID+'" data-statut="'+item.EDIT+'"' +
                            'data-user="'+id+'"' +
                            'data-type="bloc" data-field="edit" ' +
                            '>'+
                            '<i class="fa fa-check"></i>'+
                            '</a>'+
                        '</td>' +
                    '</tr>'
                    );
                });

                /** -------------------------------------------------------------------------------
                 * On traite les MODULES
                 */
                $.each(data.BLOC, function(index, bloc){

                    $.each(bloc.MODULES.MODULE, function(index, item) {
                        $('#tableModules').append(
                            '<tr>' +
                                '<td>' + (item.NAME == '' ? ' - ' : '<span class="font-grey-salsa">' + bloc.NAME + '</span>' + ' > ' + item.NAME) +
                                '</td>' +
                                '<td>' +
                                '<a data-toggle="tooltip" title="ACTIF" ' +
                                'class="update pp-actif btn btn-xs ' + (item.GRANT == 1 ? 'green-seagreen' : 'red-sunglo') + '" ' +
                                'data-id="'+item.ID+'" data-statut="'+item.GRANT+'"' +
                                'data-user="'+id+'"' +
                                'data-type="module" data-field="grant" ' +
                                '>'+
                                '<i class="fa fa-check"></i>'+
                                '</a>'+
                                '</td>' +
                                '<td>' +
                                '<a data-toggle="tooltip" title="AJOUT" ' +
                                'class="update pp-actif btn btn-xs ' + (item.ADD == 1 ? 'green-seagreen' : 'red-sunglo') + '" ' +
                                'data-id="'+item.ID+'" data-statut="'+item.ADD+'"' +
                                'data-user="'+id+'"' +
                                'data-type="module" data-field="add" ' +
                                '>'+
                                '<i class="fa fa-check"></i>'+
                                '</a>'+
                                '</td>' +
                                '<td>' +
                                '<a data-toggle="tooltip" title="EDIT" ' +
                                'class="update pp-actif btn btn-xs ' + (item.EDIT == 1 ? 'green-seagreen' : 'red-sunglo') + '" ' +
                                'data-id="'+item.ID+'" data-statut="'+item.EDIT+'"' +
                                'data-user="'+id+'"' +
                                'data-type="module" data-field="edit" ' +
                                '>'+
                                '<i class="fa fa-check"></i>'+
                                '</a>'+
                                '</td>' +
                            '</tr>'
                        );
                    });
                });

                $.each(data.BLOC, function(index, bloc){

                    $.each(bloc.MODULES.MODULE, function(index, module) {

                        $.each(module.SUBMODULES.SUBMODULE, function(index, item) {
                            $('#tableSubModules').append(
                                '<tr>' +
                                    '<td>' + (item.NAME == '' ? ' - ' :
                                        '<span class="font-grey-salsa">' + module.NAME + '</span>' + ' > ' + item.NAME) +
                                    '<td>' +
                                    '<a data-toggle="tooltip" title="ACTIF" ' +
                                    'class="update pp-actif btn btn-xs ' + (item.GRANT == 1 ? 'green-seagreen' : 'red-sunglo') + '" ' +
                                    'data-id="'+item.ID+'" data-statut="'+item.GRANT+'"' +
                                    'data-user="'+id+'"' +
                                    'data-type="submodule" data-field="grant" ' +
                                    '>'+
                                    '<i class="fa fa-check"></i>'+
                                    '</a>'+
                                    '</td>' +
                                    '<td>' +
                                    '<a data-toggle="tooltip" title="AJOUT" ' +
                                    'class="update pp-actif btn btn-xs ' + (item.ADD == 1 ? 'green-seagreen' : 'red-sunglo') + '" ' +
                                    'data-id="'+item.ID+'" data-statut="'+item.ADD+'"' +
                                    'data-user="'+id+'"' +
                                    'data-type="submodule" data-field="add" ' +
                                    '>'+
                                    '<i class="fa fa-check"></i>'+
                                    '</a>'+
                                    '</td>' +
                                    '<td>' +
                                    '<a data-toggle="tooltip" title="EDIT" ' +
                                    'class="update pp-actif btn btn-xs ' + (item.EDIT == 1 ? 'green-seagreen' : 'red-sunglo') + '" ' +
                                    'data-id="'+item.ID+'" data-statut="'+item.EDIT+'"' +
                                    'data-user="'+id+'"' +
                                    'data-type="submodule" data-field="edit" ' +
                                    '>'+
                                    '<i class="fa fa-check"></i>'+
                                    '</a>'+
                                    '</td>' +
                                '</tr>'
                            );
                        });
                    });
                });

                $(modal).modal('show');

            }
        });

    },

    /* --------------------------------------------------------------------------------------------
     * Modal d'ajout d'un enregistrement
     */
    add: function () {

        /** -----------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        var modalName = '#dialogExec';
        var modal = $(modalName);
        modal.html($('#form').html());

        /** -----------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        $(modal).find('#btnAction').addClass('green').html('<i class="fa fa-plus-circle"></i> Ajouter');
        $(modal).find('.modal-header').addClass('font-green');
        $(modal).find('.modal-title').html('<i class="fa fa-plus-circle"></i> Ajout ' + edit);
        $(modal).modal('show');

        /** ---------------------------------------------------------------------------------------
         * Init des LISTEs
         */
        ClassUser.initListes(modalName);

        $(modalName + ' #btnAction').unbind().on('click', function () {

            /** -----------------------------------------------------------------------------------
             * Vérification des ZONES Mandatory
             */
            var oblig = true;
            $(modalName + ' .required').each(function () {
                if (!$(this).val().trim()) {
                    oblig = false;
                }
            });
            if (!oblig) {
                toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                return;
            }

            $(this).off().removeClass('green').addClass('grey-cascade').html(
                '<i class="fa fa-spin fa-spinner"></i> Patientez...'
            );

            var nom = $(modalName + ' #ztNom').val();
            var mail = $(modalName + ' #ztMail').val();
            var role = $(modalName + ' #zlRole').val();

            $.ajax({
                url: '/param/' + route,
                type: 'POST',
                data: {
                    _token : $(modalName + ' #token').val(),
                    pNom : nom,
                    pMail : mail,
                    pRole : role,
                },
                dataType: 'json',
                success: function(data){
                    if (data == -1) {
                        $(modal).modal('hide');
                        toastr.error('Saisie annulée : l\'utilisateur existe déjà', "Attention");
                    } else {
                        result = true;
                        $(modal).modal('hide');
                        toastr.success('Saisie enregistrée', "Information");
                        table.ajax.reload();
                    }
                }
            });

        });

    },

    update: function (id) {

        /** -----------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        var modalName = '#dialogExec';
        var modal = $(modalName);
        modal.html($('#form').html());

        /** -----------------------------------------------------------------------------------
         * On récupère les infos de la civilite
         */
        $.ajax({
            url: '/param/' + route + '/' + id,
            type: 'GET',
            dataType: 'json',
            success: function(data){

                /** -----------------------------------------------------------------------------------
                 * Chargement du template du modal dans le modal d'exécution
                 */
                $(modal).find('#btnAction').addClass('yellow-gold').html('<i class="fa fa-plus-circle"></i> Modifier');
                $(modal).find('.modal-header').addClass('font-yellow-casablanca');
                $(modal).find('.modal-title').html('<i class="fa fa-refresh"></i> Modification ' + edit);
                $(modal).modal('show');

                /** -----------------------------------------------------------------------------------
                 * Init des zones
                 */
                var nom = data.name;
                var email = data.email;
                var role = data.role_id;
                $(modalName + ' #ztNom').val(nom).addClass('edited');
                $(modalName + ' #ztMail').val(email).addClass('edited');
                $(modalName + ' #ztNom').focus();

                ClassUser.initGeneric (modalName, 'role', '#zlRole', role);

                /** -----------------------------------------------------------------------------------
                 * Bind de la modification
                 */
                $(modalName + ' #btnAction').unbind().on('click', function () {

                    /** -----------------------------------------------------------------------------------
                     * Vérification des ZONES Mandatory
                     */
                    var oblig = true;
                    $(modalName + ' .required').each(function () {
                        if (!$(this).val().trim()) {
                            oblig = false;
                        }
                    });
                    if (!oblig) {
                        toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                        return;
                    }

                    $(this).off().removeClass('yellow-gold').addClass('grey-cascade').html(
                        '<i class="fa fa-spin fa-spinner"></i> Patientez...'
                    );

                    var nom = $(modalName + ' #ztNom').val();
                    var mail = $(modalName + ' #ztMail').val();
                    var role = $(modalName + ' #zlRole').val();
                    $.ajax({
                        url: '/param/' + route + '/' + id,
                        type: 'PUT',
                        data: {
                            _token : $(modalName + ' #token').val(),
                            pNom : nom,
                            pMail : mail,
                            pRole : role,
                        },
                        dataType: 'json',
                        success: function(data){
                            if (data == -1) {
                                $(modal).modal('hide');
                                toastr.error('Modification non effectuée', "Attention");
                            } else {
                                $(modal).modal('hide');
                                toastr.info('Modification enregistrée', "Information");
                                table.ajax.reload();
                            }
                        }
                    });
                });
            }
        });
    },

    initListes : function (modal) {

        /** ---------------------------------------------------------------------------------------
         * Listes de bases
         */
        ClassUser.initGeneric(modal, 'role', '#zlRole');

    },

    initGeneric : function (modal, route, zone, value) {

        $.ajax({
            url: '/list/' + route,
            type: 'GET',
            dataType: 'json',
            async:false,
            success: function(data){
                var select = '';
                if(data.LISTE.length > 0){
                    var zl = $(modal + ' ' + zone);
                    zl.empty();
                    if (data.LISTE.length > 1) {
                        zl.append('<option value=""></option>');
                    }
                    $.each(data.LISTE, function(index, item){
                        zl.append('<option value="'+item.id+'" ' + (item.id == value ? ' selected="selected" ' : '') + '>'+item.libelle+'</option>');
                    });
                    zl.select2({
                        width: '100%',
                        allowClear:(data.LISTE.length > 1 ? true : false),
                        placeholder: ' '
                    });
                }

            }
        });
    },

    manageActions: function () {

        /** ---------------------------------------------------------------------------------------
         * Anti CLOSE
         */
        $('#dialogExec').on('keypress', 'input', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });

        /** ---------------------------------------------------------------------------------------
         * Ajout d'un PAYS
         */
        $('#btnAdd').click(function () {
            ClassUser.add();
        });

        /** ---------------------------------------------------------------------------------------
         * Modif du SWITCH OUI / NON
         */
        $('#table').on('switchChange.bootstrapSwitch', 'input[type="checkbox"]', function(event, state) {
            var id = event.target.id;
            var value = (state == true) ? 1 : 0;

            $.ajax({
                url: '/param/' + route + '/' + id + '/edit',
                type: 'GET',
                data: {
                    pValue: value,
                },
                dataType: 'json',
                success: function (data) {
                }
            });
        });
    }
}