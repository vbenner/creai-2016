/** -----------------------------------------------------------------------------------------------
 *
 * @author : Vincent BENNER
 * @copyright : Page Up 2016
 * @version : 1.0.0
 *
 * @history : 1.0.0 - Release
 * @description : Gestion des TYPES DE PUBLIC
 *
 */

/** -----------------------------------------------------------------------------------------------
 *
 */
var tableNameType = 'tableType';
var tableNameDetail = 'tableDetail';
var tableType;
var tableDetail;
var routeType = 'typepublic';
var routeDetail = 'detailtypepublic';
var editType = ' d\'un Type de Public';
var editDetail = ' d\'un Detail de Type de Public';
var reload = false;

/** -----------------------------------------------------------------------------------------------
 * JQuery start
 */
$(function () {

    ClassTypePublic.init();
    ClassTypePublic.initDataTable();
    ClassTypePublic.manageActions();

});

/** -----------------------------------------------------------------------------------------------
 * @class : TYPE PUBLIC
 */
var ClassTypePublic = {

    /* --------------------------------------------------------------------------------------------
     * Init
     */
    init : function () {

    },

    initDataTable : function () {
        tableType = $('#' + tableNameType).DataTable({
            iDisplayLength:50,
            ajax : "/datatable/" + routeType,
            serverSide : false,
            autoWidth: false,
            columnDefs: [
                {className: "dt-body-left", "targets": [0, 1]},
                {className: "dt-body-right", "targets": [2, 3]},
            ],
            columns : [
                {orderable: true, width : null},
                {orderable: true, width : null},
                {orderable: true, width : 80},
                {orderable: true, width : 120}
            ],
            sPagingType     : "bootstrap_full_numbers",
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
            },
            initComplete : function (settingssettings) {
                //return;
                /** On change les largeurs par défaut des contrôles */
                var tableWrapper = $('#' + tableNameType + '_wrapper');
                var tableLength = $('#' + tableNameType + '_length');
                tableLength.css('width', '400px');
                tableWrapper.find('.dataTables_length select').
                    addClass("form-control input-inline").
                    select2();
                tableWrapper.find('.dataTables_filter input').addClass("form-control input-inline");
            },
            rowCallback : function( row, data, index ) {

                var colActive   = 2;
                var colAction   = 3;

                /* --------------------------------------------------------------------------------
                 * On crée les Switch
                 */
                $('td:eq('+ colActive + ')', row).children('.make-switch').bootstrapSwitch();

                /* --------------------------------------------------------------------------------
                 * On checke la colonne ACTION
                 */
                $('td:eq(' + colAction + ')', row).children('a').bind('click', function () {

                    if ($(this).hasClass('modif')) {
                        ClassTypePublic.update($(this).attr('data-id'));
                    }

                    if ($(this).hasClass('detail')) {
                        ClassTypePublic.manageDetail($(this).attr('data-id'));
                    }
                });
            }

        });


        return true;
    },

    /* --------------------------------------------------------------------------------------------
     * On va faire comme pour les téléphonees en rajoutant un MODAL
     */
    manageDetail: function (idPublic) {

        /** ---------------------------------------------------------------------------------------
         * Gestion des DETAILS
         */
        reload = false;
        var modalName = '#dialogExec';
        var modal = $(modalName);
        modal.html($('#formListDetail').html());

        ClassDetail.initDataTable(idPublic);

        /** ---------------------------------------------------------------------------------------
         * Ajout du DETAIL
         */
        $(modal).unbind().on('click', '#btnAddDetail', function () {
            ClassDetail.add(idPublic);
        });

        /** ---------------------------------------------------------------------------------------
         * Modif du SWITCH OUI / NON
         */
        $('#tableDetail').on('switchChange.bootstrapSwitch', 'input[type="checkbox"]', function(event, state) {
            var id = event.target.id;
            var value = (state == true) ? 1 : 0;

            $.ajax({
                url: '/param/' + routeDetail + '/' + id + '/edit',
                type: 'GET',
                data: {
                    pValue: value,
                },
                dataType: 'json',
                success: function (data) {
                }
            });
        });

        /** ---------------------------------------------------------------------------------------
         *
         */
        $(modal).on('hidden.bs.modal', function (e) {
            if (reload) {
                tableType.ajax.reload();
            }
        });

        $(modal).modal('show');
    },

    /* --------------------------------------------------------------------------------------------
     * Modal d'ajout d'un enregistrement
     */
    add: function () {

        /** -----------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        var modalName = '#dialogExec';
        var modal = $(modalName);
        modal.html($('#formType').html());

        /** -----------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        $(modal).find('#btnAction').addClass('green').html('<i class="fa fa-plus-circle"></i> Ajouter');
        $(modal).find('.modal-header').addClass('font-green');
        $(modal).find('.modal-title').html('<i class="fa fa-plus-circle"></i> Ajout d\'un ' + editType);
        $(modal).modal('show');

        $(modalName + ' #btnAction').unbind().on('click', function () {

            /** -----------------------------------------------------------------------------------
             * Vérification des ZONES Mandatory
             */
            var oblig = true;
            $(modalName + ' .required').each(function () {
                if (!$(this).val().trim()) {
                    oblig = false;
                }
            });
            if (!oblig) {
                toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                return;
            }

            $(this).off().removeClass('green').addClass('grey-cascade').html(
                '<i class="fa fa-spin fa-spinner"></i> Patientez...'
            );

            var libelle = $(modalName + ' #ztLibelle').val().toUpperCase();
            var note = $(modalName + ' #ztNote').val();
            $.ajax({
                url: '/param/' + routeType,
                type: 'POST',
                data: {
                    _token : $(modalName + ' #token').val(),
                    pLibelle : libelle,
                    pNote : note
                },
                dataType: 'json',
                success: function(data){
                    result = true;
                    $(modal).modal('hide');
                    toastr.success('Saisie enregistrée', "Information");
                    tableType.ajax.reload();
                }
            });

        });

    },

    update: function (id) {

        /** -----------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        var modalName = '#dialogExec';
        var modal = $(modalName);
        modal.html($('#formType').html());

        /** -----------------------------------------------------------------------------------
         * On récupère les infos du type de téléphone
         */
        $.ajax({
            url: '/param/' + routeType + '/' + id,
            type: 'GET',
            dataType: 'json',
            success: function(data){

                /** -----------------------------------------------------------------------------------
                 * Chargement du template du modal dans le modal d'exécution
                 */
                $(modal).find('#btnAction').addClass('yellow-gold').html('<i class="fa fa-plus-circle"></i> Modifier');
                $(modal).find('.modal-header').addClass('font-yellow-casablanca');
                $(modal).find('.modal-title').html('<i class="fa fa-refresh"></i> Modification ' + editType);
                $(modal).modal('show');

                /** -----------------------------------------------------------------------------------
                 * Init des zones
                 */
                var libelle = data.libelle;
                var note = data.note;
                $(modalName + ' #ztLibelle').val(libelle).addClass('edited');
                $(modalName + ' #ztNote').val(note).addClass('edited');

                /** -----------------------------------------------------------------------------------
                 * Bind de la modification
                 */
                $(modalName + ' #btnAction').unbind().on('click', function () {

                    /** -----------------------------------------------------------------------------------
                     * Vérification des ZONES Mandatory
                     */
                    var oblig = true;
                    $(modalName + ' .required').each(function () {
                        if (!$(this).val().trim()) {
                            oblig = false;
                        }
                    });
                    if (!oblig) {
                        toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                        return;
                    }

                    $(this).off().removeClass('yellow-gold').addClass('grey-cascade').html(
                        '<i class="fa fa-spin fa-spinner"></i> Patientez...'
                    );

                    libelle = $(modalName + ' #ztLibelle').val().toUpperCase();
                    note = $(modalName + ' #ztNote').val();
                    $.ajax({
                        url: '/param/' + routeType + '/' + id,
                        type: 'PUT',
                        data: {
                            _token : $(modalName + ' #token').val(),
                            pLibelle : libelle,
                            pNote : note,
                        },
                        dataType: 'json',
                        success: function(data){
                            if (data == -1) {
                                $(modal).modal('hide');
                                toastr.error('Modification non effectuée', "Attention");
                            } else {
                                $(modal).modal('hide');
                                toastr.info('Modification enregistrée', "Information");
                                tableType.ajax.reload();
                            }
                        }
                    });
                });
            }
        });
    },

    manageActions: function () {

        /** ---------------------------------------------------------------------------------------
         * Anti CLOSE
         */
        $('#dialogExec').on('keypress', 'input', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });

        /** ---------------------------------------------------------------------------------------
         * Ajout d'un TYPE DE PUBLIC
         */
        $('#btnAdd').click(function () {
            ClassTypePublic.add();
        });

        /** ---------------------------------------------------------------------------------------
         * Modif du SWITCH OUI / NON
         */
        $('#tableType').on('switchChange.bootstrapSwitch', 'input[type="checkbox"]', function(event, state) {
            var id = event.target.id;
            var value = (state == true) ? 1 : 0;

            $.ajax({
                url: '/param/' + routeType + '/' + id + '/edit',
                type: 'GET',
                data: {
                    pValue: value,
                },
                dataType: 'json',
                success: function (data) {
                }
            });
        });
    }
};


/** -----------------------------------------------------------------------------------------------
 * @class : DETAIL
 */
var ClassDetail = {

    /** -------------------------------------------------------------------------------------------
     * On récupère les PÔLES d'un ORGANISME (d'où le paramètre idOO)
     */
    initDataTable: function (idType) {

        tableDetail = $('#' + tableNameDetail).removeAttr('width').DataTable({
            iDisplayLength:50,
            ajax: "/datatable/" + routeDetail + '/' + idType,
            serverSide: false,
            autoWidth: false,
            columnDefs: [
                {className: "dt-body-left", "targets": [0, 1, 2]},
                {className: "dt-body-right", "targets": [3]}
            ],
            columns : [
                null, null, null,
                {orderable: true, width: 110},
            ],
            lengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "Tous"]
            ],

            //fixedColumns: true,
            sPagingType: "bootstrap_full_numbers",
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
            },
            initComplete: function (settingssettings) {
                /** On change les largeurs par défaut des contrôles */
                var tableWrapper = $('#' + tableNameDetail + '_wrapper');
                var tableLength = $('#' + tableNameDetail + '_length');
                tableLength.css('width', '400px');
                tableWrapper.find('.dataTables_length select').addClass("form-control input-inline").select2();
                tableWrapper.find('.dataTables_filter input').addClass("form-control input-inline");
            },
            rowCallback: function (row, data, index) {

                var colStatut = 1;
                var colAction = 3;

                /* --------------------------------------------------------------------------------
                 * On checke la colonne STATUT
                 */
                $('td:eq('+ colStatut + ')', row).children('.make-switch').bootstrapSwitch();

                $('td:eq(' + colAction + ')', row).children('a').bind('click', function () {
                    if ($(this).hasClass('modif')) {
                        ClassDetail.update($(this).attr('data-id'));
                    }
                });
            }

        });


        return true;
    },

    add: function (idType) {

        /** ---------------------------------------------------------------------------------------
         *
         */
        var modalName = '#dialogExecLvl1';
        var modal = $(modalName);
        modal.html($('#formDetail').html());

        /** ---------------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        $(modal).find('#btnAction').addClass('green').html('<i class="fa fa-plus-circle"></i> Ajouter');
        $(modal).find('.modal-header').addClass('font-green');
        $(modal).find('.modal-title').html('<i class="fa fa-plus-circle"></i> Ajout ' + editDetail);

        $(modal).modal('show');

        /** ---------------------------------------------------------------------------------------
         * Sauvegarde
         */
        $(modalName + ' #btnAction').unbind().on('click', function () {

            /** -----------------------------------------------------------------------------------
             * Vérification des ZONES Mandatory
             */
            var oblig = true;
            $(modalName + ' .required').each(function () {
                if (!$(this).val().trim()) {
                    oblig = false;
                }
            });

            if (!oblig) {
                toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                return;
            }

            // ------------------------------------------------------------------------------------
            // Données de BASE
            // ------------------------------------------------------------------------------------
            var libelle = $(modalName + ' #ztLibelle').val().trim().toUpperCase();
            var note = $(modalName + ' #ztNote').val().trim().toUpperCase();

            $(this).off().removeClass('green').addClass('grey-cascade').html(
                '<i class="fa fa-spin fa-spinner"></i> Patientez...'
            );
            reload = true;

            $.ajax({
                url: '/param/' + routeDetail,
                type: 'POST',
                data: {
                    _token: $(modalName + ' #token').val(),
                    pIdType:idType,
                    pLibelle: libelle,
                    pNote: note,
                },
                dataType: 'json',
                success: function (data) {
                    result = true;
                    $(modal).modal('hide');
                    toastr.success('Saisie enregistrée', "Information");
                    tableDetail.ajax.reload();
                }
            });

        });

    },

    update: function (idType) {

        /** ---------------------------------------------------------------------------------------
         * Chargement du template du modal dans le modal d'exécution
         */
        var modalName = '#dialogExecLvl1';
        var modal = $(modalName);
        modal.html($('#formDetail').html());

        /** ---------------------------------------------------------------------------------------
         * On récupère les infos du DETAIL
         */
        $.ajax({
            url: '/param/' + routeDetail + '/' + idType,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {

                /** -------------------------------------------------------------------------------
                 * Chargement du template du modal dans le modal d'exécution
                 */
                $(modal).find('#btnAction').addClass('yellow-gold').html('<i class="fa fa-plus-circle"></i> Modifier');
                $(modal).find('.modal-header').addClass('font-yellow-casablanca');
                $(modal).find('.modal-title').html('<i class="fa fa-refresh"></i> Modification ' + editDetail);
                $(modal).modal('show');

                /** -------------------------------------------------------------------------------
                 */
                var libelle = data.libelle;
                var note = data.note;
                var iddetail = data.id;
                $(modalName + ' #ztLibelle').val(libelle).addClass(libelle != '' ? 'edited' : '');
                $(modalName + ' #ztLibelle').focus();
                $(modalName + ' #ztNote').val(note);

                /** -----------------------------------------------------------------------------------
                 * Bind de la modification
                 */
                $(modalName + ' #btnAction').unbind().on('click', function () {

                    /** -----------------------------------------------------------------------------------
                     * Vérification des ZONES Mandatory
                     */
                    var oblig = true;
                    $(modalName + ' .required').each(function () {
                        if (!$(this).val().trim()) {
                            oblig = false;
                        }
                    });
                    if (!oblig) {
                        toastr.warning('Les champs obligatoires doivent être saisis.', "Attention");
                        return;
                    }

                    // ----------------------------------------------------------------------------
                    // Données de BASE
                    // ----------------------------------------------------------------------------
                    libelle = $(modalName + ' #ztLibelle').val().trim().toUpperCase();
                    note = $(modalName + ' #ztNote').val().trim();

                    //$(this).off().removeClass('green').addClass('grey-cascade').html(
                    //    '<i class="fa fa-spin fa-spinner"></i> Patientez...'
                    //);

                    $.ajax({
                        url: '/param/' + routeDetail + '/' + iddetail,
                        type: 'PUT',
                        data: {
                            _token: $(modalName + ' #token').val(),
                            pLibelle: libelle,
                            pNote: note,
                            pType:idType
                        },
                        dataType: 'json',
                        success: function (data) {
                            if (data == -1) {
                                $(modal).modal('hide');
                                toastr.error('Modification non effectuée', "Attention");
                            } else {
                                $(modal).modal('hide');
                                toastr.info('Modification enregistrée', "Information");
                                tableDetail.ajax.reload();
                            }
                        }
                    });
                });
            }
        });

        /** ---------------------------------------------------------------------------------------
         * On met à jour les actions
         */

    },

};

/** -----------------------------------------------------------------------------------------------
 * @class : GENERIC
 */
var ClassGENERIC = {

    switchStatut : function (element, id, statut, field, route) {
        var newStatut = 1 - statut;
        if (newStatut == 0) {
            element.removeClass('green-seagreen').addClass('red-sunglo');
        } else {
            element.removeClass('red-sunglo').addClass('green-seagreen');
        }
        element.attr('data-statut', newStatut);

        $.ajax({
            url: '/annuaire/' + route + '/switch/' + id + '/edit',
            type: 'GET',
            data: {
                pValue: newStatut,
                pType : field
            },
            dataType: 'json',
            success: function (data) {

            }
        });

    },

};