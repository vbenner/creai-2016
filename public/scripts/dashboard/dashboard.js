/** -----------------------------------------------------------------------------------------------
 *
 * @author : Vincent BENNER
 * @copyright : Page Up 2016
 * @version : 1.0.0
 *
 * @history : 1.0.0 - Release
 * @description : Gestion du DASHBOARD
 *
 */

/** -----------------------------------------------------------------------------------------------
 * Données de base
 */
var tableName = 'table';
var table;
var route = 'oo'
var edit = ' d\'un Organisme';

/** -----------------------------------------------------------------------------------------------
 * JQuery start
 */
$(function () {

    ClassTDB.init();

});

/** -----------------------------------------------------------------------------------------------
 * @class : PERSONNE PHYSIQUE
 */
var ClassTDB = {

    init : function () {
    },

}